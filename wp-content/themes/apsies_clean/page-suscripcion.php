<?php
	
	// Helper
	function get_transaccion( $id )
	{
		$transaccion = \Warp\Apsies\Wp\Transaccion::get( $id );
		if(  $transaccion === false ) {
			$_SESSION['msg'] = array('status'=>'danger', 'msg' => 'No existe la transacción solicitada');
			header('Location:' . site_url( 'recargar' ) );
			die;
		}
		return $transaccion;
	}

	function assign_dias( $dias )
	{
		global $warp;

		$ahora = time();
				
		if ( $ahora > $warp['user']->me->fechasuscripcion + ( $warp['user']->me->diassuscripcion * 86400 ) ) {
			$warp['user']->me->update( 'fechasuscripcion', $ahora );
			$warp['user']->me->update( 'diassuscripcion',  $dias ); 
		} else {
			$warp['user']->me->update( 'diassuscripcion', $dias + $warp['user']->me->diassuscripcion ); 
		}
		
	}

	$warp = require(__DIR__.'/warp.php');

	
	$page = 'suscripcion/recarga';

	if( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
		// Generamos una transaccion
		// Por ahora solo paypal
		if( isset( $_SESSION['pending_action']['suscripcion'] ) ) {
			unset( $_SESSION['pending_action']['suscripcion'] );
		}

		$method = ( ! isset ( $_POST['recarga']['metodo_pago'] ) ? 'paypal' : $_POST['recarga']['metodo_pago'] );
		if( $method == 'paypal') {
			$transaccion = \Warp\Apsies\Wp\Paypal::create_suscripcion( $_POST );
		} else {
			$transaccion = \Warp\Apsies\Wp\Paysafecard::create_suscripcion( $_POST );
		}

		$transaccion->payment();
	}

	if( isset( $_GET['status'] ) ) {

		if( $_GET['status'] == 'pn' ) {
			$transaccion = get_transaccion($_GET['pay_id'] );
			if( isset( $transaccion->model->payment_status ) && $transaccion->model->payment_status == 'pagado' ) {
				header('Location:' . site_url('pedidos'));
				die;
			} else {
				if( $transaccion->execute() ) {
					$transaccion->update('payment_status', 'pagado');
		        	$transaccion->updateStatus('publish');
		        	assign_dias( $transaccion->model->dias );  

		        	$_SESSION['msg'] = array(
						'msg' 		=> 'Suscripción realizada correctamente',
						'status' 	=> 'success'
					);
					header('Location:' . site_url('pedidos'));
					die;      	
		        } else {
		        	if( $transaccion->model->payment_type == 'paypal' ) {
			        	$transaccion->delete();
			        	$_SESSION['msg'] = array('status'=>'error', 'msg' => 'No se ha podido realizar la transferencia, vuelva a intentarlo por favor');
						header('Location:' . site_url('suscripcion'));
						die;
					}
		        }
		    }		
		} elseif ( $_GET['status'] == 'success' ) {
			$transaccion = get_transaccion($_GET['pay_id'] );		
			
			if( isset( $transaccion->model->payment_status ) && $transaccion->model->payment_status == 'pagado' ) {
				header('Location:' . site_url('pedidos'));
				die;				
			} else {

				if( $transaccion->model->payment_type == 'paypal' ) {
					$transaccion->update('token', $_GET['token']);
			        $transaccion->update('payer_id', $_GET['PayerID']); 
		        }

		        if( $transaccion->execute() ) {
		        	$transaccion->update('payment_status', 'pagado');
		        	$transaccion->updateStatus('publish');
		        	assign_dias( $transaccion->model->dias );  

		        	$_SESSION['msg'] = array(
						'msg' 		=> 'Suscripción realizada correctamente',
						'status' 	=> 'success'
					);
					
					header('Location:' . site_url('pedidos'));
					die;

		        } else {
		        	if( $transaccion->model->payment_type == 'paypal' ) {
			        	$transaccion->delete();
			        	$_SESSION['msg'] = array('status'=>'error', 'msg' => 'No se ha podido realizar la transferencia, vuelva a intentarlo por favor');
						header('Location:' . site_url('pedidos'));
						die;
					}
		        }						
			}

		} elseif ( $_GET['status'] == 'error' ) {

			if( isset( $_GET['pay_id'] ) ) {
				$transaccion = \Warp\Apsies\Wp\Transaccion::get($_GET['pay_id'] );
				$transaccion->delete();

			}

			$_SESSION['msg'] = array(
					'msg' 		=> 'No se ha realizado la transacción',
					'status' 	=> 'danger'
				);
			header('Location:' . site_url('suscripcion'));
			die;

		}
	}


	if( isset( $_GET['test_t'] ) ) {
		$transaccion = get_transaccion($_GET['pay_id'] );		

		echo '<pre>';
		print_r($transaccion);
		echo "\n\n Estado: \n";
		$transaccion->execute();
		die;
	}



echo $warp['template']->render('home', array( 'tpl_content' => $page ) );