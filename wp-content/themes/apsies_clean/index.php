<?php
/**
* @package   Master
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

// get warp
$warp = require(__DIR__.'/warp.php');

// echo '<pre>';
// print_r( $warp );
// echo '</pre>';
// load main theme file, located in /layouts/theme.php

if( $warp['user']->me->is_login ) {
	echo $warp['template']->render('pedido', $warp['pedido']->getContext(array()) );
} else {
	echo $warp['template']->render('home');
}