<?php
	
	$warp = require(__DIR__.'/warp.php');
	$extra = array();


	
	$pedido_id = 	( isset( $_GET['res_id'] ) 
								? 
							$_GET['res_id'] 
								: 
							( isset( $_SESSION['pending_action']['pedido']['confirmacion']['pedido_id'] ) 
								? 
									$_SESSION['pending_action']['pedido']['confirmacion']['pedido_id'] 
								: 
									false
							) 
						);

	if( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
		if( isset( $_GET['step'] ) && $_GET['step'] == 'confirmacion' ) {
			if( isset( $_POST['submit_confirmacion']['cambio_level'] ) ) {
				$warp['user']->me->update('level', intval( $_POST['submit_confirmacion']['cambio_level'] ) );
			}

			$_pedido = Warp\Apsies\Wp\Pedido::get($_POST['submit_confirmacion']['pedido']);
			$tipo   = $warp['user']->me->level == 1 ? 'eso' : 'uni';
			$coincide = ( $tipo == $_pedido->model->_data['level'] ); 

			if( ! $coincide ) {
				$_SESSION['msg'] = array( 'status' => 'danger', 'msg' => 'Tu nivel no corresponde con el pedido, selecciona el check cambiar de nivel o cancela el pedido y crea uno correspondiente a tu nivel' );
				header('location:' . site_url( 'pedidos?step=confirmacion' ) );
				die;
			}


			$warp['pedido']->assign( $_POST['submit_confirmacion']['pedido'] );
		} else { 
			$warp['pedido']->save( $_POST );
			//die;
		}
	}

	if( isset($_GET['step']) && $_GET['step'] == 'delete'  ) {
		@session_start();
		if( !isset( $_GET['res_id'] ) ) {
			header('Location:' . site_url( 'pedidos' ) ); die;
		}
		$pedido_id = intval( $_GET['res_id'] );
		$_pedido = Warp\Apsies\Wp\Pedido::get($pedido_id);


		if( $_pedido->model->post_status == 'draft' && $_pedido->model->user_id == $warp['user']->me->ID ) {
			$_pedido->delete();
			if( isset( $_SESSION['pending_action']['pedido']['confirmacion']['pedido_id'] ) && $_SESSION['pending_action']['pedido']['confirmacion']['pedido_id'] == $pedido_id ) {
				unset( $_SESSION['pending_action']['pedido']['confirmacion'] );
			}

			$_SESSION['msg'] = array( 'status' => 'success', 'msg' => 'pedido eliminado correctamente' );
		}


		header('Location: '. site_url('historial') );
		die;
	}

	if( isset($_GET['step']) && $_GET['step'] == 'confirmacion'  ) {
		
		if( ! $pedido_id ) {
			header('Location: '. site_url('pedidos') );	
			die;
		}

		$extra['current_pedido'] = $pedido = Warp\Apsies\Wp\Pedido::get($pedido_id);

		if( ! isset( $_GET['res_id'] ) && $warp['user']->me->is_login ) {
			// si no tiene el id esta trabajando con el pedido a confirmar,
			// en caso de que el usuario no se haya logueado el pedido queda sin asignar a borrador
			// asi que vamos a asegurar que el pedido quede guardado
			$pedido->update('user_id', $warp['user']->me->ID );
		}

		
	}

	if( isset($_GET['step']) && $_GET['step'] == 'detalle'  ) {
		$pedido_id = 	( isset( $_GET['res_id'] ) 
								? 
							$_GET['res_id'] 
								: 
							( isset( $_SESSION['pending_action']['pedido']['confirmacion']['pedido_id'] ) 
								? 
									$_SESSION['pending_action']['pedido']['confirmacion']['pedido_id'] 
								: 
									false
							) 
						);
		
		$extra['current_pedido'] = $pedido = Warp\Apsies\Wp\Pedido::get($pedido_id);
	}

	echo $warp['template']->render('pedido', $warp['pedido']->getContext($extra) );

?>
