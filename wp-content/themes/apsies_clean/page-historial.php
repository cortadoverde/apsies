<?php

	$warp = require(__DIR__.'/warp.php');

	$query = new \Wp_Query(
		array(
			'posts_per_page' => -1,
			'post_status' => 'any',
			'post_type' => 'pedido',
			'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => 'user_id',
                    'value' => $warp['user']->me->ID
                )
            )
		) 
	);

	$posts = $query->get_posts();

	foreach($posts as $post) {
	    $items[] = new \Warp\Apsies\Wp\Pedido( $post );
	}



	

$page = 'historial/index';

echo $warp['template']->render('pedido', array( 'content' => $page, '_args' => array( 'pedidos' => $items ) ) );
