<?php
/*
If you would like to edit this file, copy it to your current theme's directory and edit it there.
Theme My Login will always look in your theme's directory first, before using this default template.
*/
?>

<?php
/*
If you would like to edit this file, copy it to your current theme's directory and edit it there.
Theme My Login will always look in your theme's directory first, before using this default template.
*/
global $warp;
?>

<article class="uk-article register_form">

	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="login" id="theme-my-login-login<?php $template->the_instance(); ?>">
				<h2 class="title">Accede con tu cuenta</h2>
				<!--<?php $template->the_action_template_message( 'login' ); ?>-->

				
				<form name="loginform" id="" action="<?php $template->the_action_url( 'login' ); ?>" method="post">
					<p>
						<label for="user_login<?php $template->the_instance(); ?>"><?php _e( 'Usuario' ); ?></label>
						<input type="text" name="log" id="user_login<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'log' ); ?>" />
					</p>
					<p>
						<label for="user_pass<?php $template->the_instance(); ?>"><?php _e( 'Password' ); ?></label>
						<input type="password" name="pwd" id="user_pass<?php $template->the_instance(); ?>" class="input" value="" />
					</p>

					<?php do_action( 'login_form' ); ?>
					
					
					<p class="forgetmenot">
						<input name="rememberme" type="checkbox" id="rememberme" value="forever" />
						<label for="rememberme">Recordarme</label>
						<a style="margin-left:1.2em" href="<?php echo site_url( 'lostpassword' )?>" rel="nofollow">Recuperar la contraseña</a>
					</p>

					

					<p class="submit">
						<input type="submit" name="wp-submit" id="wp-submit<?php $template->the_instance(); ?>" value="<?php esc_attr_e( 'ACCEDER' ); ?>" />
						<input type="hidden" name="redirect_to" value="<?php $template->the_redirect_url( 'login' ); ?>" />
						<input type="hidden" name="instance" value="<?php $template->the_instance(); ?>" />
						<input type="hidden" name="action" value="login" />
					</p>
				</form>
				
			</div>
		</div>
		<div class="uk-width-1-1 uk-width-medium-1-2">

			<h2 class="alt title">... O crea tu cuenta si no la tienes aún </h2>
			<div class="uk-text-right">
				<a  class="uk-button next" href="<?php echo site_url('/register') ?>">Registro</a>
			</div>	
		</div>
	</div>
</article>






