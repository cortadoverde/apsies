<div id="user-data">
	<ul>
		<li>
			<a href="<?php bloginfo( 'url' ); ?>/perfil" title="Perfil">
				<span class="uk-icon-large uk-icon-cog"></span> Bienvenido <?php echo $current_user->user_login; ?>
			</a>
		</li>
		<li>
			<a href="<?php bloginfo( 'url' ); ?>/pedidos" title="Realizar Pedidos">
				<span class="uk-icon-large uk-icon-home"></span>
			</a>
		</li>
		<li>
			<a href="<?php bloginfo( 'url' ); ?>/tickets" title="Mensajes">
				<span class="uk-icon-large uk-icon-envelope-o"></span>
			</a>
		</li>
		<li>
			<a href="<?php bloginfo( 'url' ); ?>/historial" title="Historial de Pedidos">
				<span class="uk-icon-large uk-icon-file-text"></span>
			</a>
		</li>
		<li>
			<a id="logout" href="<?php echo wp_logout_url( home_url() ); ?>" data-redirect="<?php echo $redirect; ?>" title="Salir">
				<span class="uk-icon-large uk-icon-power-off"></span>
			</a>
		</li>						
	</ul>
</div>