<?php
/*
If you would like to edit this file, copy it to your current theme's directory and edit it there.
Theme My Login will always look in your theme's directory first, before using this default template.
*/
global $warp;
?>

<article class="uk-article register_form">
	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-2 uk-text-center">
			<img src="<?php echo $warp['path']->url('theme:/images/registro.png')?>" alt="reg">
		</div>
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="login" id="theme-my-login-login_reg">
		
				<?php $template->the_errors(); ?>

				<h2 class="title">Crea tu cuenta</h2>

				
			
				<form name="registerform" class="" id="registerform<?php $template->the_instance(); ?>" action="<?php $template->the_action_url( 'register' ); ?>" method="post">
					<div class="uk-form-row">
						<label for="user_login<?php $template->the_instance(); ?>"><?php _e( 'Usuario' ); ?></label>
						<input type="text" name="user_login" id="user_login<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'user_login' ); ?>" size="20" />
					</div>

					<div class="uk-form-row">
						<label for="user_email<?php $template->the_instance(); ?>"><?php _e( 'Email' ); ?></label>
						<input type="text" name="user_email" id="user_email<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'user_email' ); ?>" size="20" />
					</div>

					<?php do_action( 'register_form' ); ?>

					<p id="reg_passmail<?php $template->the_instance(); ?>"><?php echo apply_filters( 'tml_register_passmail_template_message', __( 'A password will be e-mailed to you.' ) ); ?></p>

					<div class="uk-form-row">
						<label for="nivel<?php $template->the_instance(); ?>"><?php _e( 'Nivel' ); ?></label>
						<select required name="nivel" id="nivel">
			                <option value="1" >ESO / Bachiller</option>
			                <option value="2" >Universidad</option>
			            </select>
			            <br/>
			            <span style="font-family: OSRegular; font-size: 10px;">Podr&aacute;s cambiarlo m&aacute;s adelante desde Tu Perfil</span>
					</div>

					<p class="forgetmenot">
						<input name="confirma_edad" type="checkbox" id="confirma_edad" value="+14" checked>
						<label for="confirma_edad"> Confirmo que soy mayor de 14 años y que estoy de acuerdo con las <a href="<?php echo site_url('condiciones-generales-de-contratacion') ?>" target="_blank">CGC</a> </label>
					</p>

					<p class="submit" class="uk-text-center">
						<input  class="uk-button uk-button-blue" type="submit" name="wp-submit" id="wp-submit<?php $template->the_instance(); ?>" value="<?php esc_attr_e( 'Regístrate' ); ?>" />
						<input type="hidden" name="redirect_to" value="<?php $template->the_redirect_url( 'register' ); ?>" />
						<input type="hidden" name="instance" value="<?php $template->the_instance(); ?>" />
						<input type="hidden" name="action" value="register" />
					</p>
				</form>
						
			</div>
		</div>
	</div>
	

</article>
