<?php
	ini_set('display_errros', 0);
	error_reporting(0);

	function assign_points( $puntos )
	{
		global $warp;
		$warp['user']->me->update( 'puntosusuario',( $warp['user']->me->puntosusuario + $puntos ) ); 
		if( isset( $_SESSION['pending_action']['pedido']['assign'] ) ) {
			//
			$warp['pedido']->assign( $_SESSION['pending_action']['pedido']['assign'] );
		} else {

			$_SESSION['msg'] = array('status'=>'success', 'msg' => 'Recarga realizada correctamente');
			header('Location:' . site_url('recargar'));
			die;
		}
		
	}
	
	function get_transaccion( $id )
	{
		$transaccion = \Warp\Apsies\Wp\Transaccion::get( $id );
		if(  $transaccion === false ) {
			$_SESSION['msg'] = array('status'=>'danger', 'msg' => 'No existe la transacciÃ³n solicitada');
			header('Location:' . site_url( 'recargar' ) );
			die;
		}
		return $transaccion;
	}

	$warp = require(__DIR__.'/warp.php');
	$page = 'puntos/recarga';

	if( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
		// Generamos una transaccion
		$method = ( ! isset ( $_POST['recarga']['metodo_pago'] ) ? 'paypal' : $_POST['recarga']['metodo_pago'] );
		if( $method == 'paypal') {
			$transaccion = \Warp\Apsies\Wp\Paypal::create_recarga( $_POST );
		} else {
			$transaccion = \Warp\Apsies\Wp\Paysafecard::create_recarga( $_POST );
		}

		$transaccion->payment();
		
	}

	if( isset( $_GET['status'] ) ) {

		if( $_GET['status'] == 'pn' ) {
			$transaccion = get_transaccion($_GET['pay_id'] );
			if( isset( $transaccion->model->payment_status ) && $transaccion->model->payment_status == 'pagado' ) {

				header('Location:' . site_url('pedidos'));
				die;
				
			} else {
				

				if( $transaccion->execute() ) {
					$transaccion->update('payment_status', 'pagado');
		        	$transaccion->updateStatus('publish');
		        	assign_points( $transaccion->model->puntos ); 

		        	$_SESSION['msg'] = array(
						'msg' 		=> 'Suscripción realizada correctamente',
						'status' 	=> 'success'
					);
					
					header('Location:' . site_url('pedidos'));
					die;      	
		        } else {
		        	$transaccion->delete();
		        	$_SESSION['msg'] = array('status'=>'error', 'msg' => 'No se ha podido realizar la transferencia, vuelva a intentarlo por favor');
					header('Location:' . site_url('suscripcion'));
					die;
		        }

		    }
		
		} elseif ( $_GET['status'] == 'success' ) {
			$transaccion = get_transaccion($_GET['pay_id'] );		
			
			if( isset( $transaccion->model->payment_status ) && $transaccion->model->payment_status == 'pagado' ) {
				header('Location:' . site_url('pedidos'));
				die;				
			} else {

				if( $transaccion->model->payment_type == 'paypal' ) {
					$transaccion->update('token', $_GET['token']);
			        $transaccion->update('payer_id', $_GET['PayerID']); 
		        }

		        if( $transaccion->execute() ) {
		        	$transaccion->update('payment_status', 'pagado');
		        	$transaccion->updateStatus('publish');
		        	assign_points( $transaccion->model->puntos ); 

		        	$_SESSION['msg'] = array(
						'msg' 		=> 'Recarga realizada correctamente',
						'status' 	=> 'success'
					);
					
					header('Location:' . site_url('pedidos'));
					die;

		        } else {
		        	if( $transaccion->model->payment_type == 'paypal' ) {
			        	$transaccion->delete();
			        	$_SESSION['msg'] = array('status'=>'error', 'msg' => 'No se ha podido realizar la transacción, vuelva a intentarlo por favor');
						header('Location:' . site_url('pedidos'));
						die;
					}
		        }						
			}

		} elseif ( $_GET['status'] == 'error' ) {

			if( isset( $_GET['pay_id'] ) ) {
				$transaccion = \Warp\Apsies\Wp\Transaccion::get($_GET['pay_id'] );
				$transaccion->delete();

			}

			$_SESSION['msg'] = array(
					'msg' 		=> 'No se ha realizado la transacción',
					'status' 	=> 'danger'
				);
			header('Location:' . site_url('suscripcion'));
			die;

		}
	}
	
echo $warp['template']->render('pedido', array( 'content' => $page ) );