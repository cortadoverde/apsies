<?php
	$warp = require(__DIR__.'/warp.php');
	the_post();
	$ticket = Warp\Apsies\Wp\Ticket::get(get_the_ID());

	echo $warp['template']->render('pedido', array( 'content' => 'tickets/detalle', '_args' => array( 'current_ticket' => $ticket ) ) );