/*
 *  webui popover plugin  - v1.0.5
 *  A lightWeight popover plugin with jquery ,enchance the  popover plugin of bootstrap with some awesome new features. It works well with bootstrap ,but bootstrap is not necessary!
 *  https://github.com/sandywalker/webui-popover
 *
 *  Made by Sandy Duan
 *  Under MIT License
 */
!function(a,b,c){function d(b,c){this.$element=a(b),("string"===a.type(c.delay)||"number"===a.type(c.delay))&&(c.delay={show:null,hide:c.delay}),this.options=a.extend({},h,c),this._defaults=h,this._name=e,this._targetclick=!1,this.init()}var e="webuiPopover",f="webui-popover",g="webui.popover",h={placement:"auto",width:"auto",height:"auto",trigger:"click",style:"",delay:{show:null,hide:null},async:{before:null,success:null},cache:!0,multi:!1,arrow:!0,title:"",content:"",closeable:!1,padding:!0,url:"",type:"html",constrains:null,template:'<div class="webui-popover"><div class="arrow"></div><div class="webui-popover-inner"><a href="#" class="close">x</a><h3 class="webui-popover-title"></h3><div class="webui-popover-content"><i class="icon-refresh"></i> <p>&nbsp;</p></div></div></div>'};d.prototype={init:function(){"click"===this.options.trigger?this.$element.off("click").on("click",a.proxy(this.toggle,this)):this.$element.off("mouseenter mouseleave").on("mouseenter",a.proxy(this.mouseenterHandler,this)).on("mouseleave",a.proxy(this.mouseleaveHandler,this)),this._poped=!1,this._inited=!0},destroy:function(){this.hide(),this.$element.data("plugin_"+e,null),this.$element.off("click"===this.options.trigger?"click":"mouseenter mouseleave"),this.$target&&this.$target.remove()},hide:function(b){b&&(b.preventDefault(),b.stopPropagation());var c=a.Event("hide."+g);this.$element.trigger(c),this.$target&&this.$target.removeClass("in").hide(),this.$element.trigger("hidden."+g)},toggle:function(a){a&&(a.preventDefault(),a.stopPropagation()),this[this.getTarget().hasClass("in")?"hide":"show"]()},hideAll:function(){a("div.webui-popover").not(".webui-popover-fixed").removeClass("in").hide()},show:function(){var a=this.getTarget().removeClass().addClass(f);if(this.options.multi||this.hideAll(),!this.options.cache||!this._poped){if(this.setTitle(this.getTitle()),this.options.closeable||a.find(".close").off("click").remove(),this.isAsync())return this.setContentASync(this.options.content),void this.displayContent();this.setContent(this.getContent()),a.show()}this.displayContent(),this.bindBodyEvents()},displayContent:function(){var b=this.getElementPosition(),d=this.getTarget().removeClass().addClass(f),e=this.getContentElement(),h=d[0].offsetWidth,i=d[0].offsetHeight,j="bottom",k=a.Event("show."+g);this.$element.trigger(k),"auto"!==this.options.width&&d.width(this.options.width),"auto"!==this.options.height&&e.height(this.options.height),this.options.arrow||d.find(".arrow").remove(),d.remove().css({top:-1e3,left:-1e3,display:"block"}).appendTo(c.body),h=d[0].offsetWidth,i=d[0].offsetHeight,j=this.getPlacement(b),this.initTargetEvents();var l=this.getTargetPositin(b,j,h,i);if(this.$target.css(l.position).addClass(j).addClass("in"),"iframe"===this.options.type){var m=d.find("iframe");m.width(d.width()).height(m.parent().height())}if(this.options.style&&this.$target.addClass(f+"-"+this.options.style),this.options.padding||(e.css("height",e.outerHeight()),this.$target.addClass("webui-no-padding")),this.options.arrow||this.$target.css({margin:0}),this.options.arrow){var n=this.$target.find(".arrow");n.removeAttr("style"),l.arrowOffset&&n.css(l.arrowOffset)}this._poped=!0,this.$element.trigger("shown."+g)},isTargetLoaded:function(){return 0===this.getTarget().find("i.glyphicon-refresh").length},getTarget:function(){return this.$target||(this.$target=a(this.options.template)),this.$target},getTitleElement:function(){return this.getTarget().find("."+f+"-title")},getContentElement:function(){return this.getTarget().find("."+f+"-content")},getTitle:function(){return this.options.title||this.$element.attr("data-title")||this.$element.attr("title")},getUrl:function(){return this.options.url||this.$element.attr("data-url")},getDelayShow:function(){return this.options.delay.show||this.$element.attr("data-delay-show")||0},getHideDelay:function(){return this.options.delay.hide||this.$element.attr("data-delay-hide")||300},getConstrains:function(){return this.options.constrains||this.$element.attr("data-contrains")},setTitle:function(a){var b=this.getTitleElement();a?b.html(a):b.remove()},hasContent:function(){return this.getContent()},getContent:function(){if(this.getUrl())"iframe"===this.options.type&&(this.content=a('<iframe frameborder="0"></iframe>').attr("src",this.getUrl()));else if(!this.content){var b="";b=a.isFunction(this.options.content)?this.options.content.apply(this.$element[0],arguments):this.options.content,this.content=this.$element.attr("data-content")||b}return this.content},setContent:function(a){var b=this.getTarget();this.getContentElement().html(a),this.$target=b},isAsync:function(){return"async"===this.options.type},setContentASync:function(b){var c=this;a.ajax({url:this.getUrl(),type:"GET",cache:this.options.cache,beforeSend:function(a){c.options.async.before&&c.options.async.before(c,a)},success:function(d){c.content=b&&a.isFunction(b)?b.apply(c.$element[0],[d]):d,c.setContent(c.content);var e=c.getContentElement();e.removeAttr("style"),c.displayContent(),c.options.async.before&&c.options.async.success(c,d)}})},bindBodyEvents:function(){a("body").off("keyup.webui-popover").on("keyup.webui-popover",a.proxy(this.escapeHandler,this)),a("body").off("click.webui-popover").on("click.webui-popover",a.proxy(this.bodyClickHandler,this))},mouseenterHandler:function(){var a=this;a._timeout&&clearTimeout(a._timeout),a._enterTimeout=setTimeout(function(){a.getTarget().is(":visible")||a.show()},this.getDelayShow())},mouseleaveHandler:function(){var a=this;clearTimeout(a._enterTimeout),a._timeout=setTimeout(function(){a.hide()},this.getHideDelay())},escapeHandler:function(a){27===a.keyCode&&this.hideAll()},bodyClickHandler:function(){this._targetclick?this._targetclick=!1:this.hideAll()},targetClickHandler:function(){this._targetclick=!0},initTargetEvents:function(){"click"!==this.options.trigger&&this.$target.off("mouseenter mouseleave").on("mouseenter",a.proxy(this.mouseenterHandler,this)).on("mouseleave",a.proxy(this.mouseleaveHandler,this)),this.$target.find(".close").off("click").on("click",a.proxy(this.hide,this)),this.$target.off("click.webui-popover").on("click.webui-popover",a.proxy(this.targetClickHandler,this))},getPlacement:function(a){var b,d=c.documentElement,e=c.body,f=d.clientWidth,g=d.clientHeight,h=Math.max(e.scrollTop,d.scrollTop),i=Math.max(e.scrollLeft,d.scrollLeft),j=Math.max(0,a.left-i),k=Math.max(0,a.top-h);if(b="function"==typeof this.options.placement?this.options.placement.call(this,this.getTarget()[0],this.$element[0]):this.$element.data("placement")||this.options.placement,"auto"===b){var l="horizontal"===this.getConstrains(),m="vertical"===this.getConstrains();b=f/3>j?g/3>k?l?"right-bottom":"bottom-right":2*g/3>k?m?g/2>=k?"bottom-right":"top-right":"right":l?"right-top":"top-right":2*f/3>j?g/3>k?l?f/2>=j?"right-bottom":"left-bottom":"bottom":2*g/3>k?l?f/2>=j?"right":"left":g/2>=k?"bottom":"top":l?f/2>=j?"right-top":"left-top":"top":g/3>k?l?"left-bottom":"bottom-left":2*g/3>k?m?g/2>=k?"bottom-left":"top-left":"left":l?"left-top":"top-left"}return b},getElementPosition:function(){return a.extend({},this.$element.offset(),{width:this.$element[0].offsetWidth,height:this.$element[0].offsetHeight})},getTargetPositin:function(a,b,c,d){var e=a,f=this.$element.outerWidth(),g=this.$element.outerHeight(),h={},i=null,j=this.options.arrow?20:0,k=j+10>f?j:0,l=j+10>g?j:0;switch(b){case"bottom":h={top:e.top+e.height,left:e.left+e.width/2-c/2};break;case"top":h={top:e.top-d,left:e.left+e.width/2-c/2};break;case"left":h={top:e.top+e.height/2-d/2,left:e.left-c};break;case"right":h={top:e.top+e.height/2-d/2,left:e.left+e.width};break;case"top-right":h={top:e.top-d,left:e.left-k},i={left:f/2+k};break;case"top-left":h={top:e.top-d,left:e.left-c+e.width+k},i={left:c-f/2-k};break;case"bottom-right":h={top:e.top+e.height,left:e.left-k},i={left:f/2+k};break;case"bottom-left":h={top:e.top+e.height,left:e.left-c+e.width+k},i={left:c-f/2-k};break;case"right-top":h={top:e.top-d+e.height+l,left:e.left+e.width},i={top:d-g/2-l};break;case"right-bottom":h={top:e.top-l,left:e.left+e.width},i={top:g/2+l};break;case"left-top":h={top:e.top-d+e.height+l,left:e.left-c},i={top:d-g/2-l};break;case"left-bottom":h={top:e.top,left:e.left-c},i={top:g/2}}return{position:h,arrowOffset:i}}},a.fn[e]=function(b){return this.each(function(){var c=a.data(this,"plugin_"+e);c?"destroy"===b?c.destroy():"string"==typeof b&&c[b]():(b?"string"==typeof b?"destroy"!==b&&(c=new d(this,null),c[b]()):"object"==typeof b&&(c=new d(this,b)):c=new d(this,null),a.data(this,"plugin_"+e,c))})}}(jQuery,window,document);


var mleftComponent,
	ctxForm,
	Formulas,
	current_form;
	spinn = 1;

function toggleInputs( context, type )
{
	jQuery('input, select, textarea', context).attr('disabled', type);
}


function spinFactory(node, up, down) {
    var spinning, delta;
    window.addEventListener('mouseup', function () {
        stopSpin();
    });
    function spin() {
    	if( +node.value + delta >= 0 ) {
        	node.value = +node.value + delta;
        }
        
        jQuery.event.trigger({
        	type: 'refresh_point',
        	original_event : { target: node },

        	time: new Date()
        });

        spinning = setTimeout(spin, 150);
    }
    function stopSpin() {
        window.clearTimeout(spinning);
        delta = 0;
    }

    up.addEventListener('mousedown', function spinUp(e) {
        delta = spinn;
        spin();
    });
    down.addEventListener('mousedown', function spinDown(e) {
        
        delta = -1 * spinn;
        spin();
    });
}

jQuery(document).on('change', '.apsies-form .number input', function(e){
	jQuery.event.trigger({
    	type: 'refresh_point',
    	original_event : e,
    	time: new Date()
    });
})

jQuery(document).on('change', '.apsies-form select', function(e){
	jQuery.event.trigger({
    	type: 'refresh_point',
    	original_event : e,
    	time: new Date()
    });
})

// Load the IFrame Player API code asynchronously.
  var tag = document.createElement('script');
  tag.src = "https://www.youtube.com/player_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  // Replace the 'ytplayer' element with an <iframe> and
  // YouTube player after the API code downloads.
  var player;

function calcWidth()
{
	
	if (jQuery(window).width() == 320) {
		mleftComponent = 60;
	} else {
		mleftComponent = 112;
	}
	console.log( mleftComponent );
}


jQuery(window).on('resize',function(e){
	calcWidth();
})





jQuery(function($) {

    var config = $('html').data('config') || {};

    // Social buttons
    $('article[data-permalink]').socialButtons(config);

    $('form .number').each(function () {
    	$('a', this).on('click',function(e){
    		e.preventDefault();
    	})
	    spinFactory(
	        this.getElementsByTagName('input')[0],
	        $('a:last-child', this)[0],
	        $('a:first-child', this)[0]
	    );
	});

    $(document).ready(function(){
    	var base = $('html').attr('base');
    	if ( $('.page-preguntas-frecuentes').length == 1 ) {
    		var sidebar = { items : [] },
	    		content = $('<div class="answers"></div>'),
	    		tpls = 	{
	    					page : 	[
		    							'<h1 class="uk-article-title">', $('h1.uk-article-title').text(), '</h1>',
						    			'<div class="uk-grid">',
						    				'<div class="uk-width-3-10">{{{sidebar}}}</div>',
						    				'<div class="uk-width-7-10">{{{content}}}</div>',
						    			'</div>'  
						    		].join(''),
						    sidebar : [
						    	'<ul class="uk-nav">',
						    		'{{# items }}',
						    			'<li {{# active }} class="uk-active" {{/ active }}>',
						    				'<a href="#{{ name }}" rel="{{index}}">{{ name }}</a>',
						    			'</li>',
						    		'{{/ items }}',
						    	'</ul>'
						    ].join('')
						};


    		$('dl.definition').each( function( i ) {
    			sidebar.items.push({
    				active : ( ( i == 0 ) ? true : false ),
    				name : $('h4', this).text(),
    				index : i + 1 
    			});	
    			
    			if( i == 0 ) {
    				content.append( $(this).clone() );

    			}else {
    				content.append( $(this).clone().hide() );
    			}

    			
    		});

    		var content_sidebar = Mustache.render( tpls.sidebar, sidebar );
    		var content_page = Mustache.render( tpls.page, {
    			sidebar : content_sidebar,
    			content: content.html()
    		});


    		$('.uk-article', '.page-preguntas-frecuentes').html(content_page);

    		$('a', '.page-preguntas-frecuentes .uk-nav').on('click', function(e){
    			e.preventDefault();
    			var index = $(this).attr('rel');
    			$('dl').hide('fast');
    			$('dl:nth-child(' + index + ')').show('fast');
    		});

    		 $.UIkit.sticky($('.page-preguntas-frecuentes .uk-nav'), { top: 45 });

    		 $("[data-uk-toggle]", '.page-preguntas-frecuentes').each(function() {
	            var ele = $(this);

	            if (!ele.data("toggle")) {
	               var obj = $.UIkit.toggle(ele, $.UIkit.Utils.options(ele.attr("data-uk-toggle")));
	            }
	        });

    	}

        $('#detalle_pedido_modal').on({

            'uk.modal.hide': function(){
                 $('.detalle', "#detalle_pedido_modal").html('');
            }
        });

        // Panel
        $(document).on('click', 'a.open_detalle', function(e){
            e.preventDefault();
            var modal = $.UIkit.modal("#detalle_pedido_modal"),
                id    = $(this).attr('rel');

            $.ajax({
                type: 'post',
                url : base + '/ajax.php?controller=pedido&action=detalle',
                dataType : 'json',
                data : {
                    id : id
                },
                success: function( data, txt, xhr )
                {
                    
                    $('.detalle', "#detalle_pedido_modal").html(data.html);

                    if ( modal.isActive() ) {
                        modal.hide();
                    } else {
                        modal.show();
                    }
                }

            })


            



           


        })

    	// Calcular puntos 
    	// Eso - trabajos
    	ctxForm = {
    		values_uni : ['', 'trabajos', 'autocad', 'apuntes', 'powerpoint', 'exposiciones'],
    		uni : {
				trabajos     : $('.replace_form.uni .form-container[rel="1"]'),
				autocad      : $('.replace_form.uni .form-container[rel="2"]'),
				apuntes      : $('.replace_form.uni .form-container[rel="3"]'),
				powerpoint   : $('.replace_form.uni .form-container[rel="4"]'),
				exposiciones : $('.replace_form.uni .form-container[rel="5"]')
    		},
    		values_eso : 
    					[
    						'', 'resolucion', 'redaccion', 'comentario_texto', 'comentario_artistico',
    						'critica', 'autocad', 'investigacion', 'trabajos'
    					],
    		eso : {
				resolucion           : $('.replace_form.eso .form-container[rel="1"]'),
				redaccion            : $('.replace_form.eso .form-container[rel="2"]'),
				comentario_texto     : $('.replace_form.eso .form-container[rel="3"]'),
				comentario_artistico : $('.replace_form.eso .form-container[rel="4"]'),
				critica              : $('.replace_form.eso .form-container[rel="5"]'),
				autocad              : $('.replace_form.eso .form-container[rel="6"]'),
				investigacion        : $('.replace_form.eso .form-container[rel="7"]'),
				trabajos             : $('.replace_form.eso .form-container[rel="8"]'),
			}
    	};

    	Formulas = {
    		uni : {
    			trabajos 		     : function() {
    				var 	ctx  = ctxForm.uni.trabajos,
						paginas  = $('.number input', ctx ).val(),
						biagrafia = ( $('[name="submit_pedido[uni][trabajos][bibliografia]"]', ctx).val() == 'si' ) ? true : false,
						factor   = getFactorEntrega( parseInt( $('.fecha_entrega', ctx).val() ) ),
						puntos   = (paginas > 30) ? ( 2 * paginas + 51 ) : ( ( paginas > 10 ) ? ( paginas * 3+21 ) : ( paginas * 5.1 ) ),
						subtotal = puntos * factor,
						total    = ( biagrafia == true ) ? ( ( 5 / 100 ) * subtotal ) + subtotal : subtotal;

						return Math.ceil( total );
    			},

    			autocad 		     : function() {
    				var ctx         = ctxForm.uni.autocad,
						nro         = $('.number input', ctx ).val(),
						vistas      = $('[name="submit_pedido[uni][autocad][vistas]"]', ctx).val() == 'si',
						acotaciones = $('[name="submit_pedido[uni][autocad][acotaciones]"]', ctx).val() == 'si',
						factor      = getFactorEntrega( parseInt( $('.fecha_entrega', ctx).val() ) ),
						subtotal    = nro * 10,
						total       = subtotal * factor * ( ( vistas ) ? 1.10 : 1 ) * ( ( acotaciones ) ? 1.5 : 1 );
    					
    					return Math.ceil( total )
    			},

    			apuntes 		     : function(){
    				var ctx = ctxForm.uni.apuntes,
    					nro = $('.number input', ctx).val(),
    					fecha = parseInt( $('.fecha_entrega', ctx).val() ),
    					factor = getFactorEntregaReducido( fecha ),
    					total = nro * 1 * factor;

    					return Math.ceil( total )

    			},

    			powerpoint 		     : function() {
    				var ctx = ctxForm.uni.powerpoint,
    					nro = $('.number input', ctx).val(),
    					tipo = $('[name="submit_pedido[uni][powerpoint][tipo]"]', ctx).val(),
    					precio = tipo == 'diapo_texto' ? 0.3 : 3,
    					fecha = parseInt( $('.fecha_entrega', ctx).val() ),
    					factor = getFactorEntregaReducido( fecha ),
    					total = nro * precio * factor;

    					return Math.ceil( total );
    			},

    			exposiciones 	     : function() {
    				var ctx = ctxForm.uni.exposiciones,
    					nro = $('.number input', ctx).val(),
    					tipo = $('[name="submit_pedido[uni][exposicion][tipo]"]', ctx).val(),
    					precio = tipo == 'trabajo_escrito' ? 4.1 : 7.1,
    					hablada = ( $('[name="submit_pedido[uni][exposicion][parte_hablada]"]', ctx).val() == 'si' ) ? 1.25 : 1,
    					fecha = parseInt( $('.fecha_entrega', ctx).val() ),
    					factor = getFactorEntregaReducido( fecha ),
    					total = nro * precio * factor * hablada;

    					return Math.ceil( total );
    			}
    		},
    		eso : {
    			resolucion           : function(){
    				var ctx = ctxForm.eso.resolucion,
    					nro = $('.number input', ctx).val(),
    					step = parseInt( $('[name="submit_pedido[eso][resolucion_ejercicio][asignatura]"] option:selected').attr('rel') ),
    					precio = 2 * ( nro / step ),
    					fecha = parseInt( $('.fecha_entrega', ctx).val() ),
    					factor = getFactorEntregaColegio( fecha ),
    					total = precio * factor;
    					console.log( nro, precio, fecha, factor );
    					return Math.ceil( total );
    			}, 

				redaccion            : function(){
					var ctx = ctxForm.eso.redaccion;
					return Formulas.eso.common_palabras( ctx );
				},

				comentario_texto     : function(){
					return Formulas.eso.common_palabras( ctxForm.eso.comentario_texto )
    				
				},

				comentario_artistico : function(){
					return Formulas.eso.common_palabras( ctxForm.eso.comentario_artistico )
				},

				critica              : function(){
					var ctx = ctxForm.eso.critica,
    					nro = $('.number input', ctx).val(),
    					precio = 8,
    					fecha = parseInt( $('.fecha_entrega', ctx).val() ),
    					factor = getFactorEntregaColegio( fecha ),
    					total = precio * nro * factor;
    					console.log( nro, precio, fecha, factor );
    				
    				return Math.ceil( total );
				},

				autocad              : function(){
					var ctx         = ctxForm.eso.autocad,
						nro         = $('.number input:enabled', ctx ).val(),
						tipo        = $('[name="submit_pedido[eso][autocad][tipo]"]').val(),
						vistas      = $('[name="submit_pedido[eso][autocad][vistas]"]', ctx).val() == 'si',
						acotaciones = $('[name="submit_pedido[eso][autocad][acotaciones]"]', ctx).val() == 'si',
						factor      = getFactorEntregaColegio( parseInt( $('.fecha_entrega:enabled', ctx).val() ) ),
						subtotal    = nro * ( ( tipo == 1 ) ? 2 : 3 ),
						tipoFactor  = ( tipo == 1 ) ? 1 : ( ( ( vistas ) ? 1.5 : 1 ) * ( ( acotaciones ) ? 1.5 : 1 ) ),
						total       = parseInt( subtotal * factor * tipoFactor );
    					//console.log( tipo, subtotal , factor , tipoFactor);
    					return Math.ceil( total )
				},

				investigacion        : function(){
					return Formulas.eso.common_palabras( ctxForm.eso.investigacion )
				},

				trabajos             : function(){
					var 	ctx  = ctxForm.eso.trabajos,
						paginas  = $('.number input', ctx ).val(),
						biagrafia = ( $('[name="submit_pedido[eso][trabajo][bibliografia]"]', ctx).val() == 'si' ) ? true : false,
						factor   = getFactorEntregaColegio( parseInt( $('.fecha_entrega', ctx).val() ) ),
						puntos   = 4.1 * paginas;  //(paginas > 30) ? ( 2 * paginas + 51 ) : ( ( paginas > 10 ) ? ( paginas * 3+21 ) : ( paginas * 5.1 ) ),
						subtotal = puntos * factor,
						total    = subtotal * ( ( biagrafia == true ) ? 1.05 : 1 );

						return Math.ceil( total );
				},

				common_palabras      : function( ctx ) {
					var nro = $('.number input', ctx).val(),
    					precio = 0.03,
    					fecha = parseInt( $('.fecha_entrega', ctx).val() ),
    					factor = getFactorEntregaColegio( fecha ),
    					total = precio * nro * factor;
    					
    				return Math.ceil( total );
				}
    		}
    	}

    	$(document).on('refresh_point', function(e) {
            if( $('[name="submit_pedido[level]"]').length == 0 ) return;
    		var level  = $('[name="submit_pedido[level]"]').val(),
    			form   = $('.select_tipo.'+level).val(),
    			idx    = ctxForm['values_' + level][form],
    			points = ( typeof Formulas[level] !== 'undefined' && typeof Formulas[level][idx] !== 'undefined' ) ? Formulas[level][idx]() : 0,
    			ns     = level + '_' + idx,
    			name   = (typeof e.original_event.target !== 'undefined' ) ? $(e.original_event.target).attr('name') : null;

    			if( current_form !== ns ) {
    				$.event.trigger({
    					type : 'change_form',
    					old  : current_form,
    					current : ns,
    					level : level,
    					idx : idx
    				});
    				current_form = ns;
    			}
				// console.log( 'level: ' + level + "\n form:" + form + " \n idx:" + idx );
                points = $('body').hasClass('suscripto') ? Math.ceil( points * 0.850 ) : points;
    			$('span', '.apsies-form .puntaje strong').text( points );
    			if( $('input[type="hidden"]', '.apsies-form .puntaje').length == 0 ) {
    				$('<input type="hidden" name="submit_pedido[pts]" />').appendTo( $('.apsies-form .puntaje') );
    			}
    			$('input[type="hidden"]', '.apsies-form .puntaje').val( points );
    	});

		$(document).on('submit', '.apsies-form', function( e ){
            var level  = $('[name="submit_pedido[level]"]').val(),
                tipo   = $('[name="submit_pedido['+ level +'][tipo]').val(),
                is_sus = ( $('.replace_form.sus').length == 1 && $("input[name='submit_pedido[pedido_suscrito]']:radio:checked").val() == 1 ),
                ctx    = is_sus ? $('.replace_form.sus') : $('.replace_form.'+ level +' .form-container[rel="' + tipo + '"]'),
                _error = false;

                $('[data-required]', ctx ).each(function(){
                    if( $(this).is('select') ) {
                        if( $(this).val() < 1 ) {
                            _error = true;
                        }
                    } else {
                        if( $.trim( $(this).val() ) == '' ) {
                            _error = true;
                        }
                    }

                })

                if( _error ) {
                    notification('error', 'Hay campos sin rellenar');
                     return false;
                }

           


            if( $('.replace_form.sus').length == 1 && $("input[name='submit_pedido[pedido_suscrito]']:radio:checked").val() == 1 ) {
                return true;
            }


			if( $('input[type="hidden"]', '.apsies-form .puntaje').length == 0 ){

                return false;
            }
			
			if( $('input[type="hidden"]', '.apsies-form .puntaje').val() > 0 ) {
				return true;
			} else {
                 notification('error', 'El puntaje debe ser superior a 0');
            }



			return false;
		})

         $(document).on('submit', '#loginform', function(e){
            e.preventDefault();  
            var user = $('#user_login', this).val(),
                pwd  = $('#user_pass', this).val(),
                form = $( this );

            $(this)[0].reset();

            $(this).fadeOut("fast", function () {
                 $('#loaderImage').fadeIn("slow");
            });

            $.ajax({
                type: 'post',
                url : base + '/ajax.php?controller=user&action=login',
                dataType : 'json',
                data : {
                    username : user,
                    password : pwd,
                    remember : 1 
                },
                success: function( data, txt, xhr )
                {
                    
                    $('#loaderImage').fadeOut("fast", function(){
                        if( data.status == 'error' ) {
                            notification("error", data.msg );
                            form.fadeIn("slow");
                        } else {

                            window.location = data.url;
                        }
                    })
                }

            })
           
            
        })

    	$('[name="submit_pedido[eso][resolucion_ejercicio][asignatura]"]').on('change', function(e){
    		var new_spinn_value = parseInt( $('[name="submit_pedido[eso][resolucion_ejercicio][asignatura]"] option:selected').attr('rel') );
			if( new_spinn_value !== spinn ) {
				var ctx = ctxForm.eso.resolucion;
				$('.number input', ctx).val(0);    					
			}
			spinn = new_spinn_value;
    	});

    	$(document).on('change_form', function(e){
    		var level = e.level,
    			idx   = e.idx,
    			ns    = e.ns,
    			old   = e.old;
    		$('span', '.apsies-form .puntaje strong').text(0);

    		if( level == 'eso' && idx == 'resolucion' ) {
    				var new_spinn_value = parseInt( $('[name="submit_pedido[eso][resolucion_ejercicio][asignatura]"] option:selected').attr('rel') );
    				if( new_spinn_value !== spinn ) {
						spinn = new_spinn_value;
						var ctx = ctxForm[level][idx];
						
						//$('.number input', ctx).val(0);	
						//$('span', '.apsies-form .puntaje strong').text(0);
					}
    					

    		} else {
    				spinn = 1;
    		}
    	})
    	

    	$("#component-selector").click(function (evt) {
			evt.preventDefault();
			if (!$(this).hasClass("changeOff")) {
				if ($("#component-selector").attr("data-selection") == "1") {
					$("#component-selector #component-handle").animate({'margin-left' : mleftComponent}, 400, "easeInOutSine");
					$('[name="submit_pedido[level]"]').val('uni');
					$('.replace_form.uni').removeClass('uk-hidden');
					$('.replace_form.eso').addClass('uk-hidden');
					$("#component-selector").attr("data-selection", 2);


				} else {
					$("#component-selector #component-handle").animate({'margin-left' : 4}, 400, "easeInOutSine");
					$('[name="submit_pedido[level]"]').val('eso');
					$('.replace_form.eso').removeClass('uk-hidden');
					$('.replace_form.uni').addClass('uk-hidden');
					$("#component-selector").attr("data-selection", 1);
				}
				jQuery.event.trigger({
		        	type: 'refresh_point',
		        	original_event : { target: { name : null } },
		        	time: new Date()
		        });
			}
		});

    	var settings    = {
								action: base + '/ajax.php', // upload url

					            params : {
					            	controller : 'Upload',
					            	action : 'tmp_file'
					            },

					            loadstart: function() {
					               console.log('load_start')
					            },

					            progress: function(percent) {
					                percent = Math.ceil(percent);
					                console.log(percent);
					            },

					            allcomplete: function(responseTxt) {
                                    response = jQuery.parseJSON(responseTxt);
                                   

                                    $.each( response.files, function(){
                                        var html = '<div class="pfiles">Archivo: <strong>' + this.name + '</strong><br> Tamaño: <strong>' + this.size + '</strong> KB <a href="#" data-pos="0" class="close" onClick="return false; jQuery(this).parent().remove(); "><span>X</span></a>'
                                             + '<input type="hidden" name="submit_pedido[adjuntos][]" value="' + this.id + '" /></div>';
                                        $('#upload-drop').parent().append( html );
                                    })
                                    
					            }
					        };

        var select = $.UIkit.uploadSelect($("#upload-select"), settings),
            drop   = $.UIkit.uploadDrop($("#upload-drop"), settings);

        $('form[action="user"]').on('submit',function(e){
        	e.preventDefault();
        	$.ajax(
        		base + '/ajax.php?controller=profile&action=update',
        		{
        			type : 'POST',
        			data : $(this).serialize()
        		})
        }) 

        $('form[action="account"]').on('submit',function(e){
        	e.preventDefault();
        	// Chequear password
        	var new_password = $('input[name="user[new_password]"]', this),
        		rep_password = $('input[name="user[repeat_password]"]', this)
        		;

        	if( new_password.val() !== rep_password.val() ) {
        		$.UIkit.notify({
				    message : 'Las contraseñas no coinciden',
				    status  : 'danger',
				    timeout : 5000,
				    pos     : 'top-center'
				});
				return false;
        	}

            if( new_password.val().length > 0 && new_password.val().length < 6 ) {
                $.UIkit.notify({
                    message : 'Las contraseñas debe tener almenos 6 caracteres',
                    status  : 'danger',
                    timeout : 5000,
                    pos     : 'top-center'
                });
                return false;
            }

        	$.ajax(
        		base + '/ajax.php?controller=profile&action=account',
        		{
        			type : 'POST',
        			data : $(this).serialize(),
                    success : function( res ) {
                        $.each( res, function( index ) {
                            if( this.type == 'info' ) {

                            }
                        })


                    } 
        		}
            );
        })   

        $('#uni_tipo_pedido').on('change',function(e){
        	var context = $('.replace_form.uni'),
        		current = parseInt( $(this).val() );
        	
        	console.log('.form-container[rel="' + current + '"]');

        	$('.form-container', context).addClass('uk-hidden');
        	$('.form-container[rel="' + current + '"]', context).removeClass('uk-hidden');
        });

        $('#eso_tipo_pedido').on('change',function(e){
        	var context = $('.replace_form.eso'),
        		current = parseInt( $(this).val() );
        	
        	console.log('.form-container[rel="' + current + '"]');

        	$('.form-container', context).addClass('uk-hidden');
        	toggleInputs($('.form-container', context), true);
        	$('.form-container[rel="' + current + '"]', context).removeClass('uk-hidden');
        	toggleInputs($('.form-container[rel="' + current + '"]', context), false);
        });

        $('#eso_autocad_tipo').on('change',function(e){
        	var context = $(this).parent().parent().parent().parent(),
        		current = parseInt( $(this).val() );
        	
        	console.log('.sub_form[rel="' + current + '"]');

        	$('.sub_form', context).addClass('uk-hidden');
        	toggleInputs($('.sub_form', context), true);
        	$('.sub_form[rel="' + current + '"]', context).removeClass('uk-hidden');
        	toggleInputs($('.sub_form[rel="' + current + '"]', context), false);
        
        });

        $('#uni_powerpoint_tipo').on('change', function(e){
        	var current = $(this).val(),
        		text    = ( current == 'diapo_texto') ? 'Nº de diapositivas' : 'Nº de paginas';
        	$('label[for="uni_powerpoint_nro_pag"]').text( text );		
        })

        $('.handle_selector').on('click', function(e){
        	e.preventDefault();
        	var context = $(this).parent();
        	$('.indicador').attr('data-info', $(this).attr('rel') );
        	$('input[type="hidden"]', context).val($(this).attr('rel'));

        })


        $("input[name='user[level]']:radio").on('change',function(e){
        	var display = ['eso','uni'],
        		index   = parseInt( $(this).val() )  - 1,
        		odd     = ( index == 0 ) ? 1 : 0 ;

        	$('.toggle.' + display[index] ).removeClass('uk-hidden');
            $('.toggle.' + display[index] + ' input').attr('disabled', false);
        	$('.toggle.' + display[odd] ).addClass('uk-hidden');
            $('.toggle.' + display[odd] + ' input' ).attr('disabled', true);
        });



    	$('a[href="#video"]').on('click',function(e){
    		e.preventDefault();

    		var modal = $.UIkit.modal("#video");

    		if( typeof player != 'object') {
	    		player = new YT.Player('ytplayer', {
			      width: '100%',
			      playerVars: {
		          	listType: 'playlist',
			      	list : 'PLtkGWrZwlwSPrxc76tQLVLCYssj4ybvxj' 
		          }
			      
			    });
    		}
    		console.log( player );
    		console.log( typeof player );
    		if ( modal.isActive() ) {
			    modal.hide();
			} else {
			    modal.show();
			}

			
    		
    	})

    	$(document).on('uk.modal.hide', '#video', function(e){
    		if( typeof player == 'object') {
    			player.pauseVideo();
    		}
    		//
    	})

    	$(document).on('uk.modal.show', '#video', function(e){
    		if( typeof player == 'object') {
    			player.playVideo();;
    		}
    	})

        $( document ).ajaxSuccess(function( event, xhr, settings ) {
            var res = xhr.responseJSON;

            if( res.length > 0 ) {
                $.each( res, function() {
                    if( this.type !== undefined ) {
                        var $this = this;
                        if( this.type == 'info' ) {
                            $.UIkit.notify({
                                message : this.msg,
                                status  : this.status,
                                timeout : 5000,
                                pos     : 'top-center',
                                onClose : function()
                                {
                                    if( typeof $this.url  != 'undefined' ) {
                                        window.location = $this.url ;
                                    }

                                }
                            });
                        }

                        if( this.type == '_redirect' ) {
                            window.location = this.url ;
                        }
                    }
                })
            }
        });


        $('.button_radio:first-child').webuiPopover({
            placement:'top',
            trigger:'hover',//values:click,hover
            content:[
            '¿Pagar de forma segura en Internet, sin cuenta bancaria ni tarjeta de crédito?', 
            '<br/>¡Cómprate paysafecard y paga online como si fuera en efectivo!',
            '<br/>Paga introduciendo un PIN de 16 dígitos o regístrate en my paysafecard y paga de forma sencilla con tu nombre de usuario y contraseña.',
            '<br/>Puedes encontrar más información en <a href="http://www.paysafecard.com" targe="_blank">www.paysafecard.com</a>'].join(' '),
        })

        $('.tooltip_suscription').webuiPopover({
            placement:'auto',
            cache: false,
            trigger:'hover',
            content:function(){
                
                var link = $('#component-selector').attr('data-selection') == 1 ? $(this).data('eso') : $(this).data('uni'); 
                return '¿Quieres saber acerca de la suscripción? Haz click <a href="' + link +'">aquí</a>';
            }
        })

        /*
        {
    placement:'auto',//values: auto,top,right,bottom,left,top-right,top-left,bottom-right,bottom-left
    width:'auto',//can be set with  number
    height:'auto',//can be set with  number
    trigger:'click',//values:click,hover
    style:'',//values:'',inverse
    constrains:null, // constrains the direction when placement is  auto,  values: horizontal,vertical
    delay: {//show and hide delay time of the popover, works only when trigger is 'hover',the value can be number or object
        show: null,
        hide: 300
    },
    async: {
        before: function(that, xhr) {},//executed before ajax request
        success: success(that, data) {}//executed after successful ajax request
    },
    cache:true,//if cache is set to false,popover will destroy and recreate
    multi:false,//allow other popovers in page at same time
    arrow:true,//show arrow or not
    title:'',//the popover title ,if title is set to empty string,title bar will auto hide
    content:'',//content of the popover,content can be function
    closeable:false,//display close button or not
    padding:true,//content padding
    type:'html',//content type, values:'html','iframe','async'
    url:''//if not empty ,plugin will load content by url
}
         */

    });
});


//var basepath = "http://www.pruebaapsiesnetwork.es/";


var contentPedido = "";
var Total = 0;
var files2Upload = new Array();
var files2UploadColegio = new Array();
var files2UploadColegioSuscrito = new Array();
var files = new Array();
var filesColegio = new Array();
var filesColegioSuscrito = new Array();
var vds = 1;
var lg = 0;
	
var barCenterPoint;
var barRightPoint;

jQuery(document).ready(function () {

	var basepath = jQuery('html').attr('base') + '/';
	//var basepath = "http://192.168.173.1/apsies/";

	var ajaxpath = basepath + "wp-admin/admin-ajax.php";
	var themepath = basepath;

	
	var mobileCreated = false;

	jQuery(window).load(function () {
		//alert(jQuery(window).width() + " - " + jQuery(window).height());

		if (jQuery(window).width() == 320) {
			mleftComponent = 60;
		} else {
			mleftComponent = 112;
		}

		prepareInformation();

		/*
		if (jQuery("#component-selector").attr("data-selection") == "2") {
			selectUniversidad();
		} else
		if (jQuery("#component-selector").attr("data-selection") == "1") {
			selectBachiller();
		}
		*/

	});

	jQuery(window).resize(function () {

		prepareInformation();
	});

	jQuery("#level-bachiller a").click(function (evt) {
		evt.preventDefault();
		if (!jQuery(this).hasClass("changeOff")) {
			selectBachiller();
		}
	});
	jQuery("#level-universidad a").click(function (evt) {
		evt.preventDefault();
		if (!jQuery(this).hasClass("changeOff")) {
			selectUniversidad();
		}
	});
	/*
	jQuery("#component-selector").click(function (evt) {
		evt.preventDefault();
		if (!jQuery(this).hasClass("changeOff")) {
			if (jQuery("#component-selector").attr("data-selection") == "1") {
				selectUniversidad();
			} else {
				selectBachiller();
			}
		}
	});
	*/
	function selectBachiller() {
		jQuery("#component-selector #component-handle").animate({'margin-left' : 4}, 400, "easeInOutSine");
		jQuery("#component-selector").attr("data-selection", "1");
		jQuery(".universidad-pedido").fadeOut("fast", function () {
			//jQuery(".colegio-pedido-instantaneo .form-element[data-nivel=0]").val("0");
			jQuery(".colegio-pedido-instantaneo").fadeIn("slow");
		});
		jQuery("div.points span strong").html("0 puntos");
	}
	function selectUniversidad() {

		jQuery("#component-selector #component-handle").animate({'margin-left' : mleftComponent}, 400, "easeInOutSine");
		jQuery("#component-selector").attr("data-selection", "2");
		jQuery(".colegio-pedido-instantaneo").fadeOut("fast", function () {
			//jQuery(".universidad-pedido .form-element[data-nivel=0]").val("0");
			jQuery(".universidad-pedido").fadeIn("slow");
		});
		jQuery("div.points span strong").html("0 puntos");
	}

	// Handling text images bar
	jQuery(".barratextoimagenes-left").click(function (evt) {
		evt.preventDefault();
		jQuery(".barratextoimagenes-handle").animate({"left" : 10}, 300).attr("data-info", "Texto");
	});
	jQuery(".barratextoimagenes-right").click(function (evt) {
		evt.preventDefault();
		jQuery(".barratextoimagenes-handle").animate({"left" : barRightPoint}, 300).attr("data-info", "Im&aacute;genes");
	});
	jQuery(".barratextoimagenes-center").click(function (evt) {
		evt.preventDefault();
		jQuery(".barratextoimagenes-handle").animate({"left" : barCenterPoint}, 300).attr("data-info", "Balanceado");
	});

	function prepareInformation() {
		// Reordering Modules by the width
		if (jQuery(window).width() <= 480) {
			jQuery(".information-module-right img").each(function () {
				var img = jQuery(this);
				var clone = img.clone().addClass("moved");
				var parent = img.parent(".information-module-right");
				var siblingParent = parent.siblings(".information-module-left");
				var elemToAppend = siblingParent.children(".info-module-title");
				elemToAppend.after(clone);
				img.remove();
			});
			jQuery(".information-module-left img").each(function () {
				if (jQuery(this).hasClass("moved") == false) {
					var img = jQuery(this);
					var clone = img.clone().addClass("moved");
					var parent = img.parent(".information-module-left");
					var siblingParent = parent.siblings(".information-module-right");
					var elemToAppend = siblingParent.children(".info-module-title");
					elemToAppend.after(clone);
					img.remove();
				}	
			});
		} else {
			jQuery(".information-module-right img.moved").each(function () {
				var img = jQuery(this);
				var clone = img.clone().removeClass("moved");
				var parent = img.parent(".information-module-right");
				var siblingParent = parent.siblings(".information-module-left");				
				siblingParent.append(clone);
				img.remove();
			});
			jQuery(".information-module-left img.moved").each(function () {				
				var img = jQuery(this);
				var clone = img.clone().removeClass("moved");
				var parent = img.parent(".information-module-left");
				var siblingParent = parent.siblings(".information-module-right");
				siblingParent.append(clone);
				img.remove();
			});
		}

		// Creating the mobile menu
		if (!mobileCreated) {
			jQuery("#main-menu ul li a").each(function () {
				mobileCreated = true;
				var selectCode = '<option value="' + jQuery(this).attr("href") + '">' + jQuery(this).html() + '</option>';
				jQuery("#select-mobile-menu").append(selectCode);
			});
		}
		jQuery("#select-mobile-menu").change(function(){
        	window.location = jQuery("#select-mobile-menu").val();
        });

        // Resizing bar component of exposiciones
        if (jQuery(window).width() <= 800) {
        	jQuery("#form .barratextoimagenes").css("height", "90px");
        } else {
        	jQuery("#form .barratextoimagenes").css("height", "80px");
        }
        if (jQuery(window).width() <= 720) {
        	jQuery(".barratextoimagenes").css("width", 160).css("background", "url(" + themepath + "images/bar-component-small.png) bottom left no-repeat");
        	jQuery(".bartexttitle").css("line-height", "12px").css("text-align", "left");
        	jQuery(".bartextbalanceado").css("left", 55);
        	jQuery(".barratextoimagenes-center").css("left", 71);
        	barCenterPoint = 71;
        	barRightPoint = 131;
        } else {
        	jQuery(".barratextoimagenes").css("width", 430).css("background", "url(" + themepath + "images/bar-component.png) bottom left no-repeat");
        	jQuery(".bartexttitle").css("line-height", "22px");
        	jQuery(".bartextbalanceado").css("left", 191);
        	jQuery(".barratextoimagenes-center").css("left", 206);
        	barCenterPoint = 206;
        	barRightPoint = 402;
        }
        jQuery(".barratextoimagenes-handle").animate({"left" : barCenterPoint}, 300).attr("data-info", "Balanceado");
	}


	// Managing user profiles fields
	if (jQuery("#nivel").val() == 0) {
		jQuery(".tr-universidad").fadeOut("fast");
	} else
	if (jQuery("#nivel").val() == 1) {
		jQuery(".tr-colegio").fadeOut("fast");
	}
	jQuery("#nivel").change(function(){
        if (jQuery("#nivel").val() == 0) {
			jQuery(".tr-universidad").fadeOut("fast");
			jQuery(".tr-colegio").fadeIn("fast");
		} else
		if (jQuery("#nivel").val() == 1) {
			jQuery(".tr-colegio").fadeOut("fast");
			jQuery(".tr-universidad").fadeIn("fast");
		}	
    });

	// Support for spinners
	// jQuery('.spinner1').spinit({  height: 36, min: 0, initValue: 0, max: 5000,  stepInc: 1 });
	// jQuery('.spinner3').spinit({  height: 36, min: 0, initValue: 0, max: 500,  stepInc: 3 });
	// jQuery('.spinner5').spinit({  height: 36, min: 0, initValue: 0, max: 500,  stepInc: 5 });
	// jQuery('.spinner30').spinit({  height: 36, min: 30, initValue: 30, max: 3000,  stepInc: 30 });

	// Support for Forms navigation
	jQuery(".colegio-pedido-instantaneo select.action").change(function () {		
		var nivel = parseInt(jQuery(this).attr("data-nivel"));
		nivel = nivel + 1;		
		refreshNivel(nivel);
		var valor = parseInt(jQuery(this).val());
		if (nivel < 2) {
			jQuery(".colegio-pedido-instantaneo .form-element[data-nivel=" + nivel + "]").each(function () {
				if ((jQuery(window).width() >= 940) || ((!jQuery(this).hasClass("form-message")) && (!jQuery(this).hasClass("form-flecha")))) {
					if (jQuery(this).attr("data-valor") == valor) {
						jQuery(this).fadeIn("slow");
					}
				}
			});
		} else
		if ((nivel == 2) && (valor > 0)) {
			var trabajo = parseInt(jQuery(".colegio-pedido-instantaneo .form-element[data-nivel=0]").val());			
			jQuery(".colegio-pedido-instantaneo .form-element[data-nivel=" + nivel + "]").each(function () {				
				if ((jQuery(window).width() >= 940) || ((!jQuery(this).hasClass("form-message")) && (!jQuery(this).hasClass("form-flecha")))) {
					if (trabajo == 1) {					
						if (jQuery(this).hasClass("base1")) {						
							if (jQuery(this).hasClass("smartspinner")) {					
								if ((valor == 2) ||
									((valor >= 8) && (valor <= 10)) ||
									((valor >= 12) && (valor <= 15))) {
									if (jQuery(this).hasClass("spinner3")) {
										jQuery(this).fadeIn("slow");
									}
								} else {
									if (jQuery(this).hasClass("spinner5")) {
										jQuery(this).fadeIn("slow");
									}
								}
							} else {
								jQuery(this).fadeIn("slow");
							}	
						}
					} else
					if (trabajo == 6) {
						if (jQuery(this).hasClass("base" + trabajo)) {
							if (jQuery(this).attr("data-valor") == valor) {
								jQuery(this).fadeIn("slow");
							}												
						}
					} else
					{
						if (jQuery(this).hasClass("base" + trabajo)) {												
							jQuery(this).fadeIn("slow");						
						}
					}
				}						
			});			
		}	
	});

	jQuery(".colegio-suscritos select.action").change(function () {		
		var nivel = parseInt(jQuery(this).attr("data-nivel"));
		nivel = nivel + 1;		
		refreshNivel(nivel);
		var valor = parseInt(jQuery(this).val());
		if (nivel < 2) {
			jQuery(".colegio-suscritos .form-element[data-nivel=" + nivel + "]").each(function () {
				if (jQuery(this).attr("data-valor") == valor) {
					jQuery(this).fadeIn("slow");
				}
			});
		} else
		if ((nivel == 2) && (valor > 0)) {			
			var asignatura = parseInt(jQuery(".colegio-suscritos .form-element[data-nivel=0]").val());	
			var ptos5 = "¡Recuerda que puedes mandar hasta 5 ejercicios diferentes dentro del mismo pedido!";		
			var ptos3 = "¡Recuerda que puedes mandar hasta 3 ejercicios diferentes dentro del mismo pedido!";
			jQuery(".colegio-suscritos .form-element[data-nivel=" + nivel + "]").each(function () {								
				if ((asignatura == 1) ||
					(asignatura == 3) ||
					(asignatura == 4) ||
					(asignatura == 6) ||
					(asignatura == 7) ||
					(asignatura == 9) ||
					(asignatura == 13) ||
					(asignatura == 16) ||
					(asignatura == 17) 
					) {					
					if (jQuery(this).hasClass("base" + asignatura)) {
						if (jQuery(this).attr("data-valor") == valor) {
							if (valor == 1) {
								jQuery(this).html(ptos5);
							}
							jQuery(this).fadeIn("slow");
						}												
					}
					
				} else
				if ((asignatura == 2) || 
					(asignatura == 8) || 
					(asignatura == 11) || 
					(asignatura == 12) || 
					(asignatura == 14) || 
					(asignatura == 15)
					) {
					if (jQuery(this).hasClass("base" + asignatura)) {
						if (jQuery(this).attr("data-valor") == valor) {
							if (valor == 1) {
								jQuery(this).html(ptos3);
							}
							jQuery(this).fadeIn("slow");
						}												
					}
				}
				else
				{
					if (jQuery(this).hasClass("base" + asignatura)) {
						if (jQuery(this).attr("data-valor") == valor) {							
							jQuery(this).fadeIn("slow");
						}												
					}
				}						
			});			
		}	
	});

	jQuery(".universidad-pedido select.action").change(function () {		
		var nivel = parseInt(jQuery(this).attr("data-nivel"));
		nivel = nivel + 1;		
		refreshNivel(nivel);
		var valor = parseInt(jQuery(this).val());
		if (nivel < 2) {
			jQuery(".universidad-pedido .form-element[data-nivel=" + nivel + "]").each(function () {
				if ((jQuery(window).width() >= 940) || ((!jQuery(this).hasClass("form-message")) && (!jQuery(this).hasClass("form-flecha")))) {
					if (jQuery(this).attr("data-valor") == valor) {
						jQuery(this).fadeIn("slow");
					}
				}	
			});
		} else
		if ((nivel == 2) && (valor > 0)) {						
			var base = parseInt(jQuery(".universidad-pedido .form-element[data-nivel=0]").val());	
			jQuery(".universidad-pedido .form-element[data-nivel=" + nivel + "]").each(function () {								
				if ((jQuery(window).width() >= 940) || ((!jQuery(this).hasClass("form-message")) && (!jQuery(this).hasClass("form-flecha")))) {
					if (jQuery(this).hasClass("base" + base)) {
						if (jQuery(this).attr("data-valor") == valor) {							
							jQuery(this).fadeIn("slow");
						}												
					}
				}						
			});			
		}	
	});

	// Refresing points
	jQuery(".form-pedido .form-element[data-nivel=0]").change(function () {
		jQuery("div.points span strong").html("0 puntos");
	});	

	// Calculating points by task
	function refreshUniversidadPedido() {
		if (jQuery(".universidad-pedido .form-element[data-nivel=0]").val() == 1) {
			// for Trabajos
			contentPedido = "Tipo de Pedido: Universidad - Trabajo <br/>";

			// Obteniendo titulo
			jQuery(".universidad-pedido .titulo").each(function () {
				if ((jQuery(this).attr("data-nivel") == 1) && (jQuery(this).attr("data-valor") == 1)) {					
					contentPedido = contentPedido + "T&iacute;tulo: " + jQuery(this).val() + " <br/>";
				}
			});
			// Obteniendo tema
			jQuery(".universidad-pedido .tema").each(function () {
				if ((jQuery(this).attr("data-nivel") == 1) && (jQuery(this).attr("data-valor") == 1)) {					
					contentPedido = contentPedido + "Tema: " + jQuery(this).find("option:selected").text() + " <br/>";
				}
			});
			// obteniendo cantidad de paginas
			var cantPaginas = 0;
			jQuery(".universidad-pedido .paginas").each(function () {
				if ((jQuery(this).attr("data-nivel") == 1) && (jQuery(this).attr("data-valor") == 1)) {
					cantPaginas = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Cantidad de p&aacute;ginas: " + cantPaginas + " <br/>";
				}
			});			
			// Obteniendo puntos por cantidad de paginas
			var ptosPagina;
			if (cantPaginas <= 10) {
				ptosPagina = cantPaginas * 5.1;
				cantPaginas = cantPaginas - 10;
			} else
			if (cantPaginas > 10) {
				ptosPagina = 51;
				cantPaginas = cantPaginas - 10;
			}

			if ((cantPaginas > 0) && (cantPaginas <= 20)) {
				ptosPagina = ptosPagina + cantPaginas * 3;
				cantPaginas = cantPaginas - 20;
			} else 
			if (cantPaginas > 20) {
				ptosPagina = ptosPagina + 60;
				cantPaginas = cantPaginas - 20;
			}

			if (cantPaginas > 0) {
				ptosPagina = ptosPagina + cantPaginas * 2;
			}

			// obteniendo tiempo entrega
			var tentrega = 0;
			jQuery(".universidad-pedido .tiempo").each(function () {
				if ((jQuery(this).attr("data-nivel") == 1) && (jQuery(this).attr("data-valor") == 1)) {
					tentrega = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";
				}
			});	
			// obteniendo valor a multiplicar por tiempo de entrega
			var factorEntrega = getFactorEntrega(tentrega);

			var subTotal = ptosPagina * factorEntrega;
			
			// obteniendo bibliografia
			var valorBibliografia = 0;
			jQuery(".universidad-pedido .bibliografia").each(function () {
				if ((jQuery(this).attr("data-nivel") == 1) && (jQuery(this).attr("data-valor") == 1)) {
					valorBibliografia = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Bibliograf&iacute;a: " + jQuery(this).find("option:selected").text() + " <br/>";
				}
			});	
			// Obteniendo el factor bibliografia
			var factorBibliografia;
			if (valorBibliografia == 1) {
				factorBibliografia = 1.05;
			} else {
				factorBibliografia = 1;
			}
			// obteniendo observaciones
			jQuery(".universidad-pedido .observaciones").each(function () {
				if ((jQuery(this).attr("data-nivel") == 1) && (jQuery(this).attr("data-valor") == 1)) {					
					contentPedido = contentPedido + "Observaciones: " + jQuery(this).val() + " <br/>";
				}
			});
			
			Total = Math.ceil(subTotal * factorBibliografia * vds);
			jQuery("div.points span strong").html(Total + " puntos");
		} else 
		if (jQuery(".universidad-pedido .form-element[data-nivel=0]").val() == 2) {
			// for AutoCAD
			contentPedido = "Tipo de Pedido: Universidad - AutoCAD <br/>";

			// obteniendo cantidad de piezas
			var cantPiezas = 0;
			jQuery(".universidad-pedido .piezas").each(function () {
				if ((jQuery(this).attr("data-nivel") == 1) && (jQuery(this).attr("data-valor") == 2)) {
					cantPiezas = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Cantidad de piezas: " + cantPiezas + " <br/>";
				}
			});

			// obteniendo vistas
			var vistas = 0;
			jQuery(".universidad-pedido .vistas").each(function () {
				if ((jQuery(this).attr("data-nivel") == 1) && (jQuery(this).attr("data-valor") == 2)) {
					vistas = parseFloat(jQuery(this).val());
					contentPedido = contentPedido + "Vistas: " + jQuery(this).find("option:selected").text() + " <br/>";
				}
			});

			// obteniendo acotacion
			var acotacion = 0;
			jQuery(".universidad-pedido .acotacion").each(function () {
				if ((jQuery(this).attr("data-nivel") == 1) && (jQuery(this).attr("data-valor") == 2)) {
					acotacion = parseFloat(jQuery(this).val());
					contentPedido = contentPedido + "Acotaci&oacute;n: " + jQuery(this).find("option:selected").text() + " <br/>";
				}
			});

			// obteniendo tiempo entrega
			var tentrega = 0;
			jQuery(".universidad-pedido .tiempo").each(function () {
				if ((jQuery(this).attr("data-nivel") == 1) && (jQuery(this).attr("data-valor") == 2)) {
					tentrega = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";
				}
			});	
			// Obteniendo Observaciones
			jQuery(".universidad-pedido .observaciones").each(function () {
				if ((jQuery(this).attr("data-nivel") == 1) && (jQuery(this).attr("data-valor") == 2)) {					
					contentPedido = contentPedido + "Observaciones: " + jQuery(this).val() + " <br/>";
				}
			});
			// obteniendo valor a multiplicar por tiempo de entrega
			var factorEntrega = getFactorEntrega(tentrega);

			Total = Math.ceil(cantPiezas * 10 * vistas * acotacion * factorEntrega * vds);
			jQuery("div.points span strong").html(Total + " puntos");
		} else 
		if (jQuery(".universidad-pedido .form-element[data-nivel=0]").val() == 3) {
			// apuntes
			contentPedido = "Tipo de Pedido: Universidad - Apuntes <br/>";

			// obteniendo cantidad de piezas
			var subiropaquete = 0;
			jQuery(".universidad-pedido .subiropaquete").each(function () {
				if ((jQuery(this).attr("data-nivel") == 1) && (jQuery(this).attr("data-valor") == 3)) {
					subiropaquete = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Tipo: " + jQuery(this).find("option:selected").text() + " <br/>";
				}
			});
			if (subiropaquete == 1) {
				var cantPaginas;
				jQuery(".universidad-pedido .paginassubir").each(function () {
					if ((jQuery(this).attr("data-nivel") == 2) && (jQuery(this).attr("data-valor") == 1)) {
						cantPaginas = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Cantidad de p&aacute;ginas: " + cantPaginas + " <br/>";
					}
				});
				var tentrega = 0;
				jQuery(".universidad-pedido .tiemposubir").each(function () {
					if ((jQuery(this).attr("data-nivel") == 2) && (jQuery(this).attr("data-valor") == 1)) {
						tentrega = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";
					}
				});
				// obteniendo valor a multiplicar por tiempo de entrega
				var factorEntrega = getFactorEntregaReducido(tentrega);				
			} else
			if (subiropaquete == 2) {
				var cantPaginas;
				jQuery(".universidad-pedido .paginaspaqueteria").each(function () {
					if ((jQuery(this).attr("data-nivel") == 2) && (jQuery(this).attr("data-valor") == 2)) {
						cantPaginas = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Cantidad de p&aacute;ginas: " + cantPaginas + " <br/>";
					}
				});
				var tentrega = 0;
				jQuery(".universidad-pedido .tiempopaqueteria").each(function () {
					if ((jQuery(this).attr("data-nivel") == 2) && (jQuery(this).attr("data-valor") == 2)) {
						tentrega = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";
					}
				});
				// obteniendo valor a multiplicar por tiempo de entrega
				var factorEntrega = getFactorEntregaReducido(tentrega);
			} else {
				cantPaginas = 0;
				factorEntrega = 0;
			}

			Total = Math.ceil(cantPaginas * factorEntrega * vds);
			jQuery("div.points span strong").html(Total + " puntos");
		} else 
		if (jQuery(".universidad-pedido .form-element[data-nivel=0]").val() == 4) {
			// power point
			contentPedido = "Tipo de Pedido: Universidad - Power Point <br/>";

			// obteniendo cantidad de piezas
			var diapositivaotexto = 0;
			jQuery(".universidad-pedido .diapositivaotexto").each(function () {
				if ((jQuery(this).attr("data-nivel") == 1) && (jQuery(this).attr("data-valor") == 4)) {
					diapositivaotexto = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Tipo: " + jQuery(this).find("option:selected").text() + " <br/>";					
				}
			});
			if (diapositivaotexto == 1) {				
				var diapositivas;
				jQuery(".universidad-pedido .diapositivascantidad").each(function () {
					if ((jQuery(this).attr("data-nivel") == 2) && (jQuery(this).attr("data-valor") == 1)) {
						diapositivas = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Diapositivas: " + diapositivas + " <br/>";						
					}
				});
				var tentrega = 0;
				jQuery(".universidad-pedido .tiempodiapositivas").each(function () {
					if ((jQuery(this).attr("data-nivel") == 2) && (jQuery(this).attr("data-valor") == 1)) {
						tentrega = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";						
					}
				});
				var factor = diapositivas * 0.3;
				// obteniendo valor a multiplicar por tiempo de entrega
				var factorEntrega = getFactorEntregaReducido(tentrega);			
			} else
			if (diapositivaotexto == 2) {
				var cantPaginas;
				jQuery(".universidad-pedido .paginascantidad").each(function () {
					if ((jQuery(this).attr("data-nivel") == 2) && (jQuery(this).attr("data-valor") == 2)) {
						cantPaginas = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "P&aacute;ginas: " + cantPaginas + " <br/>";
					}
				});
				var tentrega = 0;
				jQuery(".universidad-pedido .tiempopaginas").each(function () {
					if ((jQuery(this).attr("data-nivel") == 2) && (jQuery(this).attr("data-valor") == 2)) {
						tentrega = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";
					}
				});
				var factor = cantPaginas * 3;
				// obteniendo valor a multiplicar por tiempo de entrega
				var factorEntrega = getFactorEntregaReducido(tentrega);
			} else {
				diapositivas = 0;
				factorEntrega = 0;
				factor = 0;
			}

			Total = Math.ceil(factor * factorEntrega * vds);
			jQuery("div.points span strong").html(Total + " puntos");
		} else 
		if (jQuery(".universidad-pedido .form-element[data-nivel=0]").val() == 5) {
			// exposiciones
			contentPedido = "Tipo de Pedido: Universidad - Exposiciones <br/>";

			// obteniendo cantidad de piezas
			var partiendo = 0;
			jQuery(".universidad-pedido .partiendo").each(function () {
				if ((jQuery(this).attr("data-nivel") == 1) && (jQuery(this).attr("data-valor") == 5)) {
					partiendo = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Tipo: " + jQuery(this).find("option:selected").text() + " <br/>";					
				}
			});
			if (partiendo == 1) {				
				var diapositivas;
				jQuery(".universidad-pedido .diapositivaspartiendoescrito").each(function () {
					if ((jQuery(this).attr("data-nivel") == 2) && (jQuery(this).attr("data-valor") == 1)) {
						diapositivas = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Diapositivas: " + diapositivas + " <br/>";						
					}
				});
				// parte hablada
				var partehablada = 0;
				jQuery(".universidad-pedido .partehabladapartiendoescrito").each(function () {
					if ((jQuery(this).attr("data-nivel") == 2) && (jQuery(this).attr("data-valor") == 1)) {
						partehablada = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Incluir parte hablada: " + jQuery(this).find("option:selected").text() + " <br/>";						
					}
				});
				// tiempo entrega
				var tentrega = 0;
				jQuery(".universidad-pedido .tiempopartiendoescrito").each(function () {
					if ((jQuery(this).attr("data-nivel") == 2) && (jQuery(this).attr("data-valor") == 1)) {
						tentrega = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";						
					}
				});
				// proporcion texto - imagenes
				jQuery(".barratextoimagenes-handle").each(function () {
					contentPedido = contentPedido + "Proporci&oacute;n Texto - Im&aacute;genes: " + jQuery(this).attr("data-info") + " <br/>";
					return false;
				});				
				var factor = diapositivas * 4.1;
				var factorph;
				if (partehablada == 1) {
					factorph = 1.25;
				} else {
					factorph = 1;
				}
				// obteniendo valor a multiplicar por tiempo de entrega
				var factorEntrega = getFactorEntregaReducido(tentrega);			
			} else
			if (partiendo == 2) {
				var diapositivas;
				jQuery(".universidad-pedido .diapositivaspartiendocero").each(function () {
					if ((jQuery(this).attr("data-nivel") == 2) && (jQuery(this).attr("data-valor") == 2)) {
						diapositivas = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Diapositivas: " + diapositivas + " <br/>";						
					}
				});
				// parte hablada
				var partehablada = 0;
				jQuery(".universidad-pedido .partehabladapartiendocero").each(function () {
					if ((jQuery(this).attr("data-nivel") == 2) && (jQuery(this).attr("data-valor") == 2)) {
						partehablada = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Incluir parte hablada: " + jQuery(this).find("option:selected").text() + " <br/>";						
					}
				});
				// tiempo entrega
				var tentrega = 0;
				jQuery(".universidad-pedido .tiempopartiendocero").each(function () {
					if ((jQuery(this).attr("data-nivel") == 2) && (jQuery(this).attr("data-valor") == 2)) {
						tentrega = parseInt(jQuery(this).val());	
						contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";						
					}
				});		
				// proporcion texto - imagenes
				jQuery(".barratextoimagenes-handle").each(function () {
					contentPedido = contentPedido + "Proporci&oacute;n Texto - Im&aacute;genes: " + jQuery(this).attr("data-info") + " <br/>";
					return false;
				});		
				var factor = diapositivas * 7.1;
				var factorph;
				if (partehablada == 1) {
					factorph = 1.25;
				} else {
					factorph = 1;
				}
				// obteniendo valor a multiplicar por tiempo de entrega
				var factorEntrega = getFactorEntregaReducido(tentrega);	
			} else {
				diapositivas = 0;
				factorEntrega = 0;
				factor = 0;
				factorph = 0;
			}

			Total = Math.ceil(factor * factorph * factorEntrega * vds);
			jQuery("div.points span strong").html(Total + " puntos");
		}
	}
	jQuery(".universidad-pedido .form-element").click(function () {
		// there are changes
		refreshUniversidadPedido();
	});
	jQuery(".universidad-pedido .btn").click(function (evt) {
		evt.preventDefault();	
		refreshUniversidadPedido();
		var error = false;
		jQuery(".universidad-pedido .form-element").each(function () {
			if (jQuery(this).css("display") === "block") {
				if (jQuery(this).is("input")) {
					if (((jQuery(this).val() === "")) || ((jQuery(this).val() === "0"))) {
						error = true;						
					}
				} else 
				if (jQuery(this).is("select")) {
					if (jQuery(this).val() === "0") {
						error = true;						
					} 
				}
			}
		});
		if (error) {
			notification("error", "Llene completamente el formulario. Tiene campos sin llenar.", "");
		} else
		{
			var r = confirm("Desea hacer el pedido?");
			if (r == true) {
				if (lg == 1) {
					notificationAjaxOn("Realizando pedido ...");
				    // Haciendo el Pedido
				    jQuery.ajax({  
				  		type: 'POST',  
				  		url: ajaxpath, 
				  		dataType: "json", 			  
				  		data: {  
				  			action: 'MyAjaxFunctions',  
				        	toaction: 'doPedido',
				          	contentPedido: contentPedido,
				          	Total: Total,
				          	csuscrito: 0,
				          	factors: vds,
				          	tipopedido: jQuery(".universidad-pedido .form-element[data-nivel=0]").find("option:selected").text()
				  		},  
				  		success: function(data, textStatus, XMLHttpRequest){    				
				  				notificationAjaxOff();
				  				if (data["error"] == false) {		  					
				  					jQuery("div.points span strong").html("0 puntos");
				  					jQuery(".form-pedido .form-element[data-nivel=0]").val("0");
				  					jQuery("div#left-sidebar p#cantidad-puntos").html(data["Total"]);		  					
				  					refreshNivel(1);
				  					//jQuery("#universidad-form-post-id").val(data["post_id"]);
				  					//jQuery("#universidad-form").submit();			  					
				  					if (files.length > 0) {
				  						for (var i = 0; i < files.length; i++) {
					  						var fd = new FormData();    
											fd.append( 'action',  'MyAjaxFunctions');
											fd.append( 'toaction',  'uploadArchive');
											fd.append( 'post_id',  data["post_id"]);
											fd.append( 'fileselect',  files[i]);
						  					jQuery.ajax({  
										  		type: 'POST',  
										  		url: ajaxpath, 
										  		dataType: "json", 			  
										  		data: fd, 
										  		processData: false, 
										  		contentType: false, 
										  		cache : false,
										  		success: function(data, textStatus, XMLHttpRequest){								  			
										  			notification("success", "Pedido realizado satisfactoriamente!");
				                					jQuery("#universidad-details").html("");		  						  				
										  		},  
										  		error: function(MLHttpRequest, textStatus, errorThrown){  
										  			alert(errorThrown);  
										  		}  
										  	});
										}
				  					} else {
				  						notification("success", "Pedido realizado satisfactoriamente!");
				  					}			  					
				  				} else {
				  					if (data["url"] != "") {
				  						notification("error", data["message"], data["url"]);
				  					} else {
				  						notification("error", data["message"]);
				  					}
				  				}	  						  				
				  		},  
				  		error: function(MLHttpRequest, textStatus, errorThrown){  
				  			alert(errorThrown);  
				  		}  
				  	});	
				} else {
					notification("error", "Debe registrarse y entrar para poder realizar pedidos", "registrate");
				}
			}
		}
	});

	/*$(document).on('submit', '#universidad-form', function() {            
        $.ajax({
            url     : $(this).attr('action'),
            type    : $(this).attr('method'),
            dataType: 'json',
            data    : new FormData( this ),
	        processData: false,
	        contentType: false,
            success : function( data ) {
                //alert(data);
                jQuery("#universidad-details").html("");
            },
            error   : function( xhr, err ) {
                alert('Error');     
            }
        });    
        return false;
    });*/	

	function refreshColegioInstantaneo() {		
		if (jQuery(".colegio-pedido-instantaneo .form-element[data-nivel=0]").val() == 1) {
			// resolucion ejercicios
			contentPedido = "Tipo de Pedido: Colegio - Resolucion de ejercicios <br/>";

			// asignatura
			jQuery(".colegio-pedido-instantaneo .asignatura").each(function () {
				if ((jQuery(this).attr("data-nivel") == "1") && (jQuery(this).attr("data-valor") == "1")) {
					contentPedido = contentPedido + "Asignatura: " + jQuery(this).find("option:selected").text() + " <br/>";												
				}
			});
			// pedidos de ejercicios
			var ejercicios = 0;
			jQuery(".colegio-pedido-instantaneo .ejercicios").each(function () {
				if ((jQuery(this).hasClass("base1")) && (jQuery(this).css("display") == "block")) {
					if (jQuery(this).hasClass("spinner5")) {
						ejercicios = parseInt(jQuery(this).val()) / 5;
					} else
					if (jQuery(this).hasClass("spinner3")) {
						ejercicios = parseInt(jQuery(this).val()) / 3;
					}	
					contentPedido = contentPedido + "Pedidos de ejercicios: " + ejercicios + " <br/>";												
				}
			});
			// tiempo
			var tiempo = 0;
			jQuery(".colegio-pedido-instantaneo .tiempo").each(function () {
				if (jQuery(this).hasClass("base1")) {
					tiempo = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";						
				}
			});
			var factorEjercicios = ejercicios * 2;
			var factorEntrega = getFactorEntregaColegio(tiempo);
			Total = Math.ceil(factorEjercicios * factorEntrega * vds);
			jQuery("div.points span strong").html(Total + " puntos"); 
		} else
		if (jQuery(".colegio-pedido-instantaneo .form-element[data-nivel=0]").val() == 2) {
			// redaccion tema
			contentPedido = "Tipo de Pedido: Colegio - Redaccion de tema <br/>";

			// asignatura
			jQuery(".colegio-pedido-instantaneo .asignatura").each(function () {
				if ((jQuery(this).attr("data-nivel") == "1") && (jQuery(this).attr("data-valor") == "2")) {
					contentPedido = contentPedido + "Asignatura: " + jQuery(this).find("option:selected").text() + " <br/>";												
				}
			});
			// tema
			jQuery(".colegio-pedido-instantaneo .tema").each(function () {
				if (jQuery(this).hasClass("base2")) {
					contentPedido = contentPedido + "Tema: " + jQuery(this).val() + " <br/>";
				}
			});
			// palabras
			var palabras = 0;
			jQuery(".colegio-pedido-instantaneo .palabras").each(function () {
				if (jQuery(this).hasClass("base2")) {
					palabras = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Palabras: " + palabras + " <br/>";
				}
			});
			// tiempo
			var tiempo = 0;
			jQuery(".colegio-pedido-instantaneo .tiempo").each(function () {
				if (jQuery(this).hasClass("base2")) {
					tiempo = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";						
				}
			});
			// observaciones
			jQuery(".colegio-pedido-instantaneo .observaciones").each(function () {
				if (jQuery(this).hasClass("base2")) {
					contentPedido = contentPedido + "Observaciones: " + jQuery(this).val() + " <br/>";
				}
			});
			var factorPalabras = palabras * 0.03;
			var factorEntrega = getFactorEntregaColegio(tiempo);
			Total = Math.ceil(factorPalabras * factorEntrega * vds);
			jQuery("div.points span strong").html(Total + " puntos"); 
		} else
		if (jQuery(".colegio-pedido-instantaneo .form-element[data-nivel=0]").val() == 3) {
			// comentario texto
			contentPedido = "Tipo de Pedido: Colegio - Comentario de texto <br/>";

			// asignatura
			jQuery(".colegio-pedido-instantaneo .asignatura").each(function () {
				if ((jQuery(this).attr("data-nivel") == "1") && (jQuery(this).attr("data-valor") == "3")) {
					contentPedido = contentPedido + "Asignatura: " + jQuery(this).find("option:selected").text() + " <br/>";												
				}
			});
			// palabras
			var palabras = 0;
			jQuery(".colegio-pedido-instantaneo .palabras").each(function () {
				if (jQuery(this).hasClass("base3")) {
					palabras = parseInt(jQuery(this).val());	
					contentPedido = contentPedido + "Palabras: " + palabras + " <br/>";											
				}
			});
			// tiempo
			var tiempo = 0;
			jQuery(".colegio-pedido-instantaneo .tiempo").each(function () {
				if (jQuery(this).hasClass("base3")) {
					tiempo = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";						
				}
			});
			// observaciones
			jQuery(".colegio-pedido-instantaneo .observaciones").each(function () {
				if (jQuery(this).hasClass("base3")) {
					contentPedido = contentPedido + "Observaciones: " + jQuery(this).val() + " <br/>";
				}
			});
			// texto a comentar
			jQuery(".colegio-pedido-instantaneo .textocomentar").each(function () {
				if (jQuery(this).hasClass("base3")) {
					contentPedido = contentPedido + "Observaciones: " + jQuery(this).val() + " <br/>";
				}
			});
			var factorPalabras = palabras * 0.03;
			var factorEntrega = getFactorEntregaColegio(tiempo);
			Total = Math.ceil(factorPalabras * factorEntrega * vds);
			jQuery("div.points span strong").html(Total + " puntos"); 
		} else
		if (jQuery(".colegio-pedido-instantaneo .form-element[data-nivel=0]").val() == 4) {
			// comentario artistico
			contentPedido = "Tipo de Pedido: Colegio - Comentario artistico <br/>";

			// asignatura
			jQuery(".colegio-pedido-instantaneo .asignatura").each(function () {
				if ((jQuery(this).attr("data-nivel") == "1") && (jQuery(this).attr("data-valor") == "4")) {
					contentPedido = contentPedido + "Asignatura: " + jQuery(this).find("option:selected").text() + " <br/>";												
				}
			});
			// palabras
			var palabras = 0;
			jQuery(".colegio-pedido-instantaneo .palabras").each(function () {
				if (jQuery(this).hasClass("base4")) {
					palabras = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Cantidad de palabras: " + palabras + " <br/>";												
				}
			});
			// tiempo
			var tiempo = 0;
			jQuery(".colegio-pedido-instantaneo .tiempo").each(function () {
				if (jQuery(this).hasClass("base4")) {
					tiempo = parseInt(jQuery(this).val());	
					contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";					
				}
			});
			// observaciones
			jQuery(".colegio-pedido-instantaneo .observaciones").each(function () {
				if (jQuery(this).hasClass("base4")) {
					contentPedido = contentPedido + "Observaciones: " + jQuery(this).val() + " <br/>";
				}
			});
			// texto a comentar
			jQuery(".colegio-pedido-instantaneo .obracomentar").each(function () {
				if (jQuery(this).hasClass("base4")) {
					contentPedido = contentPedido + "Observaciones: " + jQuery(this).val() + " <br/>";
				}
			});
			var factorPalabras = palabras * 0.03;
			var factorEntrega = getFactorEntregaColegio(tiempo);
			Total = Math.ceil(factorPalabras * factorEntrega * vds);
			jQuery("div.points span strong").html(Total + " puntos"); 
		} else
		if (jQuery(".colegio-pedido-instantaneo .form-element[data-nivel=0]").val() == 7) {
			// Dudas / Investigacion sobre un tema
			contentPedido = "Tipo de Pedido: Colegio - Dudas / Investigacion sobre un tema <br/>";

			// asignatura
			jQuery(".colegio-pedido-instantaneo .asignatura").each(function () {
				if ((jQuery(this).attr("data-nivel") == "1") && (jQuery(this).attr("data-valor") == "7")) {
					contentPedido = contentPedido + "Asignatura: " + jQuery(this).find("option:selected").text() + " <br/>";												
				}
			});
			// tema
			jQuery(".colegio-pedido-instantaneo .tema").each(function () {
				if (jQuery(this).hasClass("base7")) {
					contentPedido = contentPedido + "Tema: " + jQuery(this).val() + " <br/>";
				}
			});
			// palabras
			var palabras = 0;
			jQuery(".colegio-pedido-instantaneo .palabras").each(function () {
				if (jQuery(this).hasClass("base7")) {
					palabras = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Palabras: " + palabras + " <br/>";												
				}
			});
			// tiempo
			var tiempo = 0;
			jQuery(".colegio-pedido-instantaneo .tiempo").each(function () {
				if (jQuery(this).hasClass("base7")) {
					tiempo = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";						
				}
			});
			// observaciones
			jQuery(".colegio-pedido-instantaneo .observaciones").each(function () {
				if (jQuery(this).hasClass("base7")) {
					contentPedido = contentPedido + "Observaciones: " + jQuery(this).val() + " <br/>";
				}
			});
			var factorPalabras = palabras * 0.03;
			var factorEntrega = getFactorEntregaColegio(tiempo);
			Total = Math.ceil(factorPalabras * factorEntrega * vds);
			jQuery("div.points span strong").html(Total + " puntos"); 
		} else
		if (jQuery(".colegio-pedido-instantaneo .form-element[data-nivel=0]").val() == 5) {
			// critica literaria
			contentPedido = "Tipo de Pedido: Colegio - Critica literaria <br/>";

			// titulo
			jQuery(".colegio-pedido-instantaneo .titulo").each(function () {
				if ((jQuery(this).attr("data-nivel") == "1") && (jQuery(this).attr("data-valor") == "5")) {
					contentPedido = contentPedido + "Titulo de la obra: " + jQuery(this).val() + " <br/>";												
				}
			});
			// partes
			var pain = false; 
			contentPedido = contentPedido + "Partes: ";			
			jQuery(".colegio-pedido-instantaneo .partesincluir:checked").each(function () {
				if (pain == true) { contentPedido = contentPedido + ", "; }
				contentPedido = contentPedido + jQuery(this).attr("data-valor");
				pain = true;
			});
			contentPedido = contentPedido + "<br/>";  
			// palabras
			var palabras = 0;
			jQuery(".colegio-pedido-instantaneo .palabrascritica").each(function () {
				palabras = parseInt(jQuery(this).val());
				contentPedido = contentPedido + "Palabras: " + palabras + " <br/>";
			});
			// tiempo
			var tiempo = 0;
			jQuery(".colegio-pedido-instantaneo .tiempocritica").each(function () {
				tiempo = parseInt(jQuery(this).val());
				contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";
			});
			// observaciones
			jQuery(".colegio-pedido-instantaneo .observacionescritica").each(function () {				
				contentPedido = contentPedido + "Observaciones: " + jQuery(this).val() + " <br/>";				
			});
			var factorPalabras = palabras * 8;
			var factorEntrega = getFactorEntregaColegio(tiempo);
			Total = Math.ceil(factorPalabras * factorEntrega * vds);
			jQuery("div.points span strong").html(Total + " puntos"); 
		} else
		if (jQuery(".colegio-pedido-instantaneo .form-element[data-nivel=0]").val() == 6) {
			// AutoCAD
			contentPedido = "Tipo de Pedido: Colegio - AutoCAD <br/>";

			// tipo
			var autocadtipo = 0;
			jQuery(".colegio-pedido-instantaneo .autocadtipo").each(function () {
				autocadtipo = parseInt(jQuery(this).val());
				contentPedido = contentPedido + "Tipo: " + jQuery(this).find("option:selected").text() + " <br/>";
			});

			if (autocadtipo == 1) {
				// ejercicios
				var ejercicios = 0;
				jQuery(".colegio-pedido-instantaneo .ejercicios").each(function () {
					if (jQuery(this).hasClass("base6")) {
						ejercicios = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Ejercicios: " + ejercicios + " <br/>";
					}
				});
				// tiempo
				var tiempo = 0;
				jQuery(".colegio-pedido-instantaneo .tiempoejercicios").each(function () {
					if (jQuery(this).hasClass("base6")) {
						tiempo = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";
					}
				});
				// observaciones
				jQuery(".colegio-pedido-instantaneo .observaciones").each(function () {
					if ((jQuery(this).attr("data-nivel") == "1") && (jQuery(this).attr("data-valor") == "6")) {
						contentPedido = contentPedido + "Observaciones: " + jQuery(this).val() + " <br/>";												
					}
				});
				var factorEjercicios = ejercicios * 2;
				var factorEntrega = getFactorEntregaColegio(tiempo);
				Total = Math.ceil(factorEjercicios * factorEntrega * vds);
				jQuery("div.points span strong").html(Total + " puntos"); 
			} else
			if (autocadtipo == 2) {
				// piezas
				var piezas = 0;
				jQuery(".colegio-pedido-instantaneo .piezas").each(function () {
					if (jQuery(this).hasClass("base6")) {
						piezas = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Cantidad de piezas: " + piezas + " <br/>";
					}
				});
				// vistas
				var vistaspiezas = 0;
				jQuery(".colegio-pedido-instantaneo .vistaspiezas").each(function () {
					if (jQuery(this).hasClass("base6")) {
						vistaspiezas = parseInt(jQuery(this).val());
						if (vistaspiezas == 1) {
							vistaspiezas = 1.5;
						} else {
							vistaspiezas = 1;
						}
						contentPedido = contentPedido + "Vistas: " + jQuery(this).find("option:selected").text() + " <br/>";
					}
				});
				// acotacion
				var acotacionpiezas = 0;
				jQuery(".colegio-pedido-instantaneo .acotacionpiezas").each(function () {
					if (jQuery(this).hasClass("base6")) {
						acotacionpiezas = parseInt(jQuery(this).val());
						if (acotacionpiezas == 1) {
							acotacionpiezas = 1.5;
						} else {
							acotacionpiezas = 1;
						}
						contentPedido = contentPedido + "Acotacion: " + jQuery(this).find("option:selected").text() + " <br/>";
					}
				});
				// tiempo
				var tiempo = 0;
				jQuery(".colegio-pedido-instantaneo .tiempopiezas").each(function () {
					if (jQuery(this).hasClass("base6")) {
						tiempo = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";
					}
				});
				// observaciones
				jQuery(".colegio-pedido-instantaneo .observaciones").each(function () {
					if ((jQuery(this).attr("data-nivel") == "1") && (jQuery(this).attr("data-valor") == "6")) {
						contentPedido = contentPedido + "Observaciones: " + jQuery(this).val() + " <br/>";												
					}
				});
				var factorPiezas = piezas * 3;
				var factorEntrega = getFactorEntregaColegio(tiempo);
				Total = Math.ceil(factorPiezas * vistaspiezas * acotacionpiezas * factorEntrega * vds);				
				jQuery("div.points span strong").html(Total + " puntos"); 
			}
		} else
		if (jQuery(".colegio-pedido-instantaneo .form-element[data-nivel=0]").val() == 8) {
			// trabajos
			contentPedido = "Tipo de Pedido: Colegio - Trabajos <br/>";

			// titulo
			jQuery(".colegio-pedido-instantaneo .titulo").each(function () {
				if ((jQuery(this).attr("data-nivel") == "1") && (jQuery(this).attr("data-valor") == "8")) {
					contentPedido = contentPedido + "Titulo: " + jQuery(this).val() + " <br/>";												
				}
			});
			// tema
			jQuery(".colegio-pedido-instantaneo .tema").each(function () {
				if ((jQuery(this).attr("data-nivel") == "1") && (jQuery(this).attr("data-valor") == "8")) {
					contentPedido = contentPedido + "Tema: " + jQuery(this).find("option:selected").text() + " <br/>";											
				}
			});
			// paginas
			var paginas = 0;
			jQuery(".colegio-pedido-instantaneo .paginastrabajo").each(function () {
				paginas = parseInt(jQuery(this).val());
				contentPedido = contentPedido + "Cantidad de p&aacute;ginas: " + paginas + " <br/>";

			});
			// bibliografia
			var bibliografia = 0;
			jQuery(".colegio-pedido-instantaneo .bibliografiatrabajo").each(function () {
				bibliografia = parseInt(jQuery(this).val());
				if (bibliografia == 1) {
					bibliografia = 1.05;
				} else {
					bibliografia = 1;
				}
				contentPedido = contentPedido + "Bibliografia: " + jQuery(this).find("option:selected").text() + " <br/>";
			});
			// tiempo
			var tiempo = 0;
			jQuery(".colegio-pedido-instantaneo .tiempotrabajo").each(function () {
				tiempo = parseInt(jQuery(this).val());
				contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";
			});
			// observaciones
			jQuery(".colegio-pedido-instantaneo .observaciones").each(function () {
				if ((jQuery(this).attr("data-nivel") == "1") && (jQuery(this).attr("data-valor") == "8")) {
					contentPedido = contentPedido + "Observaciones: " + jQuery(this).val() + " <br/>";												
				}
			});
			var factorPaginas = paginas * 4.1;
			var factorEntrega = getFactorEntregaColegio(tiempo);
			Total = Math.ceil(factorPaginas * bibliografia * factorEntrega * vds);			
			jQuery("div.points span strong").html(Total + " puntos"); 
		}
	}

	jQuery(".colegio-pedido-instantaneo .form-element").click(function () {
		// there are changes
		refreshColegioInstantaneo();
	});
	jQuery(".colegio-pedido-instantaneo .btn").click(function (evt) {
		evt.preventDefault();	
		refreshColegioInstantaneo();
		var error = false;
		jQuery(".colegio-pedido-instantaneo .form-element").each(function () {
			if (jQuery(this).css("display") == "block") {
				if (jQuery(this).is("input")) {
					if (((jQuery(this).val() == "")) || ((jQuery(this).val() == "0"))) {
						error = true;
					}
				} else 
				if (jQuery(this).is("select")) {
					if (jQuery(this).val() == "0") {
						error = true;
					} 
				} else {
				}				
			}
		});
		if (error) {
			notification("error", "Llene completamente el formulario. Tiene campos sin llenar.", "");
		} else
		{	
			var r = confirm("Desea hacer el pedido?");
			if (r == true) {
				if (lg == 1) {
					notificationAjaxOn("Realizando pedido ...");
				    // Haciendo el Pedido
				    jQuery.ajax({  
				  		type: 'POST',  
				  		url: ajaxpath, 
				  		dataType: "json", 			  
				  		data: {  
				  			action: 'MyAjaxFunctions',  
				        	toaction: 'doPedido',
				          	contentPedido: contentPedido,
				          	Total: Total,
				          	csuscrito: 0,
				          	factors: vds,
				          	tipopedido: jQuery(".colegio-pedido-instantaneo .form-element[data-nivel=0]").find("option:selected").text()
				  		},  
				  		success: function(data, textStatus, XMLHttpRequest){    				
				  				notificationAjaxOff();
				  				if (data["error"] == false) {		  					
				  					jQuery("div.points span strong").html("0 puntos");
				  					jQuery(".form-pedido .form-element[data-nivel=0]").val("0");
				  					jQuery("div#left-sidebar p#cantidad-puntos").html(data["Total"]);		  					
				  					refreshNivel(1);
				  					//jQuery("#colegio-form-post-id").val(data["post_id"]);
				  					//jQuery("#colegio-form").submit();
				  					if (filesColegio.length > 0) {
				  						for (var i = 0; i < filesColegio.length; i++) {
					  						var fd = new FormData();    
											fd.append( 'action',  'MyAjaxFunctions');
											fd.append( 'toaction',  'uploadArchive');
											fd.append( 'post_id',  data["post_id"]);
											fd.append( 'fileselect',  filesColegio[i]);
						  					jQuery.ajax({  
										  		type: 'POST',  
										  		url: ajaxpath, 
										  		dataType: "json", 			  
										  		data: fd, 
										  		processData: false, 
										  		contentType: false, 
										  		cache : false,
										  		success: function(data, textStatus, XMLHttpRequest){    				
										  			notification("success", "Pedido realizado satisfactoriamente!");
				                					jQuery("#colegio-details").html("");		  						  				
										  		},  
										  		error: function(MLHttpRequest, textStatus, errorThrown){  
										  			alert(errorThrown);  
										  		}  
										  	});
										}
				  					} else {
				  						notification("success", "Pedido realizado satisfactoriamente!");
				  					}	  								  					
				  				} else {
				  					if (data["url"] != "") {
				  						notification("error", data["message"], data["url"]);
				  					} else {
				  						notification("error", data["message"]);
				  					}
				  				}	  						  				
				  		},  
				  		error: function(MLHttpRequest, textStatus, errorThrown){  
				  			alert(errorThrown);  
				  		}  
				  	});	
				} else {
					notification("error", "Debe registrarse y entrar para poder realizar pedidos", "registrate");
				}
			}
		}
	});

	function refreshColegioSuscrito() {		
		var vv = jQuery(".colegio-suscritos .form-element[data-nivel=0]").val();		
		if ((vv != 2) && (vv != 9)) {
			// todos menos lengua castellana y dibujo tecnico

			contentPedido = "Tipo de Pedido: Colegio suscrito - Asignatura " + jQuery(".colegio-suscritos .form-element[data-nivel=0]").find("option:selected").text() + " <br/>";
			// tipo
			jQuery(".colegio-suscritos select").each(function () {
				if ((jQuery(this).attr("data-nivel") == "1") && (jQuery(this).attr("data-valor") == vv)) {
					contentPedido = contentPedido + "Tipo: " + jQuery(this).find("option:selected").text() + " <br/>";											
				}
			});
		} else
		if (vv == 2) {
			// Lengua castellana
			contentPedido = "Tipo de Pedido: Colegio suscrito - Asignatura " + jQuery(".colegio-suscritos .form-element[data-nivel=0]").find("option:selected").text() + " <br/>";
			// tipo
			jQuery(".colegio-suscritos select").each(function () {
				if ((jQuery(this).attr("data-nivel") == "1") && (jQuery(this).attr("data-valor") == "2")) {
					contentPedido = contentPedido + "Tipo: " + jQuery(this).find("option:selected").text() + " <br/>";											
				}

				if (jQuery(this).val() == 4) {
					// partes
					var pain = false; 
					contentPedido = contentPedido + "Partes a incluir: ";			
					jQuery(".colegio-suscritos .partesincluir:checked").each(function () {
						if (pain == true) { contentPedido = contentPedido + ", "; }
						contentPedido = contentPedido + jQuery(this).attr("data-valor");
						pain = true;
					});
					contentPedido = contentPedido + "<br/>";
				}
			});

		} else
		if (vv == 9) {
			// dibujo tecnico
			contentPedido = "Tipo de Pedido: Colegio suscrito - Asignatura " + jQuery(".colegio-suscritos .form-element[data-nivel=0]").find("option:selected").text() + " <br/>";
			// tipo
			jQuery(".colegio-suscritos select").each(function () {
				if ((jQuery(this).attr("data-nivel") == "1") && (jQuery(this).attr("data-valor") == "9")) {
					contentPedido = contentPedido + "Tipo: " + jQuery(this).find("option:selected").text() + " <br/>";

					if (jQuery(this).val() == 3) {
						// autocad
						jQuery(".colegio-suscritos select.base9").each(function () {
							contentPedido = contentPedido + "Tipo: " + jQuery(this).find("option:selected").text() + " <br/>";
						});
						contentPedido = contentPedido + "<br/>";
					}											
				}
			});
		}
	}
	jQuery(".colegio-suscritos .btn-colegiosuscrito").click(function (evt) {
		evt.preventDefault();	
		refreshColegioSuscrito();	
		var error = false;
		jQuery(".colegio-suscritos .form-element").each(function () {
			if (jQuery(this).css("display") == "block") {
				if (jQuery(this).is("input")) {
					if (((jQuery(this).val() == "")) || ((jQuery(this).val() == "0"))) {
						error = true;
					}
				} else 
				if (jQuery(this).is("select")) {
					if (jQuery(this).val() == "0") {
						error = true;
					} 
				} else {
				}				
			}
		});
		if (error) {
			notification("error", "Llene completamente el formulario. Tiene campos sin llenar.", "");
		} else
		{
			var r = confirm("Desea hacer el pedido?");
			if (r == true) {
				if (lg == 1) {
					notificationAjaxOn("Realizando pedido ...");
				    // Haciendo el Pedido
				    jQuery.ajax({  
				  		type: 'POST',  
				  		url: ajaxpath, 
				  		dataType: "json", 			  
				  		data: {  
				  			action: 'MyAjaxFunctions',  
				        	toaction: 'doPedido',
				          	contentPedido: contentPedido,
				          	Total: 0,
				          	csuscrito: 1,
				          	factors: vds,
				          	tipopedido: jQuery(".colegio-suscritos .form-element[data-nivel=0]").find("option:selected").text()
				  		},  
				  		success: function(data, textStatus, XMLHttpRequest){    				
				  				notificationAjaxOff();
				  				if (data["error"] == false) {		  					
				  					jQuery("div.points span strong").html("0 puntos");
				  					jQuery(".form-pedido .form-element[data-nivel=0]").val("0");
				  					jQuery("div#left-sidebar p#cantidad-puntos").html(data["Total"]);		  					
				  					refreshNivel(1);
				  					//jQuery("#colegio-form-post-id").val(data["post_id"]);
				  					//jQuery("#colegio-form").submit();
				  					var vv = parseInt(jQuery("#wrapper-content #content p.notification span").html());
			                		jQuery("#wrapper-content #content p.notification span").html(vv - 1);
				  					if (filesColegioSuscrito.length > 0) {
				  						for (var i = 0; i < filesColegioSuscrito.length; i++) {
					  						var fd = new FormData();    
											fd.append( 'action',  'MyAjaxFunctions');
											fd.append( 'toaction',  'uploadArchive');
											fd.append( 'post_id',  data["post_id"]);
											fd.append( 'fileselect',  filesColegioSuscrito[i]);
						  					jQuery.ajax({  
										  		type: 'POST',  
										  		url: ajaxpath, 
										  		dataType: "json", 			  
										  		data: fd, 
										  		processData: false, 
										  		contentType: false, 
										  		cache : false,
										  		success: function(data, textStatus, XMLHttpRequest){    				
										  			notification("success", "Pedido realizado satisfactoriamente!");
				                					jQuery("#colegiosuscrito-details").html("");			                					
										  		},  
										  		error: function(MLHttpRequest, textStatus, errorThrown){  
										  			alert(errorThrown);  
										  		}  
										  	});
						  				}
				  					} else {				  						
				  						notification("success", "Pedido realizado satisfactoriamente!");
				  					}	  								  					
				  				} else {
				  					if (data["url"] != "") {
				  						notification("error", data["message"], data["url"]);
				  					} else {
				  						notification("error", data["message"], "");
				  					}			  					
				  					if (data["suscritoshoy"] == true) {
				  						jQuery(".colegio-pedido-wrapper").fadeOut("fast");
				  						jQuery(".colegio-suscritos").fadeOut("fast");
				  						jQuery(".colegio-pedido-instantaneo").fadeIn("fast");
				  					}
				  				}	  						  				
				  		},  
				  		error: function(MLHttpRequest, textStatus, errorThrown){  
				  			alert(errorThrown);  
				  		}  
				  	});	
				} else {
					notification("error", "Debe registrarse y entrar para poder realizar pedidos", "registrate");
				}
			}
		}
	});

	// Managing colegio form to show
	jQuery("#radio-pedido-instantaneo").click(function () {
		jQuery(".colegio-suscritos").fadeOut("fast");
		jQuery(".colegio-pedido-instantaneo").fadeIn("fast");
	});
	jQuery("#radio-pedido-suscrito").click(function () {		
		jQuery(".colegio-pedido-instantaneo").fadeOut("fast");
		jQuery(".colegio-suscritos").fadeIn("fast");
	});

	// Controlling recharge form
	jQuery("#form-recargar .spinner1").click(function () {
		var totalPuntos = parseInt(jQuery("#form-recargar .puntos10").val() * 10);
		totalPuntos = totalPuntos + parseInt(jQuery("#form-recargar .puntos30").val() * 30);
		totalPuntos = totalPuntos + parseInt(jQuery("#form-recargar .puntos65").val() * 65);
		totalPuntos = totalPuntos + parseInt(jQuery("#form-recargar .puntos150").val() * 150);
		jQuery("#form-recargar .total-puntos").html(totalPuntos + " Puntos");
		var totalEuros = parseInt(jQuery("#form-recargar .puntos10").val() * 10);
		totalEuros = totalEuros + parseInt(jQuery("#form-recargar .puntos30").val() * 25);
		totalEuros = totalEuros + parseInt(jQuery("#form-recargar .puntos65").val() * 50);
		totalEuros = totalEuros + parseInt(jQuery("#form-recargar .puntos150").val() * 100);
		jQuery("#form-recargar .total-euros").html(totalEuros + " Euros");
	});
	jQuery("#form-recargar #form-recargar-submit").click(function (evt) {
		evt.preventDefault();
		var totalPuntos = parseInt(jQuery("#form-recargar .puntos10").val() * 10);
		totalPuntos = totalPuntos + parseInt(jQuery("#form-recargar .puntos30").val() * 30);
		totalPuntos = totalPuntos + parseInt(jQuery("#form-recargar .puntos65").val() * 65);
		totalPuntos = totalPuntos + parseInt(jQuery("#form-recargar .puntos150").val() * 150);
		var totalEuros = parseInt(jQuery("#form-recargar .puntos10").val() * 10);
		totalEuros = totalEuros + parseInt(jQuery("#form-recargar .puntos30").val() * 25);
		totalEuros = totalEuros + parseInt(jQuery("#form-recargar .puntos65").val() * 50);
		totalEuros = totalEuros + parseInt(jQuery("#form-recargar .puntos150").val() * 100);
		jQuery("#form-recargar #form-recargar-amount").val(totalEuros);
		jQuery("#form-recargar #form-recarga-itemnumber").val("Recarga de " + totalPuntos + " puntos");		
		jQuery("#form-recargar").submit();
	});
	// Controlling suscription form
	jQuery("#form-suscripcion #form-suscripcion-submit").click(function (evt) {
		evt.preventDefault();
		jQuery("#form-suscripcion #form-suscripcion-amount").val(jQuery("#form-suscripcion-dias").val() / 30 * v30s);
		jQuery("#form-suscripcion #form-suscripcion-itemnumber").val("Suscripcion por " + jQuery("#form-suscripcion-dias").val() + " dias");
		jQuery("#form-suscripcion").submit();
	});

	// Support for Drag and Drop
	// getElementById
	function $id(id) {
		return document.getElementById(id);
	}
	function clearFilesArea() {
		jQuery("#universidad-details").html("");
	}
	function clearFilesAreaColegio() {
		jQuery("#colegio-details").html("");
	}
	function clearFilesAreaColegioSuscrito() {
		jQuery("#colegiosuscrito-details").html("");
	}
	// output information
	function Output(msg) {
		//var m = $id("universidad-details");
		//m.innerHTML = msg;
		jQuery("#universidad-details").append(msg);
	}
	// output information
	function OutputColegio(msg) {
		//var m = $id("colegio-details");
		//m.innerHTML = msg;
		jQuery("#colegio-details").append(msg);
	}
	// output information
	function OutputColegioSuscrito(msg) {
		//var m = $id("colegiosuscrito-details");
		//m.innerHTML = msg;
		jQuery("#colegiosuscrito-details").append(msg);
	}


	// file drag hover
	function FileDragHover(e) {
		e.stopPropagation();
		e.preventDefault();
		e.target.className = (e.type == "dragover" ? "dnd-area-hover" : "dnd-area");
	}
	// file drag hover
	function FileDragHoverColegio(e) {
		e.stopPropagation();
		e.preventDefault();
		e.target.className = (e.type == "dragover" ? "dnd-area-hover" : "dnd-area");
	}
	// file drag hover
	function FileDragHoverColegioSuscrito(e) {
		e.stopPropagation();
		e.preventDefault();
		e.target.className = (e.type == "dragover" ? "dnd-area-hover" : "dnd-area");
	}

	// file selection
	function FileSelectHandler(e) {
		// cancel event and hover styling
		FileDragHover(e);

		// fetch FileList object
		files2Upload = e.target.files || e.dataTransfer.files;
		for (i = 0; i < files2Upload.length; i++) {
		    files[files.length] = files2Upload[i];
		} 

		refreshFilesArea();		
	}
	function refreshFilesArea() {
		// process all File objects
		clearFilesArea();
		for (var i = 0; i < files.length; i++) {
		    ParseFile(files[i], i);
		}
	}
	function deleteFilesArea(pos) {
		var tmp = new Array();
		for (var i = 0; i < files.length; i++) {
		    if (pos != i) {
		    	tmp[tmp.length] = files[i];		    	
		    }
		}
		files = tmp;
		refreshFilesArea();
	}
	jQuery("body").on("click", "#universidad-details div.pfiles a.acerrarDND", function (evt) {
		evt.preventDefault();
		deleteFilesArea(parseInt(jQuery(this).attr("data-pos")));
	});
	// file selection
	function FileSelectHandlerColegio(e) {
		// cancel event and hover styling
		FileDragHover(e);

		// fetch FileList object
		files2UploadColegio = e.target.files || e.dataTransfer.files;
		for (i = 0; i < files2UploadColegio.length; i++) {
		    filesColegio[filesColegio.length] = files2UploadColegio[i];
		} 

		refreshFilesAreaColegio();		
	}
	function refreshFilesAreaColegio() {
		// process all File objects
		clearFilesAreaColegio();
		for (var i = 0; i < filesColegio.length; i++) {
		    ParseFileColegio(filesColegio[i], i);
		}
	}
	function deleteFilesAreaColegio(pos) {
		var tmp = new Array();
		for (var i = 0; i < filesColegio.length; i++) {
		    if (pos != i) {
		    	tmp[tmp.length] = filesColegio[i];		    	
		    }
		}
		filesColegio = tmp;
		refreshFilesAreaColegio();
	}
	jQuery("body").on("click", "#colegio-details div.pfiles a.acerrarDND", function (evt) {
		evt.preventDefault();		
		deleteFilesAreaColegio(parseInt(jQuery(this).attr("data-pos")));
	});
	// file selection
	function FileSelectHandlerColegioSuscrito(e) {
		// cancel event and hover styling
		FileDragHover(e);

		// fetch FileList object
		files2UploadColegioSuscrito = e.target.files || e.dataTransfer.files;
		for (i = 0; i < files2UploadColegioSuscrito.length; i++) {
		    filesColegioSuscrito[filesColegioSuscrito.length] = files2UploadColegioSuscrito[i];
		} 

		refreshFilesAreaColegioSuscrito();		
	}
	function refreshFilesAreaColegioSuscrito() {
		// process all File objects
		clearFilesAreaColegioSuscrito();
		for (var i = 0; i < filesColegioSuscrito.length; i++) {
		    ParseFileColegioSuscrito(filesColegioSuscrito[i], i);
		}
	}
	function deleteFilesAreaColegioSuscrito(pos) {
		var tmp = new Array();
		for (var i = 0; i < filesColegioSuscrito.length; i++) {
		    if (pos != i) {
		    	tmp[tmp.length] = filesColegioSuscrito[i];		    	
		    }
		}
		filesColegioSuscrito = tmp;
		refreshFilesAreaColegioSuscrito();
	}
	jQuery("body").on("click", "#colegiosuscrito-details div.pfiles a.acerrarDND", function (evt) {
		evt.preventDefault();
		deleteFilesAreaColegioSuscrito(parseInt(jQuery(this).attr("data-pos")));
	});


	// output file information
	function ParseFile(file, i) {

		Output(
			"<div class='pfiles'>Archivo: <strong>" + file.name +
			//"</strong><br/> tipo: <strong>" + file.type +
			"</strong><br/> Tama&ntilde;o: <strong>" + Math.round(file.size / 1024 * 100) / 100  +
			"</strong> KB <a href='#' data-pos='" + i + "' class='acerrarDND'><span class='cerrarDND'>X</span></a></div>"
		);

	}
	// output file information
	function ParseFileColegio(file, i) {

		OutputColegio(
			"<div class='pfiles'>Archivo: <strong>" + file.name +
			//"</strong><br/> tipo: <strong>" + file.type +
			"</strong><br/> Tama&ntilde;o: <strong>" + Math.round(file.size / 1024 * 100) / 100  +
			"</strong> KB <a href='#' data-pos='" + i + "' class='acerrarDND'><span class='cerrarDND'>X</span></a></div>"
		);

	}
	// output file information
	function ParseFileColegioSuscrito(file, i) {

		OutputColegioSuscrito(
			"<div class='pfiles'>Archivo: <strong>" + file.name +
			//"</strong><br/> tipo: <strong>" + file.type +
			"</strong><br/> Tama&ntilde;o: <strong>" + Math.round(file.size / 1024 * 100) / 100  +
			"</strong> KB <a href='#' data-pos='" + i + "' class='acerrarDND'><span class='cerrarDND'>X</span></a></div>"
		);

	}
	// initialize
	function Init() {

		var fileselect = $id("universidad-file"),
			filedrag = $id("universidad-dnd-area");
		var fileselectcolegio = $id("colegio-file"),
			filedragcolegio = $id("colegio-dnd-area");
			var fileselectcolegiosuscrito = $id("colegiosuscrito-file"),
			filedragcolegiosuscrito = $id("colegiosuscrito-dnd-area");

		// file select
		//if (fileselect != null) fileselect.addEventListener("change", FileSelectHandler, false);

		// is XHR2 available?
		var xhr = new XMLHttpRequest();
		if (xhr.upload) {

			// file drop
			if (filedrag != null) filedrag.addEventListener("dragover", FileDragHover, false);
			if (filedrag != null) filedrag.addEventListener("dragleave", FileDragHover, false);
			if (filedrag != null) filedrag.addEventListener("drop", FileSelectHandler, false);

			if (filedragcolegio != null) filedragcolegio.addEventListener("dragover", FileDragHoverColegio, false);
			if (filedragcolegio != null) filedragcolegio.addEventListener("dragleave", FileDragHoverColegio, false);
			if (filedragcolegio != null) filedragcolegio.addEventListener("drop", FileSelectHandlerColegio, false);

			if (filedragcolegiosuscrito != null) filedragcolegiosuscrito.addEventListener("dragover", FileDragHoverColegioSuscrito, false);
			if (filedragcolegiosuscrito != null) filedragcolegiosuscrito.addEventListener("dragleave", FileDragHoverColegioSuscrito, false);
			if (filedragcolegiosuscrito != null) filedragcolegiosuscrito.addEventListener("drop", FileSelectHandlerColegioSuscrito, false);
			
		}

	}

	// call initialization file
	if (window.File && window.FileList && window.FileReader) {
		Init();
	}

});

function notification(tipo, msg, redirect) {

    jQuery.UIkit.notify({
        message : msg,
        status  : ( tipo == "error" ? 'danger' : 'success' ),
        timeout : 3000,
        pos     : 'top-center',
        onClose : function() {
            if( typeof redirect != 'undefined' ) {
                window.location = jQuery('html').attr('site-path') + redirect;
            }
        }
    });



	// var backgroundColor;
	// if (tipo == "error") {
	// 	backgroundColor = "red";
	// } else 
	// if (tipo = "success") {
	// 	backgroundColor = "green";
	// }	
	// jQuery("#notification .inner").css("background", backgroundColor).html(msg);
	// jQuery('#notification').animate({
	// 	'top': '35%',
	// 	opacity: 1
	// }, 1500, 'easeOutExpo', function() {
	// 	jQuery('#notification').animate({
	// 		'top': '-50%',
	// 		opacity: 0
	// 	}, 3000, 'easeInSine', function() {
			
 //            if( typeof redirect != 'undefined' ) {
 //                window.location = jQuery('html').attr('site-path') + redirect;
 //            }
            
	// 	});	
	// });	
}

function notificationAjaxOn(msg) {
	jQuery("#notificationAjax .inner").html(msg);
	jQuery('#notificationAjax').animate({
		'top': '35%',
		'opacity': ".8"
	}, 1500, 'easeOutExpo');	
}

function notificationAjaxOff() {
	jQuery('#notificationAjax').animate({
		'top': '-50%',
		'opacity': "0"
	}, 1000, 'easeInSine', function() {
			
	});	
}

function refreshNivel(nivel) {
	contentPedido = "";
	jQuery(".form-pedido .form-element[data-nivel=" + nivel + "]").fadeOut("fast", function () {
		if (jQuery(this).is("select")) {
            jQuery(this).val("0");  
		} else 
		if (jQuery(this).is("input.smartspinner")) {
            jQuery(this).val("0");  
		} else
		if (jQuery(this).is("textarea")) {
            jQuery(this).val("");  
		} else
		if (jQuery(this).is("input")) {
            jQuery(this).val("");  
		}
	});
	jQuery(".form-pedido .action[data-nivel=" + nivel + "]").val("0");
	if (nivel < 4) {
		refreshNivel(nivel + 1);
	}	
}

function getFactorEntrega(tentrega) {

	var factorEntrega = 1;
	if (tentrega == 0) {
		factorEntrega = 1;
	} else 
	if (tentrega / 24 == 1) {
		factorEntrega = 6;
	} else
	if (tentrega / 24 == 2) {
		factorEntrega = 3;
	} else 
	if (tentrega / 24 == 3) {
		factorEntrega = 2;
	} else 
	if (tentrega / 24 == 4) {
		factorEntrega = 1.5;
	} else 
	if (tentrega / 24 == 5) {
		factorEntrega = 1.2;
	} else 
	if (tentrega / 24 == 6) {
		factorEntrega = 1;
	} else 
	if ((tentrega / 24 >= 7) && (tentrega / 24 <= 8)) {
		factorEntrega = 0.97;
	} else 
	if ((tentrega / 24 >= 9) && (tentrega / 24 <= 11)) {
		factorEntrega = 0.95;
	} else 
	if ((tentrega / 24 >= 12) && (tentrega / 24 <= 15)) {
		factorEntrega = 0.93;
	} else 
	if (tentrega / 24 >= 16) {
		factorEntrega = 0.9;
	}
	return factorEntrega;
}

function getFactorEntregaReducido(tentrega) {
	var factorEntrega;
	if (tentrega == 120) {
		factorEntrega = 3;
	} else 
	if (tentrega == 240) {
		factorEntrega = 2;
	} else
	if (tentrega == 360) {
		factorEntrega = 1;
	} else 
	if (tentrega == 480) {
		factorEntrega = 0.95;
	} else 
	if (tentrega == 500) {
		factorEntrega = 0.95;
	} else {
		factorEntrega = 0;
	}
	return factorEntrega;
}

function getFactorEntregaColegio(hh) {
	var factorEntrega = 0;
	if (hh == 2) {
		factorEntrega = 4;
	} else
	if (hh == 5) {
		factorEntrega = 2.5;
	} else
	if (hh == 8) {
		factorEntrega = 1.5;
	} else
	if (hh == 24) {
		factorEntrega = 1;
	} else
	if (hh == 48) {
		factorEntrega = 0.75;
	} else
	if (hh == 72) {
		factorEntrega = 0.75;
	} else
	if (hh == 96) {
		factorEntrega = 0.5;
	} else
	if (hh == 120) {
		factorEntrega = 0.5;
	} else
	if (hh == 144) {
		factorEntrega = 0.5;
	} else
	if (hh == 150) {
		factorEntrega = 0.5;
	} else
	if (hh == 2) {
		factorEntrega = 0.5;
	}
	return factorEntrega;
}