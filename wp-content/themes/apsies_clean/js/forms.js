(function($,window, root){
	'use strict';
	
	var base = $('html').attr('base');

	

	$(document).ready( function( e) {
		var ctx = $('.form_puntos'),
			ctxs = $('.replace_form.sus'),
			current_step = 1;

		if( ctx.length > 0 ) {
			$('.action_recarga').on('click', function(e){
				e.preventDefault();
				var total_steps = $('.steps .step_container').length,
						next    = current_step + 1,
						prev    = current_step - 1,
						_check  = false;



				if( $( this).hasClass('next' ) ) {
					if( $( this ).hasClass('check') ) {
						$.ajax(
		        		base + '/ajax.php?controller=user&action=is_login',
		        		{
		        			type : 'POST',
		        			async : false,
		        			success: function( res ) {
		        				console.log( res);
		        				//if( res.is_login == false ) {
		        					_check = true;

		        				//}
		        			}
		        		})
					}


					if( _check == false  ) {
						$('.action_recarga.prev').removeClass('uk-hidden');
						$('.steps .step_1').addClass('uk-hidden');
						$('.steps .step_2').removeClass('uk-hidden');	
						$(this).addClass('uk-hidden');
						$('.action_recarga.submit').removeClass('uk-hidden')
					} else {
						$.ajax(
		        		base + '/ajax.php?controller=user&action=pending_action',
		        		{
		        			type : 'POST',
		        			data : { plan : $('input[name="suscripcion[type]"]:radio:checked').val() }
		        		});
					}
					
				} else if( $( this).hasClass('prev' ) ) {
					$('.action_recarga.next').removeClass('uk-hidden');
					$('.steps .step_2').addClass('uk-hidden');
					$('.steps .step_1').removeClass('uk-hidden');	
					$('.action_recarga.submit').addClass('uk-hidden')
					$(this).addClass('uk-hidden');
				} else if( $( this).hasClass('submit') ) {
					ctx[0].submit()
				}


			})
		}

		if( ctxs.length > 0 ) {
			var form_settings = {
				settings : {
					'1' : ['resolucion', 'demostracion'],
					'2' : ['resolucion', 'redaccion', 'comentario', 'critica', 'duda'],
					'3' : ['resolucion', 'demostracion'],
					'4' : ['resolucion', 'demostracion'],
					'5' : ['redaccion', 'comentario', 'duda'],
					'6' : ['resolucion', 'demostracion'],
					'7' : ['resolucion', 'demostracion'],
					'8' : ['resolucion', 'redaccion', 'comentario', 'duda'],
					'9' : ['resolucion', 'demostracion', 'autocad'],
					'10' : ['redaccion', 'comentario', 'duda'],
					'11' : ['resolucion', 'comentario_arte', 'duda'],
					'12' : ['resolucion', 'duda'],
					'13' : ['resolucion', 'comentario', 'duda'],
					'14' : ['resolucion', 'demostracion', 'duda'],
					'15' : ['resolucion', 'comentario', 'duda'],
					'16' : ['resolucion', 'redaccion'],
					'17' : ['resolucion', 'duda']

				},
				resolucion : {
					name : 'Resolución de ejercicios',
					msg  : '¡Recuerda que puedes mandar hasta 5 ejercicios diferentes dentro del mismo pedido!',
					form : '<div class="uk-width-1-1 uk-width-medium-1-1"><div class="input-group"><label for="suscrito_enunciado">Enunciados</label><textarea id="suscrito_enunciado" data-required rows="10" name="submit_pedido[suscrito][enunciado]" placeholder="Escribe los enunciados de los ejercicios a resolver, o házles una foto y arrástrala al formulario"></textarea></div></div>'
				},
				demostracion : {
					name : 'Demostración',
					msg : 'Recuerda que la demostración contendrá hasta 150 palabras máximo. Seguro que es suficiente!',
					form : '<div class="uk-width-1-1 uk-width-medium-1-1"><div class="input-group"><label for="suscrito_enunciado">Enunciados</label><textarea id="suscrito_enunciado" data-required rows="10" name="submit_pedido[suscrito][enunciado]" placeholder="Escribe los enunciados de los ejercicios a resolver, o házles una foto y arrástrala al formulario"></textarea></div></div>'
				},
				redaccion : {
					name : 'Redacción sobre un tema',
					form : '<div class="uk-width-1-1 uk-width-medium-1-1"><div class="input-group"><label for="suscrito_enunciado">Enunciados</label><textarea id="suscrito_enunciado" data-required rows="10" name="submit_pedido[suscrito][enunciado]" placeholder="Escribe los enunciados de los ejercicios a resolver, o házles una foto y arrástrala al formulario"></textarea></div></div>'
				},

				comentario : {
					name : 'Comentario de texto',
					msg : 'Recuerda que los comentarios de texto/artísticos contendrán hasta 150 palabras como máximo.',
					form: '<div class="uk-width-1-1 uk-width-medium-1-1"><div class="input-group"><label for="suscrito_enunciado">Enunciados</label><textarea id="suscrito_enunciado" data-required rows="10" name="submit_pedido[suscrito][enunciado]" placeholder="Escribe los enunciados de los ejercicios a resolver, o házles una foto y arrástrala al formulario"></textarea></div></div>'
				},

				critica : {
					name : 'Crítica obra literaria',
					form : '<div class="uk-grid uk-grid-small uk-margin-top"><div class="uk-width-1-1 uk-width-medium-1-2"><p><label><input type="checkbox" name="submit_pedido[suscrito][critica][incluye][]" value="Argumento" checked="checked">Argumento</label></p><p><label><input type="checkbox" name="submit_pedido[suscrito][critica][incluye][]" value="Personajes principales" checked="checked">Personajes principales</label></p><p><label><input type="checkbox" name="submit_pedido[suscrito][critica][incluye][]" value="Personajes secundarios">Personajes secundarios</label></p></div><div class="uk-width-1-1 uk-width-medium-1-2"><p><label><input type="checkbox" name="submit_pedido[suscrito][critica][incluye][]" value="Estilo">Estilo</label></p><p><label><input type="checkbox" name="submit_pedido[suscrito][critica][incluye][]" value="Estructura" checked="checked">Estructura</label></p><p><label><input type="checkbox" name="submit_pedido[suscrito][critica][incluye][]" value="Conclusión" checked="checked">Conclusión</label></p></div></div><div class="uk-width-1-1 uk-width-medium-1-1"><div class="input-group"><label for="suscrito_enunciado">Enunciados</label><textarea id="suscrito_enunciado" data-required rows="10" name="submit_pedido[suscrito][enunciado]" placeholder="Escribe los enunciados de los ejercicios a resolver, o házles una foto y arrástrala al formulario"></textarea></div></div>'
				},

				duda : {
					msg : 'Te resolveremos cualquier duda o pregunta que tengas o te hayan mandado acerca de cualquier tema. Recuerda también, que no ocupará más de 150 palabras!',
					name: 'Dudas / investigación sobre un tema',
					form: '<div class="uk-width-1-1 uk-width-medium-1-1"><div class="input-group"><label for="suscrito_enunciado">Enunciados</label><textarea id="suscrito_enunciado" data-required rows="10" name="submit_pedido[suscrito][enunciado]" placeholder="Escribe los enunciados de los ejercicios a resolver, o házles una foto y arrástrala al formulario"></textarea></div></div>'
				},
				autocad : {
					name : 'AutoCAD',
					form : '<div class="uk-grid uk-grid-small"><div class="uk-width-1-1"><div class="input-group"><label for="eso_autocad_tipo">Tipo de tarea</label><select type="text" id="eso_autocad_tipo" name="submit_pedido[suscrito][autocad][tipo]"><option>3D a partir de vistas</option><option>vistas a partir de 3D</option><option>Ejercicios</option></select></div></div></div><div class="uk-width-1-1 uk-width-medium-1-1"><div class="input-group"><label for="suscrito_enunciado">Enunciados</label><textarea id="suscrito_enunciado" data-required rows="10" name="submit_pedido[suscrito][enunciado]" placeholder="Escribe los enunciados de los ejercicios a resolver, o házles una foto y arrástrala al formulario"></textarea></div></div>'
				},
				comentario_arte : {
					name : 'Comentario Artístico',
					msg : 'Recuerda que los comentarios de texto/artísticos contendrán hasta 150 palabras como máximo.',
					form : '<div class="uk-width-1-1 uk-width-medium-1-1"><div class="input-group"><label for="suscrito_enunciado">Enunciados</label><textarea id="suscrito_enunciado" data-required rows="10" name="submit_pedido[suscrito][enunciado]" placeholder="Escribe los enunciados de los ejercicios a resolver, o házles una foto y arrástrala al formulario"></textarea></div></div>'
				}

			}

			$('[name="submit_pedido[suscrito][asignatura]"]').on('change', function(){
				$('.place_form').html('');
				$('.sus_sub_form').html('');
				var idx = $(this).val(),
					asignatura_name = $('[name="submit_pedido[suscrito][asignatura]"] option:selected').text();
				if( idx > 0 && idx < 18 ) {
					var options = form_settings.settings[idx],
						select  = $('<select name="submit_pedido[suscrito][tipo]" id="suscrito_asignatura_pedido" class="suscrito asignatura"></select>');
					
					var html = '<div class="uk-grid uk-grid-small">'
					+ '<div class="uk-width-1-1 uk-width-medium-1-2">'
					+		'<div class="input-group">'
					+			'<label for="suscrito_asignatura_pedido">Tipo</label>'		
					+		'</div>'
					+	'</div>'
					+ '</div>';

					$('.place_form').html( html );
					$('.place_form').append('<input type="hidden" name="submit_pedido[suscrito][asignatura_nombre]" value="' + asignatura_name + '" />');


					
					select.append('<option value="0">-- Selecciona una opción --</option>'); 
					$.each( options, function(i, v) {
						var data = form_settings[v],
							selected = idx == 1 && v == 'resolucion' ? ' selected ' : ''; 
						select.append('<option value="' + v + '" ' + selected +'> ' + data.name + '</option>'); 
					})

					select.on('change', function(){
						
						$('.sus_sub_form').html('');

						if( $(this).val() != "0" ) {
							console.log(  form_settings[$(this).val()] );
							var sub_data = form_settings[$(this).val()];

							if( typeof sub_data.form != 'undefined' ) {
								$('.sus_sub_form').append( sub_data.form );
							}

							if( typeof sub_data.msg != 'undefined' ) {
								$('.sus_sub_form').append( $('<p class="uk-margin-top" />').text(sub_data.msg) );
							}

							
							$('.sus_sub_form').append('<input type="hidden" name="submit_pedido[suscrito][tipo_nombre]" value="' + sub_data.name + '" />');
						}
					}).trigger('change');

					$('.place_form .input-group').append( select );

				}
			});
			

			$('[name="submit_pedido[suscrito][asignatura]"]').trigger('change');

			
			$("input[name='submit_pedido[pedido_suscrito]']:radio").on('change', function(e){
				var _val = parseInt( $(this).val() );
				
				if( _val == 0 ) {
					$('.replace_form').removeClass('uk-hidden');
					$('.replace_form.sus').addClass('uk-hidden');
					
					$('.puntaje *:not(button)').css('visibility', 'visible');
				} else {
					console.log ( _val );
					$('.replace_form').addClass('uk-hidden');
					$('.replace_form.sus').removeClass('uk-hidden');
					$('.puntaje *:not(button)').css('visibility', 'hidden');
				}
			})



		}
		
		$(document).on('refresh_point', function(e){
			if( ctx.length > 0 ) {
				var total = { points: 0, price: 0 },
					current_point = $('.info_point .puntos strong'),
					current_price = $('.info_point .precio strong'),
					value_point   = parseInt( current_point.text() ),
					value_price   = parseInt( current_price.text() );

				$('[data-price]').each(function(i){
					var inp = $( this ),
					
					value_inp     = parseInt( inp.attr('data-price') ),
					cant_points	  = parseInt( inp.attr('data-cant') ),
					cant          = ( inp.val() == '' ) ? 0 : parseInt( inp.val() );
					
					total.points += cant_points * cant;
					total.price  += value_inp * cant  
					
				})	


				current_point.text(  total.points );
				current_price.text(  total.price );


			}
		});
		
		$(document).on('submit', 'form[action="?step=confirmacion"]', function(e){
			if( $(this).hasClass('missing_points') ) {
				var form = $(this)[0];
				jQuery.UIkit.notify({
			        message : 'No tiene suficientes puntos. Recargue su saldo ahora',
			        status  : 'danger',
			        timeout : 1000,
			        pos     : 'top-center',
			        onClose : function() {
			           form.submit();
			        }
			    });
				e.preventDefault();
				return false;
			}
		})


		$(document).on('click', 'button.baja', function(e){
			e.preventDefault();
			if( confirm('Seguro que desea eliminar la cuenta ?' ) ) {
				$.ajax(
        		base + '/ajax.php?controller=profile&action=deleteAccount',
        		{
        			type : 'POST'
        		})
			}
		})

		$(document).on('submit', 'form[name="registerform"]', function(e){
			e.preventDefault();
			var acepta = $('#confirma_edad', this),
				user   = $('#user_login', this)
			if( ! acepta.prop('checked') ) {
				notification("error", "Debe ser mayor de 14 años y aceptar las Condiciones Cenerales de Contratación");
				return;
			}

			$(this)[0].submit();

		})

	})



})(jQuery, window, this);

