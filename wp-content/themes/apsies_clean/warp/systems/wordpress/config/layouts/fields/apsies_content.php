<?php
require_once(ABSPATH . "wp-admin" . '/includes/image.php');
require_once(ABSPATH . "wp-admin" . '/includes/file.php');
require_once(ABSPATH . "wp-admin" . '/includes/media.php');

$attachment_id = media_handle_upload('file-upload', 'module_1');

?>

<div id="config-apsies-content">
	
	<ul class="uk-tab" data-uk-tab="{connect:'#tab-content-contents'}">
		<li><a href="">Registro</a></li>
	    <li><a href="">Home - Suscripción</a></li>
	    <li><a href="">Home - modulos</a></li>

	</ul>

	<ul id="tab-content-contents" class="uk-switcher uk-margin">
		<li>
			<?php wp_editor( $this['config']['contents']['register_left'], 'content_register_left', $settings = array( 'textarea_name' => 'contents[register_left]', 'wpautop' => false) ); ?>
		</li>
		<li>
			<?php wp_editor( $this['config']['contents']['home_suscripcion'], 'content_home_suscripcion', $settings = array( 'textarea_name' => 'contents[home_suscripcion]', 'wpautop' => false) ); ?>
		</li>
		<li>
			<h2>Modulo 1</h2>
			<div class="upload">
				<a class="uk-button uploader-wp" href="" rel="module_1">Subir imagen</a>
				<input type="hidden" name="contents[module_1][image]" value="<?php echo $this['config']['contents']['module_1']['image']?>" />
			</div>

			<?php wp_editor( $this['config']['contents']['module_1']['text'], 'content_module1', $settings = array( 'textarea_name' => 'contents[module_1][text]', 'wpautop' => false) ); ?>
			
			<hr class="uk-article-divider">
			
			<h2>Modulo 2</h2>
			<div class="upload">
				<a class="uk-button uploader-wp" href="" rel="module_2">Subir imagen</a>
				<input type="hidden" name="contents[module_2][image]" value="<?php echo $this['config']['contents']['module_2']['image']?>" />
			</div>

			<?php wp_editor( $this['config']['contents']['module_2']['text'], 'content_module2', $settings = array( 'textarea_name' => 'contents[module_2][text]', 'wpautop' => false) ); ?>
			
			<hr class="uk-article-divider">
			
			<h2>Modulo 3</h2>

			<div class="upload">
				<a class="uk-button uploader-wp" href="" rel="module_3">Subir imagen</a>
				<input type="hidden" name="contents[module_3][image]" value="<?php echo $this['config']['contents']['module_3']['image']?>" />
			</div>
			<?php wp_editor( $this['config']['contents']['module_3']['text'], 'content_module3', $settings = array( 'textarea_name' => 'contents[module_3][text]', 'wpautop' => false) ); ?>
			<hr class="uk-article-divider">
			
			<h2>Modulo 4</h2>

			<div class="upload">
				<a class="uk-button uploader-wp" href="" rel="module_4">Subir imagen</a>
				<input type="hidden" name="contents[module_4][image]" value="<?php echo $this['config']['contents']['module_4']['image']?>" />
			</div>
			<?php wp_editor( $this['config']['contents']['module_4']['text'], 'content_module4', $settings = array( 'textarea_name' => 'contents[module_4][text]', 'wpautop' => false) ); ?>
			
		</li>
		<li>
			<h2>Modulo 1</h2>
			<?php wp_editor( $this['config']['contents']['module_desc_1'], 'content_module_desc1', $settings = array( 'textarea_name' => 'contents[module_desc_1]', 'wpautop' => false) ); ?>
			
			<hr class="uk-article-divider">
			
			<h2>Modulo 2</h2>
			<?php wp_editor( $this['config']['contents']['module_desc_2'], 'content_module_desc2', $settings = array( 'textarea_name' => 'contents[module_desc_2]', 'wpautop' => false) ); ?>
			
			<hr class="uk-article-divider">
			
			<h2>Modulo 3</h2>
			<?php wp_editor( $this['config']['contents']['module_desc_3'], 'content_module_desc3', $settings = array( 'textarea_name' => 'contents[module_desc_3]', 'wpautop' => false) ); ?>
			
		</li>
	</ul>

</div>



 <pre style="display:none">
 <?php
 	print_r($this['config']);
 ?>
 </pre>