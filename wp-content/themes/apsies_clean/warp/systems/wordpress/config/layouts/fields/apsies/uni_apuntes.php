<h2>Valores por defecto</h2>
<div class="uk-form uk-form-horizontal">
	<div class="uk-grid">

        <div class="uk-form-row">
            <label class="uk-form-label" for="form-h-it">Tipo</label>

            <div class="uk-form-controls">
                <select name="uni[apuntes][tipo]">
                    <option value="subir_web" <?php echo ( ( $this['config']['uni']['apuntes']['tipo'] == "subir_web" ) ? ' selected="selected" ' : '' ) ?>> Subir a la web </option>
                    <option value="paqueteria" <?php echo ( ( $this['config']['uni']['apuntes']['tipo'] == "paqueteria" ) ? ' selected="selected" ' : '' ) ?>> Paquetería </option>
                    
                </select>
            </div>
        </div>
        
        <div class="uk-width-1-2">
	    	<div class="uk-form-row">
	            <label class="uk-form-label" for="form-h-it">Tiempos de entrega</label>

	            <div class="uk-form-controls">
	                <?php echo $this['forms']->tiempos_entrega($this['config']['uni']['apuntes']['tiempo_entrega'],'uni[apuntes][tiempo_entrega]', 'uni'); ?>
	            </div>
	        </div>
	    </div>
	</div>
</div>


