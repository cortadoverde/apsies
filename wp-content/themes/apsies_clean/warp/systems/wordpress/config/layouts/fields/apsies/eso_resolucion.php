<h2>Valores por defecto</h2>
<div class="uk-form uk-form-horizontal">
	<div class="uk-grid">
	    <div class="uk-width-1-2">
	    	<div class="uk-form-row">
	            <label class="uk-form-label" for="form-h-it">Asignatura</label>

	            <div class="uk-form-controls">
	            	<select name="eso[resolucion_ejercicios][asignatura]">
	            		<option value="0">Seleccione una asignatura</option>
	            		<?php foreach( $this['forms']->asignaturas as $name => $data ) :
	            			if( isset( $this['config']['eso']['resolucion_ejercicios'][$name]['active'] ) ) :
	            					$selected = ( $this['config']['eso']['resolucion_ejercicios']['asignatura'] == $data['id'] ) ? ' selected="selected" ' : '';
	            				?>
	            				<option value="<?php echo $data['id']?>" <?php echo $selected ?>> <?php echo $data['name'] ?></option>
	            			<?php 
	            			endif;
	            		endforeach;?> 
	            	</select>
	            </div>
	        </div>		   	
	    </div>

	    <div class="uk-width-1-2">
	    	<div class="uk-form-row">
	            <label class="uk-form-label" for="form-h-it">Tiempos de entrega</label>

	            <div class="uk-form-controls">
	                <?php echo $this['forms']->tiempos_entrega($this['config']['eso']['resolucion_ejercicios']['tiempo_entrega'],'eso[resolucion_ejercicios][tiempo_entrega]'); ?>
	            </div>
	        </div>
	    </div>
	</div>
</div>

<table class="uk-table">
   
    <thead>
        <tr>
            <th>Asignatura</th>
            <th>Precio por pedido</th>
            <th>Activo</th>
        </tr>
    </thead>

    <tbody>
    	<?php foreach ($this['forms']->asignaturas as $name => $data): 
    			$checked = ( isset( $this['config']['eso']['resolucion_ejercicios'][$name]['active'] ) && $this['config']['eso']['resolucion_ejercicios'][$name]['active'] == 1 ) ? ' checked="checked" ' : ''; 
    			$value   = ( isset( $this['config']['eso']['resolucion_ejercicios'][$name]['num'] ) ) ? $this['config']['eso']['resolucion_ejercicios'][$name]['num'] : 0;
    	?>
    		<tr>
    			<td><?php echo $data['name']?></td>
    			<td><input type="text" name="eso[resolucion_ejercicios][<?php echo $name?>][num]" placeholder="" value="<?php echo $value ?>"></td>
    			<td><input type="checkbox" name="eso[resolucion_ejercicios][<?php echo $name?>][active]" value="1" <?php echo $checked ?> ></td>
    		</tr>
    	<?php endforeach ?>
            
    </tbody>
</table>