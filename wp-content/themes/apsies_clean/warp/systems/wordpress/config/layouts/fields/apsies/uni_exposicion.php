<h2>Valores por defecto</h2>
<div class="uk-form uk-form-horizontal">
	<div class="uk-grid">
        <div class="uk-width-1-2">
            <div class="uk-form-row">
                <label class="uk-form-label" for="form-h-it">Tipo</label>

                <div class="uk-form-controls">
                    <select name="uni[exposicion][tipo]">
                        <option value="trabajo_escrito" <?php echo ( ( $this['config']['uni']['exposicion']['tipo'] == "trabajo_escrito" ) ? ' selected="selected" ' : '' ) ?>> A partir de un trabajo escrito </option>
                        <option value="de_0" <?php echo ( ( $this['config']['uni']['exposicion']['tipo'] == "de_0" ) ? ' selected="selected" ' : '' ) ?>> Partiendo de 0 </option>
                        
                    </select>
                </div>
            </div>
        </div>

        <div class="uk-width-1-2">
	    	<div class="uk-form-row">
	            <label class="uk-form-label" for="form-h-it">Tiempos de entrega</label>

	            <div class="uk-form-controls">
	                <?php echo $this['forms']->tiempos_entrega($this['config']['uni']['exposicion']['tiempo_entrega'],'uni[exposicion][tiempo_entrega]', 'uni'); ?>
	            </div>
	        </div>
	    </div>
    </div>
    <div class="uk-grid">
        <div class="uk-width-1-2">
            <div class="uk-form-row">
                <label class="uk-form-label" for="form-h-it">Parte hablada</label>

                <div class="uk-form-controls">
                    <select name="uni[exposicion][parte_hablada]">
                        <option value="si" <?php echo ( ( $this['config']['uni']['exposicion']['tipo'] == "si" ) ? ' selected="selected" ' : '' ) ?>>Si</option>
                        <option value="no" <?php echo ( ( $this['config']['uni']['exposicion']['tipo'] == "no" ) ? ' selected="selected" ' : '' ) ?>> No </option>
                        
                    </select>
                </div>
            </div>
        </div>

        <div class="uk-width-1-2">
            <div class="uk-form-row">
                <label class="uk-form-label" for="form-h-it">Barra predominante</label>

                <div class="uk-form-controls">
                    <select name="uni[exposicion][predomina]">
                        <option value="texto" <?php echo ( ( $this['config']['uni']['exposicion']['tipo'] == "texto" ) ? ' selected="selected" ' : '' ) ?>>Texto</option>
                        <option value="balanceado" <?php echo ( ( $this['config']['uni']['exposicion']['tipo'] == "balanceado" ) ? ' selected="selected" ' : '' ) ?>> Balanceado </option>
                        <option value="imagenes" <?php echo ( ( $this['config']['uni']['exposicion']['tipo'] == "imagenes" ) ? ' selected="selected" ' : '' ) ?>> Imágenes </option>
                        
                    </select>
                </div>
            </div>
        </div>
	</div>
</div>


