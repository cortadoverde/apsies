(function($, window, root ){
	'use strict';

	var file_frame = [];

	$(document).on('click', '.uploader-wp', function( podcast ) {
		podcast.preventDefault();
		var id 		= $(this).attr('rel'),
			target 	= $(this).next();

		if( typeof file_frame[id] !== 'undefined') {
			file_frame[id].open();
			return;
		}

		file_frame[id] = wp.media.frames.file_frame = wp.media ({
			title:  $( this ).data( 'uploader_title' ),
			button: {
				text : $( this ).data( 'uploader_button_text' )
			},
			multiple : false
		});

		file_frame[id].on('select', function(){
			var attachment = file_frame[id].state().get('selection').first().toJSON();
			var url = attachment.url;
			target.val( url );
		});

		file_frame[id].open();
	});

	$(document).ready(function(){
		
	})

})(jQuery, window, this);


// Uploading files
