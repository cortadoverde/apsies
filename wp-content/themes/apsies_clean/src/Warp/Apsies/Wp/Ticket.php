<?php
/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies\Wp;

class Ticket extends Post 
{
    public static $type = 'ticket';


    public function __construct( \Wp_Post $post ) {
        
        $this->model  = $post;
        $this->get_meta();
        parent::import_custom( $this->model->ID);

        if( is_numeric( $this->model->pedidoticket  ) ) {
          $this->Pedido = Pedido::get( $this->model->pedidoticket );
        }
    }

    public static function create( $post_form )
    {
        global $warp;

        $post_status = 'publish';

        $post = array(
            'post_title' => $post_form['title'],
            'post_content' => $post_form['content'],
            'post_status' => $post_status,
            'post_date' => date('Y-m-d H:i:s'),
            'post_type' => self::$type
        );

        $post_id = wp_insert_post( $post );
        if( isset( $post_form['user_id'] ) ) 
            add_post_meta($post_id, 'to_user', $post_form['user_id'] );

        if( isset( $post_form['pedido_id'] ) ) 
            add_post_meta($post_id, 'pedidoticket', $post_form['pedido_id'] );

        add_post_meta($post_id, 'unread', 1 );

        $post = get_post( $post_id );
        
        return new self( $post );
    }

    public static function get( $id )
    {
        $post = get_post( $id );
        return new self( $post );
    }


    public function get_meta() {
        $customs = get_post_custom( $this->model->ID );

        if (!is_array($customs) || empty($customs)) {
            return $this;
        }
        foreach ($customs as $key => $value) {
            if (is_array($value) && count($value) == 1 && isset($value[0])) {
                $value = $value[0];
            }
            $customs[$key] = maybe_unserialize($value);
            
            $this->model->{$key} = maybe_unserialize($customs[$key]);
            
        }


        $this->model->custom = $customs;
        return $this;
    }

  

    public function update( $meta, $value )
    {
        update_post_meta( $this->model->ID, $meta, $value );
        $this->get_meta();
        return $this;
    }

    public function add_meta( $meta, $value )
    {
        add_post_meta( $this->model->ID, $meta, $value );
        $this->get_meta();
        return $this;
    }

  

}