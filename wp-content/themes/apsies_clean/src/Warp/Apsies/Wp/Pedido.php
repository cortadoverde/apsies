<?php
/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies\Wp;

class Pedido extends Post 
{
    public static $type = 'pedido';

    public $session_id = 'paymentId';

    public static $indexs = array(
        'uni' => array('', 'trabajos', 'autocad', 'apuntes', 'powerpoint', 'exposicion'),
        'eso' => array('', 'resolucion_ejercicio', 'redaccion_tema', 'comentario', 'comentario_artistico',
                            'critica_literaria', 'autocad', 'dudas', 'trabajo' )
    );

    public static $titulos = array(
        'uni' => array('', 'trabajos', 'autocad', 'apuntes', 'powerpoint', 'exposiciones'),
        'eso' => array('', 'Resolucion de Ejercicio', 'Redacción de un tema', 'comentario de texto', 'comentario artístico',
                            'Crítica literaria', 'AutoCAD', 'Dudas/Investigacion sobre un tema', 'Trabajos' )
    );

    public function __construct( \Wp_Post $post ) {
        
        $this->model  = $post;
        $this->get_meta();
    }

    public static function get( $id )
    {
        $post = get_post( $id );
        return new self( $post );
    }

    public static function create( $post_form ) {

        global $warp;

        $post_status = 'draft';
        
        $data = self::parse_pedido($post_form);

        $title = ( $data['tipo'] == 'suscrito' ) ? ' [suscrito] '. $data['form']['asignatura'] : self::$titulos[$data['level']][$data['tipo']];

        $content = " <h1> Detalles del pedido </h1>";
        $content .= "<h2>". $title . '</h2>';
        foreach( $data['form'] AS $key => $value ) {
            $content .= $key . ': ';
            if( is_array( $value ) ) {
                $content .= implode(', ', $value);
            } else {

                $content .= ( $key == 'observaciones') ? '<blockquote>' . nl2br( $value ) .'</blockquote>' : nl2br( $value );
            }

            $content .= "\n<br/>";
        }


        $post = array(
            'post_title' => $title,
            'post_content' => $content,
            'post_status' => $post_status,
            'post_date' => date('Y-m-d H:i:s'),
            'post_type' => self::$type
        );

        $post_id = wp_insert_post( $post );

        add_post_meta($post_id, 'puntos', $data['puntos'] );
        add_post_meta($post_id, 'adjuntos', $data['adjuntos'] );

        add_post_meta($post_id, 'pedido_suscrito', ( isset( $data['data']['pedido_suscrito'] ) ? $data['data']['pedido_suscrito'] : "0" ) );
        add_post_meta($post_id, '_data', serialize( $data ) );

        $post = get_post( $post_id );
        
        return new self( $post );;
    }

    public static function parse_pedido( $post )
    {
        if( ! isset( $post['submit_pedido'] ) ) return false;
        
        $data  = $post['submit_pedido'];
        // si el pedido es suscripto
        $level = $data['level'];
        
        if( isset( $data['pedido_suscrito'] ) &&  $data['pedido_suscrito'] == 1 && $level == 'eso' ) {
            $form = array(
                    'tipo' => $data['suscrito']['asignatura_nombre'],
                    'enunciado'  => $data['suscrito']['enunciado']
                );
            if( isset( $data['suscrito'][$data['suscrito']['tipo'] ] ) ) {
                $form = array_merge( $form, $data['suscrito'][$data['suscrito']['tipo'] ] );
            }

            $tipo  = 'suscrito';
            $idx   = '0'; 
        } else {
            $tipo  = $data[$level]['tipo'];
            $idx   = self::$indexs[$level][$tipo];
            $form  = $data[$level][$idx];
        }

        

        return array(
            'form' => $form,
            'idx'  => $idx,
            'level' => $level,
            'tipo'  => $tipo,
            'puntos' => $data['pts'],
            'adjuntos' => ( isset( $data['adjuntos'] ) ? $data['adjuntos'] : array() ) ,
            'data'   => $data
        );
    }

    public static function calculatePoints( $post )
    {
        $data = self::parse_pedido( $post );

        return self::formula( $data['level'], $data['idx'], $data['form'] );
    }

    public static function formula( $level, $idx, $form )
    {
        // Completar 
        return false;
    }

    public function get_meta() {
        $customs = get_post_custom( $this->model->ID );

        if (!is_array($customs) || empty($customs)) {
            return $this;
        }
        foreach ($customs as $key => $value) {
            if (is_array($value) && count($value) == 1 && isset($value[0])) {
                $value = $value[0];
            }
            $customs[$key] = maybe_unserialize($value);
            
            $this->model->{$key} = maybe_unserialize($customs[$key]);
            
        }


        $this->model->custom = $customs;
        return $this;
    }

  

    public function update( $meta, $value )
    {
        update_post_meta( $this->model->ID, $meta, $value );
        $this->get_meta();
        return $this;
    }

    public function add_meta( $meta, $value )
    {
        add_post_meta( $this->model->ID, $meta, $value );
        $this->get_meta();
        return $this;
    }

    public function entrega() {
        $dias = $this->model->_data['form']['fecha_entrega'] / 24;
        return $dias . ' día' . ( ( $dias > 1 ) ? 's' : '' );
    }

    function getParent() {
        if( $this->model->type == 'piso' ) return $this->model->propiedad;
        return Propiedad::get( $this->model->propiedad )->habitacion_piso_id ;
    }

}