<?php

/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies\Wp;

use Warp\Apsies\Vendor\SOPGClassicMerchantClient as Client;

class Paysafecard extends Transaccion 
{

	public function __construct( \Wp_Post $post )
	{
		$this->model  = $post;
        $this->get_meta();
        $this->setConnection();
	}

	private function setConnection()
	{
		global $warp;
		$user_email = ( $warp['user']->me->custom_email === NULL ) ? $warp['user']->me->data->user_email  : $warp['user']->me->custom_email;
		$user      = self::generateCID( $user_email );

		$end_point = ( $this->model->tipo == 'recarga' ) ? 'recargar' : 'suscripcion';

		$this->config = new \stdClass;
		$this->config->endpoint = site_url($end_point);
		$this->config->mCId = $user;
		$this->config->username = 'APSIES_Network__test';
		$this->config->password = '8XagguCuB9Dhv79';
		$this->config->mode = 'test';
		$this->config->debug = false;
		$this->config->autoCorrect = false;
		$this->config->sysLang = 'en';
		$this->config->okUrl = rawurlencode( site_url($end_point . '?status=success&pay_id=' . $this->model->ID .'&mtid=' . $this->model->mtid . '&cur=EUR&amo' . $this->model->precio ) );
		$this->config->nokUrl = rawurlencode( site_url( $end_point . '?status=error&pay_id=' . $this->model->ID ) );
		$this->config->pnUrl = rawurlencode( site_url($end_point . '?status=pn&pay_id=' . $this->model->ID .'&mtid=' . $this->model->mtid . '&cur=EUR&amo' . $this->model->precio) );
		$this->cliente = new Client( $this->config->debug, $this->config->sysLang, $this->config->autoCorrect, $this->config->mode );

	}



	public static function create_recarga( $post_form, $type = 'paysafecard' )
    {
    	$values = array(
    		'10' => 10,
    		'30' => 25,
    		'65' => 50,
    		'150' => 100
    	);

    	 global $warp;

        $post_status = 'draft';

        $title = 'Recarga de puntos';

        $content = "Recarga de punto:\n";
        foreach( $post_form['puntos'] AS $key => $cant ) {
        	$puntos +=  ( $key * $cant );
        	$costo   += $values[$key] * $cant;
            $content .= $cant . ' x ' . $key .' ( '. $values[$key] .' Euros) : ' . $values[$key] * $cant;
            $content .= "\n<br/>";
        }


        $post = array(
            'post_title' => $title,
            'post_content' => $content,
            'post_status' => $post_status,
            'post_date' => date('Y-m-d H:i:s'),
            'post_type' => self::$type
        );

        $post_id = wp_insert_post( $post );

        add_post_meta($post_id, 'payment_type', 'paysafecard' );
        add_post_meta( $post_id, 'mtid', self::generateMID( $post_id ) );
        add_post_meta($post_id, 'tipo', 'recarga' );
        add_post_meta($post_id, 'precio', $costo );
        add_post_meta($post_id, 'puntos', $puntos );
        add_post_meta($post_id, 'user_id',$warp['user']->me->ID );

        $post = get_post( $post_id );
        
        return new self( $post );


    }

    public static function create_suscripcion( $post_form )
    {
    	// type => ( days, price )
    	$values = array(
    		'1' => array(30,10),
    		'2' => array(90,24)
    	);

    	 global $warp;

        $post_status = 'draft';

        $type = ( isset( $post_form['suscripcion']['type'] ) && isset( $values[$post_form['suscripcion']['type']] ) ) ? $post_form['suscripcion']['type'] : 1;
        $data_price = $values[$type];

        $title = 'Suscripción '. $data_price[0] . ' dís';

        $content = "Tipo de suscripcion: ". $data_price[0] ." dÃ­as \n"
        		 . "Precio: " . $data_price[1] ." Euros"
        		 . "Fecha: " . date('Y-m-d');


        $post = array(
            'post_title' => $title,
            'post_content' => $content,
            'post_status' => $post_status,
            'post_date' => date('Y-m-d H:i:s'),
            'post_type' => self::$type
        );

        $post_id = wp_insert_post( $post );

        add_post_meta($post_id, 'payment_type', 'paysafecard' );
        add_post_meta( $post_id, 'mtid', self::generateMID( $post_id ) );
        add_post_meta($post_id, 'tipo', 'suscripcion' );
        add_post_meta($post_id, 'precio', $data_price[1] );
        add_post_meta($post_id, 'dias', $data_price[0] );
        add_post_meta($post_id, 'user_id',$warp['user']->me->ID );

        $post = get_post( $post_id );
        
        return new self( $post );
    }

	public function payment()
	{			 
		$this->cliente->merchant( $this->config->username, $this->config->password );
		$this->cliente->setCustomer( sprintf("%0.2f", $this->model->precio ) , 'EUR', $this->model->mtid, $this->config->mCId );
		$this->cliente->setUrl( $this->config->okUrl, $this->config->nokUrl, $this->config->pnUrl );

		$paymentPanel = $this->cliente->createDisposition();

		if ( $paymentPanel == false )
		{
			@session_start();
			//regardless of the result, an info must be issued for the client.
			$this->logDebug();
			$_SESSION['msg'] = array('status'=>'error', 'msg' => 'No se ha podido establecer la conexión con paysafecard');
			header("Location:". $this->config->endpoint);
			die;
		} else {
			
			header("Location:".$paymentPanel);
			die;
		}
	}

	public function execute()
	{

		//enter the access data
		$this->cliente->merchant( $this->config->username, $this->config->password );
		
		//get current status
		$status = $this->cliente->getSerialNumbers( $this->model->mtid, 'EUR', $subId = '' );
		
		//If the return is 'execute', the amount can be debited (executeDebit)
		if ( $status === 'execute' )
		{
			$testexecute = $this->cliente->executeDebit( sprintf("%0.2f", $this->model->precio ), '1' );
			if ( $testexecute === true )
			{
				// here user account topup -EXECUTE DEBIT SUCCESSFUL- !!!
				return true;
			}
		}
		return false;
	}

	private static function generateMID( $post_id )
	{
		$str = chr(rand() > 0.5 ? rand(65, 90) : rand(97,122)). chr(rand() > 0.5 ? rand(65, 90) : rand(97,122)). chr(rand() > 0.5 ? rand(65, 90) : rand(97,122))
			 . '-' . time()
			 . '-' . $post_id;
		return $str;
	}

	private static function generateCID( $user_id )
	{
		$st = 'apsies_user'.$user_id;
		return md5( $str ); 
	}

	private function logDebug()
	{
		$this->updateStatus('pending');

		$error = $this->cliente->getLog( 'error' );
		if ( !empty( $error ) )
		{
			$this->update('_error', $error);
		}

	}
}
