<?php
/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/
namespace Warp\Apsies\Wp;

ini_set('display_errros', 0);

class Transaccion extends Post {

    public static $type = 'transaccion';

    public function __construct( \Wp_Post $post ) {
            
        $this->model  = $post;
        $this->get_meta();
    }

    public static function get( $id )
    {
        $post = get_post( $id );
        
        if( !is_object( $post ) ) {
            return false;
        }

        $type = get_post_meta( $post->ID, 'payment_type', true );

        if( $type == 'paypal') {
            return new Paypal( $post );
        } else {
            return new Paysafecard( $post );
        }
    }

    public static function create( $post_form ) {

        global $warp;

        $post_status = 'draft';

        $title = self::$titulos[$data['level']][$data['tipo']];

        $content = " <h1> Detalles del pedido </h1>";
        foreach( $data['form'] AS $key => $value ) {
            $content .= $key . ': ';
            if( is_array( $value ) ) {
                $content .= implode(', ', $value);
            } else {

                $content .= ( $key == 'observaciones') ? '<blockquote>' . nl2br( $value ) .'</blockquote>' : nl2br( $value );
            }

            $content .= "\n<br/>";
        }


        $post = array(
            'post_title' => $title,
            'post_content' => $content,
            'post_status' => $post_status,
            'post_date' => date('Y-m-d H:i:s'),
            'post_type' => self::$type
        );

        $post_id = wp_insert_post( $post );

        add_post_meta($post_id, 'puntos', $data['puntos'] );
        add_post_meta($post_id, 'pedido_suscrito', ( isset( $data['data']['pedido_suscrito'] ) ? "1" : "0" ) );
        add_post_meta($post_id, '_data', serialize( $data ) );

        $post = get_post( $post_id );
        
        return new self( $post );
    }

    public function get_meta() {
        $customs = get_post_custom( $this->model->ID );

        if (!is_array($customs) || empty($customs)) {
            return $this;
        }
        foreach ($customs as $key => $value) {
            if (is_array($value) && count($value) == 1 && isset($value[0])) {
                $value = $value[0];
            }
            $customs[$key] = maybe_unserialize($value);
            
            $this->model->{$key} = maybe_unserialize($customs[$key]);
            
        }


        $this->model->custom = $customs;
        return $this;
    }

     public function update( $meta, $value )
    {
        update_post_meta( $this->model->ID, $meta, $value );
        $this->get_meta();
        return $this;
    }

    public function add_meta( $meta, $value )
    {
        add_post_meta( $this->model->ID, $meta, $value );
        $this->get_meta();
        return $this;
    }

    public function delete()
    {
        wp_delete_post( $this->model->ID);
    }
}