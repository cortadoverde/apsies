<?php

/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies\Wp;

ini_set('display_errros', 0);

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Refund;



class Paypal extends Transaccion {

	 public static function create_recarga( $post_form )
    {
    	$values = array(
    		'10' => 10,
    		'30' => 25,
    		'65' => 50,
    		'150' => 100
    	);

    	 global $warp;

        $post_status = 'draft';

        $title = 'Recarga de puntos';

        $content = "Recarga de punto:\n";
        foreach( $post_form['puntos'] AS $key => $cant ) {
        	$puntos +=  ( $key * $cant );
        	$costo   += $values[$key] * $cant;
            $content .= $cant . ' x ' . $key .' ( '. $values[$key] .' Euros) : ' . $values[$key] * $cant;
            $content .= "\n<br/>";
        }


        $post = array(
            'post_title' => $title,
            'post_content' => $content,
            'post_status' => $post_status,
            'post_date' => date('Y-m-d H:i:s'),
            'post_type' => self::$type
        );

        $post_id = wp_insert_post( $post );

        add_post_meta($post_id, 'payment_type', 'paypal' );
        add_post_meta($post_id, 'tipo', 'recarga' );
        add_post_meta($post_id, 'precio', $costo );
        add_post_meta($post_id, 'puntos', $puntos );
        add_post_meta($post_id, 'user_id',$warp['user']->me->ID );

        $post = get_post( $post_id );
        
        return new self( $post );
    }

    public static function create_suscripcion( $post_form )
    {
    	// type => ( days, price )
    	$values = array(
    		'1' => array(30,10),
    		'2' => array(90,24)
    	);

    	 global $warp;

        $post_status = 'draft';

        $type = ( isset( $post_form['suscripcion']['type'] ) && isset( $values[$post_form['suscripcion']['type']] ) ) ? $post_form['suscripcion']['type'] : 1;
        $data_price = $values[$type];

        $title = 'SuscripciÃ³n '. $data_price[0] . ' dÃ­as';

        $content = "Tipo de suscripcion: ". $data_price[0] ." dÃ­as \n"
        		 . "Precio: " . $data_price[1] ." Euros"
        		 . "Fecha: " . date('Y-m-d');


        $post = array(
            'post_title' => $title,
            'post_content' => $content,
            'post_status' => $post_status,
            'post_date' => date('Y-m-d H:i:s'),
            'payment_type' => 'paypal',
            'post_type' => self::$type
        );

        $post_id = wp_insert_post( $post );

        add_post_meta($post_id, 'payment_type', 'paypal' );
        add_post_meta($post_id, 'tipo', 'suscripcion' );
        add_post_meta($post_id, 'precio', $data_price[1] );
        add_post_meta($post_id, 'dias', $data_price[0] );
        add_post_meta($post_id, 'user_id',$warp['user']->me->ID );

        $post = get_post( $post_id );
        
        return new self( $post );
    }

	public function payment()
	{			
		$end_point = ( $this->model->tipo == 'recarga' ) ? 'recargar' : 'suscripcion';

		// Setear el tipo de pago
		$payer = new Payer();
		$payer->setPaymentMethod("paypal");

		$item = new Item();
		$item->setName( $this->model->post_title )
			->setCurrency('EUR')
			->setQuantity(1)
			->setPrice(sprintf("%0.2f", $this->model->precio ) );

		$itemList = new ItemList();
		$itemList->setItems( array( $item ) );

		$amount = new Amount();                        
		$amount->setCurrency('EUR');        
		$amount->setTotal( sprintf("%0.2f", $this->model->precio ) ); 

		$transaction = new Transaction();
		$transaction->setAmount($amount)
					->setItemList($itemList)
					->setDescription( $this->model->tipo );

		$redirectUrls = new RedirectUrls();
		$redirectUrls->setReturnUrl(site_url($end_point . '?status=success&pay_id='. $this->model->ID))
					 ->setCancelUrl(site_url($end_point . '?status=cancel&pay_id=' . $this->model->ID));

		$payment = new Payment();
		$payment->setIntent("sale")
			->setPayer($payer)
			->setRedirectUrls($redirectUrls)
			->setTransactions(array($transaction));

		try {
			$payment->create(self::getApiContext());
		} catch (\PayPal\Exception\PPConnectionException $ex) {
			echo "Exception: " . $ex->getMessage() . PHP_EOL;
			var_dump($ex->getData());	
			exit(1);
		}




		foreach($payment->getLinks() as $link) {
			if($link->getRel() == 'approval_url') {
				$redirectUrl = $link->getHref();
				break;
			}
		}

		// ### Redirect buyer to PayPal website
		// Save the payment id so that you can 'complete' the payment
		// once the buyer approves the payment and is redirected
		// back to your website.
		//
		// It is not a great idea to store the payment id
		// in the session. In a real world app, you may want to 
		// store the payment id in a database.
		$_SESSION['payment_id'] = $payment->getId();
		
		$this->update( 'payment_id', $payment->getId() );

		if(isset($redirectUrl)) {
			header('Location: '.  $redirectUrl);
			die;
			return $redirectUrl;
		}

		return false;

	}

	public function getApiContext() {
		global $warp;
		// ### Api context
		// Use an ApiContext object to authenticate 
		// API calls. The clientId and clientSecret for the 
		// OAuthTokenCredential class can be retrieved from 
		// developer.paypal.com

		$apiContext = new ApiContext(
			new OAuthTokenCredential(
				$warp['config']->get('paypal_public_id'),
				$warp['config']->get('paypal_private_id')
			)
		);



		// #### SDK configuration

		// Comment this line out and uncomment the PP_CONFIG_PATH
		// 'define' block if you want to use static file 
		// based configuration
		
			$apiContext->setConfig(
				array(
					'mode' => $warp['config']->get('paypal_use_sandbox') != NULL ? 'sandbox' : 'live',//isset( $warp['config']->paypal_use_sandbox ) ? 'sandbox' : 'live',
					'http.ConnectionTimeOut' => 30,
					'log.LogEnabled' => true,
					'log.FileName' => __DIR__ . '/PayPal.log',
					'log.LogLevel' => 'FINE'
				)
			);
		
		/*
		// Register the sdk_config.ini file in current directory
		// as the configuration source.
		if(!defined("PP_CONFIG_PATH")) {
			define("PP_CONFIG_PATH", __DIR__);
		}
		*/

		return $apiContext;
	}

	
	public function execute()
	{
		$apiContext = self::getApiContext();
		$payment = Payment::get($this->model->payment_id,  $apiContext);
		$paymentExecution = new PaymentExecution();
		$paymentExecution->setPayer_id($this->model->payer_id);
		$payment->execute($paymentExecution, $apiContext );
		return true;
	}

}
