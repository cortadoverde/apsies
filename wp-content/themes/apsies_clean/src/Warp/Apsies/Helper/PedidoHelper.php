<?php
/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies\Helper;

use Warp\Helper\AbstractHelper;
use Warp\Warp;
use Warp\Apsies\Hooks;
use Warp\Apsies\Shortcodes;

/**
 * Option helper class, store option data.
 */
class PedidoHelper extends AbstractHelper
{

    public function __construct(Warp $warp)
    {
        parent::__construct($warp);

        // set prefix
        $this->prefix = basename($this['path']->path('theme:')) . __CLASS__;
    }

    public function save( $data )
    {
        $pedido = \Warp\Apsies\Wp\Pedido::create( $data );
        
        if( ! $this['user']->me->is_login ) {
            $_SESSION['pending_action']['pedido']['registro'] = array('pedido_id' => $pedido->model->ID);
            $_SESSION['pending_action']['pedido']['confirmacion'] = array('pedido_id' => $pedido->model->ID);

            header('Location:' . site_url('/login') );
            die;
        } else {
           if( $pedido->model->post_status == 'draft' ) {
                $_SESSION['pending_action']['pedido']['confirmacion'] = array('pedido_id' => $pedido->model->ID);
                $pedido->update('user_id', $this['user']->me->ID );
                header('Location:' . site_url('/pedidos?step=confirmacion') );
                die;
           }

        }
 
    }

    public function assign( $pedido_id )
    {
        if( ! $this['user']->me->is_login ) return false;

        $pedido    =  \Warp\Apsies\Wp\Pedido::get($pedido_id);
        $pedido->update('user_id', $this['user']->me->ID);
        unset( $_SESSION['pending_action']['pedido']['confirmacion'] );

        if( $this['user']->me->puntosusuario < $pedido->model->puntos ) {
            $_SESSION['msg'] = array('status'=>'warning', 'msg' => 'Necesita cargar al menos ' . ( $pedido->model->puntos - $this['user']->me->puntosusuario ) . ' puntos para confirmar el pedido');
            $_SESSION['pending_action']['pedido']['assign'] = $pedido_id;
            header('Location: '. site_url('recargar'));
            die;
        }

        $_SESSION['msg'] = array('status'=>'success', 'msg' => 'Pedido enviado correctamente. Revise el historial para seguir su evolución', 'url' => site_url( 'historial' ) );

        $this['user']->me->update('puntosusuario', ( $this['user']->me->puntosusuario - $pedido->model->puntos ) );


        wp_update_post( array(
            'post_status' => 'publish',
            'post_author' => $this['user']->me->ID,
            'post_date'   => date('Y-m-d H:i:s', time()),
            'ID' => $pedido->model->ID
        ) );



        unset( $_SESSION['pending_action']['pedido']['assign'] );
       


    }

    public function check( $data )
    {
        if( isset( $_SESSION['pending_action'] ) ) {
            foreach( $_SESSION['pending_action'] AS $action ) {
                if( $action['action'] == 'pedido' ) {
                    if( $action['step'] == 'registro' ) {

                    }
                }
            }
        }
    }

    /* @todo
    public function cal_points( $form )
    {

        $formular = array(
            'uni' => array(
                'trabajos' => function( $data ) use ( $form ) {
                    $factor = 
                }
            )
        )

        Formulas = {
            uni : {
                trabajos             : function() {
                    var     ctx  = ctxForm.uni.trabajos,
                        paginas  = $('.number input', ctx ).val(),
                        biagrafia = ( $('[name="submit_pedido[uni][trabajos][bibliografia]"]', ctx).val() == 'si' ) ? true : false,
                        factor   = getFactorEntrega( parseInt( $('.fecha_entrega', ctx).val() ) ),
                        puntos   = (paginas > 30) ? ( 2 * paginas + 51 ) : ( ( paginas > 10 ) ? ( paginas * 3+21 ) : ( paginas * 5.1 ) ),
                        subtotal = puntos * factor,
                        total    = ( biagrafia == true ) ? ( ( 5 / 100 ) * subtotal ) + subtotal : subtotal;

                        return Math.ceil( total );
                },

                autocad              : function() {
                    var ctx         = ctxForm.uni.autocad,
                        nro         = $('.number input', ctx ).val(),
                        vistas      = $('[name="submit_pedido[uni][autocad][vistas]"]', ctx).val() == 'si',
                        acotaciones = $('[name="submit_pedido[uni][autocad][acotaciones]"]', ctx).val() == 'si',
                        factor      = getFactorEntrega( parseInt( $('.fecha_entrega', ctx).val() ) ),
                        subtotal    = nro * 10,
                        total       = subtotal * factor * ( ( vistas ) ? 1.10 : 1 ) * ( ( acotaciones ) ? 1.5 : 1 );
                        
                        return Math.ceil( total )
                },

                apuntes              : function(){
                    var ctx = ctxForm.uni.apuntes,
                        nro = $('.number input', ctx).val(),
                        fecha = parseInt( $('.fecha_entrega', ctx).val() ),
                        factor = getFactorEntregaReducido( fecha ),
                        total = nro * 1 * factor;

                        return Math.ceil( total )

                },

                powerpoint           : function() {
                    var ctx = ctxForm.uni.powerpoint,
                        nro = $('.number input', ctx).val(),
                        tipo = $('[name="submit_pedido[uni][powerpoint][tipo]"]', ctx).val(),
                        precio = tipo == 'diapo_texto' ? 0.3 : 3,
                        fecha = parseInt( $('.fecha_entrega', ctx).val() ),
                        factor = getFactorEntregaReducido( fecha ),
                        total = nro * precio * factor;

                        return Math.ceil( total );
                },

                exposiciones         : function() {
                    var ctx = ctxForm.uni.exposiciones,
                        nro = $('.number input', ctx).val(),
                        tipo = $('[name="submit_pedido[uni][exposicion][tipo]"]', ctx).val(),
                        precio = tipo == 'trabajo_escrito' ? 4.1 : 7.1,
                        hablada = ( $('[name="submit_pedido[uni][exposicion][parte_hablada]"]', ctx).val() == 'si' ) ? 1.25 : 1,
                        fecha = parseInt( $('.fecha_entrega', ctx).val() ),
                        factor = getFactorEntregaReducido( fecha ),
                        total = nro * precio * factor * hablada;

                        return Math.ceil( total );
                }
            },
            eso : {
                resolucion           : function(){
                    var ctx = ctxForm.eso.resolucion,
                        nro = $('.number input', ctx).val(),
                        step = parseInt( $('[name="submit_pedido[eso][resolucion_ejercicio][asignatura]"] option:selected').attr('rel') ),
                        precio = 2 * ( nro / step ),
                        fecha = parseInt( $('.fecha_entrega', ctx).val() ),
                        factor = getFactorEntregaColegio( fecha ),
                        total = precio * factor;
                        console.log( nro, precio, fecha, factor );
                        return Math.ceil( total );
                }, 

                redaccion            : function(){
                    var ctx = ctxForm.eso.redaccion;
                    return Formulas.eso.common_palabras( ctx );
                },

                comentario_texto     : function(){
                    return Formulas.eso.common_palabras( ctxForm.eso.comentario_texto )
                    
                },

                comentario_artistico : function(){
                    return Formulas.eso.common_palabras( ctxForm.eso.comentario_artistico )
                },

                critica              : function(){
                    var ctx = ctxForm.eso.critica,
                        nro = $('.number input', ctx).val(),
                        precio = 8,
                        fecha = parseInt( $('.fecha_entrega', ctx).val() ),
                        factor = getFactorEntregaColegio( fecha ),
                        total = precio * nro * factor;
                        console.log( nro, precio, fecha, factor );
                    
                    return Math.ceil( total );
                },

                autocad              : function(){
                    var ctx         = ctxForm.eso.autocad,
                        nro         = $('.number input:enabled', ctx ).val(),
                        tipo        = $('[name="submit_pedido[eso][autocad][tipo]"]').val(),
                        vistas      = $('[name="submit_pedido[eso][autocad][vistas]"]', ctx).val() == 'si',
                        acotaciones = $('[name="submit_pedido[eso][autocad][acotaciones]"]', ctx).val() == 'si',
                        factor      = getFactorEntregaColegio( parseInt( $('.fecha_entrega:enabled', ctx).val() ) ),
                        subtotal    = nro * ( ( tipo == 1 ) ? 2 : 3 ),
                        tipoFactor  = ( tipo == 1 ) ? 1 : ( ( ( vistas ) ? 1.5 : 1 ) * ( ( acotaciones ) ? 1.5 : 1 ) ),
                        total       = parseInt( subtotal * factor * tipoFactor );
                        //console.log( tipo, subtotal , factor , tipoFactor);
                        return Math.ceil( total )
                },

                investigacion        : function(){
                    return Formulas.eso.common_palabras( ctxForm.eso.investigacion )
                },

                trabajos             : function(){
                    var     ctx  = ctxForm.eso.trabajos,
                        paginas  = $('.number input', ctx ).val(),
                        biagrafia = ( $('[name="submit_pedido[eso][trabajo][bibliografia]"]', ctx).val() == 'si' ) ? true : false,
                        factor   = getFactorEntregaColegio( parseInt( $('.fecha_entrega', ctx).val() ) ),
                        puntos   = 4.1 * paginas;  //(paginas > 30) ? ( 2 * paginas + 51 ) : ( ( paginas > 10 ) ? ( paginas * 3+21 ) : ( paginas * 5.1 ) ),
                        subtotal = puntos * factor,
                        total    = subtotal * ( ( biagrafia == true ) ? 1.05 : 1 );

                        return Math.ceil( total );
                },

                common_palabras      : function( ctx ) {
                    var nro = $('.number input', ctx).val(),
                        precio = 0.03,
                        fecha = parseInt( $('.fecha_entrega', ctx).val() ),
                        factor = getFactorEntregaColegio( fecha ),
                        total = precio * nro * factor;
                        
                    return Math.ceil( total );
                }
            }
        }
    }
    */


    public function getContext( $extra = array() )
    {
        $content = ( isset( $_GET['step'] ) && in_array( $_GET['step'], array( 'confirmacion', 'nuevo', 'detalle' ) ) ) ? $_GET['step'] : 'nuevo';
        return array( 'content' => 'pedido/page/' . $content , '_args' => array_merge( array('setter' => __CLASS__ ) , $extra ) );
    }

    public function sessionStore( $index, $value = NULL)
    {
        $_SESSION[$this->prefix][$index] = $value;
    }

    public function clearSession( $index = false )
    {
        if( $index !== false ) {
            unset( $_SESSION[$this->prefix] ) ; return true;
        }
        unset( $_SESSION[$this->prefix][$index] );
        return true;
    }

}
