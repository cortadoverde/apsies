<?php
/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies\Helper;

use Warp\Helper\AbstractHelper;
use Warp\Warp;

/**
 * Option helper class, store option data.
 */
class SuscripcionHelper extends AbstractHelper
{

    public function __construct(Warp $warp)
    {
        parent::__construct($warp);

        // set prefix
        $this->prefix = basename($this['path']->path('theme:'));
    }

    public function test()
    {   
        echo '<pre>';
        print_r( $this->warp['user']->me );
        die;
    }
    

}
