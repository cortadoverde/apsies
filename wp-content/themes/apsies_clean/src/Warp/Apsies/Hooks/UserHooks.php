<?php
/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies\Hooks;


use Warp\Warp;


class UserHooks {
	
	public function __construct()
    {
    	$this->runHooks();
    	
    }

    public function runHooks()
    {
    	add_action( 'user_register', array( $this, 'register' ) );
    	add_filter( 'registration_redirect', array( $this, 'registration_redirect' ), 99 );
        add_filter( 'login_redirect',  array( $this, 'check_pending_actions'  ), 10, 3 );
    	add_action( 'tml_new_user_registered', array( $this, 'autologin' ) ); 
        add_action('wp_logout', array( $this, 'logout_redirect' ) );
    }

   
    public function register( $user_id )
    {
    	//if ( !empty( $_POST['colegio'] ) )
		update_user_meta( $user_id, 'colegio', $_POST['colegio'] );
		//if ( !empty( $_POST['universidad'] ) )
		update_user_meta( $user_id, 'universidad', $_POST['universidad'] );
		//if ( !empty( $_POST['ciudad'] ) )
		update_user_meta( $user_id, 'ciudad', $_POST['ciudad'] );
		//if ( !empty( $_POST['edad'] ) )
		update_user_meta( $user_id, 'edad', $_POST['edad'] );
		//if ( !empty( $_POST['nivel'] ) )
        update_user_meta( $user_id, 'level', $_POST['nivel'] );
		update_user_meta( $user_id, 'nivel', $_POST['nivel'] );

		update_user_meta( $user_id, 'fechasuscripcion', 0 );
		update_user_meta( $user_id, 'diassuscripcion', 0 );
		update_user_meta( $user_id, 'puntosusuario', 0 );


		
    }

    public function autologin( $user_id ) 
    {
    	wp_set_auth_cookie( $user_id );
    }

    public function registration_redirect()
    {
    	$redirect_to = site_url( 'pedidos' );
        
        if( isset( $_SESSION['pending_action']['suscripcion'] ) ) {
            $redirect_to = site_url('/suscripcion');
        }

        if( isset( $_SESSION['pending_action']['pedido']['confirmacion'] ) ) {
            $redirect_to = site_url( '/pedidos?step=confirmacion' );
        }        

		// Add instance to the query if specified
		if ( ! empty( $_REQUEST['instance'] ) )
			$redirect_to = add_query_arg( 'instance', $_REQUEST['instance'], $redirect_to );
		return $redirect_to;
    }

    public function check_pending_actions( $redirect_to, $request, $user )
    {
        global $warp;
        if( isset( $_SESSION['pending_action']['pedido']['confirmacion'] ) ) {
            return site_url( '/pedidos?step=confirmacion' );
        }

        if( isset( $_SESSION['pending_action']['suscripcion'] ) ) {
            return site_url('/suscripcion');
        }
        
        $is_admin = isset( $user->caps['administrator'] ) && $user->caps['administrator'] == 1  ? true : false;
        
        return site_url( ( $is_admin ? 'wp-admin' : 'pedidos' ) );
        
    }

    
    public function logout_redirect(){
        
        $_SESSION['msg'] = array(
            'status' => 'danger',
            'msg'    => 'Has cerrado la sesión correctamente'

        );

        if( isset( $_SESSION['pending_action']['pedido']['confirmacion'] ) ) {
            unset( $_SESSION['pending_action']['pedido']['confirmacion'] );
        }

        if( isset( $_SESSION['pending_action']['pedido']['suscripcion'] ) ) {
            unset( $_SESSION['pending_action']['pedido']['suscripcion'] );
        }

        wp_redirect( home_url() );
        


        exit();
    }
}
