<?php
/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies\Hooks;


use Warp\Warp;


class AdminHooks {
	
	public function __construct()
    {
    	$this->runHooks();
    	
    }

    public function runHooks()
    {
    	add_action('get_header_apsies',  array( $this, 'checkMaitenanceMode' ) );
        add_action('init', array( $this, 'post_type_pedido'), 0 );
        add_action('init', array( $this, 'post_type_ticket'), 0 );
        add_action('init', array( $this, 'post_type_transacciones'), 0 );

        add_action( 'save_post', array( $this, 'notifications'), 10, 3 );
        add_filter( 'wp_insert_post_data' , array( $this, 'filter_post_data'), '99', 2 );

        add_filter( 'wp_mail_content_type', function(){ return 'text/html';} );
        add_action( 'add_meta_boxes', array( $this, 'pedido_metabox_register') );

       // add_action('init', array( $this, 'user_settings'), 0 );
    }

    public function notifications( $post_id, $post, $update )
    {
        if( ! in_array($post->post_type, array('ticket', 'pedido') ) ) {
            return;
        }

        if ( wp_is_post_revision( $post_id ) || $post->post_status == 'auto-draft')
            return;

        call_user_func_array( array( $this, '_notification_' . $post->post_type ), array( $post, $update ) ); 
    }

    public function _notification_ticket( $post, $update )
    {
        $ticket = new \Warp\Apsies\Wp\Ticket( $post );
        global $warp;
        if( $ticket->model->post_status == 'publish' ) {
            $user_id = isset( $ticket->model->to_user ) &&  $ticket->model->to_user != NULL ?  $ticket->model->to_user : $ticket->model->Pedido->user_id;
            $UserObject = new \Warp\Apsies\Wp\User( $user_id );
            $to = ( isset( $UserObject->custom_email ) ) ? $UserObject->custom_email : $UserObject->data->user_email;
            $subject = '[Nuevo Ticket] '. $ticket->model->ID. ' - ' . $ticket->model->post_title;
            $message = $ticket->model->post_content;
            $headers = 'From: Aprobar sin estudiar <hola@aprobarsinestudiar.com>' . "\r\n";
            wp_mail( $to, $subject, $message, $headers );
        }
    }

    public function filter_post_data( $data , $postarr )
    {
        global $warp;

        if( $data['post_type'] == 'pedido') {
            $pedido = \Warp\Apsies\Wp\Pedido::get( $postarr['ID'] );
            //_baja_pedido
             if( isset( $_POST['_baja_pedido'] ) ) {
                if( isset( $_SESSION['pending_action']['pedido']['confirmacion'] ) && $_SESSION['pending_action']['pedido']['confirmacion'] == $pedido->model->ID ) {
                    unset( $_SESSION['pending_action']['pedido'] );
                }
                // Crear ticket;
                \Warp\Apsies\Wp\Ticket::create( array(
                    'title' => '[Pedido cancelado]' . $pedido->model->post_title,
                    'content' => 
                        '<h3>Pedido Original</h3>'
                        . $warp['template']->render('pedido/page/detalle', array( 'current_pedido' => $pedido ) )
                        . '<h3> Motivo del rechazo: </h3>'
                        . nl2br( $_POST['_baja_pedido_motivo'] ),
                    'user_id' => $pedido->model->user_id
                ) );
                if( $pedido->model->post_status == 'publish' ) {
                    $warp['user']->me->update( 'puntosusuario',( $warp['user']->me->puntosusuario + $pedido->model->puntos ) );
                }
                $pedido->delete();

                header('location:' . site_url( 'wp-admin/edit.php?post_type=pedido') );
                die;
            }
        }

        return $data;
    }

    public function _notification_pedido( $post, $update )
    {
        $pedido = new \Warp\Apsies\Wp\Pedido( $post );
        global $warp;

       

        if( $pedido->model->post_status == 'publish' ) {
            $to = 'p.a.samu@gmail.com';//$warp['config']->get('admin_email')
            $subject = '[Nuevo pedido] '. $pedido->model->ID. ' - ' . $pedido->model->post_title;
            $message = $warp['template']->render('pedido/page/detalle', array( 'current_pedido' => $pedido ) );
            $headers = 'From: Aprobar sin estudiar <hola@aprobarsinestudiar.com>' . "\r\n";
            wp_mail( $to, $subject, $message, $headers );
        }
    }


    public function checkMaitenanceMode()
    {
    	global $warp;
    	if( isset ( $warp['config']['site_mode_down'] ) ) {
    		if ( !current_user_can( 'edit_themes' ) || !is_user_logged_in() ) {

    			echo $warp['template']->render('404', array('type' => 'site_down') );
    			die;
    		}
    	}
    }

    public function post_type_pedido()
    {
        $args = array(
            'description' => 'Pedidos realizados',
            'show_ui' => true,
            'menu_position' => 4,
            'labels' => array(
                'name' => 'Pedidos',
                'singular_name' => 'Pedido',
                'add_new' => 'Agregar nuevo Pedido',
                'add_new_item' => 'Agregar nuevo Pedido',
                'edit' => 'Editar Pedido',
                'edit_item' => 'Editar Pedido',
                'new-item' => 'Nuevo Pedido',
                'view' => 'Ver Pedidos',
                'view_item' => 'Ver Pedido',
                'search_item' => 'Buscar Pedidos',
                'not_found' => 'No se han encontrado Pedidos',
                'not_found_in_trash' => 'No se han encontrado Pedidos en la papelera',
                'parent' => 'Pedido padre'
            ),
            'public' => true,
            'taxonomies' => array(),
            'capability_type' => 'post',
            'hierarchichal' => false,
            'rewrite' => array( 'slug' => 'pedido', 'with_front' => true),
            'supports' => array( 'title', 'editor')
        );
        register_post_type( 'pedido', $args );


    }

    public function pedido_metabox_register()
    {
        add_meta_box( 
                'apsies_pedido_detalle', 
                'Detalle del pedido', 
                array( $this, 'pedido_metabox'), 
                'pedido', 'advanced', 'high' );
    }

    public function pedido_metabox()
    {
        global $post, $warp;
        $current = new \Warp\Apsies\Wp\Pedido( $post );
        echo $warp['template']->render( 'admin/metabox/pedidos', array( 'current_pedido' => $current ) );    



    }

    public function post_type_ticket() 
    {
        $args = array(
                'description' => 'Tickets realizados',
                'show_ui' => true,
                'menu_position' => 4,
                'labels' => array(
                    'name'=> 'Tickets',
                    'singular_name' => 'Tickets',
                    'add_new' => 'Agregar nuevo Ticket',
                    'add_new_item' => 'Agregar nuevo Ticket',
                    'edit' => 'Editar Ticket',
                    'edit_item' => 'Editar Ticket',
                    'new-item' => 'Nuevo Ticket',
                    'view' => 'Ver Tickets',
                    'view_item' => 'Ver Ticket',
                    'search_items' => 'Buscar Tickets',
                    'not_found' => 'No se han encontrado Tickets',
                    'not_found_in_trash' => 'No se han encontrado Tickets en la papelera',
                    'parent' => 'Ticket padre'
                ),
                'public' => true,
                'taxonomies' => array(),
                'capability_type' => 'post',
                'hierarchical' => false,
                'rewrite' => array( 'slug' => 'ticket', 'with_front' => true ),
                'supports' => array('title', 'editor', 'comments')
        );

        register_post_type( 'ticket' , $args );
    }

     public function post_type_transacciones()
    {
        $args = array(
            'description' => 'Transacciones realizados',
            'show_ui' => true,
            'menu_position' => 4,
            'labels' => array(
                'name' => 'Transacciones',
                'singular_name' => 'Transaccion',
                'add_new' => 'Agregar nuevo Transaccion',
                'add_new_item' => 'Agregar nuevo Transaccion',
                'edit' => 'Editar Transaccion',
                'edit_item' => 'Editar Transaccion',
                'new-item' => 'Nuevo Transaccion',
                'view' => 'Ver Transacciones',
                'view_item' => 'Ver Transaccion',
                'search_item' => 'Buscar Transacciones',
                'not_found' => 'No se han encontrado Transacciones',
                'not_found_in_trash' => 'No se han encontrado Transacciones en la papelera',
                'parent' => 'Transaccion padre'
            ),
            'public' => true,
            'taxonomies' => array(),
            'capability_type' => 'post',
            'hierarchichal' => false,
            'rewrite' => array( 'slug' => 'transaccion', 'with_front' => true),
            'supports' => array( 'title', 'editor')
        );
        register_post_type( 'transaccion', $args );
    }

    public function user_settings()
    {
        global $warp;

    }


}
