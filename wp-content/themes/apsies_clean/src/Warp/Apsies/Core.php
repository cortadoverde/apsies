<?php
/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies;

abstract class Core
{
	public $id;

	public $ID;

	public $object_type;

	public function __isset( $field )
	{
		if( isset ( $this->field ) ) {
			return $this->field;
		}

		return false;
	}

	public function __call( $field, $args )
	{
		return $this->__get( $field );
	}

	public function __get( $field )
	{
		if ( ! isset ( $this->field ) ) {
			if ( $meta_value = $this->meta( $field ) ) {
				$this->field = $meta_value;
			} else if ( method_exists ( $this, $field ) ) {
				$this->field = $this->field();
			}
		}
	}

	public function import( $info ) 
	{
		if ( is_object ( $info ) ) {
			$info = get_object_vars ( $info );
		}

		if ( is_array ( $info ) ) {
			foreach ( $info as $key => $value ) {
				if ( ! empty ( $key ) ) {
					$this->{$key} = $value;
				}
			}
		}
	}

	public function update ( $key, $value )
	{
		update_metadata ( $this->object_type, $this->ID, $key, $value );
	}

	public function can_edit()
	{
		return $this->can( 'edit_post', $this->ID );
	}

	private function can( $action, $id = false )
	{
		if( $id === false ) $id = $this->ID;

		$key = '_can_' . $action;
		
		if ( isset ( $this->$key ) )
			return $this->$key;
		
		$this->$key = false;

		if( function_exists( 'current_user_can' ) )  {
			if( current_user_can( $action, $id ) ) {
				$this->$key = true;
			}
		} 

		return $this->$key;
	}

	public function get_method_values() 
	{
        $ret = array();
        $ret['can_edit'] = $this->can_edit();
        return $ret;
    }



}