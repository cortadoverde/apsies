<?php
/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies\Controller;

class User extends Controller
{
	public function login()
	{
		$info = array();

	    $info['user_login'] = $_POST['username'];
	    $info['user_password'] = $_POST['password'];

	    $remember = isset( $_POST['remember'] );
	    if ($remember) {
	    	$info['remember'] = true;
	    } else {
	    	$info['remember'] = false;
	    }
	    
				
		$user = wp_signon( $info, true );
	    $data = array();
	    if ( is_wp_error($user) ){
	    	$data = array(
	    		'type' 		=> 'info',
	    		'status'  	=> 'error',
	    		'msg'		=> 'Usuario y/o contraseña incorrecta'  
	    	);
	    } else {

	    	$is_admin = isset( $user->caps['administrator'] ) && $user->caps['administrator'] == 1  ? true : false;
	        
	        $url = site_url( ( $is_admin ? 'wp-admin' : 'pedidos' ) );

	    	if( isset( $_SESSION['pending_action']['pedido']['confirmacion'] ) ) {
	            $url = site_url( '/pedidos?step=confirmacion' );
	        }

	        if( isset( $_SESSION['pending_action']['suscripcion'] ) ) {
	            $url = site_url('/suscripcion');
	        }     
	        

	    	$data = array(
	    		'type' 		=> 'info',
	    		'status'  	=> 'success',
	    		'url'       => $url
	    	);

	    	wp_set_auth_cookie($user->ID);
	    }

	    $this->output = $data;
	}

	public function logout()
	{
		wp_logout();
		 $this->output = array(
	    		'type' 		=> 'info',
	    		'status'  	=> 'error',
	    		'msg'		=> 'Usuario y/o contraseña incorrecta'  
	    	);
	}

	public function after_action ()
	{
		header('Content-type: application/json');
		echo json_encode($this->output);
		die;
	}

	public function is_login() {
		$this->output = array( 'is_login' => $this->system['user']->me->is_login );
	}

	public function pending_action()
	{ 
		@session_start();
		if( ! $this->system['user']->me->is_login ) {
			
			$_SESSION['pending_action']['suscripcion'] = ( ! isset ( $_POST['plan'] )  ) ? 1 : $_POST['plan'];
			
			$this->output[] = array(
				'type' => 'info',
				'msg' => 'Necesitas crear una cuenta para poder suscribirte ',
				'status' => 'danger',
				'url' => site_url( 'login')
			);
		}
	}
	
}