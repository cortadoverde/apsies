<?php
/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies\Controller;

class Profile extends Controller
{

	public function update()
	{
		$allow = array(
			'user' => array( 'first_name', 'last_name', 'city', 'level', 'age', 'institute', 'grade', 'career'),
			'account' => array( 'custom_email' )
		);

		if( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
			if( isset( $_POST['user'] ) ) {
				foreach( $_POST['user'] AS $key => $value ) {
					if( in_array( $key, $allow['user'] ) ) {
						$this->system['user']->me->update($key, $value);
					}
				}
			}

			$this->output[] = array(
				'type'   => 'info',
				'msg'	 => 'Datos actualizados correctamente',
				'status' => 'success'
			);
		}
	}

	public function account()
	{
		$allow = array(
			'account' => array( 'custom_email' )
		);


		if( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
			if( isset( $_POST['user'] ) ) {
				foreach( $_POST['user'] AS $key => $value ) {
					if( in_array( $key, $allow['account'] ) ) {
						$this->system['user']->me->update($key, $value);
						$this->output[] = array(
							'type'   => 'info',
							'msg'	 => 'Datos actualizados correctamente',
							'status' => 'success'
						);
					}
				}
			}

			if( isset( $_POST['user']['new_password'] ) && !empty( $_POST['user']['new_password'] ) ) {
				if( $_POST['user']['new_password'] == $_POST['user']['repeat_password'] ) {
					$user_id = $this->system['user']->me->ID;
					$user_login = $this->system['user']->me->data->user_login;

					
					wp_set_password( $_POST['user']['new_password'], $user_id);
					wp_signon( array( 'user_login' => $user_login, 'user_password' => $_POST['user']['new_password'] ), true );

					$this->output[] = array(
							'type'   => 'info',
							'msg'	 => 'La contraseña fue cambiada correctamente',
							'status' => 'success'
						);
					/*
					$this->output[] = array(
						'type'   => '_redirect',
						'url'    => site_url('')
					);
					*/
				} else {
					$this->output[] = array(
						'type'   => 'info',
						'msg'	 => 'Las contraseñas no coinciden',
						'status' => 'danger'
					);
				}
			} 
		}
	}

	public function after_action ()
	{
		header('Content-type: application/json');
		echo json_encode($this->output);
		die;
	}

	public function deleteAccount()
	{
		require_once(ABSPATH.'wp-admin/includes/user.php' );
		$current_user = wp_get_current_user();
		$is_admin = is_super_admin( $current_user->ID );

		$out = array(
			'type'   => 'info',
			'status' => 'danger'
		);

		if( $is_admin ) {
			$out['msg'] = ' No se puede eliminar una cuenta super admin ';
		} else {
			$out['msg'] = ' la cuenta fue eliminada correctamente ';
			$out['url'] = site_url( '/' );
			wp_delete_user( $current_user->ID );
		}
		//
		

		$this->output[] = $out;
		
	}
}