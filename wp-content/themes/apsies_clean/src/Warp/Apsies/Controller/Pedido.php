<?php
/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies\Controller;

class Pedido extends Controller
{
	public function detalle()
	{
		
		$id = intval( $_POST['id'] );
		
		$pedido = \Warp\Apsies\Wp\Pedido::get($id);

		$data = $this->system['template']->render('pedido/page/detalle', array( 'current_pedido' => $pedido ) );
		//array( 'content' => 'pedido/page/detalle'
	    $this->output = array( 'html' => $data );
	}

	public function after_action ()
	{
		header('Content-type: application/json');
		echo json_encode($this->output);
		die;
	}
	
}