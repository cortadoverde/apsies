<?php
/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies\Controller;

class Template extends Controller
{

	public $data = array();

	public function load()
	{
		$this->data = array('status' => 404);
		$request = func_get_args();
		
		if( isset ( $_POST['template'] ) ) {
		
			if( $this->get( $_POST['template'] ) ) {
				$this->data['status'] = 404;
			}

		}
		
		return $this->data;
	}


	private function get( $path )
	{
		$base = dirname(dirname(dirname(dirname(__DIR__)))); 
	
		if( $base !== false ) {
			$filename = $base . '/layouts/' . $path . '.mustache';
			$this->data['path'] = $filename;
			if( file_exists( $filename ) ) {
				$this->data['status'] = 200;
				$this->data['template'] = self::getFile( $filename );
				return true;
			} 
		}
		return $filename;
		return false;

	}

	public function partial( $name, $recursive = true )
    {

        $tpl = Data::getInstance()->Template->engine->getLoader()->load($name);
        $tpl = preg_replace_callback('#{{>\s?([\w\\\\/]+)\s?}}#', array($this, 'replace_callback'), $tpl);
        return $tpl;

    }

    public function replace_callback( $p )
    {
        try {
            $tpl = Data::getInstance()->Template->engine->getLoader()->load($p[1]);
            $tpl = preg_replace_callback('#{{>\s?([\w\\\\/]+)\s?}}#', array($this, 'replace_callback'), $tpl);

        }catch (\Exception $e) {
            return 'Error al cargar el template ( ' . $p[0] . ')';
        }
        
        return $tpl;
    }


    public static function getFile( $filename )
    {
        if( file_exists( $filename ) ) {
            return file_get_contents( $filename );
        }
    }


}