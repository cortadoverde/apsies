<?php
/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies;

class Ajax
{
	public function response( $content )
	{
		$type = ( isset ( $_GET['response_type' ] ) && in_array( $_GET['response_type'], array('json', 'html', 'debug' )  ) ) ? $_GET['response_type'] : 'json';
		
		self::output( $content, $type);
		
	}

	private function output( $content, $type = 'text' )
	{
		self::setHeader( $type );
		
		switch ( $type ) {
			case 'json':
				echo json_encode($content);
				break;
			case 'debug' : 
				echo '<pre>' . print_r( $content, true ) .'</pre>';
				break;
			default:
				echo $content;
				break;
		}
		return;
		
	}

	private function setHeader( $mimeType )
	{
		$values = array(
			'json' => array(
				'Content-Type: application/json'
			),
			'html' => array(
				'Content-Type: text/html'
			),
			'js' => array(
				'Content-Type: text/html'
			),
			'default' => array(
				'Content-Type: text/plain'
			)
		);

		$headers = ( ! isset ( $values[ $mimeType ] ) ) ? $values[ 'default' ] : $values[ $mimeType ];
		foreach( $headers as $header ) {
			header( $header );
		} 
		return;
	}
}