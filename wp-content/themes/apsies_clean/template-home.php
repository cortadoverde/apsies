<?php
/*
 * Template Name: Pagina principal
 * Description: A Page Template with a darker design.
 */

// get warp
$warp = require(__DIR__.'/warp.php');

$warp['apsies']->init();

$template = 'home';

if( $warp['user']->is_login() ) {
	$template = 'profile';
} 



// echo '<pre>';
// print_r( $warp );
// echo '</pre>';
// load main theme file, located in /layouts/theme.php
echo $warp['template']->render($template);