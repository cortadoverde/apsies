<?php

wp_enqueue_script( 'user-profile' );

// get warp
$warp = require(__DIR__.'/warp.php');

$warp['apsies']->init();

if( isset( $_SESSION['pending_action']['pedido']['confirmacion'] ) ) {
    header('Location:' . site_url( '/pedidos?step=confirmacion' ) );
}


if( ! $warp['user']->me->is_login )
	header('Location:' . site_url('/wp-login.php?action=register') );



echo $warp['template']->render('profile');