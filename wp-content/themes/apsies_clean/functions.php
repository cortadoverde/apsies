<?php
/**
* @package   Master
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

// check compatibility
if (version_compare(PHP_VERSION, '5.3', '>=')) {

    // bootstrap warp
    $warp = require(__DIR__.'/warp.php');

    $warp['apsies']->init();
}

$theme_base_dir = dirname( dirname(__FILE__) ) . '/apsies';
include($theme_base_dir . '/inc/content-functions.php');

load_plugin_textdomain('theme-my-login' , get_site_url() . '/wp_content/plugins/theme-my-login/languages/');

// include($theme_base_dir . '/inc/content-types.php');
// include($theme_base_dir . '/inc/ajax-requests.php');

