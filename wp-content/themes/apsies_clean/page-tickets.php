<?php

	$warp = require(__DIR__.'/warp.php');

	$query = new \Wp_Query(
		array(
			'posts_per_page' => -1,
			'post_status' => 'any',
			'post_type' => 'ticket'
		)
	);

	$posts = $query->get_posts();

	foreach($posts as $post) {
		$ticket = new \Warp\Apsies\Wp\Ticket( $post );
		if( ( isset( $ticket->Pedido ) && $ticket->Pedido->model->user_id == $warp['user']->me->ID ) || $ticket->model->to_user == $warp['user']->me->ID ) {
			$items[] = $ticket;
		}

	}



	
$page = 'tickets/index';


echo $warp['template']->render('pedido', array( 'content' => $page, '_args' => array( 'tickets' => $items ) ) );
