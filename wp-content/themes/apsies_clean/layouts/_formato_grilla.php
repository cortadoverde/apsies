[grid]
	[column size="10-10" large="4-10"]
	<h1>Eso/Bachiller</h1>
	<p>We’ve created the fastest and most convenient cloud technology to help you easily and more efficiently manage your infrastructure so you can get back to coding. We provide all of our users with high-performance SSD Hard Drives, a flexible API, and the ability to select the nearest data center location.</p>

	[button action="link" class="uk-button-large uk-button-blue"] Registrate [/button]

	[/column]

	[column size="10-10" large="6-10"]
		[video id="EnniszFD6D0"]
	[/column]
[/grid]

[grid class="icons"]
	<h3>Titulo para la grilla de iconos</h3>

	[column size="1-1" phone="1-2" large="2-6"]
		[icon icon="home" size="large"]
		<h2>Lorem impsu</h2>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet odio, quae amet doloribus eligendi doloremque eos, assumenda quisquam dolore repellat numquam et eius, voluptatum minus. Eaque ipsam, iste laborum eos!</p>
	[/column]

	[column size="1-1" phone="1-2" large="2-6"]
		[icon icon="home" size="large"]
		<h2>Lorem impsu</h2>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet odio, quae amet doloribus eligendi doloremque eos, assumenda quisquam dolore repellat numquam et eius, voluptatum minus. Eaque ipsam, iste laborum eos!</p>
	[/column]

	[column size="1-1" phone="1-2" large="2-6"]
		[icon icon="home" size="large"]
		<h2>Lorem impsu</h2>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet odio, quae amet doloribus eligendi doloremque eos, assumenda quisquam dolore repellat numquam et eius, voluptatum minus. Eaque ipsam, iste laborum eos!</p>
	[/column]

	[column size="1-1" phone="1-2" large="2-6"]
		[icon icon="home" size="large"]
		<h2>Lorem impsu</h2>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet odio, quae amet doloribus eligendi doloremque eos, assumenda quisquam dolore repellat numquam et eius, voluptatum minus. Eaque ipsam, iste laborum eos!</p>
	[/column]

	[column size="1-1" phone="1-2" large="2-6"]
		[icon icon="home" size="large"]
		<h2>Lorem impsu</h2>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet odio, quae amet doloribus eligendi doloremque eos, assumenda quisquam dolore repellat numquam et eius, voluptatum minus. Eaque ipsam, iste laborum eos!</p>
	[/column]

	[column size="1-1" phone="1-2" large="2-6"]
		[icon icon="home" size="large"]
		<h2>Lorem impsu</h2>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet odio, quae amet doloribus eligendi doloremque eos, assumenda quisquam dolore repellat numquam et eius, voluptatum minus. Eaque ipsam, iste laborum eos!</p>
	[/column]
[/grid]

[grid class="text_imagen"]
	<h3>Titulo</h3>

	[column size="1-1" phone="2-2" tablet="1-2"]
		<img src="https://www.digitalocean.com/assets/images/features/features-global-image-transfer-1e194cc5.png" alt="" />
	[/column]

	[column size="1-1" phone="2-2" tablet="1-2"]
		<h2>Hola</h2>
		<p>
			Transfer a copy of your Droplet snapshot to all regions (Amsterdam, San Francisco, New York, London and Singapore). Once you have transferred your snapshot to all regions, you will be able to spin up your image snapshot in any region from the Droplet create page.
		</p>
	[/column]
[/grid]

[grid class="text_imagen"]
	[column size="1-1" phone="2-2" tablet="1-2"]
		<h2>Hola</h2>
		<p>	
			Transfer a copy of your Droplet snapshot to all regions (Amsterdam, San Francisco, New York, London and Singapore). Once you have transferred your snapshot to all regions, you will be able to spin up your image snapshot in any region from the Droplet create page.
		</p>
	[/column]
	[column size="1-1" phone="2-2" tablet="1-2"]
		<img src="https://www.digitalocean.com/assets/images/features/features-global-image-transfer-1e194cc5.png" alt="" />
	[/column]
[/grid]

[grid]
	[column center="1" class="uk-text-center" size="2-3" large="4-10"]
		[button action="link" class="uk-button-large uk-button-blue"] Suscríbete [/button]</p>
	[/column]
[/grid]