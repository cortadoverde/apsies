<?php
	/**
	 * Metabox Pedidos
	 */
?>
<?php
	echo $this['template']->render('pedido/page/detalle', array( 'current_pedido' => $current_pedido ) );
?>

<hr>
<h3>Operaciones</h3>
<p>
	<label>
		<input type="checkbox" name="_baja_pedido" value="1" >
		Dar de baja el pedido
	</label>
</p>
<p>
	<label for="_pedido_confirmacion_baja">
		Motivo de la baja:
	</label> <br>
	<textarea name="_baja_pedido_motivo" id="" cols="30" rows="10"></textarea>
</p>
