
<?php
	

	$current_level = ( $this['user']->me->level !== NULL ) ? $this['user']->me->level : 1;
?>

<form action="user" class="uk-form-horizontal">
	
	<div class="uk-form-row">
		<label for="user_name" class="uk-form-label">
			Nombre
		</label>
		<div class="uk-form-controls">
			<input type="text" name="user[first_name]" value="<?php echo $this['user']->me->first_name ?>" id="user_name">
		</div>
	</div>
	
	<div class="uk-form-row">
		<label for="user_lastname" class="uk-form-label">
			Apellido
		</label>
		<div class="uk-form-controls">
			<input type="text" name="user[last_name]" value="<?php echo $this['user']->me->last_name ?>" id="user_lastname">
		</div>
	</div>
	
	<div class="uk-form-row">
		<label for="user_age" class="uk-form-label">
			Edad
		</label>

		<div class="uk-form-controls">
			<div class="uk-width-1-1 uk-width-medium-1-3">
				<div class="number">
					<a href="#" class="uk-button"><span class="uk-icon-minus"></span></a>
					<input type="text" id="user_age" name="user[age]" value="<?php echo $this['user']->me->age ?>">
					<a href="#" class="uk-button"><span class="uk-icon-plus"></span></a>
				</div>
			</div>			
		</div>
	</div>

	<div class="uk-form-row">
		<span class="uk-form-label">Nivel académico</span>
		<div class="uk-form-controls uk-form-controls-text">
			<label><input type="radio" name="user[level]" value="1"  <?php if( $current_level == 1 ) { echo 'checked'; }?>> Eso/Bachiller </label>
			<label><input type="radio" name="user[level]" value="2"  <?php if( $current_level == 2 ) { echo 'checked'; }?>> Universitario </label>
		</div>
	</div>


	<div class="toggle eso <?php if( $current_level == 2 ) { echo 'uk-hidden'; }?>" >
		<div class="uk-form-row">
			<label for="user_eso_institute" class="uk-form-label">
					Colegio donde estudias
			</label>
			<div class="uk-form-controls">
				<input type="text" name="user[institute]" value="<?php echo $this['user']->me->institute ?>" id="user_eso_institute">
			</div>
		</div>

		<div class="uk-form-row">
			<label for="user_grade" class="uk-form-label">
					Curso actual
			</label>
			<div class="uk-form-controls">
				<input type="text" name="user[grade]" value="<?php echo $this['user']->me->grade ?>" id="user_grade">
			</div>
		</div>		
	</div>

	<div class="toggle uni <?php if( $current_level == 1 ) { echo 'uk-hidden'; }?>" >
		<div class="uk-form-row">
			<label for="user_uni_institute" class="uk-form-label">
					Universidad donde estudias
			</label>
			<div class="uk-form-controls">
				<input type="text" name="user[institute]" value="<?php echo $this['user']->me->institute ?>" id="user_uni_institute">
			</div>
		</div>

		<div class="uk-form-row">
			<label for="user_career" class="uk-form-label">
					Estudios que cursas
			</label>
			<div class="uk-form-controls">
				<input type="text" name="user[career]" value="<?php echo $this['user']->me->career ?>" id="user_career">
			</div>				
		</div>		
	</div>

	<div class=" uk-margin-top">
		<button type="submit" class="uk-button uk-button-blue btn-guardar">Guardar</button>
	</div>
</form>