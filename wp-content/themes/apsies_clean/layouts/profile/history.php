
<table class="uk-table uk-table-hover">
	<thead>
		<tr>
		    <th rowspan="2" class="uk-text-center">Fecha y Hora</th>
		    <th colspan="3" class="uk-text-center">Ingresos</th>
		    <th rowspan="2" class="uk-text-center">Metodo de pago</th>
	  	</tr>
		<tr>
		    <th class="uk-text-center">Tipo</th>
		    <th class="uk-text-center">Puntos / Días </th>
		    <th class="uk-text-center">Precio</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td> 2014-01-01 00:00:00 </td>
			<td> Recarga de puntos </td>
			<td> 10 pts   </td>
			<td> 10 Euros </td>
			<td> Paypal   </td>
		</tr>

		<tr>
			<td> 2014-01-01 15:00:00 </td>
			<td> Suscripsión B</td>
			<td> 90 días </td>
			<td> 25 Euros </td>
			<td> Paysafecard </td>
		</tr>
	</tbody>
</table>