<?php
	$user_email = ( $this['user']->me->custom_email === NULL ) ? $this['user']->me->data->user_email  : $this['user']->me->custom_email;
?>


<form action="account" class="uk-form-horizontal">
	
	<div class="uk-form-row">
		<label for="user_email" class="uk-form-label">
			Correo electronico
		</label>
		<div class="uk-form-controls">
			<input type="text" name="user[custom_email]" value="<?php echo $user_email ?>" id="user_email">
		</div>
	</div>
	
	<div class="uk-form-row">
		<label for="new_password" class="uk-form-label">
			Nueva contraseña
		</label>
		<div class="uk-form-controls">
			<input type="password" name="user[new_password]" id="pass1">
		</div>
	</div>

	<div class="uk-form-row">
		<label for="repeat_password" class="uk-form-label">
			Vuelva a escribir la contraseña nueva
		</label>
		<div class="uk-form-controls">
			<input type="password" name="user[repeat_password]" id="pass2">
		</div>
	</div>

	<div class="uk-form-row">
		<span class="uk-form-label">
			
		</label>
		<div class="uk-form-controls">
			<div id="pass-strength-result">Indicador de fortaleza</div>
		</div>
	</div>
	
	<div class="uk-alert uk-alert-warning">
		<h2>Atención</h2>
		<p> si desea dar de baja la cuenta perdera toda la información generada hasta el momento</p>
		<button class="uk-button uk-button-danger baja">dar de baja</button>
	</div>

	<div class="uk-margin-top">
		<button type="submit" class="uk-button uk-button-blue btn-guardar">Guardar</button>
	</div>
</form>