<section class="profile">
	<div class="uk-container uk-container-center">
		<!-- <div class="uk-article">
		    <h1 class="uk-article-title">Bienvenido</h1>
		    <p class="uk-article-meta">suscrito</p>
		    <p class="uk-article-lead">1000 pts</p>
		    <p>Hola</p>
		    <hr class="uk-article-divider">
		    <p>Chau</p>
		</div>
 -->	

		<div class="uk-grid">
			<div class="uk-width-medium-2-10">
				<ul class="uk-tab uk-tab-left" data-uk-tab="{connect:'#my-id'}">
			        <li class="uk-active"><a href="#">Perfil</a></li>
			        <li><a href="#">Configuración</a></li>
			        <li><a href="#">Historial de pagos</a></li>
			    </ul>
			</div>
			<div class="uk-width-medium-8-10">
				<ul id="my-id" class="uk-switcher">
					<li>
						<?php echo $this['template']->render('profile/user'); ?>
					</li>
					<li>
						<!-- Configuracion -->
						<?php echo $this['template']->render('profile/settings'); ?>
					</li>
					<li>
						<?php echo $this['template']->render('profile/history'); ?>
					</li>
				</ul>
			</div>
			
		</div>
		
   	
</ul>
 	</div>	
</section>