<div class="toolbar">
	<div class="uk-grid uk-grid-small">
		<div class="uk-width-2-3 uk-width-medium-4-10">
			<div class="suscripcion section">				
				<div class="uk-grid uk-grid-small">
					<div class="uk-width-10-10 uk-text-center label">
						Te quedan 9 días de suscripción
					</div>
					<div class="uk-width-9-10">
						<div class="uk-progress">
						    <div class="uk-progress-bar" style="width: 40%;">40%</div>
						</div>
					</div>
					<div class="uk-width-1-10">
						<a href="#"><span class="uk-icon-plus"></span></a>
					</div>
				</div>
			</div>
		</div>
		<div class="uk-width-1-3 uk-width-medium-3-10">
			<div class="pedidos section uk-text-center">
				<div class="uk-grid uk-grid-small">
					<div class="uk-width-10-10 uk-text-center label">
						Pedidos gratuitos
					</div>
					<div class="uk-width-10-10">
						<span class="uk-icon-gift"></span> 2
					</div>
				</div>				
			</div>
		</div>
		<div class="uk-width-1-1 uk-width-medium-3-10">
			<div class="puntos section">
				<div class="uk-grid uk-grid-small">
					<div class="uk-width-10-10 uk-text-center label">
						Puntos
					</div>
					<div class="uk-width-9-10 uk-text-right show">
						<span class="uk-icon-heart"></span> <strong>1500</strong>
					</div>
					<div class="uk-width-1-10">
						<a href="#"><span class="uk-icon-plus"></span></a>
					</div>
				</div>	
			</div>	
		</div>
	</div>	
</div>