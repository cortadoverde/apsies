<h2>HISTORIAL DE PEDIDOS</h2>
<h3>Pedidos realizados y soluciones recibidas</h3>



<!-- This is the modal -->
<div id="detalle_pedido_modal" class="uk-modal">
    <div class="uk-modal-dialog uk-modal-dialog-large">
        <a class="uk-modal-close uk-close"></a>
        <div class="detalle"></div>
    </div>
</div>

<table class="uk-table _pedidos">
	<thead>
		<tr>
			<th>Identificador <span class="uk-icon-barcode"></span></th>
			<th>Pedido <span class="uk-icon-file-text"></span></th>
			<th>Enviado <span class="uk-icon-paper-plane"></span></th>
			<th>Fecha de entrega <span class="uk-icon-calendar"></span></th>
			<th>Estado <span class="uk-icon-signal"></span></th>
			<th>Descarga <span class="uk-icon-cloud-download"></span></th>
		</tr>
	</thead>
	<tbody>
		<?php
			foreach( $pedidos AS $pedido ) : 
				$confirmado = $pedido->model->post_status != 'draft';
				$single     = !$confirmado;
			?>
			<tr>
				<td>
					<?php echo $pedido->model->ID ?>
				</td>
				<td>
					<a href="#<?php echo $pedido->model->ID?>" rel="<?php echo $pedido->model->ID?>" class="open_detalle"> <?php echo $pedido->model->post_title?> </a>
				</td>
				<td>
					<?php echo date( 'Y-m-d' , strtotime( $pedido->model->post_date ) ) ?>
				</td>
				<td>
					<?php if( $confirmado )
							echo date( 'Y-m-d', ( strtotime( $pedido->model->post_date ) + ( 60 * 60 * $pedido->model->_data['form']['fecha_entrega'] ) ) ); ?>
				</td>
				<td>
					<?php
						if( $pedido->model->post_status == 'draft' ) :
							?>
							<strong class="draft">Sin enviar</strong>
							<br>
							<a href="<?php echo site_url('pedidos?step=confirmacion&res_id=' . $pedido->model->ID );?>">
								<span class="uk-icon-check-circle icon_action green"></span>
							</a>
							<a href="<?php echo site_url('pedidos?step=delete&res_id=' . $pedido->model->ID );?>" data-action="delete">
								<span class="uk-icon-times-circle icon_action red"></span>
							</a>
							
							<?php 
						elseif( isset( $pedido->model->solucion ) ) :
							?>
								<strong class="success">Solucionado</strong>
							<?php 
						else:
							?>
							<strong class="pending">En proceso</strong>
							<?php 
						endif;
					?>
				</td>
				<td>
					<?php if( isset( $pedido->model->solucion ) ) : ?>
						<a href="<?php echo site_url('descargar?res_id=' . $pedido->model->ID );?>">
							<span class="uk-icon-file-o icon_action"></span>
						</a>
					<?php endif; ?>
				</td>
			</tr>
			<?php
			endforeach;
		?>
	</tbody>
</table>



