<?php echo $this['template']->render('components/forms/eso/resolucion_ejercicios'); ?>

<div class="colegio-pedido-instantaneo form-pedido" style="display: block;">
	<!-- Nivel 0 -->
	<div class="form-label">
		<label for="eso_resolucion_ejercicio_tipo_pedido"> Tipo de pedido </label>

		<select id="eso_resolucion_ejercicio_tipo_pedido" data-nivel="0" class="form-element action" style="margin-bottom:8px;">
			<option value="0">-- Tipo de Pedido --</option>
			<option value="1">Resolución de ejercicios</option>
			<option value="2">Redacción sobre un tema</option>
			<option value="3">Comentario de texto</option>
			<option value="4">Comentario artístico</option>
			<option value="5">Crítica literaria</option>
			<option value="6">AUTOCAD</option>
			<option value="7">Dudas/Investigacion sobre un tema</option>
			<option value="8">Trabajos</option>
		</select>
	</div>
	<!-- End Nivel 0 -->

	<!-- Nivel 1 -->
	<div class="form-label">
		<label for="eso_resolucion_ejercicios_asignaturas">Asignatura</label>
		
		<select data-nivel="1" data-valor="1" class="form-element action asignatura" id="eso_resolucion_ejercicios_asignaturas" style="margin-right: 18px; display: none;">
			<?php 
				foreach( $this['forms']->asignaturas AS $name => $data ) : 
					$selected = ( $data['id'] == $this['config']['eso']['resolucion_ejercicios']['asignatura'] ) ? ' selected = "selected" ' : '';

		
					if ( isset( $this['config']['eso']['resolucion_ejercicios'][$name]['active'] ) ) :
				?>
						<option value="<?php echo $data['id']?>" <?php echo $selected ?>><?php echo $data['name']?></option>

				<?php
					endif; 
				endforeach;
			?>
		</select>
	</div>		
</div>
