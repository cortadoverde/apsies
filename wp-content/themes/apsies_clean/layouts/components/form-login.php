<div id="loaderImage" style="margin: 0 auto; display: none; margin-top: 15px;"></div>

<script type="text/javascript">
	var cSpeed=9;
	var cWidth=150;
	var cHeight=14;
	var cTotalFrames=37;
	var cFrameWidth=150;
	var cImageSrc='<?php echo get_template_directory_uri(); ?>/images/sprites.gif';
	
	var cImageTimeout=false;
	var cIndex=0;
	var cXpos=0;
	var cPreloaderTimeout=false;
	var SECONDS_BETWEEN_FRAMES=0;
	
	function startAnimation(){
		
		document.getElementById('loaderImage').style.backgroundImage='url('+cImageSrc+')';
		document.getElementById('loaderImage').style.width=cWidth+'px';
		document.getElementById('loaderImage').style.height=cHeight+'px';
		
		//FPS = Math.round(100/(maxSpeed+2-speed));
		FPS = Math.round(100/cSpeed);
		SECONDS_BETWEEN_FRAMES = 1 / FPS;
		
		cPreloaderTimeout=setTimeout('continueAnimation()', SECONDS_BETWEEN_FRAMES/1000);
		
	}
	
	function continueAnimation(){
		
		cXpos += cFrameWidth;
		//increase the index so we know which frame of our animation we are currently on
		cIndex += 1;
		 
		//if our cIndex is higher than our total number of frames, we're at the end and should restart
		if (cIndex >= cTotalFrames) {
			cXpos =0;
			cIndex=0;
		}
		
		if(document.getElementById('loaderImage'))
			document.getElementById('loaderImage').style.backgroundPosition=(-cXpos)+'px 0';
		
		cPreloaderTimeout=setTimeout('continueAnimation()', SECONDS_BETWEEN_FRAMES*1000);
	}
	
	function stopAnimation(){//stops animation
		clearTimeout(cPreloaderTimeout);
		cPreloaderTimeout=false;
	}
	
	function imageLoader(s, fun)//Pre-loads the sprites image
	{
		clearTimeout(cImageTimeout);
		cImageTimeout=0;
		genImage = new Image();
		genImage.onload=function (){cImageTimeout=setTimeout(fun, 0)};
		genImage.onerror=new Function('alert(\'Could not load the image\')');
		genImage.src=s;
	}
	
	//The following code starts the animation
	new imageLoader(cImageSrc, 'startAnimation()');
</script>


<?php if( ! $this['user']->me->is_login ) { ?> 
<div class="login_form" id="theme-my-login">

	<form name="loginform" id="loginform" action="" method="post">
		<?php do_action( 'login_form' ); ?>
		<div class="form">
			<input placeholder="usuario" type="text" name="log" id="user_login" class="input" value="" size="20" />
			<input placeholder="contraseña" type="password" name="pwd" id="user_pass" class="input" value="" size="20" />
			<input type="submit" value="ACCEDER" id="submitLogin">

			<input type="hidden" name="redirect_to" value="" />
			<input type="hidden" name="action" value="login" />
		</div>
		<div class="links">
			<div class="links">
			<ul class="tml-action-links">
				<li><a href="<?php echo site_url('register');?>" rel="nofollow">Crea tu cuenta</a></li>
				<li><a href="<?php echo site_url('lostpassword');?>" rel="nofollow">Contraseña perdida</a></li>
			</ul>
		</div>
		</div>
	</form>
</div>
<?php } else { ?>
<div id="user-data">
	<ul>
		<li>
			<a href="<?php bloginfo( 'url' ); ?>/perfil" title="Perfil">
				Bienvenido <?php echo $this['user']->me->data->display_name; ?>
			</a>
		</li>
		<li>
			<a href="<?php bloginfo( 'url' ); ?>/pedidos" title="Realizar Pedidos">
				<span class="uk-icon-large uk-icon-home"></span>
			</a>
		</li>
		<li>
			<a href="<?php bloginfo( 'url' ); ?>/tickets" title="Mensajes">
				<span class="uk-icon-large uk-icon-envelope"></span>
			</a>
		</li>
		<li>
			<a href="<?php bloginfo( 'url' ); ?>/historial" title="Historial de Pedidos">
				<span class="uk-icon-large uk-icon-file-text"></span>
			</a>
		</li>
		<li>
			<a id="logout" href="<?php echo wp_logout_url( home_url() ); ?>" data-redirect="<?php echo $redirect; ?>" title="Salir">
				<span class="uk-icon-large uk-icon-power-off"></span>
			</a>
		</li>						
	</ul>
</div>


<?php } ?>