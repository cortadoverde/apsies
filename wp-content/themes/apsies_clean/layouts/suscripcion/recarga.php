<?php
	$plan = false;

	if( isset( $_SESSION['pending_action']['suscripcion'] ) && $this['user']->me->is_login ) {
		$plan = $_SESSION['pending_action']['suscripcion'];
	}
?>

<h2>Suscripción</h2>

<div class="uk-grid  uk-grid-small icons ">

	<div class="uk-width-1-1 uk-width-small-1-2 uk-width-large-2-6">
		<i class="uk-icon-tag uk-icon-large"></i>
		<h2>Descuento en pedidos</h2>
		<p>Todos los usuarios suscritos recibirán un 15% de descuento en cualquier pedido que realicen mientras dure el período de suscripción. Ideal para los usuarios más fieles. </p>
	</div>

	<div class="uk-width-1-1 uk-width-small-1-2 uk-width-large-2-6">
		<i class="uk-icon-file uk-icon-large"></i>
		<h2>Pedidos gratuitos</h2>
		<p>Los usuarios ESO/Bachiller que estén suscritos al servicio, podrán realizar hasta 3 pedidos especiales diarios de manera gratuita. De esta manera podrán enviar todos los deberes diarios que tengan.</p>
	</div>

	<div class="uk-width-1-1 uk-width-small-1-2 uk-width-large-2-6">
		<i class="uk-icon-certificate uk-icon-large"></i>
		<h2>Golden Member</h2>
		<p>La página personal de los usuarios suscritos recibirá un cambio estético. Queremos que los usuarios más fieles se sientan destacados de alguna forma</p>
	</div>

</div>




<div class="panel_user suscripcion">
	

	<form action="" method="POST" class="form_puntos">
		<div class="steps">
			<div class="step_1 step_container <?php if( $plan !== false ) { echo 'uk-hidden'; } ?>">
				<h3>1. Elija una opcion</h3>
				
				<div class="opciones">
					<div class="left">
						<input type="radio" name="suscripcion[type]" value="1" <?php if( $plan == false || $plan == 1 ) { echo 'checked'; } ?>  id="check_type_1" class="uk-hidden">
						<label for="check_type_1">
						</label>
					</div>

					<div class="left">
						<input type="radio" name="suscripcion[type]" value="2" <?php if( $plan == 2 ) { echo 'checked'; } ?> id="check_type_2" class="uk-hidden">
						<label for="check_type_2">
						</label>
					</div>
				</div>
				
			</div>
			<div class="step_2 step_container  <?php if( $plan == false ) { echo 'uk-hidden'; } ?>">
				<h3>2. Seleccione el método de pago</h3>
				<div class="uk-clearfix">
					<div class="button_radio">
						<input type="radio" name="recarga[metodo_pago]" value="paysafecart" id="metod_paysafecard" class="uk-hidden">
						<label for="metod_paysafecard">							
							<img src="<?php echo $this['path']->url('theme:/images/logo_paysafecard.png')?>" alt="paysafecard">
						</label>
					</div>

					<div class="button_radio">
						<input type="radio" name="recarga[metodo_pago]" value="paypal" id="metod_paypal" class="uk-hidden">
						<label for="metod_paypal">							
							<img src="<?php echo $this['path']->url('theme:/images/logo_paypal.png')?>" alt="paysafecard">
						</label>
					</div>
				</div>
			</div>
		</div>
		<div class="uk-clearfix uk-margin-top">
			<div class="uk-float-left">
				<a href="#" class="uk-button action_recarga prev uk-hidden" > volver </a> 
			</div>
			<div class="uk-float-right next">
				<a href="#" class="uk-button action_recarga next <?php if( ! $this['user']->me->is_login ) { echo 'check'; }?>" > continuar </a> 
				<a href="#" class="uk-button action_recarga submit uk-hidden" > Finalizar </a> 
			</div>
		</div>


	</form>
</div>