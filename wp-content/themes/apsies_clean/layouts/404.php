<?php 
	$_type = isset ($type ) ? $type : 'error'; 

	$msj   = $_type == 'site_down' ? 'Ups hubo un error, en este momento el sitio esta desactivado, por favor intente más tarde o pongase en contacto con nosotros: info@apsies.com' 
				: 'Ups no se encuentra la página, por favor comprueba la url o pongase en contacto con nosotros: info@apsies.com';
	global $warp;
	
	$paths = $warp['config']->get('path', array());

	
	$image = get_template_directory_uri() . '/images/404.png';

	if( $_type == 'site_down' ): 
?>

</head>



<body class="page-404">

	<div class="uk-container uk-container-center ">
		<main class="tm-content">
<?php 
	endif;
?>
			<figure style="text-align:center">
				<img src="<?php echo $image?>" alt="">
			</figure>
			<h2 style="text-align:center"><?php echo $msj ?></h2>
<?php 
	if( $_type == 'site_down' ):
?>
		</main>

	</div>
</body>
</html>
<?php
	endif;
?>	