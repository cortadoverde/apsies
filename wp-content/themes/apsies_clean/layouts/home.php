<?php
/**
* @package   Master
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

// get theme configuration
include($this['path']->path('layouts:theme.config.php'));

?>

<?php echo $this['template']->render('site/head'); ?>
	
	<!-- Content -->
	<?php 
		if ( is_home() || is_front_page() ) { 
			echo $this['template']->render('site/home');
		} else {
		?>
			<div class="uk-container uk-container-center home main_content">
				<?php echo $this['template']->render( ( isset( $tpl_content ) ? $tpl_content : 'content' ) ); ?>
			</div>
		<?php
		}
	?>
<?php echo $this['template']->render('site/footer'); ?>		