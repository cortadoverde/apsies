<?php
/**
* @package   Master
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

// get theme configuration
include($this['path']->path('layouts:theme.config.php'));

?>

<?php echo $this['template']->render('site/head'); ?>
	

	<!-- Content -->
	<div class="uk-container uk-container-center">
		<div class="uk-clearfix wrap_pedido">
			
			<?php echo $this['template']->render('pedido/sidebar'); ?>
			
			<div class="back_user main_content">
				<?php echo $this['template']->render( $content, (array) $_args ); ?>
			</div>

		</div>

	</div>
	
<?php echo $this['template']->render('site/footer'); ?>		
