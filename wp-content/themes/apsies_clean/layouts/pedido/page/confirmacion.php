<?php 
	
	$btn_add =  ( $current_pedido->model->puntos > $this['user']->me->puntosusuario ) ? 'missing_points' : '';
	$confirmado = $current_pedido->model->post_status != 'draft';
	$tipo   = $this['user']->me->level == 1 ? 'eso' : 'uni';
	$newLlv = $this['user']->me->level == 1 ? 2 : 1 ;
	$coincide = ( $tipo == $current_pedido->model->_data['level'] ); 
?>


<form action="?step=confirmacion" method="POST" class="<?php echo $btn_add ?>">
	<input type="hidden" name="submit_confirmacion[pedido]" value="<?php echo $current_pedido->model->ID ?>">
	<h3>Informacion del pedido</h3>
	<article class="detalle">
		<?php 
			if( ! $coincide ) { ?>
				<div class="uk-alert uk-alert-warning">
					<h2>Atención, el tipo de pedido no coincide con tu nivel de usuario </h2>
					<label>
						<input type="checkbox" name="submit_confirmacion[cambio_level]" value="<?php echo $newLlv ?>">
						Cambiar de nivel a <?php echo ( ( $current_pedido->model->_data['level'] == 'uni' ) ? 'Universitario' : 'ESO/Bachillerato' ) ?>
					</label> 
					<span style="padding: 0 2em;font-weight: bold;font-size: 1.2em;">o</span> 
					<a class="uk-button uk-button-danger" href="<?php echo site_url('pedidos?step=delete&res_id=' . $current_pedido->model->ID );?>">cancelar pedido</a>

				</div>
				<?php
			}
		?>		

		<dl class="uk-description-list-horizontal">
		    <dt>Nivel</dt>
		    <dd><?php echo ( ( $current_pedido->model->_data['level'] == 'uni' ) ? 'Universitario' : 'ESO/Bachillerato' ) ?></dd>
			
			<dt>Fecha de entrega</dt>
			<dd><?php 
					if( $confirmado )
							echo date( 'Y-m-d', ( strtotime( $current_pedido->model->post_date ) + ( 60 * 60 * $current_pedido->model->_data['form']['fecha_entrega'] ) ) ); 
					else 
							echo '<span class="uk-text-warning">Debe confirmar el pedido primero</span>';
				?>
			</dd>

			<dt>Puntos necesarios</dt>
			<dd><?php echo $current_pedido->model->puntos ?></dd>

			<?php 
				$detalle_tpl = ( $current_pedido->model->_data['tipo'] == 'suscrito' ) 
								? 
									'suscrito/suscrito' 
								: 
									(
										$current_pedido->model->_data['level'] == 'eso'
										?
											$current_pedido->model->_data['idx']
										:
											'uni/' . $current_pedido->model->_data['idx']

									)
									
								; 


				echo $this['template']->render('pedido/detalles/' . $detalle_tpl, array('data' => $current_pedido->model->_data ) ) ; ?>
			
			<?php 
				if( isset( $current_pedido->model->_data['form']['observaciones']  ) && trim( $current_pedido->model->_data['form']['observaciones'] ) != '' ) {
				?>
				<dt>Observaciones</dt>
				<dd><?php echo nl2br( $current_pedido->model->_data['form']['observaciones'] ) ?></dd>
				<?php
				}
			?>

			<?php 
				if( $current_pedido->model->_data['adjuntos'] ) {
					$list = array();
					foreach( $current_pedido->model->_data['adjuntos'] AS $adjunto ) {
						$list[] = wp_get_attachment_link( $adjunto );
					}
					?>
					<dt>Adjuntos</dt>
					<dd><?php echo implode(', ', $list )?></dd>
					<?php
				}
			?>

			
				
		</dl>
<!-- 
		<pre>
			<?php print_r( $current_pedido->model->_data ) ?>
		</pre> -->
		
	
	</article>
	
	<!-- <h3>Informacion del pedido</h3>
	<article class="detalle">

		
		<table class="uk-table">
			<tr>
				<td>Nivel</td>
				<td><?php echo ( ( $current_pedido->model->_data['level'] == 'uni' ) ? 'Universitario' : 'ESO/Bachillerato' ) ?></td>
			</tr>
			<tr>
				<td>Fecha de entrega</td>
				<td><?php echo $current_pedido->entrega() ?></td>
			</tr>
			<tr>
				<td>Puntos necesarios</td>
				<td><?php echo $current_pedido->model->puntos ?></td>
			</tr>
			<tr>
				<td colspan="2">
					<?php echo $current_pedido->model->post_content ?>
				</td>
			</tr>
	
		</table>
	
	</article> -->
	
	<?php if( $current_pedido->model->puntos > $this['user']->me->puntosusuario ) :
		?>
		<div class="uk-alert uk-alert-warning uk-alert-large">
			<h2>Atencion</h2>
			<p>No tiene puntos suficientes para hacer el pedido, si desea continuar se reenviara a recargar puntos para terminar el proceso </p>
		</div>
		<?php
		endif;
	?>
	<div class="uk-text-center">
		<a class="uk-button uk-button-danger" href="<?php echo site_url('pedidos?step=delete&res_id=' . $current_pedido->model->ID );?>">cancelar</a>
		<button class="uk-button uk-button-blue">confirmar</button>
		
	</div>

</form>