	<?php
		$confirmado = $current_pedido->model->post_status != 'draft';
	?>

	<h3>Informacion del pedido</h3>
	<article class="detalle">
		<dl class="uk-description-list-horizontal">
		    <dt>Nivel</dt>
		    <dd><?php echo ( ( $current_pedido->model->_data['level'] == 'uni' ) ? 'Universitario' : 'ESO/Bachillerato' ) ?></dd>
			
			<dt>Fecha de entrega</dt>
			<dd><?php 
					if( $confirmado )
							echo date( 'Y-m-d', ( strtotime( $current_pedido->model->post_date ) + ( 60 * 60 * $current_pedido->model->_data['form']['fecha_entrega'] ) ) ); 
					else 
							echo '<span class="uk-text-warning">Debe confirmar el pedido primero</span>';
				?>
			</dd>

			<dt>Puntos necesarios</dt>
			<dd><?php echo $current_pedido->model->puntos ?></dd>

			<?php 
				$detalle_tpl = ( $current_pedido->model->_data['tipo'] == 'suscrito' ) 
								? 
									'suscrito/suscrito' 
								: 
									(
										$current_pedido->model->_data['level'] == 'eso'
										?
											$current_pedido->model->_data['idx']
										:
											'uni/' . $current_pedido->model->_data['idx']

									)
									
								; 


				echo $this['template']->render('pedido/detalles/' . $detalle_tpl, array('data' => $current_pedido->model->_data ) ) ; ?>
			
			<?php 
				if( isset( $current_pedido->model->_data['form']['observaciones']  ) && trim( $current_pedido->model->_data['form']['observaciones'] ) != '' ) {
				?>
				<dt>Observaciones</dt>
				<dd><?php echo nl2br( $current_pedido->model->_data['form']['observaciones'] ) ?></dd>
				<?php
				}
			?>

			<?php 
				if( $current_pedido->model->_data['adjuntos'] ) {
					$list = array();
					foreach( $current_pedido->model->_data['adjuntos'] AS $adjunto ) {
						$list[] = wp_get_attachment_link( $adjunto );
					}
					?>
					<dt>Adjuntos</dt>
					<dd><?php echo implode(', ', $list )?></dd>
					<?php
				}
			?>

			
				
		</dl>

		
		
	
	</article>


