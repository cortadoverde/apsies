<?php
	$current_user_level = ! isset( $this['user']->me->level ) ? 'uni' : ( $this['user']->me->level == 1 ? 'eso' : 'uni' );
	$dispone_pedidos    = ( $this['user']->me->is_suscripto &&  isset( $this['user']->me->pedidos_disponibles ) && $this['user']->me->pedidos_disponibles > 0 );
?>
<h2>Realiza un nuevo pedido</h2>
<h3>Mandanos las especificaciones para que podamos realizar tu trabajo</h3>

<!-- <pre>
	<?php print_r( ) ?>
</pre> -->

<?php 
	if( $dispone_pedidos  ) : 
		?>
			<p class="notification">
				Tienes <span><?php echo $this['user']->me->pedidos_disponibles ?></span> pedidos gratuitos disponibles
			</p>
		<?php 
	endif;
?>

<form action="<?php echo $this['config']['site_url'];?>/pedidos" method="post" class="apsies-form" id="dynamic_form">

<input type="hidden" name="submit_pedido[level]" value="<?php echo $current_user_level ?>">

<?php 
	if( $dispone_pedidos) : 
		?>
			<div class="uk-grid uk-grid-small">
				<label>
					<input type="radio" name="submit_pedido[pedido_suscrito]" value="0" checked>
					Pedido convencional
				</label>

				<label>
					<input type="radio" name="submit_pedido[pedido_suscrito]" value="1" >
					Pedido gratis por estar suscrito
				</label>
			</div>

			<div class="replace_form sus uk-margin-bottom uk-hidden">
				<?php echo $this['template']->render('site/forms/suscripto'); ?>
			</div>
		<?php 
	endif;
?>

<?php 
	if( $current_user_level == 'uni' ):
	?>
		<div class="replace_form uni uk-margin-bottom">
			<div class="switch-tipos">
				<?php echo $this['template']->render('site/forms/uni/tipos'); ?>
			</div>
			<?php echo $this['template']->render('site/forms/uni/trabajos'); ?>
			<?php echo $this['template']->render('site/forms/uni/autocad'); ?>
			<?php echo $this['template']->render('site/forms/uni/apuntes'); ?>
			<?php echo $this['template']->render('site/forms/uni/powerpoint'); ?>
			<?php echo $this['template']->render('site/forms/uni/exposiciones'); ?>
		</div>
	<?php
	else : 
	?>
		<div class="replace_form eso uk-margin-bottom">
			<div class="switch-tipos">
				<?php echo $this['template']->render('site/forms/eso/tipos'); ?>
			</div>
			<?php echo $this['template']->render('site/forms/eso/resolucion_ejercicios'); ?>
			<?php echo $this['template']->render('site/forms/eso/redaccion_tema'); ?>
			<?php echo $this['template']->render('site/forms/eso/comentario_texto'); ?>
			<?php echo $this['template']->render('site/forms/eso/comentario_artistico'); ?>
			<?php echo $this['template']->render('site/forms/eso/critica_literaria'); ?>
			<?php echo $this['template']->render('site/forms/eso/autocad'); ?>
			<?php echo $this['template']->render('site/forms/eso/dudas_investigacion'); ?>
			<?php echo $this['template']->render('site/forms/eso/trabajos'); ?>
		</div>
	<?php
	endif;
?>
<!-- Common -->
<div class="uk-grid uk-grid-small">
	<div class="uk-width-1-1 uk-width-medium-1-2">
	
		<div id="upload-drop" class="uk-placeholder uk-text-center">
            <i class="uk-icon-cloud-upload uk-icon-medium uk-text-muted uk-margin-small-right"></i> Arrastra aquí lo que quieras o <a class="uk-form-file">selecciona un archivo<input id="upload-select" type="file"></a>.
        </div>
	</div>
	<div class="uk-width-1-1 uk-width-medium-1-2">
		<div class="puntaje">
			<small>Precio por este pedido</small>
			<strong><span>0</span> puntos</strong> 
			<button class="uk-button uk-button-large uk-button-blue">Pruébalo</button>
		</div>

	</div>
</div>
					</form>