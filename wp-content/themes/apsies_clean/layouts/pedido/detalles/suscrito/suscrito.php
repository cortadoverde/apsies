<?php
	$form = $data['data']['suscrito'];


?>

<?php if( isset( $form['asignatura_nombre'] ) ) : ?>

	<dt>Asignatura</dt>
	<dd><?php echo $form['asignatura_nombre'] ?></dd>

<?php endif; ?>

<?php if( isset( $form['tipo_nombre'] ) ) : ?>

	<dt>Tipo</dt>
	<dd><?php echo $form['tipo_nombre'] ?></dd>

<?php endif; ?>

<?php 
	if( isset( $form['autocad']['tipo']) ) {
	?>
	<dt>Tipo de tarea</dt>
	<dd><?php echo $form['autocad']['tipo'] ?></dd>
	<?php
	}
?>


<?php 
	if( isset( $data['form']['incluye'] ) ) {
	?>
	<dt>Incluir</dt>
	<dd><?php echo implode(', ', $data['form']['incluye']) ?></dd>
	<?php
	}
?>

<dt>Enunciado</dt>
<dd><?php echo nl2br($data['form']['enunciado'])?></dd>