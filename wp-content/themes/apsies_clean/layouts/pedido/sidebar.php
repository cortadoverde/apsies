<div id="left-sidebar">
	<p id="tienes">Tienes</p>
	<p id="cantidad-puntos"><?php echo $this['user']->me->puntosusuario ?></p>
	<p id="puntos">Puntos</p>
	<hr>
	<p id="recarga" class="ns">
		<a href="<?php echo site_url('recargar')?>" title="Recarga ahora">Recarga ahora</a>
	</p>

	<table id="tabla-precios">
		<tbody><tr>
			<td>10 puntos</td>
			<td><span>10€</span></td>
		</tr>
		<tr>
			<td>30 puntos</td>
			<td><span>25€</span></td>
		</tr>
		<tr>
			<td>65 puntos</td>
			<td><span>50€</span></td>
		</tr>
		<tr>
			<td>150 puntos</td>
			<td><span>100€</span></td>
		</tr>
	</tbody></table>
	
	<hr>

	<p class="info">
		Recuerda que puedes suscribirte a nuestro servicio <br> disfrutando de enormes ventajas. <br> ¡Hazlo ya!		
	</p>
	
	<?php
		if( $this['user']->me->is_suscripto ) :
			?>
			<div id="time-suscription">
				Te quedan <span><?php echo $this['user']->me->dias_restantes ?></span> día<?php echo ( $this['user']->me->dias_restantes == 1 ? '' : 's' )?> de suscripcion <br>
				
				<div class="counter">
					<div class="uk-progress uk-progress-mini">
						<div class="uk-progress-bar" style="width: <?php echo $this['user']->me->percent_complete ?>%;"></div>
					</div>

					<div class="uk-clearfix">
					    <div class="uk-float-left">
					    	<small>0 días</small>
					    </div>
					    <div class="uk-float-right">
					    	<small><?php echo $this['user']->me->diassuscripcion ?> días</small>
					    </div>
					</div>
				</div>
				
			</div>
			<?php 
		endif;
	?>
	
	<a href="<?php echo site_url('suscripcion')?>" class="uk-button uk-button-blue">
		<?php echo $this['user']->me->is_suscripto ? 'Aumenta la suscripción' : 'Suscríbete' ?>
	</a>
		
</div>