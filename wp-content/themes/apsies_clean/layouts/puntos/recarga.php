<h2>RECARGAR</h2>
<h3>Recarga para realizar más pedidos</h3>


<form action="" method="POST" class="form_puntos">
	<div class="steps">
		<div class="step_1 step_container">
			<h3>1. Elija la cantidad de puntos a recargar</h3>

			<table class="uk-table table_price">
					<tr>
						<td>
							<div class="number">
								<a href="#" class="uk-button"><span class="uk-icon-minus"></span></a>
								<input type="text"  data-cant="10" data-price="10" name="puntos[10]" value="">
								<a href="#" class="uk-button"><span class="uk-icon-plus"></span></a>
							</div>
						</td>
						<td>
							10 puntos
						</td>
						<td>
							<span>10&euro;</span>
						</td>
					</tr>

					<tr>
						<td>
							<div class="number">
								<a href="#" class="uk-button"><span class="uk-icon-minus"></span></a>
								<input type="text"  data-cant="30" data-price="25" name="puntos[30]" value="">
								<a href="#" class="uk-button"><span class="uk-icon-plus"></span></a>
							</div>
						</td>
						<td>
							30 puntos
						</td>
						<td>
							<span>25&euro;</span>
						</td>
					</tr>

					<tr>
						<td>
							<div class="number">
								<a href="#" class="uk-button"><span class="uk-icon-minus"></span></a>
								<input type="text"  data-cant="65" data-price="50" name="puntos[65]" value="">
								<a href="#" class="uk-button"><span class="uk-icon-plus"></span></a>
							</div>
						</td>
						<td>
							65 puntos
						</td>
						<td>
							<span>50&euro;</span>
						</td>
					</tr>

					<tr>
						<td>
							<div class="number">
								<a href="#" class="uk-button"><span class="uk-icon-minus"></span></a>
								<input type="text"  data-cant="150" data-price="100" name="puntos[150]" value="">
								<a href="#" class="uk-button"><span class="uk-icon-plus"></span></a>
							</div>
						</td>
						<td>
							150 puntos
						</td>
						<td>
							<span>100&euro;</span>
						</td>
					</tr>


				</table>
		</div>
		<div class="step_2 step_container uk-hidden">
			<h3>2. Seleccione el método de pago</h3>
			<div class="uk-clearfix">
				<div class="button_radio">
					<input type="radio" name="recarga[metodo_pago]" value="paysafecart" id="metod_paysafecard" class="uk-hidden">
					<label for="metod_paysafecard">							
						<img src="<?php echo $this['path']->url('theme:/images/logo_paysafecard.png')?>" alt="paysafecard">
					</label>
				</div>

				<div class="button_radio">
					<input type="radio" name="recarga[metodo_pago]" value="paypal" id="metod_paypal" class="uk-hidden">
					<label for="metod_paypal">							
						<img src="<?php echo $this['path']->url('theme:/images/logo_paypal.png')?>" alt="paysafecard">
					</label>
				</div>
			</div>
		</div>

		<div class="uk-clearfix uk-margin-top">
			<div class="info_point">
				<span class="puntos">
					puntos: <strong>0</strong>
				</span>
				<span class="precio">
					costo : <strong>0</strong> €
				</span>
			</div>
		</div>	
	</div>
	<div class="uk-clearfix uk-margin-top">
		<div class="uk-float-left">
			<a href="#" class="uk-button action_recarga prev uk-hidden" > volver </a> 
		</div>
		<div class="uk-float-right next">
			<a href="#" class="uk-button action_recarga next" > continuar </a> 
			<a href="#" class="uk-button action_recarga submit uk-hidden" > Finalizar </a> 
		</div>
	</div>


</form>