<h2>TICKETS RECIBIDOS</h2>
<h3>Lista de todos los tickets recibidos</h3>


<div id="div-history">
	<table class="uk-table">
		<thead>
			<tr>
				<th>Resumen del Ticket</th>
				<th>Fecha</th>
				<th>Detalles</th>							
			</tr>
		</thead>
		<tbody>
			<?php
				if( empty( $tickets ) ) {
				?>
				<tr>
					<td colspan="3">
						<div class="uk-alert" data-uk-alert>
						    <p>No ha recibido tickets</p>
						</div>
					</td>
				</tr>
				<?php
				} else {
					foreach( $tickets AS $ticket ) :
					?>
					<tr class="<?php echo $trclass . $classNT; ?>">
						<td>
							<p><?php echo $ticket->model->post_title ?></p>
						</td>
						<td class="td-date"><p><?php echo $ticket->model->post_date ?></p></td>
						<td class="td-detalles"><p><a href="<?php echo get_permalink( $ticket->model->ID ) ?>">Ver m&aacute;s</a></p></td>																				
					</tr>
					<?php
					endforeach;
				}
			?>
		</tbody>
	</table>
</div>



