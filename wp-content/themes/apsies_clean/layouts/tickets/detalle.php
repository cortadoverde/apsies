<h2>Ticket recibido</h2>
<h3><?php echo $current_ticket->model->post_title ?></h3>

<article class="ticket">
	<div class="content">
		<?php echo $current_ticket->model->post_content ?>
	</div>

	<?php 
		if( isset( $current_ticket->Pedido ) ) :

			echo $this['template']->render('pedido/page/detalle', array( 'current_pedido' => $current_ticket->Pedido ) );
		endif;
	?>
	<h3>Deja un comentario sobre el Ticket al Administrador</h3>
	<?php comments_template(); ?>
</article>

