<?php
/**
* @package   Master
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

// get theme configuration
include($this['path']->path('layouts:theme.config.php'));

?>
<!DOCTYPE HTML>
<html lang="<?php echo $this['config']->get('language'); ?>" dir="<?php echo $this['config']->get('direction'); ?>"  data-config='<?php echo $this['config']->get('body_config','{}'); ?>' base="<?php echo $this['path']->url('theme:/')?>">

<head>
<?php echo $this['template']->render('head'); ?>
</head>

<body class="<?php echo $this['config']->get('body_classes'); ?>">

	<?php if ($this['widgets']->count('toolbar-l + toolbar-r')) : ?>
		<div class="uk-container uk-container-center">
			<div class="tm-toolbar uk-clearfix uk-hidden-small">

				<?php if ($this['widgets']->count('toolbar-l')) : ?>
				<div class="uk-float-left"><?php echo $this['widgets']->render('toolbar-l'); ?></div>
				<?php endif; ?>

				<?php if ($this['widgets']->count('toolbar-r')) : ?>
				<div class="uk-float-right"><?php echo $this['widgets']->render('toolbar-r'); ?></div>
				<?php endif; ?>

			</div>
		</div>
	<?php endif; ?>
	
	<?php if ($this['widgets']->count('logo + headerbar')) : ?>
		<div class="uk-container uk-container-center">
			<div class="tm-headerbar uk-clearfix">				
				<div class="uk-grid">
					<div class="uk-width-large-4-10 uk-width-small-4-4" >
						<?php if ($this['widgets']->count('logo')) : ?>
							<a class="tm-logo" href="<?php echo $this['config']->get('site_url'); ?>"><?php echo $this['widgets']->render('logo'); ?></a>
						<?php endif; ?>
					</div>
					<div class="uk-width-large-6-10 uk-width-small-4-4">
						<?php echo $this['widgets']->render('headerbar'); ?>
				
						<?php if ($this['widgets']->count('menu')) : ?>
							<?php echo $this['widgets']->render('menu'); ?>
						<?php endif; ?>
					</div>
				</div>
				
			</div>
		</div>
	<?php endif; ?>
	
	<!-- Content -->
	<?php 
		if ( is_home() || is_front_page() ) { 
			echo $this['template']->render('site/home');
		} else {
		?>
			<div class="uk-container uk-container-center">
				<?php echo $this['template']->render( ( isset( $tpl_content ) ? $tpl_content : 'content' ) ); ?>
			</div>
		<?php
		}
	?>
		<?php if ($this['widgets']->count('bottom-a')) : ?>
		<section class="<?php echo $grid_classes['bottom-a']; echo $display_classes['bottom-a']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin><?php echo $this['widgets']->render('bottom-a', array('layout'=>$this['config']->get('grid.bottom-a.layout'))); ?></section>
		<?php endif; ?>

		<?php if ($this['widgets']->count('bottom-b')) : ?>
		<section class="<?php echo $grid_classes['bottom-b']; echo $display_classes['bottom-b']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin><?php echo $this['widgets']->render('bottom-b', array('layout'=>$this['config']->get('grid.bottom-b.layout'))); ?></section>
		<?php endif; ?>

		<?php if ($this['widgets']->count('footer + debug') || $this['config']->get('warp_branding', true) || $this['config']->get('totop_scroller', true)) : ?>
		<footer class="tm-footer">

			<?php if ($this['config']->get('totop_scroller', true)) : ?>
			<a class="tm-totop-scroller" data-uk-smooth-scroll href="#"></a>
			<?php endif; ?>

			<?php
				echo $this['widgets']->render('footer');
				
				echo $this['widgets']->render('debug');
			?>

		</footer>
		<?php endif; ?>

	</div>

	<?php echo $this->render('footer'); ?>

	<?php if ($this['widgets']->count('offcanvas')) : ?>
	<div id="offcanvas" class="uk-offcanvas">
		<div class="uk-offcanvas-bar"><?php echo $this['widgets']->render('offcanvas'); ?></div>
	</div>
	<?php endif; ?>

</body>
</html>