<?php
/**
* @package   Master
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

// get theme configuration
include($this['path']->path('layouts:theme.config.php'));

?>

<?php echo $this['template']->render('site/head'); ?>
	
	<!-- Content -->
	<div class="wrap_pedido">
		<?php echo $this['template']->render('profile/home'); ?>
	</div>
<?php echo $this['template']->render('site/footer'); ?>		
