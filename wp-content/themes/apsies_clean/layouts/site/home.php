<div id="wrapper-banner">
	<div id="banner">
		<img class="img-banner img-responsive" src="<?php echo $this['path']->url('theme:images/banner.png')?>" alt="">
		<!-- This is the modal -->
		<a href="#video" class="video"><i class="uk-icon-video-camera"></i></a>
	</div>

	<div id="video" class="uk-modal">
	    <div class="uk-modal-dialog uk-modal-dialog-frameless">
	        <a href="" class="uk-modal-close uk-close uk-close-alt"></a>
	        <div id="ytplayer"></div>
	       
	    </div>
	</div>		
</div>


<div id="wrapper-modules">
		<div id="modules">
			<div id="module">				
				<div class="module">
						<img src="<?php echo $this['config']['contents']['module_1']['image'];?>" alt="" class="img-module">
						<h4 class="title-module">Obtén tu cuenta personal</h4>
						<p class="desc-module"><?php echo strip_tags($this['config']['contents']['module_1']['text']) ?></p>
				</div>
				<div class="module">
						
						<img src="<?php echo $this['config']['contents']['module_2']['image'];?>" alt="" class="img-module">
						<h4 class="title-module">Envía tus trabajos o deberes</h4>
						<p class="desc-module"><?php echo strip_tags($this['config']['contents']['module_2']['text']) ?></p>
				</div>
				<div class="module">

					
						<img src="<?php echo $this['config']['contents']['module_3']['image'];?>" alt="" class="img-module">
						<h4 class="title-module">Disfruta y aprovecha el día </h4>
						<p class="desc-module"><?php echo strip_tags($this['config']['contents']['module_3']['text']) ?></p>
					</div>
				<div class="module">
					
						<img src="<?php echo $this['config']['contents']['module_4']['image'];?>" alt="" class="img-module">
					
					<h4 class="title-module">Recibe la solución explicada</h4>
					<p class="desc-module"><?php echo strip_tags($this['config']['contents']['module_4']['text']) ?></p>
				</div>

				<div class="clear"></div>
					
				<a href="<?php echo Theme_My_Login::get_page_link( 'register' ) ?>" title="Regístrate ahora">
					<img id="img-register" src="<?php echo $this['path']->url('theme:images/btn-register.png')?>" alt="Regístrate ahora"> </a>
				</div>
		</div>
	</div>

<!-- Suscripcion -->
<?php echo $this['template']->render('site/sections/suscripcion'); ?>

<div id="wrapper-information">
	<img id="onda" src="<?php echo $this['path']->url('theme:images/onda.png')?>" alt="">
	<div id="information">			
		<div class="information-module">
			<div class="information-module-left">
				<h2 class="info-module-title">Saldo de puntos personal</h2>
				<p class="info-module-content">
					Registrándote en Aprobar sin Estudiar, tendrás derecho a una cuenta personal desde la que podrás gestionar tus pedidos. En ella dispondrás de un saldo de puntos para poder canjearlos por aquellas tareas y trabajos que no quieras o no sepas realizar. Recargar tu saldo es fácil, intuitivo, y sobretodo instantáneo. Además, podrás ganar puntos fácilmente con promociones y concursos.</p>
				
			</div>

			<div class="information-module-right">
				<img src="<?php echo $this['path']->url('theme:images/parrafo1.png')?>" alt="Lorem Ipsum Dolor Title">
			</div>

			<div class="clear"></div>
		</div>
		<div class="clear"></div>

		<div class="information-module">
			<div class="information-module-left">
				<img src="<?php echo $this['path']->url('theme:images/parrafo2.png')?>" alt="Lorem Ipsum Dolor Title">
			</div>
			<div class="information-module-right">
				<h2 class="info-module-title">Más puntos por el mismo precio</h2>
				<p class="info-module-content">Aprovechando los descuentos a la hora de recargar, tener siempre saldo en tu cuenta no será un problema. El precio que cobramos por punto puede llegar a ser ridículo en ocasiones.</p>
				
			</div>
			<div class="clear"></div>
		</div>

		<div class="clear"></div>

		<div class="information-module">
			<div class="information-module-left">
				<h2 class="info-module-title">Pago online instantáneo</h2>
				<p class="info-module-content">Desde Aprobar sin Estudiar ponemos a tu disposición las dos formas de pago online que más se amoldan a tus necesidades. Fáciles, intuitivas, 100% seguras y, sobretodo, instantáneas. Paga con tu cuenta de Paypal o tarjeta de crédito; o si no quieres dar datos, compra una tarjeta de prepago paysafecard en cualquier establecimiento e introduce el código que te faciliten.</p>
				
			</div>
			<div class="information-module-right">
				<img src="<?php echo $this['path']->url('theme:images/parrafo3.png')?>" alt="Lorem Ipsum Dolor Title">
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>