<div id="wrapper-trial">
		<div id="trial">
			<div id="level-selector">
				<div id="level-bachiller">
					<a class="" href="#" title="ESO / Bachiller">ESO / Bachiller</a>
				</div>
				<div id="component-selector" class="" data-selection="2">
					<div id="component-handle" style="margin-left: 112px;">
						<img src="<?php echo $this['path']->url('theme:images/component-handle.png')?>" alt="">
					</div>
				</div>
				<div id="level-universidad">
					<a class="" href="#" title="Universidad">Universidad</a>
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>

			<div class="uk-grid uk-grid-small">	
				<div class="uk-width-1-1 uk-width-medium-2-4">
					<!-- <?php echo $this['config']['site_url']?>/pedidos -->
					<form action="<?php echo $this['config']['site_url'];?>/pedidos" method="post" class="apsies-form" id="dynamic_form">
						
						<input type="hidden" name="submit_pedido[level]" value="uni">

						<div class="replace_form uni uk-margin-bottom">
							<div class="switch-tipos">
								<?php echo $this['template']->render('site/forms/uni/tipos'); ?>
							</div>
							<?php echo $this['template']->render('site/forms/uni/trabajos'); ?>
							<?php echo $this['template']->render('site/forms/uni/autocad'); ?>
							<?php echo $this['template']->render('site/forms/uni/apuntes'); ?>
							<?php echo $this['template']->render('site/forms/uni/powerpoint'); ?>
							<?php echo $this['template']->render('site/forms/uni/exposiciones'); ?>
						</div>

						<div class="replace_form eso uk-margin-bottom uk-hidden">
							<div class="switch-tipos">
								<?php echo $this['template']->render('site/forms/eso/tipos'); ?>
							</div>
							<?php echo $this['template']->render('site/forms/eso/resolucion_ejercicios'); ?>
							<?php echo $this['template']->render('site/forms/eso/redaccion_tema'); ?>
							<?php echo $this['template']->render('site/forms/eso/comentario_texto'); ?>
							<?php echo $this['template']->render('site/forms/eso/comentario_artistico'); ?>
							<?php echo $this['template']->render('site/forms/eso/critica_literaria'); ?>
							<?php echo $this['template']->render('site/forms/eso/autocad'); ?>
							<?php echo $this['template']->render('site/forms/eso/dudas_investigacion'); ?>
							<?php echo $this['template']->render('site/forms/eso/trabajos'); ?>
						</div>
						<!-- Common -->
						<div class="uk-grid uk-grid-small">
							<div class="uk-width-1-1 uk-width-medium-1-2">
							
								<div id="upload-drop" class="uk-placeholder uk-text-center">
						            <i class="uk-icon-cloud-upload uk-icon-medium uk-text-muted uk-margin-small-right"></i> Arrastra aquí lo que quieras o <a class="uk-form-file">selecciona un archivo<input id="upload-select" type="file"></a>.
						        </div>
							</div>
							<div class="uk-width-1-1 uk-width-medium-1-2">
								<div class="puntaje">
									<small>Precio por este pedido</small>
									<strong><span>0</span> puntos</strong> 
									<button class="uk-button uk-button-large uk-button-blue">Pruébalo</button>
								</div>

							</div>
						</div>
					</form>
				</div>
				<div class="uk-width-1-1 uk-width-medium-2-4 text-cont">
					<h2 class="info-module-title">Si usas mucho el servicio...</h2>
					<p class="info-module-content">... ¡Te recomendamos la suscripción!
Suscribiéndote al servicio que ofrecemos, podrás disfrutar de increíbles ventajas tales como la resolución de todas las tareas y deberes que te envíen al día o grandes descuentos en los trabajos a realizar <realizar class=""></realizar></p>
					
					<a class="info-module-btn" href="<?php echo site_url('/suscripcion') ?>" title="Suscríbete">Suscríbete</a>			

					<span class="tooltip_suscription uk-icon-question-circle" data-eso="<?php echo site_url('como-funciona-eso-bachiller'); ?>" data-uni="<?php echo site_url('como-funciona-universitarios'); ?>"></span>
				</div>
			</div>

		</div>
</div>