
			<div id="trial-form">
													
					<div class="colegio-pedido-instantaneo form-pedido" style="display: block;">				
					<!-- Nivel 0 -->
					<select data-nivel="0" class="form-element action" style="margin-bottom: 8px;">
						<option value="0">-- Tipo de Pedido --</option>
						<option value="1">Resolución de ejercicios</option>
						<option value="2">Redacción sobre un tema</option>
						<option value="3">Comentario de texto</option>
						<option value="4">Comentario artístico</option>
						<option value="5">Crítica literaria</option>
						<option value="6">AUTOCAD</option>
						<option value="7">Dudas/Investigacion sobre un tema</option>
						<option value="8">Trabajos</option>
					</select>
					<div class="clear"></div>
					<!-- End Nivel 0 -->
					<!-- Nivel 1 -->
					<select data-nivel="1" data-valor="1" class="form-element action asignatura" style="margin-right: 18px; display: none;">
						<option value="0">-- Asignatura --</option>
						<option value="1">Matemáticas</option>
						<option value="2">Lengua Castellana y Literatura</option>
						<option value="3">Física</option>
						<option value="4">Química</option>
						<option value="5">Inglés</option>
						<option value="6">Francés</option>
						<option value="7">Dibujo Técnico</option>
						<option value="8">Geografía</option>
						<option value="9">Arte</option>
						<option value="10">Informática</option>
						<option value="11">Economía</option>
						<option value="12">Alemán</option>
						<option value="13">Geografía</option>
						<option value="14">Filosofía</option>
						<option value="15">Latín/Griego</option>
					</select>
					<div data-nivel="1" data-valor="1" class="form-element form-message rotatem10" style="display: none; width: 200px;  height: auto; top: -90px; left: -150px; ">
						Te resolvemos los ejercicios en los que tengas dudas
					</div>
					<div data-nivel="1" data-valor="1" class="form-element form-message" style="display: none; width: 160px;  height: auto; top: 200px; left: -175px;">
						Describe lo más claro posible los ejercicios que quieras resolver
					</div>
					<img src="http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies/images/flecha.jpg" data-nivel="1" data-valor="1" class="form-element form-flecha rotate50" style="display: none; width: 80px;  height: auto; top: 130px; left: -115px;">						
					<div data-nivel="1" data-valor="1" class="form-element form-message rotate10" style="display: none; width: 180px;  height: auto; top: 230px; left: 465px;">
						... O arrastra una foto o documento donde vengan los ejercicios
					</div>
					<img src="http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies/images/flechaBig.png" data-nivel="1" data-valor="1" class="form-element form-flecha" style="display: none; width: 220px;  height: auto; top: 240px; left: 255px;">						
					<select data-nivel="1" data-valor="2" class="form-element action asignatura" style="margin-right: 18px; display: none;">
						<option value="0">-- Asignatura --</option>
						<option value="1">Lengua Castellana y Literatura</option>
						<option value="2">Ciencias Sociales</option>
						<option value="3">Inglés</option>
						<option value="4">Francés</option>
						<option value="5">Geografía</option>	
						<option value="6">Historia</option>
						<option value="7">Alemán</option>					
					</select>
					<div data-nivel="1" data-valor="2" class="form-element form-message rotatem10" style="display: none; width: 240px;  height: auto; top: -90px; left: -150px; ">
						Escríbenos acerca de qué quieres una redacción, y la extensión de la misma!
					</div>
					<select data-nivel="1" data-valor="3" class="form-element action asignatura" style="margin-right: 18px; display: none;">
						<option value="0">-- Asignatura --</option>
						<option value="1">Lengua Castellana y Literatura</option>
						<option value="2">Ciencias Sociales</option>
						<option value="3">Inglés</option>
						<option value="4">Francés</option>
						<option value="5">Geografía</option>	
						<option value="6">Historia</option>
						<option value="7">Economía</option>	
						<option value="8">Filosofía</option>					
					</select>					
					<div data-nivel="1" data-valor="3" class="form-element form-message rotatem10" style="display: none; width: 240px;  height: auto; top: -95px; left: -165px; ">
						¿No sabes hacer un comentario de un texto? No hay problema, aquí te los hacemos!
					</div>					
					<select data-nivel="1" data-valor="4" class="form-element action asignatura" style="margin-right: 18px; display: none;">
						<option value="0">-- Asignatura --</option>
						<option value="1">Arte</option>
					</select>
					<div data-nivel="1" data-valor="4" class="form-element form-message rotatem10" style="display: none; width: 240px;  height: auto; top: -90px; left: -150px; ">
						¿No sabes hacer un comentario artístico? No hay problema, aquí te los hacemos!
					</div>
					<!-- Valor 5 -->
					<img src="http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies/images/flecha.jpg" data-nivel="1" data-valor="5" class="form-element form-flecha rotate50" style="display: none; width: 80px;  height: auto; top: 360px; left: -115px;">						
					<div data-nivel="1" data-valor="5" class="form-element form-message rotatem10" style="display: none; width: 140px;  height: auto; top: 420px; left: -155px;">
						Puedes adjuntar la obra si quieres
					</div>
					<img src="http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies/images/flecha.jpg" data-nivel="1" data-valor="5" class="form-element form-flecha rotateY180Z50" style="display: none; width: 80px;  height: auto; top: 200px; left: 465px;">						
					<div data-nivel="1" data-valor="5" class="form-element form-message rotate" style="display: none; width: 500px;  height: auto; top: 290px; left: 475px;">
						Repartiremos de forma adecuada el número de hojas de cada una de las partes, ¡no te preocupes! Pero en caso de que quieras destinar un número específico de hojas a una de las partes, ¡indícalo en las observaciones!						
					</div>
					<div data-nivel="1" data-valor="5" class="form-element form-message rotatem10" style="display: none; width: 240px;  height: auto; top: -90px; left: -150px; ">
						Te explicamos y comentamos una obra literaria en concreto
					</div>
					<input data-nivel="1" data-valor="5" class="form-element titulo" type="text" placeholder="Título de la obra" style="display: none; margin-right: 18px;margin-bottom: 12px;">
					<div class="span-element"><span style="display: none;" data-nivel="1" data-valor="5" class="form-element base4">Nº total de páginas </span></div>										
					<input data-nivel="1" data-valor="5" type="text" class="spinner1 form-element palabrascritica smartspinner" style="margin-right: 20px; margin-left: 3px; display: none; width: 50px; height: 36px;">
					<div data-nivel="1" data-valor="5" class="form-element partesincluir" style="display: none; width: 224px; float: left;">
						Partes a incluir: <br>
						<input type="checkbox" class="partesincluir" data-valor="Argumento"> Argumento <br>
						<input type="checkbox" class="partesincluir" data-valor="Personajes principales"> Personajes principales <br>
						<input type="checkbox" class="partesincluir" data-valor="Personajes secundarios"> Personajes secundarios <br>
						<input type="checkbox" class="partesincluir" data-valor="Estilo"> Estilo <br>
						<input type="checkbox" class="partesincluir" data-valor="Estructura"> Estructura <br>
						<input type="checkbox" class="partesincluir" data-valor="Conclusion"> Conclusión <br>
					</div>					
					<select data-nivel="1" data-valor="5" class="form-element tiempocritica" style="margin-right: 0px; display: none; margin-bottom: 23px;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="2">2 horas</option>
						<option value="5">5 horas</option>
						<option value="8">8 horas</option>
						<option value="24">1 dia</option>
						<option value="48">2 dias</option>
						<option value="72">3 dias</option>
						<option value="96">4 dias</option>
						<option value="120">5 dias</option>
						<option value="144">6 dias</option>
						<option value="150">6+ dias</option>
					</select>
					<textarea data-nivel="1" data-valor="5" class="form-element observacionescritica" style="margin-right: 18px; display: none;" placeholder="Observaciones"></textarea>
					<!-- End Valor 5 -->
					<select data-nivel="1" data-valor="6" class="form-element action autocadtipo" style="margin-right: 18px; display: none;">
						<option value="0">-- Tipo --</option>
						<option value="1">Ejercicios</option>
						<option value="2">Piezas</option>
					</select>
					<div data-nivel="1" data-valor="6" class="form-element form-message rotatem10" style="display: none; width: 200px;  height: auto; top: -80px; left: -150px; ">
						¿Problemas con AutoCAD?
					</div>					
					<select data-nivel="1" data-valor="7" class="form-element action asignatura" style="margin-right: 18px; display: none;">
						<option value="0">-- Asignatura --</option>
						<option value="1">Lengua Castellana y Literatura</option>
						<option value="2">Ciencias Sociales</option>
						<option value="3">Geografía</option>
						<option value="4">Historia</option>
						<option value="5">Arte</option>
						<option value="6">Informática</option>
						<option value="7">Economía</option>	
						<option value="8">Biología</option>
						<option value="9">Filosofía</option>	
						<option value="10">Latin / Griego</option>				
					</select>
					<div data-nivel="1" data-valor="7" class="form-element form-message rotatem10" style="display: none; width: 240px;  height: auto; top: -90px; left: -150px; ">
						¿Demasiadas mentiras por la red? ¿No sabes qué es cierto y qué no?
					</div>					
					<!-- Valor 8 -->					
					<div data-nivel="1" data-valor="8" class="form-element form-message rotatem10" style="display: none; width: 240px;  height: auto; top: -90px; left: -150px; ">
						Rellena los campos lo más preciso posible y ¡olvídate del trabajo!
					</div>
					<img src="http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies/images/flecha.jpg" data-nivel="1" data-valor="8" class="form-element form-flecha rotate50" style="display: none; width: 80px;  height: auto; top: 280px; left: -115px;">						
					<div data-nivel="1" data-valor="8" class="form-element form-message rotate" style="display: none; width: 140px;  height: auto; top: 350px; left: -170px;">
						¡Arrastra cualquier archivo, documento o foto que lo explique si lo prefieres!
					</div>
					<img src="http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies/images/flecha.jpg" data-nivel="1" data-valor="8" class="form-element form-flecha rotateY180Z50" style="display: none; width: 80px;  height: auto; top: 200px; left: 465px;">
					<div data-nivel="1" data-valor="8" class="form-element form-message rotate" style="display: none; width: 300px;  height: auto; top: 280px; left: 475px;">
						Especifica cualquier condición especial que quieras que tenga el trabajo
					</div>
					<input data-nivel="1" data-valor="8" class="form-element titulo" type="text" placeholder="Título" style="display: none; margin-right: 18px;">
					<select data-nivel="1" data-valor="8" class="form-element action tema" style="margin-right: 0px; display: none; margin-bottom: 8px;">
						<option value="0">-- Tema --</option>
						<option value="1">Matemáticas</option>
						<option value="2">Lengua Castellana y Literatura</option>
						<option value="3">Física</option>
						<option value="4">Química</option>
						<option value="5">Ciencias Sociales</option>
						<option value="6">Inglés</option>
						<option value="7">Francés</option>	
						<option value="8">Geografía</option>
						<option value="9">Dibujo Técnico</option>	
						<option value="10">Alemán</option>
						<option value="11">Arte</option>
						<option value="12">Informática</option>
						<option value="13">Economía</option>
						<option value="14">Biología</option>
						<option value="15">Filosía</option>	
						<option value="16">Latín / Griego</option>
						<option value="17">Historia</option>			
					</select>
					<div class="span-element"><span style="display: none;" data-nivel="1" data-valor="8" class="form-element">Nº de páginas </span></div>										
					<input data-nivel="1" data-valor="8" type="text" class="spinner1 form-element paginastrabajo smartspinner" style="margin-right: 73px; margin-left: 3px; display: none; width: 50px; height: 36px;">
					<select data-nivel="1" data-valor="8" class="form-element tiempotrabajo" style="margin-right: 0px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="2">2 horas</option>
						<option value="5">5 horas</option>
						<option value="8">8 horas</option>
						<option value="24">1 dia</option>
						<option value="48">2 dias</option>
						<option value="72">3 dias</option>
						<option value="96">4 dias</option>
						<option value="120">5 dias</option>
						<option value="144">6 dias</option>
						<option value="150">6+ dias</option>
					</select>
					<select data-nivel="1" data-valor="8" class="form-element bibliografiatrabajo" style="margin-right: 18px; display: none;">
						<option value="0">-- Bibliografía --</option>
						<option value="1">Si</option>
						<option value="2">No</option>						
					</select>
					<textarea data-nivel="1" data-valor="8" class="form-element observaciones" style="margin-right: 0px; display: none;" placeholder="Observaciones"></textarea>
					<!-- End Valor 8 -->
					<!-- End Nivel 1 -->
					<!-- Base 1 -->
					<div class="span-element"><span style="display: none;" data-nivel="2" class="form-element base1">Nº de ejercicios </span></div>
					<input data-nivel="2" type="text" class="base1 spinner5 form-element ejercicios smartspinner" style="margin-right: 43px; margin-left: 3px; display: none; margin-bottom: 12px; width: 50px; height: 36px;">
					<input data-nivel="2" type="text" class="base1 spinner3 form-element ejercicios smartspinner" style="margin-right: 43px; margin-left: 3px; display: none; margin-bottom: 12px; width: 50px; height: 36px;">
					<select data-nivel="2" class="base1 form-element tiempo" style="margin-right: 0px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="2">2 horas</option>
						<option value="5">5 horas</option>
						<option value="8">8 horas</option>
						<option value="24">1 dia</option>
						<option value="48">2 dias</option>
						<option value="72">3 dias</option>
						<option value="96">4 dias</option>
						<option value="120">5 dias</option>
						<option value="144">6 dias</option>
						<option value="150">6+ dias</option>
					</select>
					<!-- End Base 1 -->
					<!-- Base 2 -->
					<div data-nivel="2" class="base2 form-element form-message rotate" style="display: none; width: 300px;  height: auto; top: 280px; left: 475px;">
						Para el cálculo de la extensión:<br>
						<table style="margin-left: 30px;">
							<tbody><tr>
								<td>a mano:</td>
								<td>1 línea ~ 10 palabras</td>
							</tr>
							<tr>
								<td>a ordenador:</td>
								<td>1 línea ~ 15 palabras</td>
							</tr>
						</tbody></table>
					</div>
					<img src="http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies/images/flecha.jpg" data-nivel="2" class="base2 form-element form-flecha rotateY180Z50" style="display: none; width: 80px;  height: auto; top: 200px; left: 465px;">						
					<input data-nivel="2" type="text" class="base2 form-element tema" placeholder="Tema" style="display: none; margin-right: 0px; margin-bottom: 12px;">
					<div class="span-element"><span style="display: none;" data-nivel="2" class="form-element base2">Nº de palabras </span></div>
					<input data-nivel="2" type="text" class="base2 spinner1 form-element palabras smartspinner" style="margin-right: 69px; margin-left: 3px; display: none; width: 50px; height: 36px;">
					<select data-nivel="2" class="base2 form-element tiempo" style="margin-right: 0px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="2">2 horas</option>
						<option value="5">5 horas</option>
						<option value="8">8 horas</option>
						<option value="24">1 dia</option>
						<option value="48">2 dias</option>
						<option value="72">3 dias</option>
						<option value="96">4 dias</option>
						<option value="120">5 dias</option>
						<option value="144">6 dias</option>
						<option value="150">6+ dias</option>
					</select>
					<textarea data-nivel="2" class="base2 form-element observaciones" style="margin-right: 0px; display: none;" placeholder="Observaciones"></textarea>
					<!-- End Base 2 -->
					<!-- Base 3 -->
					<div data-nivel="2" class="base3 form-element form-message rotate" style="display: none; width: 300px;  height: auto; top: 280px; left: 475px;">
						Para el cálculo de la extensión:<br>
						<table style="margin-left: 30px;">
							<tbody><tr>
								<td>a mano:</td>
								<td>1 línea ~ 10 palabras</td>
							</tr>
							<tr>
								<td>a ordenador:</td>
								<td>1 línea ~ 15 palabras</td>
							</tr>
						</tbody></table>
					</div>
					<img src="http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies/images/flechaBig.png" data-nivel="2" class="base3 form-element form-flecha rotateX190NM60" style="display: none; width: 180px;  height: auto; top: 170px; left: 425px;">						
					<div class="span-element"><span style="display: none;" data-nivel="2" class="form-element base3">Nº de palabras </span></div>										
					<input data-nivel="2" type="text" class="base3 spinner1 form-element palabras smartspinner" style="margin-right: 50px; margin-left: 3px; display: none; margin-bottom: 12px; width: 50px; height: 36px;">
					<select data-nivel="2" class="base3 form-element tiempo" style="margin-right: 18px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="2">2 horas</option>
						<option value="5">5 horas</option>
						<option value="8">8 horas</option>
						<option value="24">1 dia</option>
						<option value="48">2 dias</option>
						<option value="72">3 dias</option>
						<option value="96">4 dias</option>
						<option value="120">5 dias</option>
						<option value="144">6 dias</option>
						<option value="150">6+ dias</option>
					</select>
					<textarea data-nivel="2" class="base3 form-element observaciones" style="margin-right: 0px; display: none;" placeholder="Observaciones"></textarea>
					<textarea data-nivel="2" class="base3 form-element textocomentar" style="margin-right: 18px; display: none;" placeholder="Texto a comentar"></textarea>
					<!-- End Base 3 -->
					<!-- Base 4 -->
					<div data-nivel="2" class="base4 form-element form-message rotate" style="display: none; width: 300px;  height: auto; top: 280px; left: 475px;">
						Para el cálculo de la extensión:<br>
						<table style="margin-left: 30px;">
							<tbody><tr>
								<td>a mano:</td>
								<td>1 línea ~ 10 palabras</td>
							</tr>
							<tr>
								<td>a ordenador:</td>
								<td>1 línea ~ 15 palabras</td>
							</tr>
						</tbody></table>
					</div>
					<img src="http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies/images/flechaBig.png" data-nivel="2" class="base4 form-element form-flecha rotateX190NM60" style="display: none; width: 180px;  height: auto; top: 170px; left: 425px;">
					<div class="span-element"><span style="display: none;" data-nivel="2" class="form-element base4">Nº de palabras </span></div>										
					<input data-nivel="2" type="text" class="base4 spinner1 form-element palabras smartspinner" style="margin-right: 50px; margin-left: 3px; display: none; margin-bottom: 12px; width: 50px; height: 36px;">
					<select data-nivel="2" class="base4 form-element tiempo" style="margin-right: 18px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="2">2 horas</option>
						<option value="5">5 horas</option>
						<option value="8">8 horas</option>
						<option value="24">1 dia</option>
						<option value="48">2 dias</option>
						<option value="72">3 dias</option>
						<option value="96">4 dias</option>
						<option value="120">5 dias</option>
						<option value="144">6 dias</option>
						<option value="150">6+ dias</option>
					</select>
					<textarea data-nivel="2" class="base4 form-element observaciones" style="margin-right: 0px; display: none;" placeholder="Observaciones"></textarea>
					<textarea data-nivel="2" class="base4 form-element obracomentar" style="margin-right: 18px; display: none;" placeholder="Obra de arte a comentar"></textarea>
					<!-- End Base 4 -->
					<!-- Base 6 -->
					<div data-nivel="2" data-valor="1" class="base6 form-element form-message rotatem10" style="display: none; width: 140px;  height: auto; top: 290px; left: -170px;">
						Adjunta los ejercicios a realizar
					</div>
					<img src="http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies/images/flecha.jpg" data-nivel="2" data-valor="1" class="base6 form-element form-flecha rotate50" style="display: none; width: 80px;  height: auto; top: 220px; left: -115px;">						
					<div data-nivel="2" data-valor="1" class="base6 form-element form-message rotate10" style="display: none; width: 300px;  height: auto; top: 285px; left: 475px;">
						Indica cuántos ejercicios son y para cuándo los quieres
					</div>
					<img src="http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies/images/flechaBig.png" data-nivel="2" data-valor="1" class="base6 form-element form-flecha rotateX190NM60" style="display: none; width: 180px;  height: auto; top: 170px; left: 425px;">
					<div class="span-element"><span style="display: none;" data-nivel="2" data-valor="1" class="form-element base6">Nº de ejercicios </span></div>										
					<input data-nivel="2" data-valor="1" type="text" class="base6 spinner1 form-element ejercicios smartspinner" style="margin-right: 50px; margin-left: 3px; display: none; margin-bottom: 12px; width: 50px; height: 36px;">
					<select data-nivel="2" data-valor="1" class="base6 form-element tiempoejercicios" style="margin-right: 18px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="2">2 horas</option>
						<option value="5">5 horas</option>
						<option value="8">8 horas</option>
						<option value="24">1 dia</option>
						<option value="48">2 dias</option>
						<option value="72">3 dias</option>
						<option value="96">4 dias</option>
						<option value="120">5 dias</option>
						<option value="144">6 dias</option>
						<option value="150">6+ dias</option>
					</select>
					<div data-nivel="2" data-valor="2" class="base6 form-element form-message rotate10" style="display: none; width: 300px;  height: auto; top: 280px; left: 475px;">
						Podemos dibujar la pieza si nos das las vistas; o sacarte las vistas si nos adjuntas la pieza. ¡Siempre ganas!
					</div>
					<img src="http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies/images/flecha.jpg" data-nivel="2" data-valor="2" class="base6 form-element form-flecha rotateY180Z50" style="display: none; width: 80px;  height: auto; top: 200px; left: 465px;">
					<div class="span-element"><span style="display: none;" data-nivel="2" data-valor="2" class="form-element base6">Nº de piezas a dibujar </span></div>										
					<input data-nivel="2" data-valor="2" type="text" class="base6 spinner1 form-element piezas smartspinner" style="margin-right: 10px; margin-left: 3px; display: none; margin-bottom: 12px; width: 50px; height: 36px;">
					<select data-nivel="2" data-valor="2" class="base6 form-element vistaspiezas" style="margin-right: 18px; display: none;">
						<option value="0">-- Vistas --</option>
						<option value="1">Si</option>
						<option value="2">No</option>						
					</select>
					<select data-nivel="2" data-valor="2" class="base6 form-element acotacionpiezas" style="margin-right: 0px; display: none;">
						<option value="0">-- Acotación --</option>
						<option value="1">Si</option>
						<option value="2">No</option>						
					</select>
					<select data-nivel="2" data-valor="2" class="base6 form-element tiempopiezas" style="margin-right: 18px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="2">2 horas</option>
						<option value="5">5 horas</option>
						<option value="8">8 horas</option>
						<option value="24">1 dia</option>
						<option value="48">2 dias</option>
						<option value="72">3 dias</option>
						<option value="96">4 dias</option>
						<option value="120">5 dias</option>
						<option value="144">6 dias</option>
						<option value="150">6+ dias</option>
					</select>
					<textarea data-nivel="1" data-valor="6" class="form-element observaciones" style="margin-right: 0px; display: none;" placeholder="Observaciones"></textarea>
					<!-- End Base 6 -->
					<!-- Base 7 -->
					<div data-nivel="2" class="base7 form-element form-message" style="display: none; width: 500px;  height: auto; top: 280px; left: 475px;">
						Si lo que buscas es simple información acerca de un tema. Nosotros la buscamos, filtramos, clasificamos y te la enviamos. Sólo datos y hechos contrastados
					</div>
					<img src="http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies/images/flecha.jpg" data-nivel="2" data-valor="2" class="base7 form-element form-flecha rotateY180Z50" style="display: none; width: 80px;  height: auto; top: 200px; left: 465px;">
					<input data-nivel="2" type="text" class="base7 form-element tema" placeholder="Tema" style="display: none; margin-right: 0px; margin-bottom: 12px;">
					<div class="span-element"><span style="display: none;" data-nivel="2" class="form-element base7">Nº de palabras </span></div>
					<input data-nivel="2" type="text" class="base7 spinner1 form-element palabras smartspinner" style="margin-right: 68px; margin-left: 3px; display: none; width: 50px; height: 36px;">
					<select data-nivel="2" class="base7 form-element tiempo" style="margin-right: 0px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="2">2 horas</option>
						<option value="5">5 horas</option>
						<option value="8">8 horas</option>
						<option value="24">1 dia</option>
						<option value="48">2 dias</option>
						<option value="72">3 dias</option>
						<option value="96">4 dias</option>
						<option value="120">5 dias</option>
						<option value="144">6 dias</option>
						<option value="150">6+ dias</option>
					</select>
					<textarea data-nivel="2" class="base7 form-element observaciones" style="margin-right: 0px; display: none;" placeholder="Observaciones"></textarea>
					<!-- End Base 7 -->					
					<div class="clear"></div>
					<!--<img src="http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies/images/bg-drop.png" style="cursor: pointer; margin: 10px 20px 0px 0px; float: left;" alt="">-->
					<div class="wrapper-dnd-area">
						<div id="colegio-dnd-area" class="dnd-area">
							<form id="colegio-form" name="colegio-form" action="http://www.pruebaapsiesnetwork.es/wp-admin/admin-ajax.php" method="post" enctype="multipart/form-data">
								<input type="hidden" name="action" value="MyAjaxFunctions">
								<input type="hidden" name="toaction" value="uploadArchive">
								<input id="colegio-form-post-id" type="hidden" name="post_id" value="">
								<input id="colegio-file" type="file" style="display:none;" name="fileselect">
								<input type="hidden" id="fileselect_nonce" name="fileselect_nonce" value="693c4f2044"><input type="hidden" name="_wp_http_referer" value="/">												
							</form>
						</div>
						<div id="colegio-details"></div>
					</div>
					<div class="points" style="float: left; width: 190px; text-align: center; margin-top: 10px; color: #838383;">
						<span class="price">Precio por este pedido<br> <strong style="font-family: OSBold; font-size: 16px; color: black;">0 puntos</strong></span>					
						<a class="btn" href="#" title="Pruébalo!">Pruébalo!</a>
					</div>
				</div>
												
								<div class="universidad-pedido form-pedido" style="">				
					<!-- Nivel 0 -->
					<select data-nivel="0" class="form-element action">
						<option value="0">-- Tipo de Pedido --</option>
						<option value="1">Trabajos</option>
						<option value="2">AutoCAD</option>
						<option value="3">Apuntes</option>
						<option value="4">PowerPoint</option>
						<option value="5">Exposiciones</option>						
					</select>
					<div class="clear"></div>
					<!-- End Nivel 0 -->
					<!-- Nivel 1 -->
					<!-- Valor 1 -->
					<div data-nivel="1" data-valor="1" class="form-element form-message rotatem10" style="display: none; width: 240px;  height: auto; top: -65px; left: -150px; ">
						Rellena los campos lo más preciso posible y ¡olvídate del trabajo!
					</div>
					<img src="http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies/images/flecha.jpg" data-nivel="1" data-valor="1" class="form-element form-flecha rotate50" style="display: none; width: 80px;  height: auto; top: 280px; left: -115px;">						
					<div data-nivel="1" data-valor="1" class="form-element form-message rotate" style="display: none; width: 140px;  height: auto; top: 340px; left: -170px;">
						¡Arrastra cualquier archivo, documento o foto que lo explique si lo prefieres!
					</div>
					<img src="http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies/images/flecha.jpg" data-nivel="1" data-valor="1" class="form-element form-flecha rotateY180Z50" style="display: none; width: 80px;  height: auto; top: 200px; left: 465px;">
					<div data-nivel="1" data-valor="1" class="form-element form-message rotate" style="display: none; width: 300px;  height: auto; top: 290px; left: 475px;">
						Especifica cualquier condición especial que quieras que tenga el trabajo
					</div>
					<input data-nivel="1" data-valor="1" class="form-element titulo" type="text" placeholder="Título del trabajo" style="display: none; margin-right: 18px;margin-bottom: 12px;">
					<select data-nivel="1" data-valor="1" class="form-element action tema" style="margin-right: 0px; margin-bottom: 8px; display: none; margin-bottom: 12px;">
						<option value="0">-- Tema --</option>
						<option value="1">Aeronáutica</option>
						<option value="2">Agronomía</option>
						<option value="3">Antropología</option>
						<option value="4">Arqueología</option>
						<option value="5">Arquitectura</option>
						<option value="6">Arte</option>
						<option value="7">Astrología</option>
						<option value="8">Astronomía</option>
						<option value="9">Automoción</option>
						<option value="10">Aviónica</option>
						<option value="11">Biblioteconomía</option>
						<option value="12">Biografía</option>
						<option value="13">Biología</option>
						<option value="14">Bioquímica</option>
						<option value="15">Botánica</option>
						<option value="16">Contabilidad</option>
						<option value="17">Deporte y Ciencias de la Actividad Física</option>
						<option value="18">Derecho</option>
						<option value="19">Documentación</option>
						<option value="20">Ecología y Medio Ambiente</option>
						<option value="21">Economía y Empresa</option>
						<option value="22">Educación Física</option>
						<option value="23">Educación y Pedagogía</option>
						<option value="24">Electrónica y Electricidad</option>
						<option value="25">Enfermería</option>
						<option value="26">Estadística</option>
						<option value="27">Ética y Moral</option>
						<option value="28">Farmacia</option>
						<option value="29">Filología</option>
						<option value="30">Filosofía</option>
						<option value="31">Física</option>
						<option value="32">Gastronomía y Restauración</option>
						<option value="33">Genética</option>
						<option value="34">Geografía</option>
						<option value="35">Geología</option>
						<option value="36">Historia</option>
						<option value="37">Hostelería</option>
						<option value="38">Imagen y Audiovisuales</option>
						<option value="39">Industria</option>
						<option value="40">Informática</option>
						<option value="41">Ingeniería</option>
						<option value="42">Juegos y Animación</option>
						<option value="43">Lenguaje y Gramática</option>
						<option value="44">Literatura</option>
						<option value="45">Matemáticas</option>
						<option value="46">Materiales</option>
						<option value="47">Mitología</option>
						<option value="48">Música</option>
						<option value="49">Naútica/Naval</option>
						<option value="50">Nutrición y Dietética</option>
						<option value="51">Obras y Construcción</option>
						<option value="52">óptica y Optometría</option>
						<option value="53">Organización de Empresas</option>
						<option value="54">Periodismo</option>
						<option value="55">Política y Administración Pública</option>
						<option value="56">Psicología</option>
						<option value="57">Publicidad y Marketing</option>
						<option value="58">Química</option>
						<option value="59">Química</option>
						<option value="60">Recursos Forestales</option>
						<option value="61">Recursos Humanos</option>
						<option value="62">Relaciones Laborales</option>
						<option value="63">Relaciones Públicas</option>
						<option value="64">Religión y Creencias</option>
						<option value="65">Salud</option>
						<option value="66">Sociología y Trabajo Social</option>
						<option value="67">Sonido</option>
						<option value="68">Tecnología</option>
						<option value="69">Telecomunicaciones</option>
						<option value="70">Termodinámica</option>
						<option value="71">Topografía</option>
						<option value="72">Traducción e Interpretación</option>
						<option value="73">Turismo</option>
						<option value="74">Zoología</option>
					</select>
					<div class="span-element"><span style="display: none;" data-nivel="1" data-valor="1" class="form-element">Nº de páginas </span></div>
					<input data-nivel="1" data-valor="1" type="text" class="spinner1 form-element paginas smartspinner" style="margin-right: 73px; margin-left: 3px; display: none; width: 50px; height: 36px;">
					<select data-nivel="1" data-valor="1" class="form-element tiempo" style="margin-right: 0px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="24">24 horas</option>
						<option value="48">2 dias</option>
						<option value="72">3 dias</option>
						<option value="96">4 dias</option>
						<option value="120">5 dias</option>
						<option value="144">6 dias</option>
						<option value="168">7 dias</option>
						<option value="192">8 dias</option>
						<option value="216">9 dias</option>
						<option value="240">10 dias</option>
						<option value="264">11 dias</option>
						<option value="288">12 dias</option>
						<option value="312">13 dias</option>
						<option value="336">14 dias</option>
						<option value="360">15 dias</option>
						<option value="384">16 dias</option>
						<option value="408">17 dias</option>
						<option value="432">18 dias</option>
						<option value="456">19 dias</option>
						<option value="480">20+ dias</option>
					</select>
					<select data-nivel="1" data-valor="1" class="form-element bibliografia" style="margin-right: 18px; display: none;">
						<option value="0">-- Bibliografía --</option>
						<option value="1">Si</option>
						<option value="2">No</option>						
					</select>
					<textarea data-nivel="1" data-valor="1" class="form-element observaciones" style="margin-right: 0px; display: none;" placeholder="Observaciones"></textarea>
					<!-- End Valor 1 -->
					<!-- Valor 2 -->
					<div data-nivel="1" data-valor="2" class="form-element form-message rotatem10" style="display: none; width: 240px;  height: auto; top: -65px; left: -150px; ">
						¿Te da problemas autoCAD? Tiene fácil solución ... 
					</div>
					<img src="http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies/images/flecha.jpg" data-nivel="1" data-valor="2" class="form-element form-flecha rotate50" style="display: none; width: 80px;  height: auto; top: 130px; left: -115px;">
					<div data-nivel="1" data-valor="2" class="form-element form-message rotate" style="display: none; width: 130px;  height: auto; top: 200px; left: -150px;">
						Indícanos si necesitas también las vistas y las acotaciones
					</div>
					<img src="http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies/images/flechaBig.png" data-nivel="1" data-valor="2" class="form-element form-flecha" style="display: none; width: 220px;  height: auto; top: 385px; left: 255px;">						
					<div data-nivel="1" data-valor="2" class="form-element form-message rotate10" style="display: none; width: 300px;  height: auto; top: 380px; left: 475px;">
						... O arrastra el proyecto al recuadro y explícanos lo que necesitas, ¡y te enviaremos el proyecto realizado!
					</div>
					<div class="span-element"><span style="display: none;" data-nivel="1" data-valor="2" class="form-element">Nº de piezas a dibujar </span></div>										
					<input data-nivel="1" data-valor="2" type="text" class="spinner1 form-element piezas smartspinner" style="margin-right: 28px; margin-left: 3px; display: none; margin-bottom: 12px; width: 50px; height: 36px;">
					<select data-nivel="1" data-valor="2" class="form-element vistas" style="margin-right: 0px; display: none;">
						<option value="0">-- Vistas --</option>
						<option value="1.1">Si</option>
						<option value="1">No</option>						
					</select>
					<select data-nivel="1" data-valor="2" class="form-element acotacion" style="margin-right: 18px; display: none;">
						<option value="0">-- Acotación --</option>
						<option value="1.5">Si</option>
						<option value="1">No</option>						
					</select>
					<select data-nivel="1" data-valor="2" class="form-element tiempo" style="margin-right: 0px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="24">24 horas</option>
						<option value="48">2 dias</option>
						<option value="72">3 dias</option>
						<option value="96">4 dias</option>
						<option value="120">5 dias</option>
						<option value="144">6 dias</option>
						<option value="168">7 dias</option>
						<option value="192">8 dias</option>
						<option value="216">9 dias</option>
						<option value="240">10 dias</option>
						<option value="264">11 dias</option>
						<option value="288">12 dias</option>
						<option value="312">13 dias</option>
						<option value="336">14 dias</option>
						<option value="360">15 dias</option>
						<option value="384">16 dias</option>
						<option value="408">17 dias</option>
						<option value="432">18 dias</option>
						<option value="456">19 dias</option>
						<option value="480">20+ dias</option>
					</select>
					<textarea data-nivel="1" data-valor="2" class="form-element observaciones" style="margin-right: 0px; display: none;" placeholder="Observaciones"></textarea>
					<!-- End Valor 2 -->
					<!-- Valor 3 -->
					<div data-nivel="1" data-valor="3" class="form-element form-message rotatem10" style="display: none; width: 240px;  height: auto; top: -80px; left: -165px; ">
						¡Te pasamos los apuntes a ordenador! ¡Olvídate de la mala letra de tu compañero!
					</div>
					<select data-nivel="1" data-valor="3" class="form-element action subiropaquete" style="margin-right: 18px; display: none;">
						<option value="0">-- Tipo --</option>
						<option value="1">Subir a la Web</option>
						<option value="2">Paquetería</option>						
					</select>
					<!-- End Valor 3 -->
					<!-- Valor 4 -->					
					<select data-nivel="1" data-valor="4" class="form-element action diapositivaotexto" style="margin-right: 18px; display: none;">
						<option value="0">-- Tipo --</option>
						<option value="1">Diapositivas a Texto</option>
						<option value="2">Texto a Diapositivas</option>						
					</select>
					<!-- End Valor 4 -->
					<!-- Valor 5 -->
					<div data-nivel="1" data-valor="5" class="form-element form-message rotatem10" style="display: none; width: 240px;  height: auto; top: -60px; left: -170px; ">
						¿Preocupado por la exposición oral de aquel trabajo? ¿No sabes que decir?
					</div>
					<select data-nivel="1" data-valor="5" class="form-element action partiendo" style="margin-right: 18px; display: none;">
						<option value="0">-- Tipo --</option>
						<option value="1">A partir de un trabajo escrito</option>
						<option value="2">Partiendo de cero</option>						
					</select>
					<!-- End Valor 5 -->
					<!-- End Nivel 1 -->
					<!-- Base 3 -->
					<div data-nivel="2" data-valor="1" class="base3 form-element form-message rotate" style="display: none; width: 140px;  height: auto; top: 260px; left: -170px;">
						Arrastra tus apuntes en un archivo comprimido y no esperes más
					</div>
					<img src="http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies/images/flecha.jpg" data-nivel="2" data-valor="1" class="base3 form-element form-flecha rotate50" style="display: none; width: 80px;  height: auto; top: 190px; left: -115px;">
					<div class="span-element"><span style="display: none;" data-nivel="2" data-valor="1" class="base3 form-element">Nº Páginas a sucio </span></div>										
					<input data-nivel="2" data-valor="1" type="text" class="base3 spinner1 form-element paginassubir smartspinner" style="margin-right: 20px; margin-left: 3px; display: none; margin-bottom: 12px; width: 50px; height: 36px;">
					<select data-nivel="2" data-valor="1" class="base3 form-element tiemposubir" style="margin-right: 0px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="120">5 dias</option>
						<option value="240">10 dias</option>
						<option value="360">15 dias</option>
						<option value="480">20 dias</option>
						<option value="500">20+ dias</option>
					</select>
					<div data-nivel="2" data-valor="2" class="base3 form-element form-message rotate" style="display: none; width: 140px;  height: auto; top: 170px; left: -170px;">
						Si no puedes adjuntarlos, puedes enviarnos una COPIA a:<br>
						C/ PruebaPrueba, 11, 1ra<br>
						Madrid (Madrid)
					</div>	
					<img src="http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies/images/flecha.jpg" data-nivel="2" data-valor="2" class="base3 form-element form-flecha rotateY180Z50" style="display: none; width: 80px;  height: auto; top: 200px; left: 465px;">						
					<div data-nivel="2" data-valor="2" class="base3 form-element form-message rotate" style="display: none; width: 300px;  height: auto; top: 290px; left: 475px;">
						Rellena los campos y completa el pago. En cuanto nos lleguen nos pondremos a trabajar!
					</div>	
					<div class="span-element"><span style="display: none;" data-nivel="2" data-valor="2" class="base3 form-element">Nº Páginas a sucio </span></div>										
					<input data-nivel="2" data-valor="2" type="text" class="base3 spinner1 form-element paginaspaqueteria smartspinner" style="margin-right: 20px; margin-left: 3px; display: none; margin-bottom: 12px; width: 50px; height: 36px;">
					<select data-nivel="2" data-valor="2" class="base3 form-element tiempopaqueteria" style="margin-right: 0px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="120">5 dias</option>
						<option value="240">10 dias</option>
						<option value="360">15 dias</option>
						<option value="480">20 dias</option>
						<option value="500">20+ dias</option>
					</select>				
					<!-- End Base 3 -->
					<!-- Base 4 -->
					<div data-nivel="2" data-valor="1" class="base4 form-element form-message rotatem10" style="display: none; width: 240px;  height: auto; top: -50px; left: -165px; ">
						¿Demasiadas diapositivas?
					</div>
					<div data-nivel="2" data-valor="1" class="base4 form-element form-message rotate" style="display: none; width: 140px;  height: auto; top: 120px; left: -170px;">
						¿Incómodas para estudiar, guardar o llevar de un lado a otro?
					</div>
					<div data-nivel="2" data-valor="1" class="base4 form-element form-message rotate10" style="display: none; width: 300px;  height: auto; top: 290px; left: 475px;">
						Nosotros te las redactamos en un único documento, ¡seguro te será más cómodo!
					</div>
					<div class="span-element"><span style="display: none;" data-nivel="2" data-valor="1" class="base4 form-element">Nº Diapositivas </span></div>										
					<input data-nivel="2" data-valor="1" type="text" class="base4 spinner1 form-element diapositivascantidad smartspinner" style="margin-right: 40px; margin-left: 3px; display: none; margin-bottom: 12px; width: 50px; height: 36px;">
					<select data-nivel="2" data-valor="1" class="base4 form-element tiempodiapositivas" style="margin-right: 0px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="120">5 dias</option>
						<option value="240">10 dias</option>
						<option value="360">15 dias</option>
						<option value="480">20 dias</option>
						<option value="500">20+ dias</option>
					</select>
					<div data-nivel="2" data-valor="2" class="base4 form-element form-message rotatem10" style="display: none; width: 240px;  height: auto; top: -50px; left: -165px; ">
						¿Temario muy denso?
					</div>
					<div data-nivel="2" data-valor="2" class="base4 form-element form-message rotate" style="display: none; width: 130px;  height: auto; top: 80px; left: -160px;">
						¿No te entra ese 'tocho' infumable?
					</div>
					<div data-nivel="2" data-valor="2" class="base4 form-element form-message rotate" style="display: none; width: 130px;  height: auto; top: 170px; left: -160px;">
						Puede que ha golpe de vista se te quede mejor ...
					</div>
					<div data-nivel="2" data-valor="2" class="base4 form-element form-message rotate10" style="display: none; width: 300px;  height: auto; top: 290px; left: 475px;">
						¡No pierdas más el tiempo y déjanos que te lo resumamos de una manera más visual!
					</div>		
					<div class="span-element"><span style="display: none;" data-nivel="2" data-valor="2" class="base4 form-element">Nº Páginas </span></div>										
					<input data-nivel="2" data-valor="2" type="text" class="base4 spinner1 form-element paginascantidad smartspinner" style="margin-right: 40px; margin-left: 3px; display: none; margin-bottom: 12px; width: 50px; height: 36px;">
					<select data-nivel="2" data-valor="2" class="base4 form-element tiempopaginas" style="margin-right: 0px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="120">5 dias</option>
						<option value="240">10 dias</option>
						<option value="360">15 dias</option>
						<option value="480">20 dias</option>
						<option value="500">20+ dias</option>
					</select>				
					<!-- End Base 4 -->
					<!-- Base 5 -->
					<img src="http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies/images/flecha.jpg" data-nivel="2" data-valor="1" class="base5 form-element form-flecha rotateN100Y150" style="display: none; width: 80px;  height: auto; top: 210px; left: -115px;">	
					<div data-nivel="2" data-valor="1" class="base5 form-element form-message rotatem10" style="display: none; width: 130px;  height: auto; top: 130px; left: -160px;">
						Indica si prefieres la presentación basada en imágenes o en texto
					</div>
					<img src="http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies/images/flecha.jpg" data-nivel="2" data-valor="1" class="base5 form-element form-flecha rotate50" style="display: none; width: 80px;  height: auto; top: 290px; left: -115px;">	
					<div data-nivel="2" data-valor="1" class="base5 form-element form-message rotate" style="display: none; width: 130px;  height: auto; top: 360px; left: -160px;">
						Adjunta el trabajo
					</div>
					<div data-nivel="2" data-valor="1" class="base5 form-element form-message rotate10" style="display: none; width: 300px;  height: auto; top: 310px; left: 475px;">
						Te preparamos tanto la presentación como la parte hablada. Practícalo un par de veces, ¡y listo!
					</div>
					<div class="span-element"><span style="display: none;" data-nivel="2" data-valor="1" class="base5 form-element">Nº Diapositivas </span></div>										
					<input data-nivel="2" data-valor="1" type="text" class="base5 spinner1 form-element diapositivaspartiendoescrito smartspinner" style="margin-right: 46px; margin-left: 3px; display: none; margin-bottom: 12px; width: 50px; height: 36px;">
					<select data-nivel="2" data-valor="1" class="base5 form-element action partehabladapartiendoescrito" style="margin-right: 18px; display: none;">
						<option value="0">-- Incluir parte hablada --</option>
						<option value="1">Si</option>
						<option value="2">No</option>						
					</select>
					<select data-nivel="2" data-valor="1" class="base5 form-element tiempopartiendoescrito" style="margin-right: 0px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="120">5 dias</option>
						<option value="240">10 dias</option>
						<option value="360">15 dias</option>
						<option value="480">20 dias</option>
						<option value="500">20+ dias</option>
					</select>
					<div data-nivel="2" data-valor="1" class="base5 form-element barratextoimagenes" style="float: left; position: relative; width: 430px; height: 80px; margin-right: 0px; display: none; margin-bottom: 15px; background: url(http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies//images/bar-component.png) 0% 100% no-repeat;">				
						<span class="bartexttitle" style="position: absolute; top: 6px; left: 10px; font-size: 12px; color: black; line-height: 22px;">Indica que quieres que predomine</span>
						<span class="bartexttexto" style="position: absolute; top: 36px; left: 10px; font-size: 9px;">Texto</span>
						<span class="bartextimagenes" style="position: absolute; top: 36px; right: 4px; font-size: 9px;">Imágenes</span>
						<span class="bartextbalanceado" style="position: absolute; top: 36px; left: 191px; font-size: 9px;">Balanceado</span>
						<span class="barratextoimagenes-left" style="position: absolute; top: 55px; left: 12px; font-size: 9px; width: 20px; height: 20px; cursor: pointer;">&nbsp;</span>
						<span class="barratextoimagenes-right" style="position: absolute; top: 55px; right: 12px; font-size: 9px; width: 20px; height: 20px; cursor: pointer;">&nbsp;</span>
						<span class="barratextoimagenes-center" style="position: absolute; top: 55px; left: 206px; font-size: 9px; width: 20px; height: 20px; cursor: pointer;">&nbsp;</span>
						<span class="barratextoimagenes-handle" data-info="Balanceado" style="position: absolute; top: 55px; left: 206px; font-size: 9px;"><img style="cursor: pointer" src="http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies/images/bar-component-handle.png" alt=""></span>
					</div>
					<img src="http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies/images/flecha.jpg" data-nivel="2" data-valor="2" class="base5 form-element form-flecha rotateN100Y150" style="display: none; width: 80px;  height: auto; top: 210px; left: -115px;">
					<div data-nivel="2" data-valor="2" class="base5 form-element form-message rotatem10" style="display: none; width: 130px;  height: auto; top: 130px; left: -160px;">
						Indica si prefieres la presentación basada en imágenes o en texto
					</div>
					<img src="http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies/images/flecha.jpg" data-nivel="2" data-valor="2" class="base5 form-element form-flecha rotate50" style="display: none; width: 80px;  height: auto; top: 290px; left: -115px;">	
					<div data-nivel="2" data-valor="2" class="base5 form-element form-message rotate" style="display: none; width: 130px;  height: auto; top: 360px; left: -160px;">
						Arrastra un documento explicando el tema a presentar
					</div>
					<div data-nivel="2" data-valor="2" class="base5 form-element form-message rotate10" style="display: none; width: 300px;  height: auto; top: 310px; left: 475px;">
						Te preparamos tanto la presentación como la parte hablada. Practícalo un par de veces, ¡y listo!
					</div>		
					<div class="span-element"><span style="display: none;" data-nivel="2" data-valor="2" class="base5 form-element">Nº Diapositivas </span></div>										
					<input data-nivel="2" data-valor="2" type="text" class="base5 spinner1 form-element diapositivaspartiendocero smartspinner" style="margin-right: 46px; margin-left: 3px; display: none; margin-bottom: 12px; width: 50px; height: 36px;">
					<select data-nivel="2" data-valor="2" class="base5 form-element action partehabladapartiendocero" style="margin-right: 18px; display: none;">
						<option value="0">-- Incluir parte hablada --</option>
						<option value="1">Si</option>
						<option value="2">No</option>						
					</select>
					<select data-nivel="2" data-valor="2" class="base5 form-element tiempopartiendocero" style="margin-right: 0px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="120">5 dias</option>
						<option value="240">10 dias</option>
						<option value="360">15 dias</option>
						<option value="480">20 dias</option>
						<option value="500">20+ dias</option>
					</select>	
					<div data-nivel="2" data-valor="2" class="base5 form-element barratextoimagenes" style="float: left; position: relative; width: 430px; height: 80px; margin-right: 0px; display: none; margin-bottom: 15px; background: url(http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies//images/bar-component.png) 0% 100% no-repeat;">				
						<span class="bartexttitle" style="position: absolute; top: 6px; left: 10px; font-size: 12px; color: black; line-height: 22px;">Indica que quieres que predomine</span>
						<span class="bartexttexto" style="position: absolute; top: 36px; left: 10px; font-size: 9px;">Texto</span>
						<span class="bartextimagenes" style="position: absolute; top: 36px; right: 4px; font-size: 9px;">Imágenes</span>
						<span class="bartextbalanceado" style="position: absolute; top: 36px; left: 191px; font-size: 9px;">Balanceado</span>
						<span class="barratextoimagenes-left" style="position: absolute; top: 55px; left: 12px; font-size: 9px; width: 20px; height: 20px; cursor: pointer;">&nbsp;</span>
						<span class="barratextoimagenes-right" style="position: absolute; top: 55px; right: 12px; font-size: 9px; width: 20px; height: 20px; cursor: pointer;">&nbsp;</span>
						<span class="barratextoimagenes-center" style="position: absolute; top: 55px; left: 206px; font-size: 9px; width: 20px; height: 20px; cursor: pointer;">&nbsp;</span>
						<span class="barratextoimagenes-handle" data-info="Balanceado" style="position: absolute; top: 55px; left: 206px; font-size: 9px;"><img style="cursor: pointer" src="http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies/images/bar-component-handle.png" alt=""></span>
					</div>			
					<!-- End Base 5 -->
					<div class="clear"></div>
					<!--<img src="http://www.pruebaapsiesnetwork.es/wp-content/themes/apsies/images/bg-drop.png" style="cursor: pointer; margin: 10px 20px 0px 0px; float: left;" alt="">-->
					<div class="wrapper-dnd-area">
						<div id="universidad-dnd-area" class="dnd-area">
							<form id="universidad-form" name="universidad-form" action="http://www.pruebaapsiesnetwork.es/wp-admin/admin-ajax.php" method="post" enctype="multipart/form-data">
								<input type="hidden" name="action" value="MyAjaxFunctions">
								<input type="hidden" name="toaction" value="uploadArchive">
								<input id="universidad-form-post-id" type="hidden" name="post_id" value="">
								<input id="universidad-file" type="file" style="display:none;" name="fileselect">
								<input type="hidden" id="fileselect_nonce" name="fileselect_nonce" value="693c4f2044"><input type="hidden" name="_wp_http_referer" value="/">												
							</form>
						</div>
						<div id="universidad-details"></div>
					</div>
					<div class="points" style="float: left; width: 190px; text-align: center; margin-top: 10px; color: #838383;">
						<span class="price">Precio por este pedido<br> <strong style="font-family: OSBold; font-size: 16px; color: black;">0 puntos</strong></span>					
						<a class="btn" href="#" title="Pruébalo!">Pruébalo!</a>
					</div>
				</div>	
							</div>
			<div id="trial-information">				
				<h2 class="info-module-title">Lorem Ipsum Dolor Title</h2><p class="info-module-content">Sed non massa quis nisl tincidunt posuere sit amet vitae sem. Praesent  sed convallis quam. Donec hendrerit neque sit amet tellus ornare et  semper lorem lacinia. Suspendisse ac pellentesque odio. Nunc pharetra lorem ipsum est.</p><a class="info-module-btn" href="http://www.pruebaapsiesnetwork.es/suscripcion" title="Suscríbete">Suscríbete</a>			</div>
			<div class="clear"></div>