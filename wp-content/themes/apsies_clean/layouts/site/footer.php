	<footer class="tm-footer">
		<div class="uk-container uk-container-center">
			<div class="uk-clearfix">
				<div class="left">
					<ul class="left">
					
						<li>
							<a href="<?php echo site_url('aviso_legal');?>">Aviso legal</a>
						</li>
						<li>
							<a href="<?php echo site_url('condiciones-generales-de-contratacion');?>">Condiciones Generales de Contratación</a>
						</li>
						<li>
							<a href="<?php echo site_url('preguntas-frecuentes');?>">Preguntas Frecuentes</a>
						</li>
					</ul>

					<ul class="center">
						<li>
							©  Copyright Aprobar sin Estudiar 2014
						</li>
						<li>
							contacto: <a href="mailto:hola@aprobarsinestudiar.com">hola@aprobarsinestudiar.com</a>
						</li>
					</ul>
				</div>
				
				<div class="right">
					<ul class="rigth icons">
						<li>
							<a href="https://www.facebook.com/pages/Aprobar-Sin-Estudiar/128179243970306" target="_blanck"><span class="uk-icon-facebook"></span></a>
						</li>
						<li>
							<a href="https://twitter.com/AprobarSinEstud" target="_blanck"><span class="uk-icon-twitter"></span></a>
						</li>
						<li>
							<a href="#" target="_blanck"><span class="icon-tuenti"></span></a>
						</li>
					</ul>
				</div>				
			</div>

			
		</div>
		
		<a class="tm-totop-scroller" data-uk-smooth-scroll href="#"></a>
	</footer>

	<?php echo $this->render('footer'); ?>

	<?php if ($this['widgets']->count('offcanvas')) : ?>
	<div id="offcanvas" class="uk-offcanvas">
		<div class="uk-offcanvas-bar"><?php echo $this['widgets']->render('offcanvas'); ?></div>
	</div>
	<?php endif; ?>
	
	<div id="notification" style="position: fixed; top: -50px; left: 50%; opacity: 0; font-family: OSRegular;">		
		<div class="inner" style="position: relative; left: -50%; color: white; padding: 20px;">			
		</div>		
	</div>
	
	<?php 
		@session_start();
		if( isset( $_SESSION['msg'] ) ) {

				$msg = $_SESSION['msg'];
				unset( $_SESSION['msg']);
			?>
			<script type="text/javascript">
				(function($){
					$(document).ready(function(){
						console.log( 'msg dispense ');
						jQuery.UIkit.notify({
					        message : '<?php echo $msg['msg']; ?>',
					        status  : '<?php echo $msg['status']; ?>',
					        timeout : 2500,
					        pos     : 'top-center',
					        onClose : function() {
					            <?php 
						           	if( isset( $msg['url'] ) ) {
						           		?>
						           		window.location = '<?php echo $msg['url']?>';
						           		<?php
						            } 
					            ?>	
					        }
					    });
					})
				})(jQuery)
			</script>
			<?php
		}
	?>

</body>
</html>