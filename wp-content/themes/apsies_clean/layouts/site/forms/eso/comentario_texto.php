<?php
	$config  = $this['config']['eso']['comentario'];
?>


<div class="form-container uk-hidden" rel="3">
	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-2-2">
			<div class="input-group">
				<label for="uni_comentario_asignatura">Asignatura</label>
				<select id="uni_comentario_asignatura" name="submit_pedido[eso][comentario][asignatura]" data-required>
						<option value="">-- Selecciona una asignatura --</option>
					<?php 
						foreach( $this['forms']->asignaturas AS $id => $value ) {
							if( isset( $config[$id]['active'] ) ) { 
								 $selected = ( $value['id'] == $config['asignatura'] ) ? ' selected="selected" ' : '' ;
								?>

								<option id="<?php echo $value['id']?>" <?php echo $selected; ?>> <?php echo $value['name'] ?></option>

								<?php
							}
						}
					?>
				</select>
			</div>
		</div>
	</div>

	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="eso_comentario_nro">Nº de palabras</label>
				<div class="number">
					<a href="#" class="uk-button"><span class="uk-icon-minus"></span></a>
					<input type="text" id="eso_comentario_nro" value="0" name="submit_pedido[eso][comentario][nro_palabras]">
					<a href="#" class="uk-button"><span class="uk-icon-plus"></span></a>
				</div>
			</div>
		</div>
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="eso_comentario_fecha">Fecha de entrega</label>
				<select id="eso_comentario_fecha" name="submit_pedido[eso][comentario][fecha_entrega]" class="fecha_entrega">
					<?php echo $this['template']->render('site/forms/common/fechas_smart', array('config' => $config ) ); ?>
				</select>
			</div>
		</div>
	</div>

	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-1">
			<div class="input-group">
				<label for="eso_comentario_texto_a_comentar">Texto a comentar</label>
				<textarea id="eso_comentario_texto_a_comentar" rows="10" name="submit_pedido[eso][comentario][texto_a_comentar]" placeholder="Ingresa aquí el texto a comentar o adjunta el archivo al final del formulario"></textarea>
			</div>
		</div>
	</div>

	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-1">
			<div class="input-group">
				<label for="eso_comentario_observaciones">Observaciones</label>
				<textarea id="eso_comentario_observaciones" rows="5" name="submit_pedido[eso][comentario][observaciones]" placeholder="¿Algo que necesitemos saber?"></textarea>
			</div>
		</div>
	</div>
</div>
