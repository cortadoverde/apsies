<?php
	$config  = $this['config']['eso']['comentario_artistico'];
?>


<div class="form-container uk-hidden" rel="4">
	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-2-2">
			<div class="input-group">
				<label for="uni_comentario_artistico_asignatura">Asignatura</label>
				<select id="uni_comentario_artistico_asignatura" name="submit_pedido[eso][comentario_artistico][asignatura]" data-required>
						<option value="">-- Selecciona una opción --</option>
					<?php 
						foreach( $this['forms']->asignaturas AS $id => $value ) {
							if( isset( $config[$id]['active'] ) ) { 
								 $selected = ( $value['id'] == $config['asignatura'] ) ? ' selected="selected" ' : '' ;
								?>

								<option id="<?php echo $value['id']?>" <?php echo $selected; ?>> <?php echo $value['name'] ?></option>

								<?php
							}
						}
					?>
				</select>
			</div>
		</div>
	</div>

	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="eso_comentario_artistico_nro">Nº de palabras</label>
				<div class="number">
					<a href="#" class="uk-button"><span class="uk-icon-minus"></span></a>
					<input type="text" id="eso_comentario_artistico_nro" value="0" name="submit_pedido[eso][comentario_artistico][nro_palabras]">
					<a href="#" class="uk-button"><span class="uk-icon-plus"></span></a>
				</div>
			</div>
		</div>
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="eso_comentario_artistico_fecha">Fecha de entrega</label>
				<select id="eso_comentario_artistico_fecha" name="submit_pedido[eso][comentario_artistico][fecha_entrega]" class="fecha_entrega" data-required>
						<option value="">-- Selecciona una fecha --</option>
					<?php echo $this['template']->render('site/forms/common/fechas_smart', array('config' => $config ) ); ?>
				</select>
			</div>
		</div>
	</div>

	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-1">
			<div class="input-group">
				<label for="eso_comentario_artistico_obra_a_comentar">Obra de arte a comentar</label>
				<input type="text" id="eso_comentario_artistico_obra_a_comentar"  name="submit_pedido[eso][comentario_artistico][obra_a_comentar]" placeholder="Nombre de la obra, si no lo conoces puedes adjuntar una imágen"  data-required></textarea>
			</div>
		</div>
	</div>

	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-1">
			<div class="input-group">
				<label for="eso_comentario_artistico_observaciones">Observaciones</label>
				<textarea id="eso_comentario_artistico_observaciones" rows="5" name="submit_pedido[eso][comentario_artistico][observaciones]" placeholder="¿Algo que necesitemos saber?"></textarea>
			</div>
		</div>
	</div>
</div>
