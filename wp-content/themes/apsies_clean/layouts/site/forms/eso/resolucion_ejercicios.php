<?php
	$config = $config = $this['config']['eso']['resolucion_ejercicios'];
?>
<div class="form-container" rel="1">
	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-1">
			<div class="input-group">
				<label for="eso_resolucion_asignatura">Asignatura</label>
				<select id="eso_resolucion_asignatura" name="submit_pedido[eso][resolucion_ejercicio][asignatura]" data-required>
						<option value="">-- Selecciona una opción --</option>
					<?php 
						foreach( $this['forms']->asignaturas AS $id => $value ) {
							if( isset( $config[$id]['active'] ) ) { 
								 $selected = ( $value['id'] == $config['asignatura'] ) ? ' selected="selected" ' : '' ;
								?>

								<option rel="<?php echo $config[$id]['num'] ?>" id="<?php echo $value['id']?>" <?php echo $selected; ?>> <?php echo $value['name'] ?></option>

								<?php
							}
						}
					?>
				</select>
			</div>
		</div>
	</div>

	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="eso_resolucion_ejercicios_nro">Nº de ejercicios</label>
				<div class="number">
					<a href="#" class="uk-button"><span class="uk-icon-minus"></span></a>
					<input type="text" id="eso_resolucion_ejercicios_nro" value="0" name="submit_pedido[eso][resolucion_ejercicio][nro_ejercicios]">
					<a href="#" class="uk-button"><span class="uk-icon-plus"></span></a>
				</div>
			</div>
		</div>
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="eso_resolucion_ejercicios_fecha">Fecha de entrega</label>
				<select id="eso_resolucion_ejercicios_fecha" name="submit_pedido[eso][resolucion_ejercicio][fecha_entrega]" class="fecha_entrega" data-required>
						<option value="">-- Selecciona una fecha --</option>
					<?php echo $this['template']->render('site/forms/common/fechas_smart', array('config' => $config ) ); ?>
				</select>
			</div>
		</div>
	</div>

	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-1">
			<div class="input-group">
				<label for="eso_resolucion_ejercicios_observaciones">Enunciados</label>
				<textarea id="eso_resolucion_ejercicios_observaciones" rows="10" name="submit_pedido[eso][resolucion_ejercicio][enunciado]" placeholder="Escribe los enunciados de los ejercicios a resolver, o házles una foto y arrástrala al formulario"></textarea>
			</div>
		</div>
	</div>

		
</div>
