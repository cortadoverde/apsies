<div class="uk-grid uk-grid-small">
	<div class="uk-width-1-1 uk-width-medium-1-2">
		<div class="input-group">
			<label for="eso_tipo_pedido">Tipo de pedido</label>
			<select name="submit_pedido[eso][tipo]" id="eso_tipo_pedido" class="select_tipo eso">
				<option value="0">-- Selecciona una opción --</option>
				<option value="1" selected>Resolución de ejercicios</option>
				<option value="2">Redacción sobre un tema</option>
				<option value="3">Comentario de texto</option>
				<option value="4">Comentario artístico</option>
				<option value="5">Crítica literaria</option>
				<option value="6">AUTOCAD</option>
				<option value="7">Dudas/Investigación sobre un tema</option>
				<option value="8">Trabajos</option>
			</select>
		</div>
	</div>
</div>