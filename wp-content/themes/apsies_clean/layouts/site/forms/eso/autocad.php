<?php
	$config  = $this['config']['eso']['autocad'];
	$def_tipo = $config['selected'];
	//print_r( $config );
?>


<div class="form-container uk-hidden" rel="6">
	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1">
			<div class="input-group">
				<label for="eso_autocad_tipo">Tipo</label>
				<select type="text" id="eso_autocad_tipo" name="submit_pedido[eso][autocad][tipo]" data-required>
					<option value="">-- Selecciona un opción --</option>
					<?php 
						foreach( $this['forms']->autocad as $id => $value ) {
							if( isset($config[$id]['active']) ) {
								$selected = ( $value['id'] == $def_tipo ) ? ' selected="selected" ' : '';
								?>
									<option value="<?php echo $value['id']?>" <?php echo $selected; ?>><?php echo $value['name'] ?></option>
								<?php
							}
						}
					?>
				</select>
			</div>
		</div>
	</div>

	<div class="sub_form <?php if( $def_tipo != 1 ) { echo 'uk-hidden';}?>" rel="1">
		<div class="uk-grid uk-grid-small">
			<div class="uk-width-1-1 uk-width-medium-1-2">
				<div class="input-group">
					<label for="eso_autocad_nro">Nº de ejercicios</label>
					<div class="number">
						<a href="#" class="uk-button"><span class="uk-icon-minus"></span></a>
						<input type="text" id="eso_autocad_nro" value="0" name="submit_pedido[eso][autocad][nro_ejercicios]">
						<a href="#" class="uk-button"><span class="uk-icon-plus"></span></a>
					</div>
				</div>
			</div>
			<div class="uk-width-1-1 uk-width-medium-1-2">
				<div class="input-group">
					<label for="eso_autocad_fecha">Fecha de entrega</label>
					<select id="eso_autocad_fecha" name="submit_pedido[eso][autocad][fecha_entrega]" class="fecha_entrega" >
					<option value="">-- Selecciona un fecha --</option>
						<?php echo $this['template']->render('site/forms/common/fechas_smart', array('config' => $config ) ); ?>
					</select>
				</div>
			</div>
		</div>
	</div>

	<div class="sub_form <?php if( $def_tipo != 2 ) { echo 'uk-hidden';}?>" rel="2">
		<div class="uk-grid uk-grid-small">
			<div class="uk-width-1-1 uk-width-medium-1-2">
				<div class="input-group">
					<label for="eso_autocad_nro">Número de piezas</label>
					<div class="number">
						<a href="#" class="uk-button"><span class="uk-icon-minus"></span></a>
						<input type="text" id="eso_autocad_nro" value="0" name="submit_pedido[eso][autocad][nro_piezas]">
						<a href="#" class="uk-button"><span class="uk-icon-plus"></span></a>
					</div>
				</div>
			</div>
			<div class="uk-width-1-1 uk-width-medium-1-2">
				<div class="input-group">
					<label for="eso_autocad_fecha_2"> Fecha de entrega</label>
					<select id="eso_autocad_fecha_2" name="submit_pedido[eso][autocad][fecha_entrega]" class="fecha_entrega">
						<option value="">-- Selecciona un fecha --</option>
						<?php echo $this['template']->render('site/forms/common/fechas_smart', array('config' => $config) ); ?>
					</select>
				</div>
			</div>
		</div>
		
		<div class="uk-grid uk-grid-small">
			<div class="uk-width-1-1 uk-width-medium-1-2">
				<div class="input-group">
					<label for="eso_autocad_vistas">Vistas?</label>
					<select id="eso_autocad_vistas" name="submit_pedido[eso][autocad][vistas]" data-required>
						<option value="">-- Selecciona una opción --</option>
						<option value="si">Sí</option>
						<option value="no">No</option>
					</select>
				</div>
			</div>
			<div class="uk-width-1-1 uk-width-medium-1-2">
				<div class="input-group">
					<label for="eso_autocad_acotaciones">Acotaciones?</label>
					<select name="submit_pedido[eso][autocad][acotaciones]" id="eso_autocad_acotaciones" data-required>
						<option value="">-- Selecciona una opción --</option>
						<option value="si">Sí</option>
						<option value="no">No</option>
					</select>
				</div>
			</div>
		</div>
	</div>

	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-1">
			<div class="input-group">
				<label for="eso_autocad_observaciones">Observaciones</label>
				<textarea id="eso_autocad_observaciones" rows="5" name="submit_pedido[eso][autocad][observaciones]" placeholder="¿Algo que necesitemos saber?"></textarea>
			</div>
		</div>
	</div>
</div>
