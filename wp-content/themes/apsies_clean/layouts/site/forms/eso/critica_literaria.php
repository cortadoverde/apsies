<?php
	$config  = $this['config']['eso']['critica'];
	$partes  = array_chunk($this['forms']->partes, 3, true );

?>


<div class="form-container uk-hidden" rel="5">
	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-2-2">
			<div class="input-group">
				<label for="uni_critica_literaria_titulo">Título de la obra</label>
				<input  placeholder="Escribe el título de la obra literaria" type="text" id="uni_critica_literaria_titulo" name="submit_pedido[eso][critica_literaria][titulo]"  data-required>
			</div>
		</div>
	</div>

	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="eso_critica_literaria_nro">Nº de páginas</label>
				<div class="number">
					<a href="#" class="uk-button"><span class="uk-icon-minus"></span></a>
					<input type="text" id="eso_critica_literaria_nro" value="0" name="submit_pedido[eso][critica_literaria][nro_paginas]">
					<a href="#" class="uk-button"><span class="uk-icon-plus"></span></a>
				</div>
			</div>
		</div>
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="eso_critica_literaria_fecha">Fecha de entrega</label>
				<select id="eso_critica_literaria_fecha" name="submit_pedido[eso][critica_literaria][fecha_entrega]"  class="fecha_entrega" data-required>
						<option value="">-- Selecciona una fecha --</option>
					<?php echo $this['template']->render('site/forms/common/fechas_smart', array('config' => $config ) ); ?>
				</select>
			</div>
		</div>
	</div>

	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-2">
			
			<?php 
				foreach( $partes[0] AS $id => $value ) {
					if( isset( $config[$id]['active'] ) ) { 
						 $checked = isset( $config[$id]['selected'] ) ? ' checked="checked" ' : '' ;
						?>
						<p>
							<label>
								<input type="checkbox" name="submit_pedido[eso][critica_literaria][incluye][]" value="<?php echo $value['name']; ?>" <?php echo $checked; ?>>
								<?php echo $value['name'] ?>
							</label>
						</p>

						<?php
					}
				}
			?>
		</div>
		<div class="uk-width-1-1 uk-width-medium-1-2">
			
			<?php 
				foreach( $partes[1] AS $id => $value ) {
					if( isset( $config[$id]['active'] ) ) { 
						 $checked = isset( $config[$id]['selected'] ) ? ' checked="checked" ' : '' ;
						?>
						<p>
							<label>
								<input type="checkbox" name="submit_pedido[eso][critica_literaria][incluye][]" value="<?php echo $value['name']; ?>" <?php echo $checked; ?>>
								<?php echo $value['name'] ?>
							</label>
						</p>
						<?php
					}
				}
			?>
		</div>
	</div>

	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-1">
			<div class="input-group">
				<label for="eso_critica_literaria_observaciones">Observaciones</label>
				<textarea id="eso_critica_literaria_observaciones" rows="5" name="submit_pedido[eso][critica_literaria][observaciones]" placeholder="¿Algo que necesitemos saber?"></textarea>
			</div>
		</div>
	</div>
</div>
