<?php
	$config = $config = $this['config']['eso']['trabajo'];
?>
<div class="form-container uk-hidden" rel="8">
	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-1">
			<div class="input-group">
				<label for="eso_trabajo_titulo">Título del trabajo</label>
				<input type="text" name="submit_pedido[eso][trabajo][titulo]" id="eso_trabajo_titulo" value="" placeholder="Escribe un título descriptivo"  data-required>
			</div>
		</div>
	</div>

	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="eso_trabajo_tema">Tema</label>
				<select id="eso_trabajo_tema" name="submit_pedido[eso][trabajo][tema]" data-required>
						<option value="">-- Selecciona un tema --</option>
					<?php 
						foreach( $this['forms']->asignaturas AS $id => $value ) {
							if( isset( $config[$id]['active'] ) ) { 
									$checked = isset( $config[$id]['selected'] ) ? ' checked="checked" ' : '' ;
								?>

								<option id="<?php echo $value['id']?>" <?php echo $checked ?>> <?php echo $value['name'] ?></option>

								<?php
							}
						}
					?>
				</select>
			</div>
		</div>
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="eso_trabajo_nro_pag">Nº de páginas</label>
				<div class="number">
					<a href="#" class="uk-button"><span class="uk-icon-minus"></span></a>
					<input type="text" id="eso_trabajo_nro_pag" value="0" name="submit_pedido[eso][trabajo][nro_pag]">
					<a href="#" class="uk-button"><span class="uk-icon-plus"></span></a>
				</div>
			</div>
		</div>
	</div>

	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="eso_trabajo_fecha">Fecha de entrega</label>
				<select id="eso_trabajo_fecha" name="submit_pedido[eso][trabajo][fecha_entrega]"  class="fecha_entrega" data-required>
						<option value="">-- Selecciona una fecha --</option>
					<?php echo $this['template']->render('site/forms/common/fechas_smart', array('config' => $config ) ); ?>
				</select>
			</div>
		</div>
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="eso_trabajo_bibliografia">Bibliografía</label>
				<select name="submit_pedido[eso][trabajo][bibliografia]" id="eso_trabajo_bibliografia" data-required>
						<option value="">-- Selecciona una opción --</option>
					<option value="si" <?php if( $config['bibliografia'] == 'si' ) { echo "selected"; } ?>>Sí</option>
					<option value="no" <?php if( $config['bibliografia'] == 'no' ) { echo "selected"; } ?>>No</option>
				</select>
			</div>
		</div>
	</div>

	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-1">
			<div class="input-group">
				<label for="eso_trabajo_observaciones">Observaciones</label>
				<textarea id="eso_trabajo_observaciones" name="submit_pedido[eso][trabajo][observaciones]" placeholder="¿Algo que necesitemos saber?"></textarea>
			</div>
		</div>
	</div>	
</div>
