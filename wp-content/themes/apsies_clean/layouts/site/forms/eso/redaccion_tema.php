<?php
	$config  = $this['config']['eso']['redaccion_tema'];
?>


<div class="form-container uk-hidden" rel="2">
	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="uni_redaccion_tema_asignatura">Asignatura</label>
				<select id="uni_redaccion_tema_asignatura" name="submit_pedido[eso][redaccion_tema][asignatura]" data-required>
					<option value="">-- Selecciona un asignatura --</option>
					<?php 
						foreach( $this['forms']->asignaturas AS $id => $value ) {
							if( isset( $config[$id]['active'] ) ) { 
								 $selected = ( $value['id'] == $config['asignatura'] ) ? ' selected="selected" ' : '' ;
								?>

								<option id="<?php echo $value['id']?>" <?php echo $selected; ?>> <?php echo $value['name'] ?></option>

								<?php
							}
						}
					?>
				</select>
			</div>
		</div>

		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="uni_redaccion_tema_tema">Tema</label>
				<input placeholder="Escribe el tema a redactar" type="text" id="uni_redaccion_tema_tema" name="submit_pedido[eso][redaccion_tema][tema]" data-required>
			</div>
		</div>
	</div>

	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="eso_redaccion_tema_nro">Nº de palabras</label>
				<div class="number">
					<a href="#" class="uk-button"><span class="uk-icon-minus"></span></a>
					<input type="text" id="eso_redaccion_tema_nro" value="0" name="submit_pedido[eso][redaccion_tema][nro_palabras]">
					<a href="#" class="uk-button"><span class="uk-icon-plus"></span></a>
				</div>
			</div>
		</div>
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="eso_redaccion_tema_fecha">Fecha de entrega</label>
				<select id="eso_redaccion_tema_fecha" name="submit_pedido[eso][redaccion_tema][fecha_entrega]" class="fecha_entrega" data-required>
					<option value="">-- Selecciona un fecha --</option>
					<?php echo $this['template']->render('site/forms/common/fechas_smart', array('config' => $config ) ); ?>
				</select>
			</div>
		</div>
	</div>

	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-1">
			<div class="input-group">
				<label for="eso_redaccion_tema_observaciones">Observaciones</label>
				<textarea id="eso_redaccion_tema_observaciones" rows="5" name="submit_pedido[eso][redaccion_tema][observaciones]" placeholder="¿Algo que necesitemos saber?"></textarea>
			</div>
		</div>
	</div>

		
</div>
