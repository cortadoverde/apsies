<?php
	$config  = $this['config']['eso']['dudas'];
?>


<div class="form-container uk-hidden" rel="7">
	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="eso_dudas_asignatura">Asignatura</label>
				<select id="eso_dudas_asignatura" name="submit_pedido[eso][dudas][asignatura]" data-required>
						<option value="">-- Selecciona una asignatura --</option>
					<?php 
						foreach( $this['forms']->asignaturas AS $id => $value ) {
							if( isset( $config[$id]['active'] ) ) { 
								 $selected = ( $value['id'] == $config['asignatura'] ) ? ' selected="selected" ' : '' ;
								?>

								<option id="<?php echo $value['id']?>" <?php echo $selected; ?>> <?php echo $value['name'] ?></option>

								<?php
							}
						}
					?>
				</select>
			</div>
		</div>

		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="eso_dudas_tema">Tema</label>
				<input type="text" id="eso_dudas_tema" name="submit_pedido[eso][dudas][tema]"  data-required placeholder="Escribe el tema sobre el que quieras información">
			</div>
		</div>
	</div>

	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="eso_dudas_nro">Nº de palabras</label>
				<div class="number">
					<a href="#" class="uk-button"><span class="uk-icon-minus"></span></a>
					<input type="text" id="eso_dudas_nro" value="0" name="submit_pedido[eso][dudas][nro_palabras]">
					<a href="#" class="uk-button"><span class="uk-icon-plus"></span></a>
				</div>
			</div>
		</div>
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="eso_dudas_fecha">Fecha de entrega</label>
				<select id="eso_dudas_fecha" name="submit_pedido[eso][dudas][fecha_entrega]" class="fecha_entrega" data-required>
						<option value="">-- Selecciona una fecha --</option>
					<?php echo $this['template']->render('site/forms/common/fechas_smart', array('config' => $config ) ); ?>
				</select>
			</div>
		</div>
	</div>

	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-1">
			<div class="input-group">
				<label for="eso_dudas_observaciones">Observaciones</label>
				<textarea id="eso_dudas_observaciones" rows="5" name="submit_pedido[eso][dudas][observaciones]" placeholder="¿Algo que necesitemos saber?"></textarea>
			</div>
		</div>
	</div>

		
</div>
