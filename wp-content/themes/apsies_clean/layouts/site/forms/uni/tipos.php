<div class="uk-grid uk-grid-small">
	<div class="uk-width-1-1 uk-width-medium-1-2">
		<div class="input-group">
			<label for="uni_tipo_pedido">Tipo de pedido</label>
			<select name="submit_pedido[uni][tipo]" id="uni_tipo_pedido" class="select_tipo uni">
				<option value="0">-- Selecciona una opción --</option>
				<option value="1" selected>Trabajos</option>
				<option value="2">AutoCAD</option>
				<!-- <option value="3">Apuntes</option> -->
				<option value="4">PowerPoint</option>
				<option value="5">Exposiciones</option>	
			</select>
		</div>
	</div>
</div>