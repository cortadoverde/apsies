<?php
	$config = $config = $this['config']['uni']['autocad'];

?>

<div class="form-container uk-hidden" rel="2">
	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="uni_autocad_nro">Número de piezas</label>
				<div class="number">
					<a href="#" class="uk-button"><span class="uk-icon-minus"></span></a>
					<input type="text" id="uni_autocad_nro" value="0" name="submit_pedido[uni][autocad][nro_piezas]">
					<a href="#" class="uk-button"><span class="uk-icon-plus"></span></a>
				</div>
			</div>
		</div>
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="uni_autocad_fecha"> Fecha de entrega</label>
				<select id="uni_autocad_fecha" name="submit_pedido[uni][autocad][fecha_entrega]" class="fecha_entrega" data-required>
					<option value="">-- Selecciona un fecha --</option>
					<?php echo $this['template']->render('site/forms/common/fechas_smart', array('config' => $config, 'type' => 'autocad') ); ?>
				</select>
			</div>
		</div>
	</div>
 
	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="uni_autocad_vistas">¿Vistas?</label>
				<select id="uni_autocad_vistas" name="submit_pedido[uni][autocad][vistas]" data-required>
					<option value="">-- Selecciona un opción --</option>
					<option value="si">Sí</option>
					<option value="no">No</option>
				</select>
			</div>
		</div>
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="uni_autocad_acotaciones">¿Acotaciones?</label>
				<select name="submit_pedido[uni][autocad][acotaciones]" id="uni_autocad_acotaciones" data-required>
					<option value="">-- Selecciona un opción --</option>
					<option value="si">Sí</option>
					<option value="no">No</option>
				</select>
			</div>
		</div>
	</div>

	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-1">
			<div class="input-group">
				<label for="uni_autocad_observaciones">Observaciones</label>
				<textarea name="submit_pedido[uni][autocad][observaciones]" placeholder="¿Algo que necesitemos saber?"></textarea>
			</div>
		</div>
	</div>
</div>