<?php
	$config = $config = $this['config']['uni']['apuntes'];

?>

<div class="form-container uk-hidden" rel="3">
	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="uni_apuntes_tipo">Tipo</label>
				<select name="submit_pedido[uni][apuntes][tipo]" id="uni_apuntes_tipo">
					<option value="subir a la web" <?php if( $config['tipo'] == 'subir_web' ) { echo ' selected ';}?> > Subir a la web</option>
					<option value="paqueteria" <?php if( $config['tipo'] != 'subir_web' ) { echo ' selected ';}?> > Paquetería</option>
				</select>
			</div>
		</div>
	</div>
 
	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="uni_apuntes_nro">Nº Páginas a sucio</label>
				<div class="number">
					<a href="#" class="uk-button"><span class="uk-icon-minus"></span></a>
					<input type="text" id="uni_apuntes_nro" value="0" name="submit_pedido[uni][apuntes][nro_paginas]">
					<a href="#" class="uk-button"><span class="uk-icon-plus"></span></a>
				</div>
			</div>
		</div>

		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="uni_apuntes_fecha"> Fecha de entrega</label>
		
				<select id="uni_apuntes_fecha" name="submit_pedido[uni][apuntes][fecha_entrega]" class="fecha_entrega"> 
					<?php echo $this['template']->render('site/forms/common/fechas_smart', array('config' => $config, 'type' => 'uni') ); ?>
				</select>
			</div>
		</div>
	</div>

	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-1">
			<div class="input-group">
				<label for="uni_pedidos_observaciones">Observaciones</label>
				<textarea id="uni_pedidos_observaciones" name="submit_pedido[uni][apuntes][observaciones]" placeholder="¿Algo que necesitemos saber?"></textarea>
			</div>
		</div>
	</div>
</div>