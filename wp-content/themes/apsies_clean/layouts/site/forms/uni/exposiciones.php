<?php
	$config = $config = $this['config']['uni']['exposicion'];

?>

<div class="form-container uk-hidden" rel="5">
	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="uni_exposicion_tipo">Tipo</label>
				<select name="submit_pedido[uni][exposicion][tipo]" id="uni_exposicion_tipo" data-required>
					<option value="">-- Selecciona un opción --</option>
					<option value="trabajo_escrito" <?php if( $config['tipo'] == 'trabajo_escrito' ) { echo ' selected ';}?> > A partir de un trabajo escrito </option>
					<option value="de_0" <?php if( $config['tipo'] != 'trabajo_escrito' ) { echo ' selected ';}?> > Partiendo de cero </option>
				</select>
			</div>
		</div>

		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="uni_exposicion_parte_hablada">¿Parte hablada?</label>
				<select name="submit_pedido[uni][exposicion][parte_hablada]" id="uni_exposicion_parte_hablada" data-required>
					<option value="">-- Selecciona un opción --</option>
					<option value="si" <?php if( $config['parte_hablada'] == 'si' ) { echo ' selected ';}?> >Sí</option>
					<option value="no" <?php if( $config['parte_hablada'] != 'si' ) { echo ' selected ';}?> >No</option>
				</select>
			</div>
		</div>
	</div>
 
	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="uni_exposicion_nro_pag">Nº de Diapositivas</label>
				<div class="number">
					<a href="#" class="uk-button"><span class="uk-icon-minus"></span></a>
					<input type="text" id="uni_exposicion_nro_pag" value="0" name="submit_pedido[uni][exposicion][nro_paginas]">
					<a href="#" class="uk-button"><span class="uk-icon-plus"></span></a>
				</div>
			</div>
		</div>

		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="uni_exposicion_fecha"> Fecha de entrega</label>
		
				<select id="uni_exposicion_fecha" name="submit_pedido[uni][exposicion][fecha_entrega]" class="fecha_entrega" data-required>
					<option value="">-- Selecciona un fecha --</option>
					<?php echo $this['template']->render('site/forms/common/fechas_smart', array('config' => $config, 'type' => 'uni') ); ?>
				</select>
			</div>
		</div>
	</div>

	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-1">
			<div class="input-group">
				<small>Indica qué quieres que predomine</small>
				<div class="slider">
					<!-- labels -->
					<span class="label bartexttexto" >Texto</span>
					<span class="label bartextimagenes" >Imágenes</span>
					<span class="label bartextbalanceado" >Balanceado</span>
					<!-- Handlers -->
					<div class="handle_selector selector_left" rel="texto"></div>
					<div class="handle_selector selector_center" rel="balanceado"></div>
					<div class="handle_selector selector_right" rel="imagenes"></div>
					
					<!-- Image position -->
					<span class="indicador" data-info="balanceado"><img style="cursor: pointer" src="<?php echo $this['path']->url('theme:/images/bar-component-handle.png')?>" alt=""></span>
					<input type="hidden" value="balanceado" name="submit_pedido[uni][exposicion][predominio]">
				</div>
			</div>
		</div>
	</div>

	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-1">
			<div class="input-group">
				<label for="uni_exposicion_observacion">Observaciones</label>
				<textarea id="uni_exposicion_observacion" name="submit_pedido[uni][exposicion][observaciones]" placeholder="¿Algo que necesitemos saber?"></textarea>
			</div>
		</div>
	</div>
</div>