<?php
	$config = $config = $this['config']['uni']['trabajo'];
	//print_r($config);
?>
<div class="form-container" rel="1">
	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-1">
			<div class="input-group">
				<label for="uni_trabajos_titulo">Título del trabajo</label>
				<input data-required type="text" id="uni_trabajos_titulo" name="submit_pedido[uni][trabajos][titulo]" value="" placeholder="Escribe un título descriptivo">
			</div>	
		</div>
	</div>

	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="uni_trabajos_tema">Tema</label>
				<select id="uni_trabajos_tema" name="submit_pedido[uni][trabajos][tema]" data-required>
					<option value="">-- Selecciona un tema --</option>
					<?php 
						foreach( $this['forms']->asignaturas_uni AS $id => $value ) {
							if( isset( $config[$id]['active'] ) ) { 
								$checked = isset( $config[$id]['selected'] ) ? ' checked="checked" ' : 'data-t="'.$value['id'].'"' ;
								//$checked = isset( $config[$id]['asignatura'] ) && $config[$id]['asignatura'] == $id ? ' checked="checked" ' : ''; 

								?>

								<option id="<?php echo $value['id']?>" <?php echo $checked ?>> <?php echo $value['name'] ?></option>

								<?php
							}
						}
					?>
				</select>
			</div>
		</div>
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="uni_trabajos_nro_pag">Nº de páginas</label>
				<div class="number">
					<a href="#" class="uk-button"><span class="uk-icon-minus"></span></a>
					<input type="text" id="uni_trabajos_nro_pag" value="0" name="submit_pedido[uni][trabajos][nro_pag]">
					<a href="#" class="uk-button"><span class="uk-icon-plus"></span></a>
				</div>
			</div>
		</div>
	</div>

	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="uni_trabajos_fecha">Fecha de entrega</label>
				<select id="uni_trabajos_fecha" name="submit_pedido[uni][trabajos][fecha_entrega]" class="fecha_entrega" data-required>
					<option value="">-- Selecciona un fecha --</option>
					<?php echo $this['template']->render('site/forms/common/fechas_smart', array('config' => $config, 'type' => 'autocad' ) ); ?>
				</select>
			</div>
		</div>
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="uni_trabajos_bibliografia">Bibliografía</label>
				<select name="submit_pedido[uni][trabajos][bibliografia]" id="uni_trabajos_bibliografia" data-required>
					<option value="">-- Selecciona una opción --</option>
					<option value="si" <?php if( $config['bibliografia'] == 'si' ) { echo "selected"; } ?>>Sí</option>
					<option value="no" <?php if( $config['bibliografia'] == 'no' ) { echo "selected"; } ?>>No</option>
				</select>
			</div>
		</div>
	</div>

	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-1">
			<div class="input-group">
				<label for="uni_trabajos_observaciones">Observaciones</label>
				<textarea name="submit_pedido[uni][trabajos][observaciones]" placeholder="¿Algo que necesitemos saber?"></textarea>
			</div>
		</div>
	</div>	
</div>
