<?php
	$config = $config = $this['config']['uni']['powepoint'];

?>

<div class="form-container uk-hidden" rel="4">
	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="uni_powerpoint_tipo">Tipo</label>
				<select name="submit_pedido[uni][powerpoint][tipo]" id="uni_powerpoint_tipo" data-required>
					<option value="">-- Selecciona un opción --</option>
					<option value="diapo_texto" <?php if( $config['tipo'] == 'diapo_texto' ) { echo ' selected ';}?> > Diapositivas a Texto </option>
					<option value="texto_diapo" <?php if( $config['tipo'] != 'diapo_texto' ) { echo ' selected ';}?> > Texto a Diapositivas </option>
				</select>
			</div>
		</div> 
	</div>
 
	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="uni_powerpoint_nro_pag"><?php echo ( ($config['tipo'] == 'diapo_texto' ) ? 'Nº de diapositivas' : 'Nº de paginas' ); ?></label>
				<div class="number">
					<a href="#" class="uk-button"><span class="uk-icon-minus"></span></a>
					<input type="text" id="uni_powerpint_nro_pag" value="0" name="submit_pedido[uni][powerpoint][nro_paginas]">
					<a href="#" class="uk-button"><span class="uk-icon-plus"></span></a>
				</div>
			</div>
		</div>

		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="uni_powerpoint_fecha"> Fecha de entrega</label>
		
				<select id="uni_powerpoint_fecha" name="submit_pedido[uni][powerpoint][fecha_entrega]" class="fecha_entrega" data-required>
					<option value="">-- Selecciona un fecha --</option>
					<?php echo $this['template']->render('site/forms/common/fechas_smart', array('config' => $config, 'type' => 'uni') ); ?>
				</select>
			</div>
		</div>
	</div>

	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-1">
			<div class="input-group">
				<label for="uni_powerpoint_observaciones" >Observaciones</label>
				<textarea id="uni_powerpoint_observaciones" name="submit_pedido[uni][powerpoint][observaciones]" placeholder="¿Algo que necesitemos saber?"></textarea>
			</div>
		</div>
	</div>
</div>