
	<div class="uk-grid uk-grid-small">
		<div class="uk-width-1-1 uk-width-medium-1-2">
			<div class="input-group">
				<label for="suscrito_asignatura_pedido">Asignatura</label>
				<select name="submit_pedido[suscrito][asignatura]" id="suscrito_asignatura_pedido" class="suscrito asignatura">
					<option value="0">-- Selecciona una opción --</option>
					<option value="1" selected>Matemáticas</option>
					<option value="2">Lengua Castellana</option>
					<option value="3">Física</option>
					<option value="4">Química</option>
					<option value="5">Ciencias Sociales</option>
					<option value="6">Inglés</option>
					<option value="7">Francés</option>
					<option value="8">Geografía</option>
					<option value="9">Dibujo Técnico</option>
					<option value="10">Historia</option>
					<option value="11">Arte</option>
					<option value="12">Informática</option>
					<option value="13">Economía</option>
					<option value="14">Biología</option>
					<option value="15">Filosofía</option>
					<option value="16">Alemán</option>
					<option value="17">Latín / Griego</option>
				</select>
			</div>
		</div>
	</div>
	<div class="place_form">
		
	</div>
	<div class="sus_sub_form">
		
	</div>
