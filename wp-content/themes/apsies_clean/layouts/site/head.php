<!DOCTYPE HTML>
<html lang="<?php echo $this['config']->get('language'); ?>" dir="<?php echo $this['config']->get('direction'); ?>"  data-config='<?php echo $this['config']->get('body_config','{}'); ?>' base="<?php echo $this['path']->url('theme:/')?>" site-path="<?php echo site_url( );?>">

<head>
<?php echo $this['template']->render('head'); ?>
</head>

<body class="<?php echo $this['config']->get('body_classes'); ?> <?php if ( $this['user']->me->is_suscripto) { echo 'suscripto'; } ?>">
	
	<div class="uk-container uk-container-center header">
		<div class="tm-headerbar uk-clearfix">				
			<div class="uk-grid">
				<div class="uk-width-large-4-10 uk-width-small-4-4" >
					<?php if ($this['widgets']->count('logo')) : ?>
						<a class="tm-logo" href="<?php echo $this['config']->get('site_url'); ?>"><?php echo $this['widgets']->render('logo'); ?></a>
					<?php endif; ?>
				</div>
				<div class="actions">
					<?php echo $this['template']->render('components/form-login'); //echo $this['widgets']->render('headerbar'); ?>
			
					<?php if ($this['widgets']->count('menu')) : ?>
						<?php echo $this['widgets']->render('menu'); ?>
					<?php endif; ?>
				</div>
			</div>
			
		</div>
	</div>