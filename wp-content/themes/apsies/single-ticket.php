<?php 

  global $usuariologueado;

  if ( !is_user_logged_in() ) {
    $usuariologueado = false;  
    $vds = 1;
    $lg = 0;
    $suscrito = false;      
  } else {
    $usuariologueado = true;
    $lg = 1;
    global $current_user;
	get_currentuserinfo();
	$user_id = $current_user->ID;
	$fechasuscripcion = get_user_meta( $user_id, 'fechasuscripcion', true );
	$diassuscripcion = get_user_meta( $user_id, 'diassuscripcion', true );
	$suscrito = false;
	$ahora = time();
	$nivel = get_user_meta( $user_id, 'nivel', true );
	if (($fechasuscripcion <= $ahora) && ($ahora <= $fechasuscripcion + ($diassuscripcion * 86400))) {
		$suscrito = true;
		$vds = getOption("eto_valordescuentosuscritos", 1);
		$vds = 1 - ($vds / 100);
	} else {
		$vds = 1;
	}
  }

  if ($usuariologueado) {
  	get_header(); 
 ?>
	
	<?php while ( have_posts() ) : the_post(); ?>

	<?php 
	// Getting relative pedido
	$pedido_id = get_post_meta(get_the_ID(), "pedidoticket", true);
	// Getting relative user
	$pedido_user_id = get_post_meta($pedido_id, "usuario", true);
	if ($user_id == $pedido_user_id) {
		$post_id = get_the_ID();
		if ($post_id > 0) {			
			update_post_meta($post_id, "ticketvisto", "yes");			
		}			
	?>
	<div id="wrapper-content">
		<?php get_sidebar('left'); ?>	

		<div id="content">
			<div class="titling">
				<?php 
				if ($suscrito) {
				?>
				<h2 class="content-title">Ticket recibido</h2>
				<?php
				} else {
				?>
				<h2 class="content-title-ns">Ticket recibido</h2>
				<?php
				}
				?>
				<p><?php the_title(); ?></p>
			</div>
			<div class="ticket-content">
				<p class="ticket-date"><?php the_date(); ?> </p>
				<?php the_content(); ?>				
				<p class="ticket-pedido-asociado">Detalles del pedido asociado al ticket</p>
				<p>
					<?php echo get_the_title($pedido_id); ?>
				</p>								

				<?php
				$the_post_content = get_post($pedido_id);
				$content = $the_post_content->post_content;
				$content = apply_filters('the_content', $content);
				$content = str_replace(']]>', ']]>', $content);
				// Print Content data or further use it as required
				echo $content;
				?>

				<?php // If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					} 
				?>
			</div>

		</div>

		<div class="clear"></div>
	</div>
	<?php 
	} else {
	?>
	<div id="wrapper-content">
		<div id="page-content">
			
			<div class="titling">
				<h2 class="content-title">Error</h2>
				<p>No tiene autorizaci&oacute;n para ver esta p&aacute;gina</p>
			</div>
					
		</div>			
	</div>
	<?php
	} 
	?>	
		
	<?php endwhile; // end of the loop. ?>
		
	<?php get_footer(); 

	} else {
  	?>
  	<html>
  		<head>
  			<script type="text/javascript">      
            	window.location = "<?php echo wp_login_url(); ?>";        
          	</script>
  		</head>
  		<body></body>
  	</html>
  	<?
  }
	?>