<?php 

/*
	Helpers
*/
function getOption($name, $default = "") {	
	if (eto_get_option($name) != "") {
		return eto_get_option($name);
	} else {
		return $default;
	}
}

function getImage($name, $default = "") {	
	if (eto_get_image($name) != "") {
		return eto_get_image($name);
	} else {
		return $default;
	}
}
/*
	End Helpers
*/

/*
	HOME
*/

/*
	4 Modules
*/	
function home4modules() {	
	$output  = "";

	$output .= '<div class="module">';
	$output .= 	'<a href="' . getOption("eto_home4modulesU1M", "#") . '" title="' . getOption("eto_home4modulesT1M") . '"><img src="' . getImage("eto_home4modulesI1M", get_template_directory_uri() . "/images/module1.png") . '" alt="' . getOption("eto_home4modulesT1M") . '" class="img-module"></a>';
	$output .= 	'<h4 class="title-module">' . getOption("eto_home4modulesT1M", "Suscr&iacute;bete a<br/> nuestro servicio") . '</h4>';
	$output .= 	'<p class="desc-module">' . getOption("eto_home4modulesD1M", "This is Photoshop''s version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin.") . '</p>';
	$output .= '</div>';

	$output .= '<div class="module">';
	$output .= 	'<a href="' . getOption("eto_home4modulesU2M", "#") . '" title="' . getOption("eto_home4modulesT2M") . '"><img src="' . getImage("eto_home4modulesI2M", get_template_directory_uri() . "/images/module2.png") . '" alt="' . getOption("eto_home4modulesT2M") . '" class="img-module"></a>';
	$output .= 	'<h4 class="title-module">' . getOption("eto_home4modulesT2M", "Suscr&iacute;bete a<br/> nuestro servicio") . '</h4>';
	$output .= 	'<p class="desc-module">' . getOption("eto_home4modulesD2M", "This is Photoshop''s version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin.") . '</p>';
	$output .= '</div>';

	$output .= '<div class="module">';
	$output .= 	'<a href="' . getOption("eto_home4modulesU3M", "#") . '" title="' . getOption("eto_home4modulesT3M") . '"><img src="' . getImage("eto_home4modulesI3M", get_template_directory_uri() . "/images/module3.png") . '" alt="' . getOption("eto_home4modulesT3M") . '" class="img-module"></a>';
	$output .= 	'<h4 class="title-module">' . getOption("eto_home4modulesT3M", "Suscr&iacute;bete a<br/> nuestro servicio") . '</h4>';
	$output .= 	'<p class="desc-module">' . getOption("eto_home4modulesD3M", "This is Photoshop''s version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin.") . '</p>';
	$output .= '</div>';

	$output .= '<div class="module">';
	$output .= 	'<a href="' . getOption("eto_home4modulesU4M", "#") . '" title="' . getOption("eto_home4modulesT4M") . '"><img src="' . getImage("eto_home4modulesI4M", get_template_directory_uri() . "/images/module4.png") . '" alt="' . getOption("eto_home4modulesT4M") . '" class="img-module"></a>';
	$output .= 	'<h4 class="title-module">' . getOption("eto_home4modulesT4M", "Suscr&iacute;bete a<br/> nuestro servicio") . '</h4>';
	$output .= 	'<p class="desc-module">' . getOption("eto_home4modulesD4M", "This is Photoshop''s version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin.") . '</p>';
	$output .= '</div>';

	echo $output;
}


/*
	End 4 Modules
*/

/*
	Suscription Area
*/
function suscriptionArea() {
	$output  = "";
	$output .= '<h2 class="info-module-title">' . getOption("eto_homesuscripcionT", "Lorem Ipsum Dolor Title") . '</h2>';
	$output .= '<p class="info-module-content">' . getOption("eto_homesuscripcionD", "Sed non massa quis nisl tincidunt posuere sit amet vitae sem. Praesent  sed convallis quam. Donec hendrerit neque sit amet tellus ornare et  semper lorem lacinia. Suspendisse ac pellentesque odio. Nunc pharetra lorem ipsum est.") . '</p>';
	$output .= '<a class="info-module-btn" href="' . getOption("eto_homesuscripcionU", get_bloginfo("url") . "/suscripcion") . '" title="' . getOption("eto_homesuscripcionB", "Suscr&iacute;bete") . '">' . getOption("eto_homesuscripcionB", "Suscr&iacute;bete") . '</a>';	
	echo $output;
}				

/*
	End Suscription Area
*/

/*
	Posts Area
*/
function postsArea() {
	$output  = "";
	$output .= '<div class="information-module">';
	$output .= 	'<div class="information-module-left">';
	$output .= 		'<h2 class="info-module-title">' . getOption("eto_homepostsA1T", "Lorem Ipsum Dolor Title") . '</h2>';
	$output .= 		'<p class="info-module-content">' . getOption("eto_homepostsA1D", "Sed non massa quis nisl tincidunt posuere sit amet vitae sem. Praesent  sed convallis quam. Donec hendrerit neque sit amet tellus ornare et  semper lorem lacinia. Suspendisse ac pellentesque odio. Nunc pharetra lorem ipsum est.") . '</p>';
	$output .= 		'<a class="info-module-btn" href="' . getOption("eto_homepostsA1BU", "#") . '" title="' . getOption("eto_homepostsA1BT", "Lorem Ipsum") . '">' . getOption("eto_homepostsA1BT", "Lorem Ipsum") . '</a>';
	$output .= 	'</div>';
	$output .= 	'<div class="information-module-right">';
	$output .= 		'<img src="' . getImage("eto_homepostsA1I", get_template_directory_uri() . "/images/dummy-info.jpg") . '" alt="' . getOption("eto_homepostsA1T", "Lorem Ipsum Dolor Title") . '">';
	$output .= 	'</div>';
	$output .= 	'<div class="clear"></div>';
	$output .= '</div>';
	$output .= '<div class="clear"></div>';
	$output .= '<div class="information-module">';				
	$output .= 	'<div class="information-module-left">';
	$output .= 		'<img src="' . getImage("eto_homepostsA2I", get_template_directory_uri() . "/images/dummy-info.jpg") . '" alt="' . getOption("eto_homepostsA2T", "Lorem Ipsum Dolor Title") . '">';
	$output .= 	'</div>';
	$output .= 	'<div class="information-module-right">';
	$output .= 		'<h2 class="info-module-title">' . getOption("eto_homepostsA2T", "Lorem Ipsum Dolor Title") . '</h2>';
	$output .= 		'<p class="info-module-content">' . getOption("eto_homepostsA2D", "Sed non massa quis nisl tincidunt posuere sit amet vitae sem. Praesent  sed convallis quam. Donec hendrerit neque sit amet tellus ornare et  semper lorem lacinia. Suspendisse ac pellentesque odio. Nunc pharetra lorem ipsum est.") . '</p>';
	$output .= 		'<a class="info-module-btn" href="' . getOption("eto_homepostsA2BU", "#") . '" title="' . getOption("eto_homepostsA2BT", "Lorem Ipsum") . '">' . getOption("eto_homepostsA2BT", "Lorem Ipsum") . '</a>';
	$output .= 	'</div>';
	$output .= 	'<div class="clear"></div>';
	$output .= '</div>';
	$output .= '<div class="clear"></div>';
	$output .= '<div class="information-module">';
	$output .= 	'<div class="information-module-left">';
	$output .= 		'<h2 class="info-module-title">' . getOption("eto_homepostsA3T", "Lorem Ipsum Dolor Title") . '</h2>';
	$output .= 		'<p class="info-module-content">' . getOption("eto_homepostsA3D", "Sed non massa quis nisl tincidunt posuere sit amet vitae sem. Praesent  sed convallis quam. Donec hendrerit neque sit amet tellus ornare et  semper lorem lacinia. Suspendisse ac pellentesque odio. Nunc pharetra lorem ipsum est.") . '</p>';
	$output .= 		'<a class="info-module-btn" href="' . getOption("eto_homepostsA3BU", "#") . '" title="' . getOption("eto_homepostsA3BT", "Lorem Ipsum") . '">' . getOption("eto_homepostsA3BT", "Lorem Ipsum") . '</a>';
	$output .= 	'</div>';
	$output .= 	'<div class="information-module-right">';
	$output .= 		'<img src="' . getImage("eto_homepostsA3I", get_template_directory_uri() . "/images/dummy-info.jpg") . '" alt="' . getOption("eto_homepostsA3T", "Lorem Ipsum Dolor Title") . '">';
	$output .= 	'</div>';
	$output .= 	'<div class="clear"></div>';
	$output .= '</div>';
	$output .= '<div class="clear"></div>';
	echo $output;
}				

/*
	End Posts Area
*/

/*
	Footer
*/
function socialsArea() {
	$output  = "";
	$output .= '<ul>';
	if (getOption("eto_homefooterFBU", "#") != "#") {
		$output .= 	'<li><a href="' . getOption("eto_homefooterFBU", "#") . '" title="S&iacute;guenos en Facebook"><img src="' . getImage("eto_homefooterFBI", get_template_directory_uri() . "/images/fb.png") . '" alt="S&iacute;guenos en Facebook"></a></li>';
	}	
	if (getOption("eto_homefooterGPU", "#") != "#") {
		$output .= 	'<li><a href="' . getOption("eto_homefooterGPU", "#") . '" title="S&iacute;guenos en Google+"><img src="' . getImage("eto_homefooterGPI", get_template_directory_uri() . "/images/gp.png") . '" alt="S&iacute;guenos en Google+"></a></li>';
	}	
	if (getOption("eto_homefooterTTU", "#") != "#") {
		$output .= 	'<li><a href="' . getOption("eto_homefooterTTU", "#") . '" title="S&iacute;guenos en Twitter"><img src="' . getImage("eto_homefooterTTI", get_template_directory_uri() . "/images/tt.png") . '" alt="S&iacute;guenos en Twitter"></a></li>';
	}	
	if (getOption("eto_homefooterTUU", "#") != "#") {
		$output .= 	'<li><a href="' . getOption("eto_homefooterTUU", "#") . '" title="S&iacute;guenos en Tuenti"><img src="' . getImage("eto_homefooterTUI", get_template_directory_uri() . "/images/fb.png") . '" alt="S&iacute;guenos en Tuenti"></a></li>';
	}	
	$output .= '</ul>';
	echo $output;
}				

/*
	End Footer
*/

/*
	End HOME
*/	

/*
	Lateral Sidebar
*/

/*
	End Lateral Sidebar
*/

