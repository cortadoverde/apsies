<?php 

/*
	Implementing AJAX
*/
function MyAjaxFunctions() {
	$result = "";
	
	if ($_POST['toaction'] == "login") {
		$info = array();
	    $info['user_login'] = $_POST['username'];
	    $info['user_password'] = $_POST['password'];
	    $remember = $_POST['remember'];
	    if ($remember) {
	    	$info['remember'] = true;
	    } else {
	    	$info['remember'] = false;
	    }
	    
				
		$user = wp_signon( $info, true );
	    $data = array();
	    if ( is_wp_error($user) ){
	        $data["error"] = true;
	        $data["url"] = "login";
	    } else {
	    	$data["error"] = false;
	    	$data["user"] = $user;
	    }
	    $result = json_encode($data);
	} else
	if ($_POST['toaction'] == "logout") {
		wp_logout();
		$result = json_encode(1);	    
	} else
	if ($_POST['toaction'] == "doPedido") {
		if (isset($_POST['Total'])) {
			global $current_user;
      		get_currentuserinfo();
			$Total = $_POST['Total'];
			// Parcheando que el usuario no cierre la pagina y se le venza la suscripcion
			$user_id = $current_user->ID;
			$fechasuscripcion = get_user_meta( $user_id, 'fechasuscripcion', true );
			$diassuscripcion = get_user_meta( $user_id, 'diassuscripcion', true );
			$suscrito = false;
			$ahora = time();
			$nivel = get_user_meta( $user_id, 'nivel', true );	
			$factorsus = true;		
			if ($_POST['factors'] != 1) {
				if (!(($fechasuscripcion <= $ahora) && ($ahora <= $fechasuscripcion + ($diassuscripcion * 86400)))) {
					$factorsus = false;
					$vds = getOption("eto_valordescuentosuscritos", 1);
					$Total = $Total + (($vds / 100) * $Total);
				}
			}
			// fin del parcheo
			$Puntos = get_user_meta( $current_user->ID, 'puntosusuario', true );
			if (($_POST['csuscrito'] == 0) || (($_POST['csuscrito'] == 1) && (canRequestForFree($current_user->ID)) && ($factorsus))) {
				if ($Total <= $Puntos) {
					$new_post = array(
						'post_title' => $_POST['tipopedido'] . " - " . $current_user->user_login,
						'post_content' => $_POST['contentPedido'] . "<br/> Total Puntos: " . $Total,
						'post_status' => 'publish',
						'post_date' => date('Y-m-d H:i:s'),
						'post_author' => $current_user->ID,
						'post_type' => 'pedido'
					);
					$post_id = wp_insert_post($new_post);
					if ($post_id) {
						$u_post = array(
							'post_title' => $post_id . "-" . $_POST['tipopedido'] . "-" . $current_user->user_login,
							'ID' => $post_id						
						);
						$upost_id = wp_update_post($u_post);
						$pr = $Puntos - $Total;
						update_user_meta( $current_user->ID, 'puntosusuario', "$pr" );
						update_post_meta($post_id, 'usuario', "$current_user->ID");
						$data["error"] = false;
						$data["message"] = "Se ha realizado el pedido satisfactoriamente!";
						$data["Total"] = $Puntos - $Total;
						$data["post_id"] = $post_id;
						if ($_POST['csuscrito'] == 1) {
							add_post_meta( $post_id, 'pedido_suscrito', "1" );
						} else {
							add_post_meta( $post_id, 'pedido_suscrito', "0" );
						}
					} else {
						$data["error"] = true;
						$data["message"] = "No se ha podido realizar el pedido, int&eacute;ntelo m&aacute;s tarde";
						$data["url"] = "";
					}
			      	
				} else {
					$data["error"] = true;
					$data["message"] = "No tiene la cantidad de puntos necesarios para realizar el pedido";
					$data["url"] = "recargar";
				}
			} else {
				$data["error"] = true;
				$data["message"] = "Ya ha agotado los pedidos gratis por el dia de hoy";
				$data["url"] = "";
				$data["suscritoshoy"] = true;
			}
			$result = json_encode($data);
		}		      	
	} else {
		if ($_POST['toaction'] == "uploadArchive") {
			$filename = $_FILES["fileselect"];
			$post_id = $_POST["post_id"];
			
			// Check that the nonce is valid, and the user can edit this post.
			//if ( 
			//	isset( $_POST['fileselect_nonce'], $_POST['post_id'] ) 
			//	&& wp_verify_nonce( $_POST['fileselect_nonce'], 'fileselect' )
			//) {
				// The nonce was valid and the user has the capabilities, it is safe to continue.

				// These files need to be included as dependencies when on the front end.
				require_once( ABSPATH . 'wp-admin/includes/image.php' );
				require_once( ABSPATH . 'wp-admin/includes/file.php' );
				require_once( ABSPATH . 'wp-admin/includes/media.php' );
				
				// Let WordPress handle the upload.				
				$attachment_id = media_handle_upload( 'fileselect', $post_id );
				/*$attachment = array(
				    'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ), 
				    'post_mime_type' => $filetype['type'],
				    'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
				    'post_content'   => '',
				    'post_status'    => 'inherit'
				);
				$attachment_id = wp_insert_attachment( $attachment, $filename, $post_id );				
				$attach_data = wp_generate_attachment_metadata( $attachment_id, $filename );
				wp_update_attachment_metadata( $attachment_id, $attach_data );*/
				if ( is_wp_error( $attachment_id ) ) {
					// There was an error uploading the image.
					$result = json_encode($attachment_id);
				} else {
					// The image was uploaded successfully!
					$result = json_encode($attachment_id);
				}

			//} else {
				// The security check failed, maybe show the user an error.
			//	$result = json_encode($_FILES);
			//}
		}
	}

	die($result);
}

// creating Ajax call for WordPress  
add_action( 'wp_ajax_nopriv_MyAjaxFunctions', 'MyAjaxFunctions' );  
add_action( 'wp_ajax_MyAjaxFunctions', 'MyAjaxFunctions' );

function canRequestForFree($user_id) {
	$cont = 0;
	$cant = getOption("eto_valorpedidosgratiscolegiosus", 3);
	$args = array( 'numberposts' => -1, 'post_type'=> 'pedido' );
	$query = new WP_Query( $args );	
	if ( $query->have_posts() )	{			
		while ( $query->have_posts() ) {
			$query->the_post();
			// Getting project thumbnail
			$post_id = get_the_ID();
						    
			if (get_post_meta($post_id, "pedido_suscrito", true) == "1") {
				if (get_post_meta($post_id, 'usuario', true) == $user_id) {
					$post_date = get_the_date('Y/m/d');
					$today = date('Y/m/d');
					if ($post_date == $today) {
						$cont++;
					}
				}
			}
		}
	}
	wp_reset_postdata();
	return $cant > $cont;
}
