var basepath = "http://www.pruebaapsiesnetwork.es/";
//var basepath = "http://localhost/apsies/";
//var basepath = "http://192.168.173.1/apsies/";

var ajaxpath = basepath + "wp-admin/admin-ajax.php";
var themepath = basepath + "wp-content/themes/apsies/";

var contentPedido = "";
var Total = 0;
var files2Upload = new Array();
var files2UploadColegio = new Array();
var files2UploadColegioSuscrito = new Array();
var files = new Array();
var filesColegio = new Array();
var filesColegioSuscrito = new Array();

var barCenterPoint;
var barRightPoint;

jQuery(document).ready(function () {

	var mleftComponent;
	var mobileCreated = false;

	jQuery(window).load(function () {
		//alert(jQuery(window).width() + " - " + jQuery(window).height());

		if (jQuery(window).width() == 320) {
			mleftComponent = 60;
		} else {
			mleftComponent = 112;
		}

		prepareInformation();

		
		if (jQuery("#component-selector").attr("data-selection") == "2") {
			selectUniversidad();
		} else
		if (jQuery("#component-selector").attr("data-selection") == "1") {
			selectBachiller();
		}

	});

	jQuery(window).resize(function () {

		prepareInformation();
	});

	jQuery("#level-bachiller a").click(function (evt) {
		evt.preventDefault();
		if (!jQuery(this).hasClass("changeOff")) {
			selectBachiller();
		}
	});
	jQuery("#level-universidad a").click(function (evt) {
		evt.preventDefault();
		if (!jQuery(this).hasClass("changeOff")) {
			selectUniversidad();
		}
	});
	jQuery("#component-selector").click(function (evt) {
		evt.preventDefault();
		if (!jQuery(this).hasClass("changeOff")) {
			if (jQuery("#component-selector").attr("data-selection") == "1") {
				selectUniversidad();
			} else {
				selectBachiller();
			}
		}
	});

	function selectBachiller() {
		jQuery("#component-selector #component-handle").animate({'margin-left' : 4}, 400, "easeInOutSine");
		jQuery("#component-selector").attr("data-selection", "1");
		jQuery(".universidad-pedido").fadeOut("fast", function () {
			//jQuery(".colegio-pedido-instantaneo .form-element[data-nivel=0]").val("0");
			jQuery(".colegio-pedido-instantaneo").fadeIn("slow");
		});
		jQuery("div.points span strong").html("0 puntos");
	}
	function selectUniversidad() {
		jQuery("#component-selector #component-handle").animate({'margin-left' : mleftComponent}, 400, "easeInOutSine");
		jQuery("#component-selector").attr("data-selection", "2");
		jQuery(".colegio-pedido-instantaneo").fadeOut("fast", function () {
			//jQuery(".universidad-pedido .form-element[data-nivel=0]").val("0");
			jQuery(".universidad-pedido").fadeIn("slow");
		});
		jQuery("div.points span strong").html("0 puntos");
	}

	// Handling text images bar
	jQuery(".barratextoimagenes-left").click(function (evt) {
		evt.preventDefault();
		jQuery(".barratextoimagenes-handle").animate({"left" : 10}, 300).attr("data-info", "Texto");
	});
	jQuery(".barratextoimagenes-right").click(function (evt) {
		evt.preventDefault();
		jQuery(".barratextoimagenes-handle").animate({"left" : barRightPoint}, 300).attr("data-info", "Im&aacute;genes");
	});
	jQuery(".barratextoimagenes-center").click(function (evt) {
		evt.preventDefault();
		jQuery(".barratextoimagenes-handle").animate({"left" : barCenterPoint}, 300).attr("data-info", "Balanceado");
	});

	function prepareInformation() {
		// Reordering Modules by the width
		if (jQuery(window).width() <= 480) {
			jQuery(".information-module-right img").each(function () {
				var img = jQuery(this);
				var clone = img.clone().addClass("moved");
				var parent = img.parent(".information-module-right");
				var siblingParent = parent.siblings(".information-module-left");
				var elemToAppend = siblingParent.children(".info-module-title");
				elemToAppend.after(clone);
				img.remove();
			});
			jQuery(".information-module-left img").each(function () {
				if (jQuery(this).hasClass("moved") == false) {
					var img = jQuery(this);
					var clone = img.clone().addClass("moved");
					var parent = img.parent(".information-module-left");
					var siblingParent = parent.siblings(".information-module-right");
					var elemToAppend = siblingParent.children(".info-module-title");
					elemToAppend.after(clone);
					img.remove();
				}	
			});
		} else {
			jQuery(".information-module-right img.moved").each(function () {
				var img = jQuery(this);
				var clone = img.clone().removeClass("moved");
				var parent = img.parent(".information-module-right");
				var siblingParent = parent.siblings(".information-module-left");				
				siblingParent.append(clone);
				img.remove();
			});
			jQuery(".information-module-left img.moved").each(function () {				
				var img = jQuery(this);
				var clone = img.clone().removeClass("moved");
				var parent = img.parent(".information-module-left");
				var siblingParent = parent.siblings(".information-module-right");
				siblingParent.append(clone);
				img.remove();
			});
		}

		// Creating the mobile menu
		if (!mobileCreated) {
			jQuery("#main-menu ul li a").each(function () {
				mobileCreated = true;
				var selectCode = '<option value="' + jQuery(this).attr("href") + '">' + jQuery(this).html() + '</option>';
				jQuery("#select-mobile-menu").append(selectCode);
			});
		}
		jQuery("#select-mobile-menu").change(function(){
        	window.location = jQuery("#select-mobile-menu").val();
        });

        // Resizing bar component of exposiciones
        if (jQuery(window).width() <= 800) {
        	jQuery("#form .barratextoimagenes").css("height", "90px");
        } else {
        	jQuery("#form .barratextoimagenes").css("height", "80px");
        }
        if (jQuery(window).width() <= 720) {
        	jQuery(".barratextoimagenes").css("width", 160).css("background", "url(" + themepath + "/images/bar-component-small.png) bottom left no-repeat");
        	jQuery(".bartexttitle").css("line-height", "12px").css("text-align", "left");
        	jQuery(".bartextbalanceado").css("left", 55);
        	jQuery(".barratextoimagenes-center").css("left", 71);
        	barCenterPoint = 71;
        	barRightPoint = 131;
        } else {
        	jQuery(".barratextoimagenes").css("width", 430).css("background", "url(" + themepath + "/images/bar-component.png) bottom left no-repeat");
        	jQuery(".bartexttitle").css("line-height", "22px");
        	jQuery(".bartextbalanceado").css("left", 191);
        	jQuery(".barratextoimagenes-center").css("left", 206);
        	barCenterPoint = 206;
        	barRightPoint = 402;
        }
        jQuery(".barratextoimagenes-handle").animate({"left" : barCenterPoint}, 300).attr("data-info", "Balanceado");
	}

	// Ajax request to Log in
	jQuery("#submitLogin").click(function (evt) {
		evt.preventDefault();		
		var username = jQuery("#login-form #user_name").val();
	    var password = jQuery("#login-form #passwd").val();
	    jQuery("#login-form #user_name").val("");
	    jQuery("#login-form #passwd").val("");
	    jQuery("#login-form form").fadeOut("fast", function () {
	    	jQuery("#loaderImage").fadeIn("slow");
	    });	    
		jQuery.ajax({  
	  		type: 'POST',  
	  		url: ajaxpath, 
	  		dataType: "json", 			  
	  		data: {  
	  			action: 'MyAjaxFunctions',  
	        	toaction: 'login',
	          	username: username,
	          	password: password,
	          	remember: 1
	  		},  
	  		success: function(data, textStatus, XMLHttpRequest){    				
	  			jQuery("#loaderImage").fadeOut("fast", function () {
	  				if (data["error"] == true) {
	  					jQuery("#login-form form").fadeIn("slow");
	  					notification("error", "Usuario y/o contraseña incorrecto(s)", data["url"]);
	  				} else {
	  					window.location = basepath + "/pedidos";
	  				}
	  			});	    		
	  		},  
	  		error: function(MLHttpRequest, textStatus, errorThrown){  
	  			alert(errorThrown);  
	  		}  
	  	});		
	});

	// Ajax request to Log out
	jQuery("#logout").click(function (evt) {
		evt.preventDefault();				
	    jQuery("#user-data").fadeOut("fast", function () {
	    	jQuery("#loaderImage").fadeIn("slow");
	    });	    
		jQuery.ajax({  
	  		type: 'POST',  
	  		url: ajaxpath, 
	  		dataType: "json", 			  
	  		data: {  
	  			action: 'MyAjaxFunctions',  
	        	toaction: 'logout'
	  		},  
	  		success: function(data, textStatus, XMLHttpRequest){	  			
	  			if (jQuery("#logout").attr("data-redirect") == "0") {
		  			jQuery("#loaderImage").fadeOut("fast", function () {
		  				lg = 0;
		  				jQuery("#login-form").fadeIn("slow");
		  				notification("success", "Ha salido satisfactoriamente.");
		  			});	
	  			} else {
	  				window.location = basepath;	
	  			}    		
	  		},  
	  		error: function(MLHttpRequest, textStatus, errorThrown){  
	  			alert(errorThrown);  
	  		}  
	  	});
	});

	// Managing user profiles fields
	if (jQuery("#nivel").val() == 0) {
		jQuery(".tr-universidad").fadeOut("fast");
	} else
	if (jQuery("#nivel").val() == 1) {
		jQuery(".tr-colegio").fadeOut("fast");
	}
	jQuery("#nivel").change(function(){
        if (jQuery("#nivel").val() == 0) {
			jQuery(".tr-universidad").fadeOut("fast");
			jQuery(".tr-colegio").fadeIn("fast");
		} else
		if (jQuery("#nivel").val() == 1) {
			jQuery(".tr-colegio").fadeOut("fast");
			jQuery(".tr-universidad").fadeIn("fast");
		}	
    });

	// Support for spinners
	jQuery('.spinner1').spinit({  height: 36, min: 0, initValue: 0, max: 5000,  stepInc: 1 });
	jQuery('.spinner3').spinit({  height: 36, min: 0, initValue: 0, max: 500,  stepInc: 3 });
	jQuery('.spinner5').spinit({  height: 36, min: 0, initValue: 0, max: 500,  stepInc: 5 });
	jQuery('.spinner30').spinit({  height: 36, min: 30, initValue: 30, max: 3000,  stepInc: 30 });

	// Support for Forms navigation
	jQuery(".colegio-pedido-instantaneo select.action").change(function () {		
		var nivel = parseInt(jQuery(this).attr("data-nivel"));
		nivel = nivel + 1;		
		refreshNivel(nivel);
		var valor = parseInt(jQuery(this).val());
		if (nivel < 2) {
			jQuery(".colegio-pedido-instantaneo .form-element[data-nivel=" + nivel + "]").each(function () {
				if ((jQuery(window).width() >= 940) || ((!jQuery(this).hasClass("form-message")) && (!jQuery(this).hasClass("form-flecha")))) {
					if (jQuery(this).attr("data-valor") == valor) {
						jQuery(this).fadeIn("slow");
					}
				}
			});
		} else
		if ((nivel == 2) && (valor > 0)) {
			var trabajo = parseInt(jQuery(".colegio-pedido-instantaneo .form-element[data-nivel=0]").val());			
			jQuery(".colegio-pedido-instantaneo .form-element[data-nivel=" + nivel + "]").each(function () {				
				if ((jQuery(window).width() >= 940) || ((!jQuery(this).hasClass("form-message")) && (!jQuery(this).hasClass("form-flecha")))) {
					if (trabajo == 1) {					
						if (jQuery(this).hasClass("base1")) {						
							if (jQuery(this).hasClass("smartspinner")) {					
								if ((valor == 2) ||
									((valor >= 8) && (valor <= 10)) ||
									((valor >= 12) && (valor <= 15))) {
									if (jQuery(this).hasClass("spinner3")) {
										jQuery(this).fadeIn("slow");
									}
								} else {
									if (jQuery(this).hasClass("spinner5")) {
										jQuery(this).fadeIn("slow");
									}
								}
							} else {
								jQuery(this).fadeIn("slow");
							}	
						}
					} else
					if (trabajo == 6) {
						if (jQuery(this).hasClass("base" + trabajo)) {
							if (jQuery(this).attr("data-valor") == valor) {
								jQuery(this).fadeIn("slow");
							}												
						}
					} else
					{
						if (jQuery(this).hasClass("base" + trabajo)) {												
							jQuery(this).fadeIn("slow");						
						}
					}
				}						
			});			
		}	
	});

	jQuery(".colegio-suscritos select.action").change(function () {		
		var nivel = parseInt(jQuery(this).attr("data-nivel"));
		nivel = nivel + 1;		
		refreshNivel(nivel);
		var valor = parseInt(jQuery(this).val());
		if (nivel < 2) {
			jQuery(".colegio-suscritos .form-element[data-nivel=" + nivel + "]").each(function () {
				if (jQuery(this).attr("data-valor") == valor) {
					jQuery(this).fadeIn("slow");
				}
			});
		} else
		if ((nivel == 2) && (valor > 0)) {			
			var asignatura = parseInt(jQuery(".colegio-suscritos .form-element[data-nivel=0]").val());	
			var ptos5 = "¡Recuerda que puedes mandar hasta 5 ejercicios diferentes dentro del mismo pedido!";		
			var ptos3 = "¡Recuerda que puedes mandar hasta 3 ejercicios diferentes dentro del mismo pedido!";
			jQuery(".colegio-suscritos .form-element[data-nivel=" + nivel + "]").each(function () {								
				if ((asignatura == 1) ||
					(asignatura == 3) ||
					(asignatura == 4) ||
					(asignatura == 6) ||
					(asignatura == 7) ||
					(asignatura == 9) ||
					(asignatura == 13) ||
					(asignatura == 16) ||
					(asignatura == 17) 
					) {					
					if (jQuery(this).hasClass("base" + asignatura)) {
						if (jQuery(this).attr("data-valor") == valor) {
							if (valor == 1) {
								jQuery(this).html(ptos5);
							}
							jQuery(this).fadeIn("slow");
						}												
					}
					
				} else
				if ((asignatura == 2) || 
					(asignatura == 8) || 
					(asignatura == 11) || 
					(asignatura == 12) || 
					(asignatura == 14) || 
					(asignatura == 15)
					) {
					if (jQuery(this).hasClass("base" + asignatura)) {
						if (jQuery(this).attr("data-valor") == valor) {
							if (valor == 1) {
								jQuery(this).html(ptos3);
							}
							jQuery(this).fadeIn("slow");
						}												
					}
				}
				else
				{
					if (jQuery(this).hasClass("base" + asignatura)) {
						if (jQuery(this).attr("data-valor") == valor) {							
							jQuery(this).fadeIn("slow");
						}												
					}
				}						
			});			
		}	
	});

	jQuery(".universidad-pedido select.action").change(function () {		
		var nivel = parseInt(jQuery(this).attr("data-nivel"));
		nivel = nivel + 1;		
		refreshNivel(nivel);
		var valor = parseInt(jQuery(this).val());
		if (nivel < 2) {
			jQuery(".universidad-pedido .form-element[data-nivel=" + nivel + "]").each(function () {
				if ((jQuery(window).width() >= 940) || ((!jQuery(this).hasClass("form-message")) && (!jQuery(this).hasClass("form-flecha")))) {
					if (jQuery(this).attr("data-valor") == valor) {
						jQuery(this).fadeIn("slow");
					}
				}	
			});
		} else
		if ((nivel == 2) && (valor > 0)) {						
			var base = parseInt(jQuery(".universidad-pedido .form-element[data-nivel=0]").val());	
			jQuery(".universidad-pedido .form-element[data-nivel=" + nivel + "]").each(function () {								
				if ((jQuery(window).width() >= 940) || ((!jQuery(this).hasClass("form-message")) && (!jQuery(this).hasClass("form-flecha")))) {
					if (jQuery(this).hasClass("base" + base)) {
						if (jQuery(this).attr("data-valor") == valor) {							
							jQuery(this).fadeIn("slow");
						}												
					}
				}						
			});			
		}	
	});

	// Refresing points
	jQuery(".form-pedido .form-element[data-nivel=0]").change(function () {
		jQuery("div.points span strong").html("0 puntos");
	});	

	// Calculating points by task
	function refreshUniversidadPedido() {
		if (jQuery(".universidad-pedido .form-element[data-nivel=0]").val() == 1) {
			// for Trabajos
			contentPedido = "Tipo de Pedido: Universidad - Trabajo <br/>";

			// Obteniendo titulo
			jQuery(".universidad-pedido .titulo").each(function () {
				if ((jQuery(this).attr("data-nivel") == 1) && (jQuery(this).attr("data-valor") == 1)) {					
					contentPedido = contentPedido + "T&iacute;tulo: " + jQuery(this).val() + " <br/>";
				}
			});
			// Obteniendo tema
			jQuery(".universidad-pedido .tema").each(function () {
				if ((jQuery(this).attr("data-nivel") == 1) && (jQuery(this).attr("data-valor") == 1)) {					
					contentPedido = contentPedido + "Tema: " + jQuery(this).find("option:selected").text() + " <br/>";
				}
			});
			// obteniendo cantidad de paginas
			var cantPaginas = 0;
			jQuery(".universidad-pedido .paginas").each(function () {
				if ((jQuery(this).attr("data-nivel") == 1) && (jQuery(this).attr("data-valor") == 1)) {
					cantPaginas = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Cantidad de p&aacute;ginas: " + cantPaginas + " <br/>";
				}
			});			
			// Obteniendo puntos por cantidad de paginas
			var ptosPagina;
			if (cantPaginas <= 10) {
				ptosPagina = cantPaginas * 5.1;
				cantPaginas = cantPaginas - 10;
			} else
			if (cantPaginas > 10) {
				ptosPagina = 51;
				cantPaginas = cantPaginas - 10;
			}

			if ((cantPaginas > 0) && (cantPaginas <= 20)) {
				ptosPagina = ptosPagina + cantPaginas * 3;
				cantPaginas = cantPaginas - 20;
			} else 
			if (cantPaginas > 20) {
				ptosPagina = ptosPagina + 60;
				cantPaginas = cantPaginas - 20;
			}

			if (cantPaginas > 0) {
				ptosPagina = ptosPagina + cantPaginas * 2;
			}

			// obteniendo tiempo entrega
			var tentrega = 0;
			jQuery(".universidad-pedido .tiempo").each(function () {
				if ((jQuery(this).attr("data-nivel") == 1) && (jQuery(this).attr("data-valor") == 1)) {
					tentrega = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";
				}
			});	
			// obteniendo valor a multiplicar por tiempo de entrega
			var factorEntrega = getFactorEntrega(tentrega);

			var subTotal = ptosPagina * factorEntrega;
			
			// obteniendo bibliografia
			var valorBibliografia = 0;
			jQuery(".universidad-pedido .bibliografia").each(function () {
				if ((jQuery(this).attr("data-nivel") == 1) && (jQuery(this).attr("data-valor") == 1)) {
					valorBibliografia = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Bibliograf&iacute;a: " + jQuery(this).find("option:selected").text() + " <br/>";
				}
			});	
			// Obteniendo el factor bibliografia
			var factorBibliografia;
			if (valorBibliografia == 1) {
				factorBibliografia = 1.05;
			} else {
				factorBibliografia = 1;
			}
			// obteniendo observaciones
			jQuery(".universidad-pedido .observaciones").each(function () {
				if ((jQuery(this).attr("data-nivel") == 1) && (jQuery(this).attr("data-valor") == 1)) {					
					contentPedido = contentPedido + "Observaciones: " + jQuery(this).val() + " <br/>";
				}
			});
			
			Total = Math.ceil(subTotal * factorBibliografia * vds);
			jQuery("div.points span strong").html(Total + " puntos");
		} else 
		if (jQuery(".universidad-pedido .form-element[data-nivel=0]").val() == 2) {
			// for AutoCAD
			contentPedido = "Tipo de Pedido: Universidad - AutoCAD <br/>";

			// obteniendo cantidad de piezas
			var cantPiezas = 0;
			jQuery(".universidad-pedido .piezas").each(function () {
				if ((jQuery(this).attr("data-nivel") == 1) && (jQuery(this).attr("data-valor") == 2)) {
					cantPiezas = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Cantidad de piezas: " + cantPiezas + " <br/>";
				}
			});

			// obteniendo vistas
			var vistas = 0;
			jQuery(".universidad-pedido .vistas").each(function () {
				if ((jQuery(this).attr("data-nivel") == 1) && (jQuery(this).attr("data-valor") == 2)) {
					vistas = parseFloat(jQuery(this).val());
					contentPedido = contentPedido + "Vistas: " + jQuery(this).find("option:selected").text() + " <br/>";
				}
			});

			// obteniendo acotacion
			var acotacion = 0;
			jQuery(".universidad-pedido .acotacion").each(function () {
				if ((jQuery(this).attr("data-nivel") == 1) && (jQuery(this).attr("data-valor") == 2)) {
					acotacion = parseFloat(jQuery(this).val());
					contentPedido = contentPedido + "Acotaci&oacute;n: " + jQuery(this).find("option:selected").text() + " <br/>";
				}
			});

			// obteniendo tiempo entrega
			var tentrega = 0;
			jQuery(".universidad-pedido .tiempo").each(function () {
				if ((jQuery(this).attr("data-nivel") == 1) && (jQuery(this).attr("data-valor") == 2)) {
					tentrega = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";
				}
			});	
			// Obteniendo Observaciones
			jQuery(".universidad-pedido .observaciones").each(function () {
				if ((jQuery(this).attr("data-nivel") == 1) && (jQuery(this).attr("data-valor") == 2)) {					
					contentPedido = contentPedido + "Observaciones: " + jQuery(this).val() + " <br/>";
				}
			});
			// obteniendo valor a multiplicar por tiempo de entrega
			var factorEntrega = getFactorEntrega(tentrega);

			Total = Math.ceil(cantPiezas * 10 * vistas * acotacion * factorEntrega * vds);
			jQuery("div.points span strong").html(Total + " puntos");
		} else 
		if (jQuery(".universidad-pedido .form-element[data-nivel=0]").val() == 3) {
			// apuntes
			contentPedido = "Tipo de Pedido: Universidad - Apuntes <br/>";

			// obteniendo cantidad de piezas
			var subiropaquete = 0;
			jQuery(".universidad-pedido .subiropaquete").each(function () {
				if ((jQuery(this).attr("data-nivel") == 1) && (jQuery(this).attr("data-valor") == 3)) {
					subiropaquete = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Tipo: " + jQuery(this).find("option:selected").text() + " <br/>";
				}
			});
			if (subiropaquete == 1) {
				var cantPaginas;
				jQuery(".universidad-pedido .paginassubir").each(function () {
					if ((jQuery(this).attr("data-nivel") == 2) && (jQuery(this).attr("data-valor") == 1)) {
						cantPaginas = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Cantidad de p&aacute;ginas: " + cantPaginas + " <br/>";
					}
				});
				var tentrega = 0;
				jQuery(".universidad-pedido .tiemposubir").each(function () {
					if ((jQuery(this).attr("data-nivel") == 2) && (jQuery(this).attr("data-valor") == 1)) {
						tentrega = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";
					}
				});
				// obteniendo valor a multiplicar por tiempo de entrega
				var factorEntrega = getFactorEntregaReducido(tentrega);				
			} else
			if (subiropaquete == 2) {
				var cantPaginas;
				jQuery(".universidad-pedido .paginaspaqueteria").each(function () {
					if ((jQuery(this).attr("data-nivel") == 2) && (jQuery(this).attr("data-valor") == 2)) {
						cantPaginas = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Cantidad de p&aacute;ginas: " + cantPaginas + " <br/>";
					}
				});
				var tentrega = 0;
				jQuery(".universidad-pedido .tiempopaqueteria").each(function () {
					if ((jQuery(this).attr("data-nivel") == 2) && (jQuery(this).attr("data-valor") == 2)) {
						tentrega = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";
					}
				});
				// obteniendo valor a multiplicar por tiempo de entrega
				var factorEntrega = getFactorEntregaReducido(tentrega);
			} else {
				cantPaginas = 0;
				factorEntrega = 0;
			}

			Total = Math.ceil(cantPaginas * factorEntrega * vds);
			jQuery("div.points span strong").html(Total + " puntos");
		} else 
		if (jQuery(".universidad-pedido .form-element[data-nivel=0]").val() == 4) {
			// power point
			contentPedido = "Tipo de Pedido: Universidad - Power Point <br/>";

			// obteniendo cantidad de piezas
			var diapositivaotexto = 0;
			jQuery(".universidad-pedido .diapositivaotexto").each(function () {
				if ((jQuery(this).attr("data-nivel") == 1) && (jQuery(this).attr("data-valor") == 4)) {
					diapositivaotexto = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Tipo: " + jQuery(this).find("option:selected").text() + " <br/>";					
				}
			});
			if (diapositivaotexto == 1) {				
				var diapositivas;
				jQuery(".universidad-pedido .diapositivascantidad").each(function () {
					if ((jQuery(this).attr("data-nivel") == 2) && (jQuery(this).attr("data-valor") == 1)) {
						diapositivas = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Diapositivas: " + diapositivas + " <br/>";						
					}
				});
				var tentrega = 0;
				jQuery(".universidad-pedido .tiempodiapositivas").each(function () {
					if ((jQuery(this).attr("data-nivel") == 2) && (jQuery(this).attr("data-valor") == 1)) {
						tentrega = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";						
					}
				});
				var factor = diapositivas * 0.3;
				// obteniendo valor a multiplicar por tiempo de entrega
				var factorEntrega = getFactorEntregaReducido(tentrega);			
			} else
			if (diapositivaotexto == 2) {
				var cantPaginas;
				jQuery(".universidad-pedido .paginascantidad").each(function () {
					if ((jQuery(this).attr("data-nivel") == 2) && (jQuery(this).attr("data-valor") == 2)) {
						cantPaginas = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "P&aacute;ginas: " + cantPaginas + " <br/>";
					}
				});
				var tentrega = 0;
				jQuery(".universidad-pedido .tiempopaginas").each(function () {
					if ((jQuery(this).attr("data-nivel") == 2) && (jQuery(this).attr("data-valor") == 2)) {
						tentrega = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";
					}
				});
				var factor = cantPaginas * 3;
				// obteniendo valor a multiplicar por tiempo de entrega
				var factorEntrega = getFactorEntregaReducido(tentrega);
			} else {
				diapositivas = 0;
				factorEntrega = 0;
				factor = 0;
			}

			Total = Math.ceil(factor * factorEntrega * vds);
			jQuery("div.points span strong").html(Total + " puntos");
		} else 
		if (jQuery(".universidad-pedido .form-element[data-nivel=0]").val() == 5) {
			// exposiciones
			contentPedido = "Tipo de Pedido: Universidad - Exposiciones <br/>";

			// obteniendo cantidad de piezas
			var partiendo = 0;
			jQuery(".universidad-pedido .partiendo").each(function () {
				if ((jQuery(this).attr("data-nivel") == 1) && (jQuery(this).attr("data-valor") == 5)) {
					partiendo = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Tipo: " + jQuery(this).find("option:selected").text() + " <br/>";					
				}
			});
			if (partiendo == 1) {				
				var diapositivas;
				jQuery(".universidad-pedido .diapositivaspartiendoescrito").each(function () {
					if ((jQuery(this).attr("data-nivel") == 2) && (jQuery(this).attr("data-valor") == 1)) {
						diapositivas = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Diapositivas: " + diapositivas + " <br/>";						
					}
				});
				// parte hablada
				var partehablada = 0;
				jQuery(".universidad-pedido .partehabladapartiendoescrito").each(function () {
					if ((jQuery(this).attr("data-nivel") == 2) && (jQuery(this).attr("data-valor") == 1)) {
						partehablada = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Incluir parte hablada: " + jQuery(this).find("option:selected").text() + " <br/>";						
					}
				});
				// tiempo entrega
				var tentrega = 0;
				jQuery(".universidad-pedido .tiempopartiendoescrito").each(function () {
					if ((jQuery(this).attr("data-nivel") == 2) && (jQuery(this).attr("data-valor") == 1)) {
						tentrega = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";						
					}
				});
				// proporcion texto - imagenes
				jQuery(".barratextoimagenes-handle").each(function () {
					contentPedido = contentPedido + "Proporci&oacute;n Texto - Im&aacute;genes: " + jQuery(this).attr("data-info") + " <br/>";
					return false;
				});				
				var factor = diapositivas * 4.1;
				var factorph;
				if (partehablada == 1) {
					factorph = 1.25;
				} else {
					factorph = 1;
				}
				// obteniendo valor a multiplicar por tiempo de entrega
				var factorEntrega = getFactorEntregaReducido(tentrega);			
			} else
			if (partiendo == 2) {
				var diapositivas;
				jQuery(".universidad-pedido .diapositivaspartiendocero").each(function () {
					if ((jQuery(this).attr("data-nivel") == 2) && (jQuery(this).attr("data-valor") == 2)) {
						diapositivas = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Diapositivas: " + diapositivas + " <br/>";						
					}
				});
				// parte hablada
				var partehablada = 0;
				jQuery(".universidad-pedido .partehabladapartiendocero").each(function () {
					if ((jQuery(this).attr("data-nivel") == 2) && (jQuery(this).attr("data-valor") == 2)) {
						partehablada = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Incluir parte hablada: " + jQuery(this).find("option:selected").text() + " <br/>";						
					}
				});
				// tiempo entrega
				var tentrega = 0;
				jQuery(".universidad-pedido .tiempopartiendocero").each(function () {
					if ((jQuery(this).attr("data-nivel") == 2) && (jQuery(this).attr("data-valor") == 2)) {
						tentrega = parseInt(jQuery(this).val());	
						contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";						
					}
				});		
				// proporcion texto - imagenes
				jQuery(".barratextoimagenes-handle").each(function () {
					contentPedido = contentPedido + "Proporci&oacute;n Texto - Im&aacute;genes: " + jQuery(this).attr("data-info") + " <br/>";
					return false;
				});		
				var factor = diapositivas * 7.1;
				var factorph;
				if (partehablada == 1) {
					factorph = 1.25;
				} else {
					factorph = 1;
				}
				// obteniendo valor a multiplicar por tiempo de entrega
				var factorEntrega = getFactorEntregaReducido(tentrega);	
			} else {
				diapositivas = 0;
				factorEntrega = 0;
				factor = 0;
				factorph = 0;
			}

			Total = Math.ceil(factor * factorph * factorEntrega * vds);
			jQuery("div.points span strong").html(Total + " puntos");
		}
	}
	jQuery(".universidad-pedido .form-element").click(function () {
		// there are changes
		refreshUniversidadPedido();
	});
	jQuery(".universidad-pedido .btn").click(function (evt) {
		evt.preventDefault();	
		refreshUniversidadPedido();
		var error = false;
		jQuery(".universidad-pedido .form-element").each(function () {
			if (jQuery(this).css("display") === "block") {
				if (jQuery(this).is("input")) {
					if (((jQuery(this).val() === "")) || ((jQuery(this).val() === "0"))) {
						error = true;						
					}
				} else 
				if (jQuery(this).is("select")) {
					if (jQuery(this).val() === "0") {
						error = true;						
					} 
				}
			}
		});
		if (error) {
			notification("error", "Llene completamente el formulario. Tiene campos sin llenar.", "");
		} else
		{
			var r = confirm("Desea hacer el pedido?");
			if (r == true) {
				if (lg == 1) {
					notificationAjaxOn("Realizando pedido ...");
				    // Haciendo el Pedido
				    jQuery.ajax({  
				  		type: 'POST',  
				  		url: ajaxpath, 
				  		dataType: "json", 			  
				  		data: {  
				  			action: 'MyAjaxFunctions',  
				        	toaction: 'doPedido',
				          	contentPedido: contentPedido,
				          	Total: Total,
				          	csuscrito: 0,
				          	factors: vds,
				          	tipopedido: jQuery(".universidad-pedido .form-element[data-nivel=0]").find("option:selected").text()
				  		},  
				  		success: function(data, textStatus, XMLHttpRequest){    				
				  				notificationAjaxOff();
				  				if (data["error"] == false) {		  					
				  					jQuery("div.points span strong").html("0 puntos");
				  					jQuery(".form-pedido .form-element[data-nivel=0]").val("0");
				  					jQuery("div#left-sidebar p#cantidad-puntos").html(data["Total"]);		  					
				  					refreshNivel(1);
				  					//jQuery("#universidad-form-post-id").val(data["post_id"]);
				  					//jQuery("#universidad-form").submit();			  					
				  					if (files.length > 0) {
				  						for (var i = 0; i < files.length; i++) {
					  						var fd = new FormData();    
											fd.append( 'action',  'MyAjaxFunctions');
											fd.append( 'toaction',  'uploadArchive');
											fd.append( 'post_id',  data["post_id"]);
											fd.append( 'fileselect',  files[i]);
						  					jQuery.ajax({  
										  		type: 'POST',  
										  		url: ajaxpath, 
										  		dataType: "json", 			  
										  		data: fd, 
										  		processData: false, 
										  		contentType: false, 
										  		cache : false,
										  		success: function(data, textStatus, XMLHttpRequest){								  			
										  			notification("success", "Pedido realizado satisfactoriamente!");
				                					jQuery("#universidad-details").html("");		  						  				
										  		},  
										  		error: function(MLHttpRequest, textStatus, errorThrown){  
										  			alert(errorThrown);  
										  		}  
										  	});
										}
				  					} else {
				  						notification("success", "Pedido realizado satisfactoriamente!");
				  					}			  					
				  				} else {
				  					if (data["url"] != "") {
				  						notification("error", data["message"], data["url"]);
				  					} else {
				  						notification("error", data["message"]);
				  					}
				  				}	  						  				
				  		},  
				  		error: function(MLHttpRequest, textStatus, errorThrown){  
				  			alert(errorThrown);  
				  		}  
				  	});	
				} else {
					notification("error", "Debe registrarse y entrar para poder realizar pedidos", "registrate");
				}
			}
		}
	});

	/*$(document).on('submit', '#universidad-form', function() {            
        $.ajax({
            url     : $(this).attr('action'),
            type    : $(this).attr('method'),
            dataType: 'json',
            data    : new FormData( this ),
	        processData: false,
	        contentType: false,
            success : function( data ) {
                //alert(data);
                jQuery("#universidad-details").html("");
            },
            error   : function( xhr, err ) {
                alert('Error');     
            }
        });    
        return false;
    });*/	

	function refreshColegioInstantaneo() {		
		if (jQuery(".colegio-pedido-instantaneo .form-element[data-nivel=0]").val() == 1) {
			// resolucion ejercicios
			contentPedido = "Tipo de Pedido: Colegio - Resolucion de ejercicios <br/>";

			// asignatura
			jQuery(".colegio-pedido-instantaneo .asignatura").each(function () {
				if ((jQuery(this).attr("data-nivel") == "1") && (jQuery(this).attr("data-valor") == "1")) {
					contentPedido = contentPedido + "Asignatura: " + jQuery(this).find("option:selected").text() + " <br/>";												
				}
			});
			// pedidos de ejercicios
			var ejercicios = 0;
			jQuery(".colegio-pedido-instantaneo .ejercicios").each(function () {
				if ((jQuery(this).hasClass("base1")) && (jQuery(this).css("display") == "block")) {
					if (jQuery(this).hasClass("spinner5")) {
						ejercicios = parseInt(jQuery(this).val()) / 5;
					} else
					if (jQuery(this).hasClass("spinner3")) {
						ejercicios = parseInt(jQuery(this).val()) / 3;
					}	
					contentPedido = contentPedido + "Pedidos de ejercicios: " + ejercicios + " <br/>";												
				}
			});
			// tiempo
			var tiempo = 0;
			jQuery(".colegio-pedido-instantaneo .tiempo").each(function () {
				if (jQuery(this).hasClass("base1")) {
					tiempo = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";						
				}
			});
			var factorEjercicios = ejercicios * 2;
			var factorEntrega = getFactorEntregaColegio(tiempo);
			Total = Math.ceil(factorEjercicios * factorEntrega * vds);
			jQuery("div.points span strong").html(Total + " puntos"); 
		} else
		if (jQuery(".colegio-pedido-instantaneo .form-element[data-nivel=0]").val() == 2) {
			// redaccion tema
			contentPedido = "Tipo de Pedido: Colegio - Redaccion de tema <br/>";

			// asignatura
			jQuery(".colegio-pedido-instantaneo .asignatura").each(function () {
				if ((jQuery(this).attr("data-nivel") == "1") && (jQuery(this).attr("data-valor") == "2")) {
					contentPedido = contentPedido + "Asignatura: " + jQuery(this).find("option:selected").text() + " <br/>";												
				}
			});
			// tema
			jQuery(".colegio-pedido-instantaneo .tema").each(function () {
				if (jQuery(this).hasClass("base2")) {
					contentPedido = contentPedido + "Tema: " + jQuery(this).val() + " <br/>";
				}
			});
			// palabras
			var palabras = 0;
			jQuery(".colegio-pedido-instantaneo .palabras").each(function () {
				if (jQuery(this).hasClass("base2")) {
					palabras = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Palabras: " + palabras + " <br/>";
				}
			});
			// tiempo
			var tiempo = 0;
			jQuery(".colegio-pedido-instantaneo .tiempo").each(function () {
				if (jQuery(this).hasClass("base2")) {
					tiempo = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";						
				}
			});
			// observaciones
			jQuery(".colegio-pedido-instantaneo .observaciones").each(function () {
				if (jQuery(this).hasClass("base2")) {
					contentPedido = contentPedido + "Observaciones: " + jQuery(this).val() + " <br/>";
				}
			});
			var factorPalabras = palabras * 0.03;
			var factorEntrega = getFactorEntregaColegio(tiempo);
			Total = Math.ceil(factorPalabras * factorEntrega * vds);
			jQuery("div.points span strong").html(Total + " puntos"); 
		} else
		if (jQuery(".colegio-pedido-instantaneo .form-element[data-nivel=0]").val() == 3) {
			// comentario texto
			contentPedido = "Tipo de Pedido: Colegio - Comentario de texto <br/>";

			// asignatura
			jQuery(".colegio-pedido-instantaneo .asignatura").each(function () {
				if ((jQuery(this).attr("data-nivel") == "1") && (jQuery(this).attr("data-valor") == "3")) {
					contentPedido = contentPedido + "Asignatura: " + jQuery(this).find("option:selected").text() + " <br/>";												
				}
			});
			// palabras
			var palabras = 0;
			jQuery(".colegio-pedido-instantaneo .palabras").each(function () {
				if (jQuery(this).hasClass("base3")) {
					palabras = parseInt(jQuery(this).val());	
					contentPedido = contentPedido + "Palabras: " + palabras + " <br/>";											
				}
			});
			// tiempo
			var tiempo = 0;
			jQuery(".colegio-pedido-instantaneo .tiempo").each(function () {
				if (jQuery(this).hasClass("base3")) {
					tiempo = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";						
				}
			});
			// observaciones
			jQuery(".colegio-pedido-instantaneo .observaciones").each(function () {
				if (jQuery(this).hasClass("base3")) {
					contentPedido = contentPedido + "Observaciones: " + jQuery(this).val() + " <br/>";
				}
			});
			// texto a comentar
			jQuery(".colegio-pedido-instantaneo .textocomentar").each(function () {
				if (jQuery(this).hasClass("base3")) {
					contentPedido = contentPedido + "Observaciones: " + jQuery(this).val() + " <br/>";
				}
			});
			var factorPalabras = palabras * 0.03;
			var factorEntrega = getFactorEntregaColegio(tiempo);
			Total = Math.ceil(factorPalabras * factorEntrega * vds);
			jQuery("div.points span strong").html(Total + " puntos"); 
		} else
		if (jQuery(".colegio-pedido-instantaneo .form-element[data-nivel=0]").val() == 4) {
			// comentario artistico
			contentPedido = "Tipo de Pedido: Colegio - Comentario artistico <br/>";

			// asignatura
			jQuery(".colegio-pedido-instantaneo .asignatura").each(function () {
				if ((jQuery(this).attr("data-nivel") == "1") && (jQuery(this).attr("data-valor") == "4")) {
					contentPedido = contentPedido + "Asignatura: " + jQuery(this).find("option:selected").text() + " <br/>";												
				}
			});
			// palabras
			var palabras = 0;
			jQuery(".colegio-pedido-instantaneo .palabras").each(function () {
				if (jQuery(this).hasClass("base4")) {
					palabras = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Cantidad de palabras: " + palabras + " <br/>";												
				}
			});
			// tiempo
			var tiempo = 0;
			jQuery(".colegio-pedido-instantaneo .tiempo").each(function () {
				if (jQuery(this).hasClass("base4")) {
					tiempo = parseInt(jQuery(this).val());	
					contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";					
				}
			});
			// observaciones
			jQuery(".colegio-pedido-instantaneo .observaciones").each(function () {
				if (jQuery(this).hasClass("base4")) {
					contentPedido = contentPedido + "Observaciones: " + jQuery(this).val() + " <br/>";
				}
			});
			// texto a comentar
			jQuery(".colegio-pedido-instantaneo .obracomentar").each(function () {
				if (jQuery(this).hasClass("base4")) {
					contentPedido = contentPedido + "Observaciones: " + jQuery(this).val() + " <br/>";
				}
			});
			var factorPalabras = palabras * 0.03;
			var factorEntrega = getFactorEntregaColegio(tiempo);
			Total = Math.ceil(factorPalabras * factorEntrega * vds);
			jQuery("div.points span strong").html(Total + " puntos"); 
		} else
		if (jQuery(".colegio-pedido-instantaneo .form-element[data-nivel=0]").val() == 7) {
			// Dudas / Investigacion sobre un tema
			contentPedido = "Tipo de Pedido: Colegio - Dudas / Investigacion sobre un tema <br/>";

			// asignatura
			jQuery(".colegio-pedido-instantaneo .asignatura").each(function () {
				if ((jQuery(this).attr("data-nivel") == "1") && (jQuery(this).attr("data-valor") == "7")) {
					contentPedido = contentPedido + "Asignatura: " + jQuery(this).find("option:selected").text() + " <br/>";												
				}
			});
			// tema
			jQuery(".colegio-pedido-instantaneo .tema").each(function () {
				if (jQuery(this).hasClass("base7")) {
					contentPedido = contentPedido + "Tema: " + jQuery(this).val() + " <br/>";
				}
			});
			// palabras
			var palabras = 0;
			jQuery(".colegio-pedido-instantaneo .palabras").each(function () {
				if (jQuery(this).hasClass("base7")) {
					palabras = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Palabras: " + palabras + " <br/>";												
				}
			});
			// tiempo
			var tiempo = 0;
			jQuery(".colegio-pedido-instantaneo .tiempo").each(function () {
				if (jQuery(this).hasClass("base7")) {
					tiempo = parseInt(jQuery(this).val());
					contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";						
				}
			});
			// observaciones
			jQuery(".colegio-pedido-instantaneo .observaciones").each(function () {
				if (jQuery(this).hasClass("base7")) {
					contentPedido = contentPedido + "Observaciones: " + jQuery(this).val() + " <br/>";
				}
			});
			var factorPalabras = palabras * 0.03;
			var factorEntrega = getFactorEntregaColegio(tiempo);
			Total = Math.ceil(factorPalabras * factorEntrega * vds);
			jQuery("div.points span strong").html(Total + " puntos"); 
		} else
		if (jQuery(".colegio-pedido-instantaneo .form-element[data-nivel=0]").val() == 5) {
			// critica literaria
			contentPedido = "Tipo de Pedido: Colegio - Critica literaria <br/>";

			// titulo
			jQuery(".colegio-pedido-instantaneo .titulo").each(function () {
				if ((jQuery(this).attr("data-nivel") == "1") && (jQuery(this).attr("data-valor") == "5")) {
					contentPedido = contentPedido + "Titulo de la obra: " + jQuery(this).val() + " <br/>";												
				}
			});
			// partes
			var pain = false; 
			contentPedido = contentPedido + "Partes: ";			
			jQuery(".colegio-pedido-instantaneo .partesincluir:checked").each(function () {
				if (pain == true) { contentPedido = contentPedido + ", "; }
				contentPedido = contentPedido + jQuery(this).attr("data-valor");
				pain = true;
			});
			contentPedido = contentPedido + "<br/>";  
			// palabras
			var palabras = 0;
			jQuery(".colegio-pedido-instantaneo .palabrascritica").each(function () {
				palabras = parseInt(jQuery(this).val());
				contentPedido = contentPedido + "Palabras: " + palabras + " <br/>";
			});
			// tiempo
			var tiempo = 0;
			jQuery(".colegio-pedido-instantaneo .tiempocritica").each(function () {
				tiempo = parseInt(jQuery(this).val());
				contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";
			});
			// observaciones
			jQuery(".colegio-pedido-instantaneo .observacionescritica").each(function () {				
				contentPedido = contentPedido + "Observaciones: " + jQuery(this).val() + " <br/>";				
			});
			var factorPalabras = palabras * 8;
			var factorEntrega = getFactorEntregaColegio(tiempo);
			Total = Math.ceil(factorPalabras * factorEntrega * vds);
			jQuery("div.points span strong").html(Total + " puntos"); 
		} else
		if (jQuery(".colegio-pedido-instantaneo .form-element[data-nivel=0]").val() == 6) {
			// AutoCAD
			contentPedido = "Tipo de Pedido: Colegio - AutoCAD <br/>";

			// tipo
			var autocadtipo = 0;
			jQuery(".colegio-pedido-instantaneo .autocadtipo").each(function () {
				autocadtipo = parseInt(jQuery(this).val());
				contentPedido = contentPedido + "Tipo: " + jQuery(this).find("option:selected").text() + " <br/>";
			});

			if (autocadtipo == 1) {
				// ejercicios
				var ejercicios = 0;
				jQuery(".colegio-pedido-instantaneo .ejercicios").each(function () {
					if (jQuery(this).hasClass("base6")) {
						ejercicios = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Ejercicios: " + ejercicios + " <br/>";
					}
				});
				// tiempo
				var tiempo = 0;
				jQuery(".colegio-pedido-instantaneo .tiempoejercicios").each(function () {
					if (jQuery(this).hasClass("base6")) {
						tiempo = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";
					}
				});
				// observaciones
				jQuery(".colegio-pedido-instantaneo .observaciones").each(function () {
					if ((jQuery(this).attr("data-nivel") == "1") && (jQuery(this).attr("data-valor") == "6")) {
						contentPedido = contentPedido + "Observaciones: " + jQuery(this).val() + " <br/>";												
					}
				});
				var factorEjercicios = ejercicios * 2;
				var factorEntrega = getFactorEntregaColegio(tiempo);
				Total = Math.ceil(factorEjercicios * factorEntrega * vds);
				jQuery("div.points span strong").html(Total + " puntos"); 
			} else
			if (autocadtipo == 2) {
				// piezas
				var piezas = 0;
				jQuery(".colegio-pedido-instantaneo .piezas").each(function () {
					if (jQuery(this).hasClass("base6")) {
						piezas = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Cantidad de piezas: " + piezas + " <br/>";
					}
				});
				// vistas
				var vistaspiezas = 0;
				jQuery(".colegio-pedido-instantaneo .vistaspiezas").each(function () {
					if (jQuery(this).hasClass("base6")) {
						vistaspiezas = parseInt(jQuery(this).val());
						if (vistaspiezas == 1) {
							vistaspiezas = 1.5;
						} else {
							vistaspiezas = 1;
						}
						contentPedido = contentPedido + "Vistas: " + jQuery(this).find("option:selected").text() + " <br/>";
					}
				});
				// acotacion
				var acotacionpiezas = 0;
				jQuery(".colegio-pedido-instantaneo .acotacionpiezas").each(function () {
					if (jQuery(this).hasClass("base6")) {
						acotacionpiezas = parseInt(jQuery(this).val());
						if (acotacionpiezas == 1) {
							acotacionpiezas = 1.5;
						} else {
							acotacionpiezas = 1;
						}
						contentPedido = contentPedido + "Acotacion: " + jQuery(this).find("option:selected").text() + " <br/>";
					}
				});
				// tiempo
				var tiempo = 0;
				jQuery(".colegio-pedido-instantaneo .tiempopiezas").each(function () {
					if (jQuery(this).hasClass("base6")) {
						tiempo = parseInt(jQuery(this).val());
						contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";
					}
				});
				// observaciones
				jQuery(".colegio-pedido-instantaneo .observaciones").each(function () {
					if ((jQuery(this).attr("data-nivel") == "1") && (jQuery(this).attr("data-valor") == "6")) {
						contentPedido = contentPedido + "Observaciones: " + jQuery(this).val() + " <br/>";												
					}
				});
				var factorPiezas = piezas * 3;
				var factorEntrega = getFactorEntregaColegio(tiempo);
				Total = Math.ceil(factorPiezas * vistaspiezas * acotacionpiezas * factorEntrega * vds);				
				jQuery("div.points span strong").html(Total + " puntos"); 
			}
		} else
		if (jQuery(".colegio-pedido-instantaneo .form-element[data-nivel=0]").val() == 8) {
			// trabajos
			contentPedido = "Tipo de Pedido: Colegio - Trabajos <br/>";

			// titulo
			jQuery(".colegio-pedido-instantaneo .titulo").each(function () {
				if ((jQuery(this).attr("data-nivel") == "1") && (jQuery(this).attr("data-valor") == "8")) {
					contentPedido = contentPedido + "Titulo: " + jQuery(this).val() + " <br/>";												
				}
			});
			// tema
			jQuery(".colegio-pedido-instantaneo .tema").each(function () {
				if ((jQuery(this).attr("data-nivel") == "1") && (jQuery(this).attr("data-valor") == "8")) {
					contentPedido = contentPedido + "Tema: " + jQuery(this).find("option:selected").text() + " <br/>";											
				}
			});
			// paginas
			var paginas = 0;
			jQuery(".colegio-pedido-instantaneo .paginastrabajo").each(function () {
				paginas = parseInt(jQuery(this).val());
				contentPedido = contentPedido + "Cantidad de p&aacute;ginas: " + paginas + " <br/>";

			});
			// bibliografia
			var bibliografia = 0;
			jQuery(".colegio-pedido-instantaneo .bibliografiatrabajo").each(function () {
				bibliografia = parseInt(jQuery(this).val());
				if (bibliografia == 1) {
					bibliografia = 1.05;
				} else {
					bibliografia = 1;
				}
				contentPedido = contentPedido + "Bibliografia: " + jQuery(this).find("option:selected").text() + " <br/>";
			});
			// tiempo
			var tiempo = 0;
			jQuery(".colegio-pedido-instantaneo .tiempotrabajo").each(function () {
				tiempo = parseInt(jQuery(this).val());
				contentPedido = contentPedido + "Tiempo de entrega: " + jQuery(this).find("option:selected").text() + " <br/>";
			});
			// observaciones
			jQuery(".colegio-pedido-instantaneo .observaciones").each(function () {
				if ((jQuery(this).attr("data-nivel") == "1") && (jQuery(this).attr("data-valor") == "8")) {
					contentPedido = contentPedido + "Observaciones: " + jQuery(this).val() + " <br/>";												
				}
			});
			var factorPaginas = paginas * 4.1;
			var factorEntrega = getFactorEntregaColegio(tiempo);
			Total = Math.ceil(factorPaginas * bibliografia * factorEntrega * vds);			
			jQuery("div.points span strong").html(Total + " puntos"); 
		}
	}

	jQuery(".colegio-pedido-instantaneo .form-element").click(function () {
		// there are changes
		refreshColegioInstantaneo();
	});
	jQuery(".colegio-pedido-instantaneo .btn").click(function (evt) {
		evt.preventDefault();	
		refreshColegioInstantaneo();
		var error = false;
		jQuery(".colegio-pedido-instantaneo .form-element").each(function () {
			if (jQuery(this).css("display") == "block") {
				if (jQuery(this).is("input")) {
					if (((jQuery(this).val() == "")) || ((jQuery(this).val() == "0"))) {
						error = true;
					}
				} else 
				if (jQuery(this).is("select")) {
					if (jQuery(this).val() == "0") {
						error = true;
					} 
				} else {
				}				
			}
		});
		if (error) {
			notification("error", "Llene completamente el formulario. Tiene campos sin llenar.", "");
		} else
		{	
			var r = confirm("Desea hacer el pedido?");
			if (r == true) {
				if (lg == 1) {
					notificationAjaxOn("Realizando pedido ...");
				    // Haciendo el Pedido
				    jQuery.ajax({  
				  		type: 'POST',  
				  		url: ajaxpath, 
				  		dataType: "json", 			  
				  		data: {  
				  			action: 'MyAjaxFunctions',  
				        	toaction: 'doPedido',
				          	contentPedido: contentPedido,
				          	Total: Total,
				          	csuscrito: 0,
				          	factors: vds,
				          	tipopedido: jQuery(".colegio-pedido-instantaneo .form-element[data-nivel=0]").find("option:selected").text()
				  		},  
				  		success: function(data, textStatus, XMLHttpRequest){    				
				  				notificationAjaxOff();
				  				if (data["error"] == false) {		  					
				  					jQuery("div.points span strong").html("0 puntos");
				  					jQuery(".form-pedido .form-element[data-nivel=0]").val("0");
				  					jQuery("div#left-sidebar p#cantidad-puntos").html(data["Total"]);		  					
				  					refreshNivel(1);
				  					//jQuery("#colegio-form-post-id").val(data["post_id"]);
				  					//jQuery("#colegio-form").submit();
				  					if (filesColegio.length > 0) {
				  						for (var i = 0; i < filesColegio.length; i++) {
					  						var fd = new FormData();    
											fd.append( 'action',  'MyAjaxFunctions');
											fd.append( 'toaction',  'uploadArchive');
											fd.append( 'post_id',  data["post_id"]);
											fd.append( 'fileselect',  filesColegio[i]);
						  					jQuery.ajax({  
										  		type: 'POST',  
										  		url: ajaxpath, 
										  		dataType: "json", 			  
										  		data: fd, 
										  		processData: false, 
										  		contentType: false, 
										  		cache : false,
										  		success: function(data, textStatus, XMLHttpRequest){    				
										  			notification("success", "Pedido realizado satisfactoriamente!");
				                					jQuery("#colegio-details").html("");		  						  				
										  		},  
										  		error: function(MLHttpRequest, textStatus, errorThrown){  
										  			alert(errorThrown);  
										  		}  
										  	});
										}
				  					} else {
				  						notification("success", "Pedido realizado satisfactoriamente!");
				  					}	  								  					
				  				} else {
				  					if (data["url"] != "") {
				  						notification("error", data["message"], data["url"]);
				  					} else {
				  						notification("error", data["message"]);
				  					}
				  				}	  						  				
				  		},  
				  		error: function(MLHttpRequest, textStatus, errorThrown){  
				  			alert(errorThrown);  
				  		}  
				  	});	
				} else {
					notification("error", "Debe registrarse y entrar para poder realizar pedidos", "registrate");
				}
			}
		}
	});

	function refreshColegioSuscrito() {		
		var vv = jQuery(".colegio-suscritos .form-element[data-nivel=0]").val();		
		if ((vv != 2) && (vv != 9)) {
			// todos menos lengua castellana y dibujo tecnico

			contentPedido = "Tipo de Pedido: Colegio suscrito - Asignatura " + jQuery(".colegio-suscritos .form-element[data-nivel=0]").find("option:selected").text() + " <br/>";
			// tipo
			jQuery(".colegio-suscritos select").each(function () {
				if ((jQuery(this).attr("data-nivel") == "1") && (jQuery(this).attr("data-valor") == vv)) {
					contentPedido = contentPedido + "Tipo: " + jQuery(this).find("option:selected").text() + " <br/>";											
				}
			});
		} else
		if (vv == 2) {
			// Lengua castellana
			contentPedido = "Tipo de Pedido: Colegio suscrito - Asignatura " + jQuery(".colegio-suscritos .form-element[data-nivel=0]").find("option:selected").text() + " <br/>";
			// tipo
			jQuery(".colegio-suscritos select").each(function () {
				if ((jQuery(this).attr("data-nivel") == "1") && (jQuery(this).attr("data-valor") == "2")) {
					contentPedido = contentPedido + "Tipo: " + jQuery(this).find("option:selected").text() + " <br/>";											
				}

				if (jQuery(this).val() == 4) {
					// partes
					var pain = false; 
					contentPedido = contentPedido + "Partes a incluir: ";			
					jQuery(".colegio-suscritos .partesincluir:checked").each(function () {
						if (pain == true) { contentPedido = contentPedido + ", "; }
						contentPedido = contentPedido + jQuery(this).attr("data-valor");
						pain = true;
					});
					contentPedido = contentPedido + "<br/>";
				}
			});

		} else
		if (vv == 9) {
			// dibujo tecnico
			contentPedido = "Tipo de Pedido: Colegio suscrito - Asignatura " + jQuery(".colegio-suscritos .form-element[data-nivel=0]").find("option:selected").text() + " <br/>";
			// tipo
			jQuery(".colegio-suscritos select").each(function () {
				if ((jQuery(this).attr("data-nivel") == "1") && (jQuery(this).attr("data-valor") == "9")) {
					contentPedido = contentPedido + "Tipo: " + jQuery(this).find("option:selected").text() + " <br/>";

					if (jQuery(this).val() == 3) {
						// autocad
						jQuery(".colegio-suscritos select.base9").each(function () {
							contentPedido = contentPedido + "Tipo: " + jQuery(this).find("option:selected").text() + " <br/>";
						});
						contentPedido = contentPedido + "<br/>";
					}											
				}
			});
		}
	}
	jQuery(".colegio-suscritos .btn-colegiosuscrito").click(function (evt) {
		evt.preventDefault();	
		refreshColegioSuscrito();	
		var error = false;
		jQuery(".colegio-suscritos .form-element").each(function () {
			if (jQuery(this).css("display") == "block") {
				if (jQuery(this).is("input")) {
					if (((jQuery(this).val() == "")) || ((jQuery(this).val() == "0"))) {
						error = true;
					}
				} else 
				if (jQuery(this).is("select")) {
					if (jQuery(this).val() == "0") {
						error = true;
					} 
				} else {
				}				
			}
		});
		if (error) {
			notification("error", "Llene completamente el formulario. Tiene campos sin llenar.", "");
		} else
		{
			var r = confirm("Desea hacer el pedido?");
			if (r == true) {
				if (lg == 1) {
					notificationAjaxOn("Realizando pedido ...");
				    // Haciendo el Pedido
				    jQuery.ajax({  
				  		type: 'POST',  
				  		url: ajaxpath, 
				  		dataType: "json", 			  
				  		data: {  
				  			action: 'MyAjaxFunctions',  
				        	toaction: 'doPedido',
				          	contentPedido: contentPedido,
				          	Total: 0,
				          	csuscrito: 1,
				          	factors: vds,
				          	tipopedido: jQuery(".colegio-suscritos .form-element[data-nivel=0]").find("option:selected").text()
				  		},  
				  		success: function(data, textStatus, XMLHttpRequest){    				
				  				notificationAjaxOff();
				  				if (data["error"] == false) {		  					
				  					jQuery("div.points span strong").html("0 puntos");
				  					jQuery(".form-pedido .form-element[data-nivel=0]").val("0");
				  					jQuery("div#left-sidebar p#cantidad-puntos").html(data["Total"]);		  					
				  					refreshNivel(1);
				  					//jQuery("#colegio-form-post-id").val(data["post_id"]);
				  					//jQuery("#colegio-form").submit();
				  					var vv = parseInt(jQuery("#wrapper-content #content p.notification span").html());
			                		jQuery("#wrapper-content #content p.notification span").html(vv - 1);
				  					if (filesColegioSuscrito.length > 0) {
				  						for (var i = 0; i < filesColegioSuscrito.length; i++) {
					  						var fd = new FormData();    
											fd.append( 'action',  'MyAjaxFunctions');
											fd.append( 'toaction',  'uploadArchive');
											fd.append( 'post_id',  data["post_id"]);
											fd.append( 'fileselect',  filesColegioSuscrito[i]);
						  					jQuery.ajax({  
										  		type: 'POST',  
										  		url: ajaxpath, 
										  		dataType: "json", 			  
										  		data: fd, 
										  		processData: false, 
										  		contentType: false, 
										  		cache : false,
										  		success: function(data, textStatus, XMLHttpRequest){    				
										  			notification("success", "Pedido realizado satisfactoriamente!");
				                					jQuery("#colegiosuscrito-details").html("");			                					
										  		},  
										  		error: function(MLHttpRequest, textStatus, errorThrown){  
										  			alert(errorThrown);  
										  		}  
										  	});
						  				}
				  					} else {				  						
				  						notification("success", "Pedido realizado satisfactoriamente!");
				  					}	  								  					
				  				} else {
				  					if (data["url"] != "") {
				  						notification("error", data["message"], data["url"]);
				  					} else {
				  						notification("error", data["message"], "");
				  					}			  					
				  					if (data["suscritoshoy"] == true) {
				  						jQuery(".colegio-pedido-wrapper").fadeOut("fast");
				  						jQuery(".colegio-suscritos").fadeOut("fast");
				  						jQuery(".colegio-pedido-instantaneo").fadeIn("fast");
				  					}
				  				}	  						  				
				  		},  
				  		error: function(MLHttpRequest, textStatus, errorThrown){  
				  			alert(errorThrown);  
				  		}  
				  	});	
				} else {
					notification("error", "Debe registrarse y entrar para poder realizar pedidos", "registrate");
				}
			}
		}
	});

	// Managing colegio form to show
	jQuery("#radio-pedido-instantaneo").click(function () {
		jQuery(".colegio-suscritos").fadeOut("fast");
		jQuery(".colegio-pedido-instantaneo").fadeIn("fast");
	});
	jQuery("#radio-pedido-suscrito").click(function () {		
		jQuery(".colegio-pedido-instantaneo").fadeOut("fast");
		jQuery(".colegio-suscritos").fadeIn("fast");
	});

	// Controlling recharge form
	jQuery("#form-recargar .spinner1").click(function () {
		var totalPuntos = parseInt(jQuery("#form-recargar .puntos10").val() * 10);
		totalPuntos = totalPuntos + parseInt(jQuery("#form-recargar .puntos30").val() * 30);
		totalPuntos = totalPuntos + parseInt(jQuery("#form-recargar .puntos65").val() * 65);
		totalPuntos = totalPuntos + parseInt(jQuery("#form-recargar .puntos150").val() * 150);
		jQuery("#form-recargar .total-puntos").html(totalPuntos + " Puntos");
		var totalEuros = parseInt(jQuery("#form-recargar .puntos10").val() * 10);
		totalEuros = totalEuros + parseInt(jQuery("#form-recargar .puntos30").val() * 25);
		totalEuros = totalEuros + parseInt(jQuery("#form-recargar .puntos65").val() * 50);
		totalEuros = totalEuros + parseInt(jQuery("#form-recargar .puntos150").val() * 100);
		jQuery("#form-recargar .total-euros").html(totalEuros + " Euros");
	});
	jQuery("#form-recargar #form-recargar-submit").click(function (evt) {
		evt.preventDefault();
		var totalPuntos = parseInt(jQuery("#form-recargar .puntos10").val() * 10);
		totalPuntos = totalPuntos + parseInt(jQuery("#form-recargar .puntos30").val() * 30);
		totalPuntos = totalPuntos + parseInt(jQuery("#form-recargar .puntos65").val() * 65);
		totalPuntos = totalPuntos + parseInt(jQuery("#form-recargar .puntos150").val() * 150);
		var totalEuros = parseInt(jQuery("#form-recargar .puntos10").val() * 10);
		totalEuros = totalEuros + parseInt(jQuery("#form-recargar .puntos30").val() * 25);
		totalEuros = totalEuros + parseInt(jQuery("#form-recargar .puntos65").val() * 50);
		totalEuros = totalEuros + parseInt(jQuery("#form-recargar .puntos150").val() * 100);
		jQuery("#form-recargar #form-recargar-amount").val(totalEuros);
		jQuery("#form-recargar #form-recarga-itemnumber").val("Recarga de " + totalPuntos + " puntos");		
		jQuery("#form-recargar").submit();
	});
	// Controlling suscription form
	jQuery("#form-suscripcion #form-suscripcion-submit").click(function (evt) {
		evt.preventDefault();
		jQuery("#form-suscripcion #form-suscripcion-amount").val(jQuery("#form-suscripcion-dias").val() / 30 * v30s);
		jQuery("#form-suscripcion #form-suscripcion-itemnumber").val("Suscripcion por " + jQuery("#form-suscripcion-dias").val() + " dias");
		jQuery("#form-suscripcion").submit();
	});

	// Support for Drag and Drop
	// getElementById
	function $id(id) {
		return document.getElementById(id);
	}
	function clearFilesArea() {
		jQuery("#universidad-details").html("");
	}
	function clearFilesAreaColegio() {
		jQuery("#colegio-details").html("");
	}
	function clearFilesAreaColegioSuscrito() {
		jQuery("#colegiosuscrito-details").html("");
	}
	// output information
	function Output(msg) {
		//var m = $id("universidad-details");
		//m.innerHTML = msg;
		jQuery("#universidad-details").append(msg);
	}
	// output information
	function OutputColegio(msg) {
		//var m = $id("colegio-details");
		//m.innerHTML = msg;
		jQuery("#colegio-details").append(msg);
	}
	// output information
	function OutputColegioSuscrito(msg) {
		//var m = $id("colegiosuscrito-details");
		//m.innerHTML = msg;
		jQuery("#colegiosuscrito-details").append(msg);
	}


	// file drag hover
	function FileDragHover(e) {
		e.stopPropagation();
		e.preventDefault();
		e.target.className = (e.type == "dragover" ? "dnd-area-hover" : "dnd-area");
	}
	// file drag hover
	function FileDragHoverColegio(e) {
		e.stopPropagation();
		e.preventDefault();
		e.target.className = (e.type == "dragover" ? "dnd-area-hover" : "dnd-area");
	}
	// file drag hover
	function FileDragHoverColegioSuscrito(e) {
		e.stopPropagation();
		e.preventDefault();
		e.target.className = (e.type == "dragover" ? "dnd-area-hover" : "dnd-area");
	}

	// file selection
	function FileSelectHandler(e) {
		// cancel event and hover styling
		FileDragHover(e);

		// fetch FileList object
		files2Upload = e.target.files || e.dataTransfer.files;
		for (i = 0; i < files2Upload.length; i++) {
		    files[files.length] = files2Upload[i];
		} 

		refreshFilesArea();		
	}
	function refreshFilesArea() {
		// process all File objects
		clearFilesArea();
		for (var i = 0; i < files.length; i++) {
		    ParseFile(files[i], i);
		}
	}
	function deleteFilesArea(pos) {
		var tmp = new Array();
		for (var i = 0; i < files.length; i++) {
		    if (pos != i) {
		    	tmp[tmp.length] = files[i];		    	
		    }
		}
		files = tmp;
		refreshFilesArea();
	}
	jQuery("body").on("click", "#universidad-details div.pfiles a.acerrarDND", function (evt) {
		evt.preventDefault();
		deleteFilesArea(parseInt(jQuery(this).attr("data-pos")));
	});
	// file selection
	function FileSelectHandlerColegio(e) {
		// cancel event and hover styling
		FileDragHover(e);

		// fetch FileList object
		files2UploadColegio = e.target.files || e.dataTransfer.files;
		for (i = 0; i < files2UploadColegio.length; i++) {
		    filesColegio[filesColegio.length] = files2UploadColegio[i];
		} 

		refreshFilesAreaColegio();		
	}
	function refreshFilesAreaColegio() {
		// process all File objects
		clearFilesAreaColegio();
		for (var i = 0; i < filesColegio.length; i++) {
		    ParseFileColegio(filesColegio[i], i);
		}
	}
	function deleteFilesAreaColegio(pos) {
		var tmp = new Array();
		for (var i = 0; i < filesColegio.length; i++) {
		    if (pos != i) {
		    	tmp[tmp.length] = filesColegio[i];		    	
		    }
		}
		filesColegio = tmp;
		refreshFilesAreaColegio();
	}
	jQuery("body").on("click", "#colegio-details div.pfiles a.acerrarDND", function (evt) {
		evt.preventDefault();		
		deleteFilesAreaColegio(parseInt(jQuery(this).attr("data-pos")));
	});
	// file selection
	function FileSelectHandlerColegioSuscrito(e) {
		// cancel event and hover styling
		FileDragHover(e);

		// fetch FileList object
		files2UploadColegioSuscrito = e.target.files || e.dataTransfer.files;
		for (i = 0; i < files2UploadColegioSuscrito.length; i++) {
		    filesColegioSuscrito[filesColegioSuscrito.length] = files2UploadColegioSuscrito[i];
		} 

		refreshFilesAreaColegioSuscrito();		
	}
	function refreshFilesAreaColegioSuscrito() {
		// process all File objects
		clearFilesAreaColegioSuscrito();
		for (var i = 0; i < filesColegioSuscrito.length; i++) {
		    ParseFileColegioSuscrito(filesColegioSuscrito[i], i);
		}
	}
	function deleteFilesAreaColegioSuscrito(pos) {
		var tmp = new Array();
		for (var i = 0; i < filesColegioSuscrito.length; i++) {
		    if (pos != i) {
		    	tmp[tmp.length] = filesColegioSuscrito[i];		    	
		    }
		}
		filesColegioSuscrito = tmp;
		refreshFilesAreaColegioSuscrito();
	}
	jQuery("body").on("click", "#colegiosuscrito-details div.pfiles a.acerrarDND", function (evt) {
		evt.preventDefault();
		deleteFilesAreaColegioSuscrito(parseInt(jQuery(this).attr("data-pos")));
	});


	// output file information
	function ParseFile(file, i) {

		Output(
			"<div class='pfiles'>Archivo: <strong>" + file.name +
			//"</strong><br/> tipo: <strong>" + file.type +
			"</strong><br/> Tama&ntilde;o: <strong>" + Math.round(file.size / 1024 * 100) / 100  +
			"</strong> KB <a href='#' data-pos='" + i + "' class='acerrarDND'><span class='cerrarDND'>X</span></a></div>"
		);

	}
	// output file information
	function ParseFileColegio(file, i) {

		OutputColegio(
			"<div class='pfiles'>Archivo: <strong>" + file.name +
			//"</strong><br/> tipo: <strong>" + file.type +
			"</strong><br/> Tama&ntilde;o: <strong>" + Math.round(file.size / 1024 * 100) / 100  +
			"</strong> KB <a href='#' data-pos='" + i + "' class='acerrarDND'><span class='cerrarDND'>X</span></a></div>"
		);

	}
	// output file information
	function ParseFileColegioSuscrito(file, i) {

		OutputColegioSuscrito(
			"<div class='pfiles'>Archivo: <strong>" + file.name +
			//"</strong><br/> tipo: <strong>" + file.type +
			"</strong><br/> Tama&ntilde;o: <strong>" + Math.round(file.size / 1024 * 100) / 100  +
			"</strong> KB <a href='#' data-pos='" + i + "' class='acerrarDND'><span class='cerrarDND'>X</span></a></div>"
		);

	}
	// initialize
	function Init() {

		var fileselect = $id("universidad-file"),
			filedrag = $id("universidad-dnd-area");
		var fileselectcolegio = $id("colegio-file"),
			filedragcolegio = $id("colegio-dnd-area");
			var fileselectcolegiosuscrito = $id("colegiosuscrito-file"),
			filedragcolegiosuscrito = $id("colegiosuscrito-dnd-area");

		// file select
		//if (fileselect != null) fileselect.addEventListener("change", FileSelectHandler, false);

		// is XHR2 available?
		var xhr = new XMLHttpRequest();
		if (xhr.upload) {

			// file drop
			if (filedrag != null) filedrag.addEventListener("dragover", FileDragHover, false);
			if (filedrag != null) filedrag.addEventListener("dragleave", FileDragHover, false);
			if (filedrag != null) filedrag.addEventListener("drop", FileSelectHandler, false);

			if (filedragcolegio != null) filedragcolegio.addEventListener("dragover", FileDragHoverColegio, false);
			if (filedragcolegio != null) filedragcolegio.addEventListener("dragleave", FileDragHoverColegio, false);
			if (filedragcolegio != null) filedragcolegio.addEventListener("drop", FileSelectHandlerColegio, false);

			if (filedragcolegiosuscrito != null) filedragcolegiosuscrito.addEventListener("dragover", FileDragHoverColegioSuscrito, false);
			if (filedragcolegiosuscrito != null) filedragcolegiosuscrito.addEventListener("dragleave", FileDragHoverColegioSuscrito, false);
			if (filedragcolegiosuscrito != null) filedragcolegiosuscrito.addEventListener("drop", FileSelectHandlerColegioSuscrito, false);
			
		}

	}

	// call initialization file
	if (window.File && window.FileList && window.FileReader) {
		Init();
	}

});

function notification(tipo, msg, redirect) {
	var backgroundColor;
	if (tipo == "error") {
		backgroundColor = "red";
	} else 
	if (tipo = "success") {
		backgroundColor = "green";
	}	
	jQuery("#notification .inner").css("background", backgroundColor).html(msg);
	jQuery('#notification').animate({
		'top': '35%',
		'opacity': ".8"
	}, 1500, 'easeOutExpo', function() {
		jQuery('#notification').animate({
			'top': '-50%',
			'opacity': "0"
		}, 3000, 'easeInSine', function() {
			if ((tipo == "error") && (redirect != "")) {
				window.location = basepath + redirect;
			}
		});	
	});	
}

function notificationAjaxOn(msg) {
	jQuery("#notificationAjax .inner").html(msg);
	jQuery('#notificationAjax').animate({
		'top': '35%',
		'opacity': ".8"
	}, 1500, 'easeOutExpo');	
}

function notificationAjaxOff() {
	jQuery('#notificationAjax').animate({
		'top': '-50%',
		'opacity': "0"
	}, 1000, 'easeInSine', function() {
			
	});	
}

function refreshNivel(nivel) {
	contentPedido = "";
	jQuery(".form-pedido .form-element[data-nivel=" + nivel + "]").fadeOut("fast", function () {
		if (jQuery(this).is("select")) {
            jQuery(this).val("0");  
		} else 
		if (jQuery(this).is("input.smartspinner")) {
            jQuery(this).val("0");  
		} else
		if (jQuery(this).is("textarea")) {
            jQuery(this).val("");  
		} else
		if (jQuery(this).is("input")) {
            jQuery(this).val("");  
		}
	});
	jQuery(".form-pedido .action[data-nivel=" + nivel + "]").val("0");
	if (nivel < 4) {
		refreshNivel(nivel + 1);
	}	
}

function getFactorEntrega(tentrega) {
	var factorEntrega;
	if (tentrega == 0) {
		factorEntrega = 1;
	} else 
	if (tentrega / 24 == 1) {
		factorEntrega = 6;
	} else
	if (tentrega / 24 == 2) {
		factorEntrega = 3;
	} else 
	if (tentrega / 24 == 3) {
		factorEntrega = 2;
	} else 
	if (tentrega / 24 == 4) {
		factorEntrega = 1.5;
	} else 
	if (tentrega / 24 == 5) {
		factorEntrega = 1.2;
	} else 
	if (tentrega / 24 == 6) {
		factorEntrega = 1;
	} else 
	if ((tentrega / 24 >= 7) && (tentrega / 24 <= 8)) {
		factorEntrega = 0.97;
	} else 
	if ((tentrega / 24 >= 9) && (tentrega / 24 <= 11)) {
		factorEntrega = 0.95;
	} else 
	if ((tentrega / 24 >= 12) && (tentrega / 24 <= 15)) {
		factorEntrega = 0.93;
	} else 
	if (tentrega / 24 >= 16) {
		factorEntrega = 0.9;
	}
	return factorEntrega;
}

function getFactorEntregaReducido(tentrega) {
	var factorEntrega;
	if (tentrega == 120) {
		factorEntrega = 3;
	} else 
	if (tentrega == 240) {
		factorEntrega = 2;
	} else
	if (tentrega == 360) {
		factorEntrega = 1;
	} else 
	if (tentrega == 480) {
		factorEntrega = 0.95;
	} else 
	if (tentrega == 500) {
		factorEntrega = 0.95;
	} else {
		factorEntrega = 0;
	}
	return factorEntrega;
}

function getFactorEntregaColegio(hh) {
	var factorEntrega = 0;
	if (hh == 2) {
		factorEntrega = 4;
	} else
	if (hh == 5) {
		factorEntrega = 2.5;
	} else
	if (hh == 8) {
		factorEntrega = 1.5;
	} else
	if (hh == 24) {
		factorEntrega = 1;
	} else
	if (hh == 48) {
		factorEntrega = 0.75;
	} else
	if (hh == 72) {
		factorEntrega = 0.75;
	} else
	if (hh == 96) {
		factorEntrega = 0.5;
	} else
	if (hh == 120) {
		factorEntrega = 0.5;
	} else
	if (hh == 144) {
		factorEntrega = 0.5;
	} else
	if (hh == 150) {
		factorEntrega = 0.5;
	} else
	if (hh == 2) {
		factorEntrega = 0.5;
	}
	return factorEntrega;
}