<?php
/*
If you would like to edit this file, copy it to your current theme's directory and edit it there.
Theme My Login will always look in your theme's directory first, before using this default template.
*/
?>
<script type="text/javascript">
	/*jQuery(document).ready(function () {
		jQuery("#wp-submit").click(function (evt) {
			evt.preventDefault();
			var username = jQuery("#user_login").val();
		    var password = jQuery("#user_pass").val();
		    var remember;
		    if (jQuery("#rememberme").is(":checked")) {
		    	remember = 1;
		    } else {
		    	remember = 0;
		    }
		    jQuery("#user_login").val("");
		    jQuery("#user_pass").val("");
		    notificationAjaxOn("Revisando credenciales ...");    
			jQuery.ajax({  
		  		type: 'POST',  
		  		url: ajaxpath, 
		  		dataType: "json", 			  
		  		data: {  
		  			action: 'MyAjaxFunctions',  
		        	toaction: 'login',
		          	username: username,
		          	password: password,
		          	remember: remember
		  		},  
		  		success: function(data, textStatus, XMLHttpRequest){   
		  			notificationAjaxOff(); 				
		  			if (data["error"] == true) {
		  				jQuery("#login-form form").fadeIn("slow");
		  				notification("error", "Usuario y/o contraseña incorrecto(s)", data["url"]);
		  			} else {
		  				window.location = basepath + "/pedidos";
		  			}   		
		  		},  
		  		error: function(MLHttpRequest, textStatus, errorThrown){  
		  			alert(errorThrown);  
		  		}  
		  	});
		});
	});*/
</script>
<div class="login" id="theme-my-login-login<?php $template->the_instance(); ?>">
	<h2 class="title">Entrar en Apsies</h2>
	<!--<?php $template->the_action_template_message( 'login' ); ?>-->

	<?php $template->the_errors(); ?>
	<form name="loginform" id="loginform<?php $template->the_instance(); ?>" action="<?php $template->the_action_url( 'login' ); ?>" method="post">
		<p>
			<label for="user_login<?php $template->the_instance(); ?>"><?php _e( 'Usuario' ); ?></label>
			<input type="text" name="log" id="user_login<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'log' ); ?>" />
		</p>
		<p>
			<label for="user_pass<?php $template->the_instance(); ?>"><?php _e( 'Password' ); ?></label>
			<input type="password" name="pwd" id="user_pass<?php $template->the_instance(); ?>" class="input" value="" />
		</p>

		<?php do_action( 'login_form' ); ?>

		<p class="forgetmenot">
			<input name="rememberme" type="checkbox" id="rememberme<?php $template->the_instance(); ?>" value="forever" />
			<label for="rememberme<?php $template->the_instance(); ?>"><?php esc_attr_e( 'Recordarme' ); ?></label>
		</p>
		<p class="submit">
			<input type="submit" name="wp-submit" id="wp-submit<?php $template->the_instance(); ?>" value="<?php esc_attr_e( 'ACCEDER' ); ?>" />
			<input type="hidden" name="redirect_to" value="<?php $template->the_redirect_url( 'login' ); ?>" />
			<input type="hidden" name="instance" value="<?php $template->the_instance(); ?>" />
			<input type="hidden" name="action" value="login" />
		</p>
	</form>
	<?php $template->the_action_links( array( 'login' => false ) ); ?>
</div>
