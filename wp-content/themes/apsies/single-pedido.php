<?php 

  global $usuariologueado;

  if ( !is_user_logged_in() ) {
    $usuariologueado = false;  
    $vds = 1;
    $lg = 0;
    $suscrito = false;      
  } else {
    $usuariologueado = true;
    $lg = 1;
    global $current_user;
	get_currentuserinfo();
	$user_id = $current_user->ID;
	$fechasuscripcion = get_user_meta( $user_id, 'fechasuscripcion', true );
	$diassuscripcion = get_user_meta( $user_id, 'diassuscripcion', true );
	$suscrito = false;
	$ahora = time();
	$nivel = get_user_meta( $user_id, 'nivel', true );
	if (($fechasuscripcion <= $ahora) && ($ahora <= $fechasuscripcion + ($diassuscripcion * 86400))) {
		$suscrito = true;
		$vds = getOption("eto_valordescuentosuscritos", 1);
		$vds = 1 - ($vds / 100);
	} else {
		$vds = 1;
	}
  }

  if ($usuariologueado) {
  	get_header(); 
 ?>
	
	<?php while ( have_posts() ) : the_post(); ?>

	<?php 
	// Getting relative pedido
	$pedido_id = get_the_ID();
	// Getting relative user
	$pedido_user_id = get_post_meta($pedido_id, "usuario", true);
	if ($user_id == $pedido_user_id) {
		$post_id = get_the_ID();
		if ($post_id > 0) {			
			update_post_meta($post_id, "solucionnueva", "no");
		}			
	?>
	<div id="wrapper-content">
		<?php get_sidebar('left'); ?>	

		<div id="content">
			<div class="titling">
				<?php 
				if ($suscrito) {
				?>
				<h2 class="content-title">Pedido</h2>
				<?php
				} else {
				?>
				<h2 class="content-title-ns">Pedido</h2>
				<?php
				}
				?>	
				<p>
					<?php the_title(); ?>
				</p>
				<p>
					<?php 
						$archivourl = get_field("solucion");
						if ($archivourl != "") {
						?>
						<a style="color: green; font-size: 18px; font-weight: bold;" href="<?php echo $archivourl; ?>" download>Descargar Soluci&oacute;n</a>
							<?php
						} else {
							?>
						<span style="color: red; font-size: 18px; font-weight: bold;" class="sin-solucionar">Sin soluci&oacute;n</span>
						<?php
						}
					?>
				</p>
			</div>
			<div class="ticket-content">				
				<p class="ticket-date">
					<?php the_date(); ?>
				</p>
				<?php the_content(); ?>								
			</div>

		</div>

		<div class="clear"></div>
	</div>
	<?php 
	} else {
	?>
	<div id="wrapper-content">
		<div id="page-content">
			
			<div class="titling">
				<h2 class="content-title">Error</h2>
				<p>No tiene autorizaci&oacute;n para ver esta p&aacute;gina</p>
			</div>
					
		</div>			
	</div>
	<?php
	} 
	?>	
		
	<?php endwhile; // end of the loop. ?>
		
	<?php get_footer(); 

	} else {
  	?>
  	<html>
  		<head>
  			<script type="text/javascript">      
            	window.location = "<?php echo wp_login_url(); ?>";        
          	</script>
  		</head>
  		<body></body>
  	</html>
  	<?
  }
	?>