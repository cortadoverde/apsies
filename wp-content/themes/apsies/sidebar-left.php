		<?php 
		global $current_user;
	    get_currentuserinfo();
	    $user_id = $current_user->ID;
	    $fechasuscripcion = get_user_meta( $user_id, 'fechasuscripcion', true );
	    $diassuscripcion = get_user_meta( $user_id, 'diassuscripcion', true );
	    $suscrito = false;
	    $ahora = time();

	    if ($ahora <= $fechasuscripcion + ($diassuscripcion * 86400)) {
	    	if (($fechasuscripcion <= $ahora) && ($ahora <= $fechasuscripcion + ($diassuscripcion * 86400))) {
	    		$suscrito = true;
	    		$fechatopesuscripcion = $diassuscripcion * 84600;
	    		$ahorasuscripcion = $ahora - $fechasuscripcion;
	    		$stylecumplidos =  round($ahorasuscripcion * 210 / $fechatopesuscripcion);
	    		$stylequedan = 210 - $stylecumplidos;
	    		$diasquedan = abs(round($stylequedan * $diassuscripcion / 210));	    		
	    	}
	    }
	    if ($suscrito) {
	    	$sidebarClass = ' class="sidebarSuscrito"';
	    } else {
	    	$sidebarClass = '';
	    }
		?>
		<div id="left-sidebar" <?php echo $sidebarClass; ?>>
			<p id="tienes">Tienes</p>
			<p id="cantidad-puntos"><?php echo get_user_meta( $user_id, 'puntosusuario', true ); ?></p>
			<p id="puntos">Puntos</p>
			<hr/>
			<p id="recarga" <?php if (!$suscrito) { echo ' class="ns"';} ?> >
				<a href="<?php bloginfo("url") ?>/recargar" title="Recarga ahora">Recarga ahora</a>
			</p>
			<table id="tabla-precios">
				<tr>
					<td>10 puntos</td>
					<td><span>10&euro;</span></td>
				</tr>
				<tr>
					<td>30 puntos</td>
					<td><span>25&euro;</span></td>
				</tr>
				<tr>
					<td>65 puntos</td>
					<td><span>50&euro;</span></td>
				</tr>
				<tr>
					<td>150 puntos</td>
					<td><span>100&euro;</span></td>
				</tr>
			</table>
			<hr/>
			<p class="info">
				<?php echo getOption("eto_sidebarsp", "Sed non massa quis nisl tincidunt posuere sit amet vitae sem. <br/>Praesent  sed convallis quam <br/>donec hendrerit neque sit amet."); ?>
			</p>
			<div id="time-suscription">
				<?php 
				if ($suscrito) {
				?>
				<?php 
				if ($diasquedan != 1) {
				?>
					Te quedan <span><?php echo $diasquedan; ?> d&iacute;as</span> de suscripci&oacute;n <br/>
				<?php
				} else {
				?>
					Te queda <span><?php echo $diasquedan; ?> d&iacute;a</span> de suscripci&oacute;n <br/>
				<?php
				}
				?>
								
				<div style="width: 210px; height: 6px; display: block; margin: 6px auto; padding: 0px;">
					<div style="background: #00adf0; width: <?php echo $stylecumplidos; ?>px; height: 6px; float: left; display: block;"></div>
					<div style="background: #d0d0d0; width: <?php echo $stylequedan; ?>px; height: 6px; float: left; display: block"></div>
				</div>
				<span class="days-init">0 d&iacute;as</span>
				<span class="days-end"><?php echo $diassuscripcion; ?> d&iacute;as</span><br/>
				<a class="btn" href="<?php bloginfo("url") ?>/suscripcion" title="Aumenta la suscripci&oacute;n">Aumenta la suscripci&oacute;n</a>
				<?php 
				} else { 
				?>
				<a class="btn" href="<?php bloginfo("url") ?>/suscripcion" title="Suscr&iacute;bete">Suscr&iacute;bete</a>
				<?php 
				}
				?>
			</div>
		</div>