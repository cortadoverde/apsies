<?php get_header(); ?>
	<?php while ( have_posts() ) : the_post(); ?>

	<div id="wrapper-content">
		<div id="page-content">
			<?php the_content(); ?>
		</div>			
	</div>	
		
	<?php endwhile; // end of the loop. ?>
		
	<?php get_footer(); ?>