<?php 

  global $usuariologueado;

  if ( !is_user_logged_in() ) {
    $usuariologueado = false;  
    $vds = 1;
    $lg = 0;
    $suscrito = false;      
  } else {
    $usuariologueado = true;
    $lg = 1;
    global $current_user;
	get_currentuserinfo();
	$user_id = $current_user->ID;
	$fechasuscripcion = get_user_meta( $user_id, 'fechasuscripcion', true );
	$diassuscripcion = get_user_meta( $user_id, 'diassuscripcion', true );
	$suscrito = false;
	$ahora = time();
	$nivel = get_user_meta( $user_id, 'nivel', true );
	if (($fechasuscripcion <= $ahora) && ($ahora <= $fechasuscripcion + ($diassuscripcion * 86400))) {
		$suscrito = true;
		$vds = getOption("eto_valordescuentosuscritos", 1);
		$vds = 1 - ($vds / 100);
	} else {
		$vds = 1;
	}
  }

  if ($usuariologueado) {
  	get_header(); 
 ?>

	<div id="wrapper-content">
		<?php get_sidebar('left'); ?>

		<div id="content">

			<?php 
			if (isset($_GET['tx'])) {
				
				$pp_hostname = "www.sandbox.paypal.com"; // Change to www.sandbox.paypal.com to test against sandbox


				// read the post from PayPal system and add 'cmd'
				$req = 'cmd=_notify-synch';
				 
				$tx_token = $_GET['tx'];
				$auth_token = "JI_Hf2IxhVYIlxiYB2jtdU6iCzIrvEVAEVDGhT7RQ4lq6a1N7bS2PWhJFJ8";
				$req .= "&tx=$tx_token&at=$auth_token";
				 
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, "https://$pp_hostname/cgi-bin/webscr");
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
				//set cacert.pem verisign certificate path in curl using 'CURLOPT_CAINFO' field here,
				//if your server does not bundled with default verisign certificates.
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: $pp_hostname"));
				$res = curl_exec($ch);
				curl_close($ch);

				if(!$res){
				    //HTTP ERROR
				    echo "Error HTTP";
				}else{
				     // parse the data
				    $lines = explode("\n", $res);
				    $keyarray = array();
				    if (strcmp ($lines[0], "SUCCESS") == 0) {
				        for ($i=1; $i<count($lines);$i++){
				        list($key,$val) = explode("=", $lines[$i]);
				        $keyarray[urldecode($key)] = urldecode($val);
				    }
				    // check the payment_status is Completed
				    // check that txn_id has not been previously processed
				    // check that receiver_email is your Primary PayPal email
				    // check that payment_amount/payment_currency are correct
				    // process payment
				    $firstname = $keyarray['first_name'];
				    $lastname = $keyarray['last_name'];
				    $itemname = $keyarray['item_name'];
				    $amount = $keyarray['mc_gross'];				    
				     
				    echo ('<div class="titling">');
				    echo ('<h2 class="content-title">Suscripci&oacute;n realizada satisfactoriamente</h2>');
				    echo ('<p>Gracias por su pago! ha completado el proceso de suscripci&oacute;n</p>');
				    echo ('</div>');

				    $fechasuscripcion = get_user_meta( $user_id, 'fechasuscripcion', true );
	    			$diassuscripcion = get_user_meta( $user_id, 'diassuscripcion', true );
	    			$ahora = time();
	    			// Si no esta suscrito o se le acabo la suscripcion
	    			if ($ahora > $fechasuscripcion + ($diassuscripcion * 86400)) {
	    				// Se define nueva fecha de suscripcion y dias
	    				update_user_meta( $user_id, 'fechasuscripcion', $ahora );
						update_user_meta( $user_id, 'diassuscripcion', $amount / getOption("eto_valores30suscripcion", 10) * 30 );	
	    			} else {
	    				// Ya esta suscrito y esta aumentando suscripcion
	    				// Se deja fecha de suscripcion y se aumentan solo los dias
	    				update_user_meta( $user_id, 'diassuscripcion', ($amount / getOption("eto_valores30suscripcion", 10) * 30) + $diassuscripcion );	
	    			}

				    echo "<div id='payment-details'>";
					echo "<p><strong>Detalles del pago</strong></p>\n";
					echo "<ul>";
				    echo "<li>Name: $firstname $lastname</li>\n";
				    echo "<li>Item: $itemname</li>\n";
				    echo "<li>Amount: $amount Euros</li>\n";
				    echo "</ul>";
				    echo "\n";
				    echo '<a class="btn" href="' . get_bloginfo("url") . '/suscripcion" title="Actualizar">Actualizar</a>';
				    echo "</div>";				    
				    }
				    else if (strcmp ($lines[0], "FAIL") == 0) {
				        // log for manual investigation
				        echo "Ha ocurrido un Error.";
				    }
				}

			} else { 
			?>
			<div class="titling">
				<?php 
				if ($suscrito) {
				?>
				<h2 class="content-title">Suscripci&oacute;n</h2>
				<?php
				} else {
				?>
				<h2 class="content-title-ns">Suscripci&oacute;n</h2>
				<?php
				}
				?>					
				<p>Suscr&iacute;bete y obt&eacute;n un importante descuento en todos tus pedidos</p>
			</div>			
			<div id="payment_form">							
				<FORM action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" id="form-suscripcion" name="form-suscripcion">
					<p style="margin: 30px 0px;">
						<?php 
						$fechasuscripcion = get_user_meta( $user_id, 'fechasuscripcion', true );
		    			$diassuscripcion = get_user_meta( $user_id, 'diassuscripcion', true );
		    			$ahora = time();
		    			// Si no esta suscrito o se le acabo la suscripcion
		    			if ($ahora > $fechasuscripcion + ($diassuscripcion * 86400)) {
						?>
						Suscribirse por <input id="form-suscripcion-dias" autocomplete="false" data-nivel="1" data-valor="5" type="text" class="spinner30 form-element" style=""> d&iacute;as	
						<?php 
						} else {
						?>
						Aumentar suscripci&oacute;n en <input id="form-suscripcion-dias" autocomplete="false" data-nivel="1" data-valor="5" type="text" class="spinner30 form-element" style=""> d&iacute;as	
						<?php
						}
						?>
					</p>
					<INPUT TYPE="hidden" name="cmd" value="_xclick">						
					<INPUT TYPE="hidden" name="charset" value="utf-8">
					<INPUT TYPE="hidden" NAME="return" value="<?php bloginfo("url") ?>/suscripcion/?confirm=true">	
					<INPUT TYPE="hidden" NAME="currency_code" value="EUR">	
					<input type="hidden" name="business" value="anabel.osuna@madridnyc.com">
					<input type="hidden" name="item_name" value="Suscripcion">
					<input type="hidden" name="item_number" id="form-suscripcion-itemnumber" value="Suscripcion">
					<input type="hidden" name="amount" id="form-suscripcion-amount" value="1">
					<input type="hidden" name="tax" value="0">
					<input type="hidden" name="quantity" value="1">
					<input type="hidden" name="no_note" value="1">

					<input id="form-suscripcion-submit" type="image" name="submit" border="0" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" alt="PayPal - la via mas fácil y segura de pagar en Internet">
				</FORM>
			</div>
			<?php 
			}
			?>
		</div>
		<div class="clear"></div>
	</div>

	<?php get_footer(); 

	} else {
  	?>
  	<html>
  		<head>
  			<script type="text/javascript">      
            	window.location = "<?php echo wp_login_url(); ?>";        
          	</script>
  		</head>
  		<body></body>
  	</html>
  	<?
  }
	?>