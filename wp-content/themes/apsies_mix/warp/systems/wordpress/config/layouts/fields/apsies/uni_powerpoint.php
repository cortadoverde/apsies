<h2>Valores por defecto</h2>
<div class="uk-form uk-form-horizontal">
	<div class="uk-grid">

        <div class="uk-form-row">
            <label class="uk-form-label" for="form-h-it">Tipo</label>

            <div class="uk-form-controls">
                <select name="uni[powepoint][tipo]">
                    <option value="diapo_texto" <?php echo ( ( $this['config']['uni']['powepoint']['tipo'] == "diapo_texto" ) ? ' selected="selected" ' : '' ) ?>> Diapositiva a Texto </option>
                    <option value="texto_diapo" <?php echo ( ( $this['config']['uni']['powepoint']['tipo'] == "texto_diapo" ) ? ' selected="selected" ' : '' ) ?>> Texto a Diapositiva </option>
                    
                </select>
            </div>
        </div>
        
        <div class="uk-width-1-2">
	    	<div class="uk-form-row">
	            <label class="uk-form-label" for="form-h-it">Tiempos de entrega</label>

	            <div class="uk-form-controls">
	                <?php echo $this['forms']->tiempos_entrega($this['config']['uni']['powepoint']['tiempo_entrega'],'uni[powepoint][tiempo_entrega]', 'uni'); ?>
	            </div>
	        </div>
	    </div>
	</div>
</div>


