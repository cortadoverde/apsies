<h2>Valores por defecto</h2>
<div class="uk-form uk-form-horizontal">
	<div class="uk-grid">
        <div class="uk-width-1-2">
	    	<div class="uk-form-row">
	            <label class="uk-form-label" for="form-h-it">Tiempos de entrega</label>

	            <div class="uk-form-controls">
	                <?php echo $this['forms']->tiempos_entrega($this['config']['uni']['autocad']['tiempo_entrega'],'uni[autocad][tiempo_entrega]', 'autocad'); ?>
	            </div>
	        </div>
	    </div>
	</div>
</div>


