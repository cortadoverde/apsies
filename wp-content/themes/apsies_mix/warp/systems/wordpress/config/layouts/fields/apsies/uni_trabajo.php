<h2>Valores por defecto</h2>
<div class="uk-form uk-form-horizontal">
	<div class="uk-grid">
	    <div class="uk-width-1-2">
            <div class="uk-form-row">
                <label class="uk-form-label" for="form-h-it">Bibliografía</label>

                <div class="uk-form-controls">
                    <select name="uni[trabajo][bibliografia]">
                        <option value="0">-- Bibliografía --</option>
                        <option value="si" <?php echo ( ( $this['config']['uni']['trabajo']['bibliografia'] == "si" ) ? ' selected="selected" ' : '' ) ?>> Si </option>
                        <option value="no" <?php echo ( ( $this['config']['uni']['trabajo']['bibliografia'] == "no" ) ? ' selected="selected" ' : '' ) ?>> No </option>
                        
                    </select>
                </div>
            </div>
        </div>     
	    <div class="uk-width-1-2">
	    	<div class="uk-form-row">
	            <label class="uk-form-label" for="form-h-it">Tiempos de entrega</label>

	            <div class="uk-form-controls">
	                <?php echo $this['forms']->tiempos_entrega($this['config']['uni']['trabajo']['tiempo_entrega'],'uni[trabajo][tiempo_entrega]'); ?>
	            </div>
	        </div>
	    </div>
	</div>
</div>

<table class="uk-table">
   
    <thead>
        <tr>
            <th>Asignatura</th>
            <th>Seleccionado</th>
            <th>Activo</th>
        </tr>
    </thead>

    <tbody>
    	<?php foreach ($this['forms']->asignaturas_uni as $name => $data): 
    			$checked_active 	= isset( $this['config']['uni']['trabajo'][$name]['active'] ) ? ' checked="checked" ' : ''; 
    			$checked_selected 	= isset( $this['config']['uni']['trabajo']['asignatura'] ) && $this['config']['uni']['trabajo']['asignatura'] == $data['id'] ? ' checked="checked" ' : ''; 
    	?>
    		<tr>
    			<td><?php echo $data['name']?></td>
    			<td><input type="radio" name="uni[trabajo][asignatura]" value="<?php echo $data['id'] ?>" <?php echo $checked_selected ?> ></td>
    			<td><input type="checkbox" name="uni[trabajo][<?php echo $name?>][active]" value="1" <?php echo $checked_active ?> ></td>
    		</tr>
    	<?php endforeach ?>
            
    </tbody>
</table>


