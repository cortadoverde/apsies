<?php
	$config = $this['config'];
?>

<div id="forms" data-field-name="<?php echo $name ?>">
	<h2>ESO / Bachiller</h2>
	<ul class="uk-tab" data-uk-tab="{connect:'#tab-content-eso'}">
	    <li><a href="">Resolución de ejercicios</a></li>
	    <li><a href="">Redacción sobre un tema</a></li>
	    <li><a href="">Comentario de texto</a></li>
	    <li><a href="">Comentario Artistico</a></li>
	    <li><a href="">Crítica literaria</a></li>
	    <li><a href="">AutoCAD</a></li>
	    <li><a href="">Dudas / Investigación sobre un trabajo</a></li>
	    <li><a href="">Trabajos</a></li>
	</ul>
	<ul id="tab-content-eso" class="uk-switcher uk-margin">
	    <li class="uk-active">
	    	
			<?php echo $this->render( 'config:layouts/fields/apsies/eso_resolucion', array( 'config' => $config ) ) ?>	
	    </li>
	    <li class="">
	    	<?php echo $this->render( 'config:layouts/fields/apsies/eso_redaccion', array( 'config' => $config ) ) ?>
	    </li>
	    <li class="">
	    	<?php echo $this->render( 'config:layouts/fields/apsies/eso_comentario', array( 'config' => $config ) ) ?>
	    </li>
	    
	    <li class="">
	    	<?php echo $this->render( 'config:layouts/fields/apsies/eso_comentario_artistico', array( 'config' => $config ) ) ?>
	    </li>

	    <li class="">
	    	<?php echo $this->render( 'config:layouts/fields/apsies/eso_critica', array( 'config' => $config ) ) ?>
	    </li>
	    <li class="">
	    	<?php echo $this->render( 'config:layouts/fields/apsies/eso_autocad', array( 'config' => $config ) ) ?>
	    </li>
	    <li class="">
	    	<?php echo $this->render( 'config:layouts/fields/apsies/eso_dudas', array( 'config' => $config ) ) ?>
	    </li>
	    <li class="">
	    	<?php echo $this->render( 'config:layouts/fields/apsies/eso_trabajo', array( 'config' => $config ) ) ?>
	    </li>
	</ul>

	<hr class="uk-article-divider">

	<h2>Universidad</h2>
	<ul class="uk-tab" data-uk-tab="{connect:'#tab-content'}">
	    <li><a href="">Trabajos</a></li>
	    <li><a href="">AutoCAD</a></li>
	    <li><a href="">Apuntes</a></li>
	    <li><a href="">PowerPoint</a></li>
	    <li><a href="">Exposiciones</a></li>
	</ul>
	<ul id="tab-content" class="uk-switcher uk-margin">
		<li class="uk-active">
	    	
			<?php echo $this->render( 'config:layouts/fields/apsies/uni_trabajo', array( 'config' => $config ) ) ?>	
	    </li>
		<li>
	    	
			<?php echo $this->render( 'config:layouts/fields/apsies/uni_autocad', array( 'config' => $config ) ) ?>	
	    </li>
		<li>
	    	
			<?php echo $this->render( 'config:layouts/fields/apsies/uni_apuntes', array( 'config' => $config ) ) ?>	
	    </li>
		<li>
	    	
			<?php echo $this->render( 'config:layouts/fields/apsies/uni_powerpoint', array( 'config' => $config ) ) ?>	
	    </li>
		<li>
	    	
			<?php echo $this->render( 'config:layouts/fields/apsies/uni_exposicion', array( 'config' => $config ) ) ?>	
	    </li>
	</ul>

</div>
