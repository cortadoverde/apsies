<h2>Valores por defecto</h2>
<div class="uk-form uk-form-horizontal">
	<div class="uk-grid">
	 
	    <div class="uk-width-1-2">
	    	<div class="uk-form-row">
	            <label class="uk-form-label" for="form-h-it">Tiempos de entrega</label>

	            <div class="uk-form-controls">
	                <?php echo $this['forms']->tiempos_entrega($this['config']['eso']['critica']['tiempo_entrega'],'eso[critica][tiempo_entrega]'); ?>
	            </div>
	        </div>
	    </div>
	</div>
</div>

<table class="uk-table">
   
    <thead>
        <tr>
            <th>Asignatura</th>
            <th>Seleccionado</th>
            <th>Activo</th>
        </tr>
    </thead>

    <tbody>
    	<?php foreach ($this['forms']->partes as $name => $data): 
    			$checked_active 	= isset( $this['config']['eso']['critica'][$name]['active'] ) ? ' checked="checked" ' : ''; 
    			$checked_selected 	= isset( $this['config']['eso']['critica'][$name]['selected'] ) ? ' checked="checked" ' : ''; 
    	?>
    		<tr>
    			<td><?php echo $data['name']?></td>
    			<td><input type="checkbox" name="eso[critica][<?php echo $name?>][selected]" value="1" <?php echo $checked_selected ?> ></td>
    			<td><input type="checkbox" name="eso[critica][<?php echo $name?>][active]" value="1" <?php echo $checked_active ?> ></td>
    		</tr>
    	<?php endforeach ?>
            
    </tbody>
</table>

