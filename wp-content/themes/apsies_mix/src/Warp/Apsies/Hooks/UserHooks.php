<?php
/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies\Hooks;


use Warp\Warp;


class UserHooks {
	
	public function __construct()
    {
    	$this->runHooks();
    	
    }

    public function runHooks()
    {
    	add_action( 'user_register', array( $this, 'register' ) );
    }

   
    public function register( $user_id )
    {
    	//if ( !empty( $_POST['colegio'] ) )
		update_user_meta( $user_id, 'colegio', $_POST['colegio'] );
		//if ( !empty( $_POST['universidad'] ) )
		update_user_meta( $user_id, 'universidad', $_POST['universidad'] );
		//if ( !empty( $_POST['ciudad'] ) )
		update_user_meta( $user_id, 'ciudad', $_POST['ciudad'] );
		//if ( !empty( $_POST['edad'] ) )
		update_user_meta( $user_id, 'edad', $_POST['edad'] );
		//if ( !empty( $_POST['nivel'] ) )
		update_user_meta( $user_id, 'nivel', $_POST['nivel'] );

		update_user_meta( $user_id, 'fechasuscripcion', 0 );
		update_user_meta( $user_id, 'diassuscripcion', 0 );
		update_user_meta( $user_id, 'puntosusuario', 0 );
    }

}
