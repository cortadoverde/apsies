<?php
/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies\Hooks;


use Warp\Warp;


class SiteHooks {
	
	public function __construct()
    {
    	$this->runHooks();
    	
    }

    public function runHooks()
    {
    	add_action('get_header_apsies',  array( $this, 'checkMaitenanceMode' ) );
        add_filter( 'body_class', array( $this, 'body_class' ) );
        add_filter( 'warp_content', array( $this, 'warp_content' ) );
    }

    public function checkMaitenanceMode()
    {
    	global $warp;
    	if( isset ( $warp['config']['site_mode_down'] ) ) {
    		if ( !current_user_can( 'edit_themes' ) || !is_user_logged_in() ) {

    			echo $warp['template']->render('404');
    			die;
    		}
    	}
    }

    public function body_class( $classes )
    {
        global $post;
        if ( isset( $post ) ) {
            $classes[] = $post->post_type . '-' . $post->post_name;
        }
        return $classes;
    }

    public function warp_content( $content )
    {
        return apply_filters('the_content', $content );
    }


}
