<?php
/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies\Controller;

class Controller
{
	public $system;

	public function __construct()
	{
		global $warp;

		$this->system = $warp;
	}
}