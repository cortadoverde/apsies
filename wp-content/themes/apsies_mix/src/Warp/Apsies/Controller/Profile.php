<?php
/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies\Controller;

class Profile extends Controller
{

	public function update()
	{
		$allow = array(
			'user' => array( 'first_name', 'last_name', 'city', 'level', 'age', 'institute', 'grade', 'career')
		);

		if( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
			if( isset( $_POST['user'] ) ) {
				foreach( $_POST['user'] AS $key => $value ) {
					if( in_array( $key, $allow['user'] ) ) {
						echo $key;
						$this->system['user']->me->update($key, $value);
					}
				}
			}
		}
	}

}