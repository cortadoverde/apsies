<?php
/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies\Controller;

class Config extends Controller
{

	public function index()
	{
		return array( 
			'eso' => $this->system['config']['eso'],
			'uni' => $this->system['config']['uni']
		);
	}
}