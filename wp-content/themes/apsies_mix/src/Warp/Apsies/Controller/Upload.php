<?php
/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies\Controller;

use Warp\Apsies\Wp\Upload AS WpUpload;

class Upload extends Controller
{

	public $data = array();

	public function tmp_file()
	{
		$base = dirname(dirname(dirname(dirname(dirname(dirname(dirname(__DIR__)))))));
		// Required WordPress files and hooks
		require_once( $base . "/wp-load.php" );
		require_once( $base . "/wp-admin/includes/media.php" );
		require_once( $base . "/wp-admin/includes/file.php" );
		require_once( $base . "/wp-admin/includes/image.php" );
		
		$upload_dir = wp_upload_dir();

		new WpUpload(array(
		    'upload_dir' => $upload_dir['path'].'/',
		    'upload_url' => $upload_dir['url'] . '/'

		));
	}

}