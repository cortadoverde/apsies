<?php
/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies\Shortcodes;


use Warp\Warp;


class UiShortcodes {
	
	public function __construct()
    {
    	$this->register();
    }

    public function register()
    {
    	add_shortcode( 'faq', array( $this, 'faq' ) );
      add_shortcode( 'listFaq', array( $this, 'listFaq' ) );
      add_shortcode( 'grid', array( $this, 'grid' ) );
      add_shortcode( 'column', array( $this, 'column' ) );
      add_shortcode( 'button', array( $this, 'button' ) );
      add_shortcode( 'video', array( $this, 'video' ) );
    	add_shortcode( 'icon', array( $this, 'icon' ) );
    }
   

   	public function faq( $attrs, $answer )
   	{
   		$id = 'id_' . time() . rand(0,900);

   		$html = '<dt data-uk-toggle="{target:\'#' . $id . '\'}">' . $attrs['pregunta'] .'</dt>';
   		$html .= '<dd class="answer uk-hidden" id="' . $id . '">' . do_shortcode($answer) . '</dd>';
   		return $html;
   	}

   	public function listFaq( $attrs, $content )
   	{

   		$html 	= '<dl class="definition">' 
   			  	. ( ( isset( $attrs['title'] ) ) ? '<h4>' . $attrs['title'] . '</h4>' : '' )
   			  	. do_shortcode( $content ) . '</dl>';
   		return $html;
   	}

    public function grid( $attrs, $content )
    {
        $html = '<div class="uk-grid ' 
              . ( 
                  ( isset( $attrs['small'] ) ) ? 'uk-grid-small' : '' 
                ) 
              . ( 
                  ( isset( $attrs['class'] ) ) ? ' ' . $attrs['class'] . ' ' : '' 
                ) 
              . '">'
                    . do_shortcode( $content )

              . '</div>';
        return $html;
    }

    public function column( $attrs, $content )
    {
        $attrs = array_merge( array(
            'phone' => 'same',
            'tablet' => 'same',
            'large' => 'same',
            'size'  => '10-10'
        ), $attrs );

        $html = '<div class="uk-width-' . $attrs['size']
              . ( ( $attrs['phone'] !== 'same' ) ? ' uk-width-small-' . $attrs['phone'] : '' )
              . ( ( $attrs['tablet'] !== 'same' ) ? ' uk-width-medium-' . $attrs['tablet'] : '' )
              . ( ( $attrs['large'] !== 'same' ) ? ' uk-width-large-' . $attrs['large'] : '' )
              . ( ( isset ( $attrs['center'] ) ) ? ' uk-container-center ' : '' )
              . ( 
                  ( isset( $attrs['class'] ) ) ? ' ' . $attrs['class'] . ' ' : '' 
                ) 
              . '">' 
              . do_shortcode( $content ) 

              . '</div>';
        return $html;
    }

    public function button( $attrs, $content )
    {
      $attrs = shortcode_atts( array(
        'class' => 'uk-button-large uk-button-success',
        'action' => 'link'
      ), $attrs );  

      $callbackResult = $this->_button_callback( $attrs['action'], $attrs );



      $html .= '<'  . $callbackResult['element'] .' ' 
                    . $this->_parseAttrs( $callbackResult )  . ' ' 
                    . ( 
                        ( isset ( $callbackResult['attrs']['class'] ) ) ? '' : ' class="uk-button ' . $attrs['class'] . '"' 
                      )

                    .'>' . do_shortcode( $content ) . '</' . $callbackResult['element'] .'>';
      return $html;
    }

    public function icon( $attrs )
    {
      $attrs = shortcode_atts(array(
          'icon' => 'ban',
          'size' => ''
      ), $attrs );

      
      $class = 'uk-icon-'. $attrs['icon'] . ' ' . ( ( $attrs['size'] !== '' ) ? 'uk-icon-' . str_replace('uk-icon-', '', $attrs['size'] ) : '' );
      return $html = '<i class="' . $class .'"></i>'; 
    }

    public function video( $attrs, $content )
    {
      $html = '<div class="videoWrapper">
                  <!-- Copy & Pasted from YouTube -->
                  <iframe width="560" height="349" src="http://www.youtube.com/embed/'. $attrs['id'] .'?rel=0&hd=1" frameborder="0" allowfullscreen></iframe>
              </div>';
      return $html;
    }

    private function _parseAttrs( $result )
    {
      $return = '';
      if( isset( $result['attrs'] ) ) {
        foreach( $result['attrs'] as $attr => $value ) {
          $return .= ' ' . $attr . '="' . $value .'" ';
        }
      }

      return $return;
    }

    private function _button_callback( $action, $attrs )
    {
      switch ($action) {
        case 'link':
          $val =   
                array(
                  'element' => 'a',
                  'attrs'   => array(
                      'href' => ( isset( $attrs['url'] ) ? $attrs['url'] : '#' )
                  )
                );

          break;

        case 'registro' : 


          break;
                    
        default:
            $val =   
                 array(
                  'element' => 'button'
                );
          break;
      }

      return $val;
    }

}
