<?php

/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies;

class Dispatch 
{
	private static $_default = array(
		'controller' => 'Controller\Config',
		'action'	 => 'Index',
		'args'		 => null
	);

	protected static $controller = 'Controller\Config';

	protected static $action = 'Index';

	protected static $args = array();

	public function run( )
	{
		self::$controller = ( isset ( $_GET['controller'] ) ) ? $_GET['controller'] : self::$_default['controller'];
		self::$action     = ( isset ( $_GET['action'] ) ) ? $_GET['action'] : self::$_default['action'];

		if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
			self::$controller = ( isset ( $_POST['controller'] ) ) ? $_POST['controller'] : self::$controller;
			self::$action     = ( isset ( $_POST['action'] ) ) ? $_POST['action'] : self::$action;
			self::$args       = array_diff_key( $_POST, array_flip( array( 'controller', 'action' ) ) ); 
		}

		if( ($pos = stripos( 'Controller\\', self::$controller ) ) === false ) {
			self::$controller = 'Controller\\' . ucfirst( strtolower(self::$controller) );
		}	

		self::$controller = __NAMESPACE__ . '\\' . rtrim(self::$controller, '\\');

		return self::call( self::$controller, self::$action, self::$args );
	}

	static private function _checkReflection( $namespace )
    {
        try {
            $r = new \ReflectionClass($namespace);
            return $r;
        } catch (\ReflectionException $e) {
            echo $e->getMessage();
            return false;
        }
        return false;
    }

    static public function call( $ns, $action, $args = array() ) 
    {
        $return = NULL;

        if ( ( $r = self::_checkReflection( $ns ) ) === false ) {
           
            return false;
        }

        $instance = $r->newInstance();

        // Hook before action
        if( $r->hasMethod('before_action') ){
            $method = $r->getMethod( 'before_action' );
            $method->invoke($instance);
        }

        if( $r->hasMethod($action) ) {
            $method = $r->getMethod( $action );
            try {
                if( empty( $args ) ) {
                    $return = $method->invoke($instance);
                } else {
                    $return = $method->invokeArgs($instance, $args);
                }    
            } catch (\Exception $e) {
                throw new \Exception(' DIspatch Error:: ' . $e->getMessage() );
            }
            
        } else {
           if( $r->hasMethod('_global') ) {
                $global_method = $r->getMethod( '_global' );
                $return = empty( $args ) ? $global_method->invoke( $instance ) : $global_method->invokeArgs( $instance, $args);
           }  
        }

        // Hook before action
        if( $r->hasMethod('after_action') ){
            $method = $r->getMethod( 'after_action' );
            $method->invoke($instance);
        }

        return $return;
    }
}