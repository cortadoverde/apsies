<?php
/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies\Helper;

use Warp\Helper\AbstractHelper;
use Warp\Warp;
use Warp\Apsies\Hooks;
use Warp\Apsies\Shortcodes;

/**
 * Option helper class, store option data.
 */
class ApsiesHelper extends AbstractHelper
{

    public function __construct(Warp $warp)
    {
        parent::__construct($warp);

        // set prefix
        $this->prefix = basename($this['path']->path('theme:'));
    }

    public function init()
    {
        $this->hooks['user'] = new Hooks\UserHooks();
        $this->hooks['site'] = new Hooks\SiteHooks();
        $this->shortCodes['Ui'] = new Shortcodes\UiShortcodes();
    }

}
