<?php
/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies\Helper;

use Warp\Helper\AbstractHelper;
use Warp\Warp;

/**
 * Option helper class, store option data.
 */
class FormHelper extends AbstractHelper
{
    /**
     * Option prefix
     *
     * @var string
     */
    protected $prefix;

    public $asignaturas = array(
        'matematicas' => array(
            'id' => 1,
            'name' => 'Matemáticas'
        ),

        'lengua_castellana' => array(
            'id' => 2,
            'name'=> 'Lengua Castellana y Literatura'
        ),

        'fisica' => array(
            'id' => 3,
            'name' => 'Física'
        ),

        'quimica' => array(
            'id' => 4,
            'name' => 'Química'
        ),

        'ingles' => array(
            'id' => 5,
            'name' => 'Inglés'
        ),

        'frances' => array(
            'id' => 6,
            'name' => 'Francés'
        ),

        'dibujo_tecnico' => array(
            'id' => 7,
            'name' => 'Dibujo Técnico'
        ),

        'geografia' => array(
            'id' => 8,
            'name' => 'Geografía'
        ),

        'arte' => array(
            'id' => 9,
            'name' => 'Arte'
        ),

        'informatica' => array(
            'id' => 10,
            'name' => 'Informática'
        ),

        'economia' => array(
            'id' => 11,
            'name' => 'Economía'
        ),

        'aleman' => array(
            'id' => 12,
            'name' => 'Alemán'
        ),

        'filosofia' => array(
            'id' => 13,
            'name' => 'Filosofía'
        ),

        'latin_griego' => array(
            'id' => 14,
            'name' => 'Latín / Griego'
        ),

        'ciencias_sociales' => array(
            'id' => 15,
            'name' => 'Ciencias Sociales'
        ),

        'historia' => array(
            'id' => 16,
            'name' => 'Historia'
        )
    );

    public $asignaturas_uni = array(
        'uni_1' => array(
            'id' => 1,
            'name' => 'Aeronáutica'
        ),
        'uni_2' => array(
            'id' => 2,
            'name' => 'Agronomía'
        ),
        'uni_3' => array(
            'id' => 3,
            'name' => 'Antropología'
        ),
        'uni_4' => array(
            'id' => 4,
            'name' => 'Arqueología'
        ),
        'uni_5' => array(
            'id' => 5,
            'name' => 'Arquitectura'
        ),
        'uni_6' => array(
            'id' => 6,
            'name' => 'Arte'
        ),
        'uni_7' => array(
            'id' => 7,
            'name' => 'Astrología'
        ),
        'uni_8' => array(
            'id' => 8,
            'name' => 'Astronomía'
        ),
        'uni_9' => array(
            'id' => 9,
            'name' => 'Automoción'
        ),
        'uni_10' => array(
            'id' => 10,
            'name' => 'Aviónica'
        ),
        'uni_11' => array(
            'id' => 11,
            'name' => 'Biblioteconomía'
        ),
        'uni_12' => array(
            'id' => 12,
            'name' => 'Biografía'
        ),
        'uni_13' => array(
            'id' => 13,
            'name' => 'Biología'
        ),
        'uni_14' => array(
            'id' => 14,
            'name' => 'Bioquímica'
        ),
        'uni_15' => array(
            'id' => 15,
            'name' => 'Botánica'
        ),
        'uni_16' => array(
            'id' => 16,
            'name' => 'Contabilidad'
        ),
        'uni_17' => array(
            'id' => 17,
            'name' => 'Deporte y Ciencias de la Actividad Física'
        ),
        'uni_18' => array(
            'id' => 18,
            'name' => 'Derecho'
        ),
        'uni_19' => array(
            'id' => 19,
            'name' => 'Documentación'
        ),
        'uni_20' => array(
            'id' => 20,
            'name' => 'Ecología y Medio Ambiente'
        ),
        'uni_21' => array(
            'id' => 21,
            'name' => 'Economía y Empresa'
        ),
        'uni_22' => array(
            'id' => 22,
            'name' => 'Educación Física'
        ),
        'uni_23' => array(
            'id' => 23,
            'name' => 'Educación y Pedagogía'
        ),
        'uni_24' => array(
            'id' => 24,
            'name' => 'Electrónica y Electricidad'
        ),
        'uni_25' => array(
            'id' => 25,
            'name' => 'Enfermería'
        ),
        'uni_26' => array(
            'id' => 26,
            'name' => 'Estadística'
        ),
        'uni_27' => array(
            'id' => 27,
            'name' => 'Ética y Moral'
        ),
        'uni_28' => array(
            'id' => 28,
            'name' => 'Farmacia'
        ),
        'uni_29' => array(
            'id' => 29,
            'name' => 'Filología'
        ),
        'uni_30' => array(
            'id' => 30,
            'name' => 'Filosofía'
        ),
        'uni_31' => array(
            'id' => 31,
            'name' => 'Física'
        ),
        'uni_32' => array(
            'id' => 32,
            'name' => 'Gastronomía y Restauración'
        ),
        'uni_33' => array(
            'id' => 33,
            'name' => 'Genética'
        ),
        'uni_34' => array(
            'id' => 34,
            'name' => 'Geografía'
        ),
        'uni_35' => array(
            'id' => 35,
            'name' => 'Geología'
        ),
        'uni_36' => array(
            'id' => 36,
            'name' => 'Historia'
        ),
        'uni_37' => array(
            'id' => 37,
            'name' => 'Hostelería'
        ),
        'uni_38' => array(
            'id' => 38,
            'name' => 'Imagen y Audiovisuales'
        ),
        'uni_39' => array(
            'id' => 39,
            'name' => 'Industria'
        ),
        'uni_40' => array(
            'id' => 40,
            'name' => 'Informática'
        ),
        'uni_41' => array(
            'id' => 41,
            'name' => 'Ingeniería'
        ),
        'uni_42' => array(
            'id' => 42,
            'name' => 'Juegos y Animación'
        ),
        'uni_43' => array(
            'id' => 43,
            'name' => 'Lenguaje y Gramática'
        ),
        'uni_44' => array(
            'id' => 44,
            'name' => 'Literatura'
        ),
        'uni_45' => array(
            'id' => 45,
            'name' => 'Matemáticas'
        ),
        'uni_46' => array(
            'id' => 46,
            'name' => 'Materiales'
        ),
        'uni_47' => array(
            'id' => 47,
            'name' => 'Mitología'
        ),
        'uni_48' => array(
            'id' => 48,
            'name' => 'Música'
        ),
        'uni_49' => array(
            'id' => 49,
            'name' => 'Naútica/Naval'
        ),
        'uni_50' => array(
            'id' => 50,
            'name' => 'Nutrición y Dietética'
        ),
        'uni_51' => array(
            'id' => 51,
            'name' => 'Obras y Construcción'
        ),
        'uni_52' => array(
            'id' => 52,
            'name' => 'óptica y Optometría'
        ),
        'uni_53' => array(
            'id' => 53,
            'name' => 'Organización de Empresas'
        ),
        'uni_54' => array(
            'id' => 54,
            'name' => 'Periodismo'
        ),
        'uni_55' => array(
            'id' => 55,
            'name' => 'Política y Administración Pública'
        ),
        'uni_56' => array(
            'id' => 56,
            'name' => 'Psicología'
        ),
        'uni_57' => array(
            'id' => 57,
            'name' => 'Publicidad y Marketing'
        ),
        'uni_58' => array(
            'id' => 58,
            'name' => 'Química'
        ),
        'uni_59' => array(
            'id' => 59,
            'name' => 'Química'
        ),
        'uni_60' => array(
            'id' => 60,
            'name' => 'Recursos Forestales'
        ),
        'uni_61' => array(
            'id' => 61,
            'name' => 'Recursos Humanos'
        ),
        'uni_62' => array(
            'id' => 62,
            'name' => 'Relaciones Laborales'
        ),
        'uni_63' => array(
            'id' => 63,
            'name' => 'Relaciones Públicas'
        ),
        'uni_64' => array(
            'id' => 64,
            'name' => 'Religión y Creencias'
        ),
        'uni_65' => array(
            'id' => 65,
            'name' => 'Salud'
        ),
        'uni_66' => array(
            'id' => 66,
            'name' => 'Sociología y Trabajo Social'
        ),
        'uni_67' => array(
            'id' => 67,
            'name' => 'Sonido'
        ),
        'uni_68' => array(
            'id' => 68,
            'name' => 'Tecnología'
        ),
        'uni_69' => array(
            'id' => 69,
            'name' => 'Telecomunicaciones'
        ),
        'uni_70' => array(
            'id' => 70,
            'name' => 'Termodinámica'
        ),
        'uni_71' => array(
            'id' => 71,
            'name' => 'Topografía'
        ),
        'uni_72' => array(
            'id' => 72,
            'name' => 'Traducción e Interpretación'
        ),
        'uni_73' => array(
            'id' => 73,
            'name' => 'Turismo'
        ),
        'uni_74' => array(
            'id' => 74,
            'name' => 'Zoología'
        )

    );
    
    public $partes = array(
        //
        'argumento' => array(
            'id' => 1,
            'name' => 'Argumento'
        ),
        //
        'personajes_principales' => array(
            'id' => 2,
            'name' => 'Personajes principales'
        ),
        //
        'personajes_secundarios' => array(
            'id' => 3,
            'name' => 'Personajes secundarios'
        ),
        //
        'estilo' => array(
            'id' => 4,
            'name' => 'Estilo'
        ),
        //
        'estructura' => array(
            'id' => 5,
            'name' => 'Estructura'
        ),
        //
        'conclusion' => array(
            'id' => 6,
            'name' => 'Conclusión'
        )
    );

    public $autocad = array(
        //
        'Ejercicios' => array(
            'id' => 1,
            'name' => 'Ejercicios'
        ),

        //
        'piezas' => array(
            'id' => 2,
            'name' => 'Piezas'
        )
    );


    /**
     * Class constructor.
     *
     * @param Warp $warp
     */
    public function __construct(Warp $warp)
    {
        parent::__construct($warp);

        // set prefix
        $this->prefix = basename($this['path']->path('theme:'));
    }


    public function tiempos_entrega( $default = 0, $input_name = '', $_default = '_default' )
    {
        $_values = array(
            array(
                'value' => 0,
                'name' => 'Seleccione tiempo de entrega',
            ),
            array(
                'value' => 2,
                'name' => '2 horas'
            ),
            array(
                'value' => 5,
                'name' => '5 horas'
            ),
            array(
                'value' => 8,
                'name' => '8 horas'
            ),
            array(
                'value' => 24,
                'name' => '1 día'
            ),
            array(
                'value' => 48,
                'name' => '2 dias'
            ),
            array(
                'value' => 72,
                'name' => '3 dias'
            ),
            array(
                'value' => 96,
                'name' => '4 dias'
            ),
            array(
                'value' => 120,
                'name' => '5 dias'
            ),
            array(
                'value' => 144,
                'name' => '6 dias'
            ),
            array(
                'value' => 150,
                'name' => '+6 dias'
            )
        );

        $autocad = array(
            array(
                'value' => 24,
                'name' => '24 horas'
            ),
            array(
                'value' => 48,
                'name' => '2 dias'
            ),
            array(
                'value' => 72,
                'name' => '3 dias'
            ),
            array(
                'value' => 96,
                'name' => '4 dias'
            ),
            array(
                'value' => 120,
                'name' => '5 dias'
            ),
            array(
                'value' => 144,
                'name' => '6 dias'
            ),
            array(
                'value' => 168,
                'name' => '7 dias'
            ),
            array(
                'value' => 192,
                'name' => '8 dias'
            ),
            array(
                'value' => 216,
                'name' => '9 dias'
            ),
            array(
                'value' => 240,
                'name' => '10 dias'
            ),
            array(
                'value' => 264,
                'name' => '11 dias'
            ),
            array(
                'value' => 288,
                'name' => '12 dias'
            ),
            array(
                'value' => 312,
                'name' => '13 dias'
            ),
            array(
                'value' => 336,
                'name' => '14 dias'
            ),
            array(
                'value' => 360,
                'name' => '15 dias'
            ),
            array(
                'value' => 384,
                'name' => '16 dias'
            ),
            array(
                'value' => 408,
                'name' => '17 dias'
            ),
            array(
                'value' => 432,
                'name' => '18 dias'
            ),
            array(
                'value' => 456,
                'name' => '19 dias'
            ),
            array(
                'value' => 480,
                'name' => '20+ dias'
            )
        );

        $uni = array(
            array(
                'value' => 120,
                'name' => '5 dias'
            ),
            array(
                'value' => 240,
                'name' => '10 dias'
            ),
            array(
                'value' => 360,
                'name' => '15 dias'
            ),
            array(
                'value' => 480,
                'name' => '20 dias'
            ),
            array(
                'value' => 500,
                'name' => '20+ dias'
            )
        );

        $horas = array(
            '_default'  => $_values,
            'autocad'   => $autocad,
            'uni'       => $uni
        );

        $values = ( isset( $horas[$_default] ) ) ? $horas[$_default] : $horas['_default'];


        $html = '<p class="uk-form-controls-condensed"><select name='. $input_name.'>';
        foreach( $values as $option )
        {
            $selected = $default == $option['value'] ? 'selected="selected"' : '';

            $html .= '<option value="' . $option['value'] . '" ' . $selected . '>' . $option['name'] . '</option>';
        }
        $html .= '</select></p>';
        return $html;
    }


}
