<?php
/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies\Helper;

use Warp\Helper\AbstractHelper;
use Warp\Warp;

/**
 * Option helper class, store option data.
 */
class UserHelper extends AbstractHelper
{

    private $is_login = false;

    private $user;

    public $me;

    public function __construct(Warp $warp)
    {
        parent::__construct($warp);

        // set prefix
        $this->prefix = basename($this['path']->path('theme:'));
        $this->me = new \Warp\Apsies\Wp\User();
        $this->_checkUser();
    }

    private function _checkUser()
    {
        $uid = get_current_user_id();

        if( $uid ) {


            
            $this->user = get_userdata( $uid );
            
            if( isset( $this->user->data ) ) {
                $this->is_login = true;
                $this->ID = $this->id = $uid;
                $this->_getMeta();
            }

        }

    }

    private function _getMeta()
    {
        if( $this->ID ) {
            $custom = array();
            $um = get_user_meta($this->ID);

            foreach ($um as $key => $value) {
                if( is_array( $value ) && count( $value ) == 1 )
                    $value = $value[0];
                $custom[$key] = maybe_unserialize( $value );
            }
            $this->user->meta = $custom;
        }
    }

    public function setMeta( $key, $value )
    {
        add_user_meta( $this->ID, $key, $value );
    }

    public function deleteMeta( $key )
    {
        delete_user_meta($this->ID, $key );
    }

    public function is_login()
    {
        return $this->is_login;
    }

    public function getData()
    {
        return $this->user;
    }


}
