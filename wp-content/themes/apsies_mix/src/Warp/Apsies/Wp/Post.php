<?php
/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies\Wp;

use Warp\Apsies\Core;

class Post extends Core
{
	public $object_type = 'post';

	public $PostClass = 'Post';

	public $ImageClass = 'Image';

	public $representation = 'Post';


	public $_can_edit;
    public $_custom_imported = false;
    public $_content;
    public $_get_terms;

    public $class;
    public $display_date;
    public $id;
    public $ID;
    public $post_content;
    public $post_date;
    public $post_parent;
    public $post_title;
    public $post_type;
    public $slug;

    public function __construct( $pid = null )
    {
    	if ( $pid === null && get_the_ID() ) {
    		$pid = get_the_ID();
    		$this->ID = $pid;
    	}

    	if( is_numeric ( $pid ) ) {
    		$this->ID = $pid;
    	}

    	$this->PostClass = __NAMESPACE__ . '\\' . $this->PostClass;
    	$this->ImageClass = __NAMESPACE__ . '\\' . $this->ImageClass;

    	$this->init( $pid );
    }

    protected function init ( $pid = false ) 
    {
    	if ( $pid == false ) {
    		$pid = get_the_ID();
    	}

    	$post_info = $this->get_info( $pid );

    	$this->display_date = $this->date();

    	$post_class = $this->post_class();

    	$this->class = $post_class;
    }

    public function get_edit_url() 
    {
    	if ( $this->can_edit() ) {
    		return get_edit_post_link( $this->ID );
    	}
    	return false;
    }

    public function update( $field, $value )
    {
    	if ( isset ( $this->ID ) ) {
    		update_post_meta ( $this->ID, $field, $value );
    		$this->field = $value;
    	}
    }

    private function prepare_post_info($pid = 0) 
    {
        if (is_string($pid) || is_numeric($pid) || (is_object($pid) && !isset($pid->post_title)) || $pid === 0) {
            $pid = self::check_post_id($pid);
            $post = get_post($pid);
            if ($post) {
                return $post;
            } else {
                $post = get_page($pid);
                return $post;
            }
        }
        //we can skip if already is WP_Post
        return $pid;
    }


    private function check_post_id($pid) 
    {
        if( is_object( $pid ) && $pid instanceof \Wp_Post ) {
            return $pid->ID;
        }
        if (is_numeric($pid) && $pid === 0) {
            $pid = get_the_ID();
            return $pid;
        }
        if (!is_numeric($pid) && is_string($pid)) {
            $pid = self::get_post_id_by_name($pid);
            return $pid;
        }
        if (!$pid) {
            return null;
        }
        return $pid;
    }

    public static function get_post_id_by_name($post_name) 
    {
        global $wpdb;
        $query = $wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_name = %s LIMIT 1", $post_name);
        $result = $wpdb->get_row($query);
        if (!$result) {
            return null;
        }
        return $result->ID;
    }

    function get_preview($len = 50, $force = false, $readmore = 'Read More', $strip = true) 
    {
        $text = '';
        $trimmed = false;
        if (isset($this->post_excerpt) && strlen($this->post_excerpt)) {
            if ($force) {
                $text = WpHelper::trim_words($this->post_excerpt, $len, false);
                $trimmed = true;
            } else {
                $text = $this->post_excerpt;
            }
        }
        if (!strlen($text) && strpos($this->post_content, '<!--more-->') !== false) {
            $pieces = explode('<!--more-->', $this->post_content);
            $text = $pieces[0];
            if ($force) {
                $text = WpHelper::trim_words($text, $len, false);
                $trimmed = true;
            }
        }
        if (!strlen($text)) {
            $text = WpHelper::trim_words($this->get_content(), $len, false);
            $trimmed = true;
        }
        if (!strlen(trim($text))) {
            return $text;
        }
        if ($strip) {
            $text = trim(strip_tags($text));
        }
        if (strlen($text)) {
            $text = trim($text);
            $last = $text[strlen($text) - 1];
            if ($last != '.' && $trimmed) {
                $text .= ' &hellip; ';
            }
            if (!$strip) {
                $last_p_tag = strrpos($text, '</p>');
                if ($last_p_tag !== false) {
                    $text = substr($text, 0, $last_p_tag);
                }
                if ($last != '.' && $trimmed) {
                    $text .= ' &hellip; ';
                }
            }

            if ($readmore) {
                $text .= ' <a href="' . $this->get_permalink() . '" class="read-more">' . $readmore . '</a>';
            }
            if (!$strip) {
                $text .= '</p>';
            }
        }
        return $text;
    }

    function import_custom($pid = false) 
    {
        if (!$pid) {
            $pid = $this->ID;
        }
        $customs = $this->get_post_custom($pid);
        $this->import($customs);
    }

    protected function get_post_custom($pid) 
    {
        apply_filters('lionclick_post_get_meta_pre', array(), $pid, $this);
        
        $customs = get_post_custom($pid);
        
        if (!is_array($customs) || empty($customs)) {
            return array();
        }
        foreach ($customs as $key => $value) {
            if (is_array($value) && count($value) == 1 && isset($value[0])) {
                $value = $value[0];
            }
            $customs[$key] = maybe_unserialize($value);
        }
        $customs = apply_filters('lionclick_post_get_meta', $customs, $pid, $this);
        return $customs;
    }

    function get_thumbnail() 
    {
        if (function_exists('get_post_thumbnail_id')) {
            $tid = get_post_thumbnail_id($this->ID);
            if ($tid) {
                return new $this->ImageClass($tid);
            }
        }
        return null;
    }

    function get_permalink() 
    {
        if (isset($this->permalink)) {
            return $this->permalink;
        }
        $this->permalink = get_permalink($this->ID);
        return $this->permalink;
    }

    function get_link() 
    {
        return $this->get_permalink();
    }

    function get_next($by_taxonomy = false) 
    {
        if (!isset($this->_next) || !isset($this->_next[$by_taxonomy])) {
            global $post;
            $this->_next = array();
            $old_global = $post;
            $post = $this;
            if ($by_taxonomy == 'category' || $by_taxonomy == 'categories') {
                $in_same_cat = true;
            } else {
                $in_same_cat = false;
            }
            $adjacent = get_adjacent_post($in_same_cat, '', false);
            if ($adjacent) {
                $this->_next[$by_taxonomy] = new $this->PostClass($adjacent);
            } else {
                $this->_next[$by_taxonomy] = false;
            }
            $post = $old_global;
        }
        return $this->_next[$by_taxonomy];
    }

    public function pagination() 
    {
       // return function(){
            global $wp_query, $wp_rewrite;

            
            if( $wp_query->is_single() ) {
                return 'nop';
            }
            
            $pages = '';
            
            $max = $wp_query->max_num_pages;
            
            if (!$current = get_query_var('paged')) $current = 1;
            $a['base'] = str_replace(999999999, '%#%', get_pagenum_link(999999999));
            $a['total'] = $max;
            $a['current'] = $current;

            $total = 1; //1 – muestra el texto "Página N de N", 0 – para no mostrar nada
            $a['mid_size'] = 5; //cuantos enlaces a mostrar a izquierda y derecha del actual
            $a['end_size'] = 1; //cuantos enlaces mostrar al comienzo y al fin
            $a['prev_text'] = '&laquo; Anterior'; //texto para el enlace "Página siguiente"
            $a['next_text'] = 'Siguiente &raquo;'; //texto para el enlace "Página anterior"


            ob_start();

            if ($max > 1) echo '<div class="navigation">';
           // if ($total == 1 && $max > 1) $pages = '<span class="pages">Página ' . $current . ' de ' . $max . '</span>'."\r\n";
            echo $pages . paginate_links($a);
            if ($max > 1) echo '</div>';

            return ob_get_clean();
       // };
        
    }

    public function get_pagination() 
    {
        global $post, $page, $numpages, $multipage;
        $post = $this;
        $ret = array();
        if ($multipage) {
            for ($i = 1; $i <= $numpages; $i++) {
                $link = self::get_wp_link_page($i);
                $data = array('name' => $i, 'title' => $i, 'text' => $i, 'link' => $link);
                if ($i == $page) {
                    $data['current'] = true;
                }
                $ret['pages'][] = $data;
            }
            $i = $page - 1;
            if ($i) {
                $link = self::get_wp_link_page($i);
                $ret['prev'] = array('link' => $link);
            }
            $i = $page + 1;
            if ($i <= $numpages) {
                $link = self::get_wp_link_page($i);
                $ret['next'] = array('link' => $link);
            }
        }
        return $ret;
    }

    private static function get_wp_link_page($i) 
    {
        $link = _wp_link_page($i);
        $link = new SimpleXMLElement($link . '</a>');
        if (isset($link['href'])) {
            return $link['href'];
        }
        return '';
    }

    function get_prev($by_taxonomy = false) 
    {
        if (!isset($this->_prev) || !isset($this->_prev[$by_taxonomy])) {
            global $post;
            $this->_prev = array();
            $old_global = $post;
            $post = $this;
            if ($by_taxonomy == 'category' || $by_taxonomy == 'categories') {
                $in_same_cat = true;
            } else {
                $in_same_cat = false;
            }
            $adjacent = get_adjacent_post($in_same_cat, '', true);
            if ($adjacent) {
                $this->_prev[$by_taxonomy] = new $this->PostClass($adjacent);
            } else {
                $this->_prev[$by_taxonomy] = false;
            }
            $post = $old_global;
        }
        return $this->_prev[$by_taxonomy];
    }

    function get_parent() 
    {
        if (!$this->post_parent) {
            return false;
        }
        return new $this->PostClass($this->post_parent);
    }

    function get_author() 
    {
        if (isset($this->post_author)) {
            return new User($this->post_author);
        }
        return false;
    }

    function get_modified_author() 
    {
        $user_id = get_post_meta($this->ID, '_edit_last', true);
        return ($user_id ? new User($user_id) : $this->get_author());
    }

    function get_info($pid) 
    {
        $post = $this->prepare_post_info($pid);
        if (!isset($post->post_status)) {
            return null;
        }
        $post->status = $post->post_status;
        $post->id = $post->ID;
        $post->slug = $post->post_name;
        $post->pretty_url = get_permalink( $post->ID, true);
        $customs = $this->get_post_custom($post->ID);
        $post = (object)array_merge((array)$post, (array)$customs);
        return $post;
    }

    function get_display_date($use = 'post_date') 
    {
        return date(get_option('date_format'), strtotime($this->$use));
    }

    function get_date($date_format = '') 
    {
        $df = $date_format ? $date_format : get_option('date_format');
        $the_date = (string)mysql2date($df, $this->post_date);
        return apply_filters('get_the_date', $the_date, $date_format);
    }

    function get_modified_date($date_format = '') 
    {
        $df = $date_format ? $date_format : get_option('date_format');
        $the_time = $this->get_modified_time($df, null, $this->ID, true);
        return apply_filters('get_the_modified_date', $the_time, $date_format);
    }

    function get_modified_time($time_format = '') 
    {
        $tf = $time_format ? $time_format : get_option('time_format');
        $the_time = get_post_modified_time($tf, false, $this->ID, true);
        return apply_filters('get_the_modified_time', $the_time, $time_format);
    }

    function get_children($post_type = 'any', $childPostClass = false) 
    {
        if ($childPostClass == false) {
            $childPostClass = $this->PostClass;
        }
        if ($post_type == 'parent') {
            $post_type = $this->post_type;
        }
        $children = get_children('post_parent=' . $this->ID . '&post_type=' . $post_type . '&numberposts=-1&orderby=menu_order title&order=ASC');
        foreach ($children as &$child) {
            $child = new $childPostClass($child->ID);
        }
        $children = array_values($children);
        return $children;
    }

}