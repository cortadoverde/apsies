<?php
/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies\Wp;

class Pedido extends Post 
{
public static $type = 'Reservation';

    public $session_id = 'paymentId';

    public function __construct( \Wp_Post $post ) {
        
        $this->model  = $post;
        $this->get_meta();
    }

    public static function create( $data ) {

        if( !isset( $data['propiedad'] ) ) return false;
    

        $propiedad = Propiedad::get( $data['propiedad'] );

        
        if(! $propiedad instanceof \Wp_Post ) return false;
        
        
        $inquilino_id = get_current_user_id();

        


        $post = array(
            'post_title' => $propiedad->post_title,
            'post_status' => 'pending',
            'post_type' => self::$type
        );

        $post_id = wp_insert_post( $post );

        add_post_meta($post_id, 'inquilino_id', $inquilino_id );
        add_post_meta($post_id, 'propietario_id', $propiedad->post_author );
        add_post_meta($post_id, 'precio', $propiedad->precio );
        add_post_meta($post_id, 'type', $propiedad->post_type );
        add_post_meta($post_id, 'propiedad', $propiedad->ID );
        add_post_meta($post_id, 'date', $data['date'] );

        $post = get_post( $post_id );
        
        return new self( $post );;
    }

    public function get_meta() {
        $customs = get_post_custom( $this->model->ID );

        if (!is_array($customs) || empty($customs)) {
            return $this;
        }
        foreach ($customs as $key => $value) {
            if (is_array($value) && count($value) == 1 && isset($value[0])) {
                $value = $value[0];
            }
            $customs[$key] = maybe_unserialize($value);
            
            $this->model->{$key} = $customs[$key];
            
        }


        $this->model->custom = $customs;
        return $this;
    }

    public static function getApiContext() 
    {

        // ### Api context
        // Use an ApiContext object to authenticate 
        // API calls. The clientId and clientSecret for the 
        // OAuthTokenCredential class can be retrieved from 
        // developer.paypal.com

        $apiContext = new ApiContext(
            new OAuthTokenCredential(
                'AXasShAhr2bXChEKD_JRPqmY-fYAQd2xp8lm2zioXQstHHgUtrOCTwA9SpxO',
                'ELlGYRBQek0OpLbl2zE9TPY9w7AjTEHGqg76cqTdxLgvw0TumSKwjrXSkh5a'
            )
        );



        // #### SDK configuration

        // Comment this line out and uncomment the PP_CONFIG_PATH
        // 'define' block if you want to use static file 
        // based configuration

        $apiContext->setConfig(
            array(
                'mode' => 'sandbox',
                'http.ConnectionTimeOut' => 30,
                'log.LogEnabled' => true,
                'log.FileName' => __DIR__ . '/../../api/PayPal.log',
                'log.LogLevel' => 'FINE'
            )
        );

        return $apiContext;
    }

    public function update( $meta, $value )
    {
        update_post_meta( $this->model->ID, $meta, $value );
        $this->get_meta();
        return $this;
    }

    public function add_meta( $meta, $value )
    {
        add_post_meta( $this->model->ID, $meta, $value );
        $this->get_meta();
        return $this;
    }

    function getParent() {
        if( $this->model->type == 'piso' ) return $this->model->propiedad;
        return Propiedad::get( $this->model->propiedad )->habitacion_piso_id ;
    }

    public function transaction()
    {
        
        // Setear el tipo de pago
        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        $item = new Item();
        $item->setName( $this->model->post_title )
            ->setCurrency('EUR')
            ->setQuantity(1)
            ->setPrice(sprintf("%0.2f", $this->model->precio ) );

        $itemList = new ItemList();
        $itemList->setItems( array( $item ) );

        $amount = new Amount();                        
        $amount->setCurrency('EUR');        
        $amount->setTotal( sprintf("%0.2f", $this->model->precio ) ); 

        $transaction = new Transaction();
        $transaction->setAmount($amount)
                    ->setItemList($itemList)
                    ->setDescription( "Reserva " . $this->model->type . ': ' . $this->model->post_title );

        $baseUrl = get_stylesheet_directory_uri();
        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl("$baseUrl/api/reservas/{$this->model->ID}/success")
                     ->setCancelUrl("$baseUrl/api/reservas/{$this->model->ID}/cancel");

        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

        try {
            $payment->create(self::getApiContext());
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            echo "Exception: " . $ex->getMessage() . PHP_EOL;
            var_dump($ex->getData());   
            exit(1);
        }

        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirectUrl = $link->getHref();
                break;
            }
        }

        // ### Redirect buyer to PayPal website
        // Save the payment id so that you can 'complete' the payment
        // once the buyer approves the payment and is redirected
        // back to your website.
        //
        // It is not a great idea to store the payment id
        // in the session. In a real world app, you may want to 
        // store the payment id in a database.
        $_SESSION[$this->session_id] = $payment->getId();
        
        $this->update( 'payment_id', $payment->getId() );

        if(isset($redirectUrl)) {
            return $redirectUrl;
        }

        return false;

    }

    public function execute()
    {
        $apiContext = self::getApiContext();
        $payment = Payment::get($this->model->payment_id,  $apiContext);
        $paymentExecution = new PaymentExecution();
        $paymentExecution->setPayer_id($this->model->payer_id);
        $payment->execute($paymentExecution, $apiContext );
        return $this;
    }

    public static function check_status( $payment_id ) {
        //$payment = Payment::get( $payment_id, self::getApiContext() );
        $payment = Payment::get($payment_id, self::getApiContext() );//Payment::all(array('count' => 10, 'start_index' => 0), self::getApiContext());//Payment::get( 
        print_r( $payment->transactions[0] );
        die;
        //return $payment;
    }


    public function refund()
    {
        $apiContext = self::getApiContext();
        $payment = Payment::get($this->model->payment_id,  $apiContext);
        $sale_id = $payment->transactions[0]->related_resources[0]->getSale()->getId();

        $amt = $payment->transactions[0]->amount;

        $refund = new Refund();
        $refund->setAmount($amt);
        $refund = $sale->refund($refund, $apiContext);

        return $this;
    }    
}