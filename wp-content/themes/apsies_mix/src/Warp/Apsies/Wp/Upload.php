<?php
/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies\Wp;

use Warp\Apsies\Helper\UploadHandlerHelper;

class Upload extends UploadHandlerHelper
{

    protected $options;

    protected function handle_file_upload($uploaded_file, $name, $size, $type, $error,
        $index = null, $content_range = null) {

    	$user = new User();
    	$logged_in_user = $user->ID;

        $file = new \stdClass();
        $file->name = $this->get_file_name($uploaded_file, $name, $size, $type, $error,
            $index, $content_range);
        $file->size = $this->fix_integer_overflow(intval($size));
        $file->type = $type;
        if ($this->validate($uploaded_file, $file, $error, $index)) {
            $this->handle_form_data($file, $index);
            $upload_dir = $this->get_upload_path();
            if (!is_dir($upload_dir)) {
                mkdir($upload_dir, $this->options['mkdir_mode'], true);
            }
            $file_path = $this->get_upload_path($file->name);
            $append_file = $content_range && is_file($file_path) &&
                $file->size > $this->get_file_size($file_path);
            if ($uploaded_file && is_uploaded_file($uploaded_file)) {
                // multipart/formdata uploads (POST method uploads)
                if ($append_file) {
                    file_put_contents(
                        $file_path,
                        fopen($uploaded_file, 'r'),
                        FILE_APPEND
                    );
                } else {
                    move_uploaded_file($uploaded_file, $file_path);
                }
            } else {
                // Non-multipart uploads (PUT method support)
                file_put_contents(
                    $file_path,
                    fopen('php://input', 'r'),
                    $append_file ? FILE_APPEND : 0
                );
            }
            $file_size = $this->get_file_size($file_path, $append_file);
            if ($file_size === $file->size) {
                $file->url = $this->get_download_url($file->name);
                if ($this->is_valid_image_file($file_path)) {
                    $this->handle_image_file($file_path, $file);
                }
            } else {
                $file->size = $file_size;
                if (!$content_range && $this->options['discard_aborted_uploads']) {
                    unlink($file_path);
                    $file->error = $this->get_error_message('abort');
                }
            }
            $this->set_additional_file_properties($file);
        }

        $attachment = array(
	        'post_mime_type'    => $file->type,
	        'post_title'        => preg_replace( '/\.[^.]+$/', '', basename( $file_path )),
	        'post_content'      => '',
	        'post_author'       => $logged_in_user,
	        'post_status'       => 'inherit',
	        'post_type'         => 'attachment',
	        'guid'              => $this->options['upload_url'] . $name
	    );

	    $attachment_id = wp_insert_attachment( $attachment, $file_path );

	    // Generate the attachment data
	    $attachment_data = wp_generate_attachment_metadata( $attachment_id, $file_path );

	    // Update the attachment metadata
	    wp_update_attachment_metadata( $attachment_id, $attachment_data );

	    $file->id = $attachment_id;

	    return $file;
    }

}