<?php
/**
* @package   Apsies
* @author    Pablo Samudia
* @copyright Copyright (C) Apsies
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

namespace Warp\Apsies\Wp;

use Warp\Apsies\Core;

class User extends Core 
{
    public $object_type = 'user';

    public static $representation = 'user';

    public $link;

    public $display_name;

    public $id;

    public $name;

    public $user_name;

    public function __construct( $uid = false )
    {
        $this->init( $uid );
    }

    public function __toString()
    {
        $name = $this->name();
        if ( strlen( $name ) ) {
            return $name;
        }

        if( strlen( $this->name ) ) {
            return $this->name;
        }

        return '';
    }

    public function get_meta( $field_name )
    {
        $value = null;
        $value = apply_filters( 'apsies_user_get_meta_field_pre', $value, $this->ID, $field_name, $this );
        if ( $value === null ) {
            $value = get_post_meta( $this->ID, $field_name, true );
        } 
        $value = apply_filters( 'apsies_user_get_meta_field', $value, $this->ID, $field_name, $this );
        
        return $value;
    }

    public function __set( $field, $value )
    {
        if( $field == 'name' ) {
            $this->display_name = $value;
        }
        $this->$field = $value;
    }

    public function get_link()
    {
        if( ! $this->_link )
            $this->_link = get_author_posts_url( $this->ID );
        return $this->_link;
    }


    public function init( $uid = false )
    {
        $this->is_login = false;
        
        if( $uid === false ) $uid = get_current_user_id();

        if( $uid ) {
            $data = get_userdata( $uid );

            if( is_object( $data ) && isset( $data->data ) ) {
                $this->import( $data);
            }
            $this->is_login = true;
            $this->ID = $this->id = $uid;
            $this->name = $this->name();
            $this->import_custom();

            if( isset( $this->image ) && is_numeric( $this->image ) ) {
                $this->image = new Image( $this->image );
            }
        }
        
    }

    public static function show_admin_bar()
    {
        $user = new User();
        
        return $user->is_admin();
    }

    public function is_admin() {
        if( $this->ID > 0 && isset ( $this->caps ) ) {
            return isset( $this->caps['administrator'] ) && $this->caps['administrator'] == 1 ;
        }
        return false;
    }


    public function get_meta_field( $field_name )
    {
        $value = null;
        $value = apply_filters( 'apsies_user_get_meta_field_pre', $value, $this->ID, $field_name, $this );
        if ( $value === null ) {
            $value = get_user_meta( $this->ID, $field_name, true );
        } 
        $value = apply_filters( 'apsies_user_get_meta_field', $value, $this->ID, $field_name, $this );
        
        return $value;
    }

    public function update ( $key, $value )
    {
       update_user_meta ( $this->ID, $key, $value );
    }

    public function get_custom()
    {
        if( $this->ID ) {
            $um = $custom = array();
            $um = apply_filters( 'apsies_user_get_meta_pre', $um, $this->ID, $this );
            
            if( empty( $um ) ) 
                $um = get_user_meta( $this->ID );
            
            foreach( $um as $key => $value ) {
                if( is_array( $value ) && count( $value ) == 1 ) 
                    $value = $value[0];

                $custom[$key] = maybe_unserialize( $value );
            }

            $custom = apply_filters( 'apsies_user_get_meta', $custom, $this->ID, $this );

            return $custom;
        }

        return null;
    }

    public function import_custom()
    {
        $this->import( $this->get_custom() );
    }

    public function name()
    {
        return $this->display_name;
    }

    public function get_permalink()
    {
        return $this->get_link();
    }

    // alias
    public function permalink()
    {
        return $this->get_permalink();
    }

    public function path()
    {
        return $this->get_permalink();
    }

    public function link()
    {
        return $this->get_permalink();
    }

    public function meta( $field_name )
    {
        return $this->get_meta_field( $field_name );
    }

    public function slug()
    {
        return $this->user_nicename;
    }
}