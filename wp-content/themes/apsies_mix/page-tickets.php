<?php 

  global $usuariologueado;

  if ( !is_user_logged_in() ) {
    $usuariologueado = false;  
    $vds = 1;
    $lg = 0;
    $suscrito = false;      
  } else {
    $usuariologueado = true;
    $lg = 1;
    global $current_user;
	get_currentuserinfo();
	$user_id = $current_user->ID;
	$fechasuscripcion = get_user_meta( $user_id, 'fechasuscripcion', true );
	$diassuscripcion = get_user_meta( $user_id, 'diassuscripcion', true );
	$suscrito = false;
	$ahora = time();
	$nivel = get_user_meta( $user_id, 'nivel', true );
	if (($fechasuscripcion <= $ahora) && ($ahora <= $fechasuscripcion + ($diassuscripcion * 86400))) {
		$suscrito = true;
		$vds = getOption("eto_valordescuentosuscritos", 1);
		$vds = 1 - ($vds / 100);
	} else {
		$vds = 1;
	}
  }

  if ($usuariologueado) {
  	get_header(); 
 ?>

	<div id="wrapper-content">
		<?php get_sidebar('left'); ?>	

		<div id="content">
			<div class="titling">
				<?php 
				if ($suscrito) {
				?>
				<h2 class="content-title">Tickets recibidos</h2>
				<?php
				} else {
				?>
				<h2 class="content-title-ns">Tickets recibidos</h2>
				<?php
				}
				?>				
				<p>Lista de todos los tickets recibidos</p>
			</div>
			
			<div id="div-history">
				<table cellpading="0" cellspacing="0">
					<thead>
						<tr>
							<td>Resumen del Ticket</td>
							<td>Fecha</td>
							<td>Detalles</td>							
						</tr>
					</thead>
					<tbody>
						<?php 
						$args = array( 'posts_per_page' => -1, 'post_type'=> 'ticket' );
						$query = new WP_Query( $args );	
						$trclass = "even";
						$count = 0;
						if ( $query->have_posts() )	{			
							while ( $query->have_posts() ) {
								$query->the_post();
								// Getting relative pedido
								$pedido_id = get_post_meta(get_the_ID(), "pedidoticket", true);
								// Getting relative user
								$pedido_user_id = get_post_meta($pedido_id, "usuario", true);
								if ($user_id == $pedido_user_id) {
									$count++;
									if ($trclass == "even") {
										$trclass = "odd";
									} else {
										$trclass = "even";
									}
									$ticketvisto = get_post_meta(get_the_ID(), "ticketvisto", true);
									if ($ticketvisto != "yes") {
										$classNT = ' new-ticket ';
									} else {
										$classNT = '';
									}
									?>
									<tr class="<?php echo $trclass . $classNT; ?>">
										<td><p><?php the_title(); ?></p> <?php the_excerpt(); ?> <p>Pedido: <?php echo get_the_title($pedido_id); ?> </p> </td>
										<td class="td-date"><p><?php the_date(); ?></p></td>
										<td class="td-detalles"><p><a href="<?php the_permalink(); ?>">Ver m&aacute;s</a></p></td>																				
									</tr>	
								<?php
								}
							}							
						}
						wp_reset_postdata();
						if ($count == 0) {
							?>
							<tr class="no-pedidos">
								<td colspan="3"><p>No ha recibido tickets</p></td>										
							</tr>
							<?php
						}
						?>						
					</tbody>
				</table>
			</div>

		</div>
		<div class="clear"></div>
	</div>

	<?php get_footer(); 

	} else {
  	?>
  	<html>
  		<head>
  			<script type="text/javascript">      
            	window.location = "<?php echo wp_login_url(); ?>";        
          	</script>
  		</head>
  		<body></body>
  	</html>
  	<?
  }
	?>