<?php 

  global $usuariologueado;

  if ( !is_user_logged_in() ) {
    $usuariologueado = false;  
    $vds = 1;
    $lg = 0;
    $suscrito = false;      
  } else {
    $usuariologueado = true;
    $lg = 1;
    global $current_user;
	get_currentuserinfo();
	$user_id = $current_user->ID;
	$fechasuscripcion = get_user_meta( $user_id, 'fechasuscripcion', true );
	$diassuscripcion = get_user_meta( $user_id, 'diassuscripcion', true );
	$suscrito = false;
	$ahora = time();
	$nivel = get_user_meta( $user_id, 'nivel', true );
	if (($fechasuscripcion <= $ahora) && ($ahora <= $fechasuscripcion + ($diassuscripcion * 86400))) {
		$suscrito = true;
		$vds = getOption("eto_valordescuentosuscritos", 1);
		$vds = 1 - ($vds / 100);
	} else {
		$vds = 1;
	}
  }

  if ($usuariologueado) {
  	get_header(); 
 ?>

	<div id="wrapper-content">
		<?php get_sidebar('left'); ?>	

		<div id="content">
			<div class="titling">
				<?php 
				if ($suscrito) {
				?>
				<h2 class="content-title">Historial de Pedidos</h2>
				<?php
				} else {
				?>
				<h2 class="content-title-ns">Historial de Pedidos</h2>
				<?php
				}
				?>		
				<p>Pedidos realizados y soluciones recibidas</p>
			</div>
			
			<div id="div-history">
				<table cellpading="0" cellspacing="0">
					<thead>
						<tr>
							<td>Detalles del Pedido</td>
							<td>Fecha</td>
							<td>Detalles</td>
						</tr>
					</thead>
					<tbody>
						<?php 
						$args = array( 'posts_per_page' => -1, 'post_type'=> 'pedido' );
						$query = new WP_Query( $args );	
						$trclass = "even";
						$count = 0;
						if ( $query->have_posts() )	{			
							while ( $query->have_posts() ) {
								$query->the_post();
								$pedido_user_id = get_field("usuario", get_the_ID());
								$pedido_user_id = $pedido_user_id['ID'];
								if ($user_id == $pedido_user_id) {
									$count++;
									if ($trclass == "even") {
										$trclass = "odd";
									} else {
										$trclass = "even";
									}
									$solucionnueva = get_post_meta(get_the_ID(), "solucionnueva", true);
									if ($solucionnueva == "yes") {
										$classNT = ' new-solution ';
									} else {
										$classNT = '';
									}
									?>
									<tr class="<?php echo $trclass . $classNT; ?>">
										<td><?php the_excerpt(); ?></td>
										<td class="td-date"><?php the_date(); ?></td>
										<td class="td-detalles">
											<p>
												<a href="<?php the_permalink(); ?>">Ver m&aacute;s</a><br/> 
												<?php
													$archivourl = get_field("solucion");
													if ($archivourl != "") {
													?>
													<span style="color: green; font-size: 9px; ">Con soluci&oacute;n</span>
														<?php
													} else {
														?>
													<span style="color: red; font-size: 9px; " class="sin-solucionar">Sin soluci&oacute;n</span>
													<?php
													}
												?>
											</p>
										</td>										
									</tr>	
								<?php
								}
							}							
						}
						wp_reset_postdata();
						if ($count == 0) {
							?>
							<tr class="no-pedidos">
								<td colspan="3"><p>No ha realizado pedidos</p></td>										
							</tr>
							<?php
						}
						?>						
					</tbody>
				</table>
			</div>

		</div>
		<div class="clear"></div>
	</div>

	<?php get_footer(); 

	} else {
  	?>
  	<html>
  		<head>
  			<script type="text/javascript">      
            	window.location = "<?php echo wp_login_url(); ?>";        
          	</script>
  		</head>
  		<body></body>
  	</html>
  	<?
  }
	?>