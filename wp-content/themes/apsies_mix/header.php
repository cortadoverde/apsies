<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="<?php bloginfo( 'charset' ); ?>" />	
	<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
  	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/x-icon" />
  	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/x-icon" />

	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
      <script src="<?php echo get_template_directory_uri(); ?>/js/respond.min.js"></script>
    <![endif]-->    
	<?php
	if (is_home()) {
		$GLOBALS["isHome"] = true;
	?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
	<?php
	} else {
	?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style-int.css">
	<script type="text/javascript">
	var v30s = <?php echo getOption("eto_valorsuscripcion", 10) * 1; ?>; 
	</script>
	<?php
		$GLOBALS["isHome"] = false;
	}
	?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/spinner/smartspinner.css">

	<?php
	   	wp_head();    
	?>
</head>
<body>
	<div id="wrapper-header">
		<header id="header">
			<div id="logo">
				<a href="<?php bloginfo( 'url' ); ?>" title="<?php bloginfo( 'name' ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="<?php bloginfo( 'name' ); ?>"></a>
			</div>
			<div id="menu-access">
				<div id="loaderImage" style="margin: 0 auto; display: none; margin-top: 15px;"></div>
					<script type="text/javascript">
						var cSpeed=9;
						var cWidth=150;
						var cHeight=14;
						var cTotalFrames=37;
						var cFrameWidth=150;
						var cImageSrc='<?php echo get_template_directory_uri(); ?>/images/sprites.gif';
						
						var cImageTimeout=false;
						var cIndex=0;
						var cXpos=0;
						var cPreloaderTimeout=false;
						var SECONDS_BETWEEN_FRAMES=0;
						
						function startAnimation(){
							
							document.getElementById('loaderImage').style.backgroundImage='url('+cImageSrc+')';
							document.getElementById('loaderImage').style.width=cWidth+'px';
							document.getElementById('loaderImage').style.height=cHeight+'px';
							
							//FPS = Math.round(100/(maxSpeed+2-speed));
							FPS = Math.round(100/cSpeed);
							SECONDS_BETWEEN_FRAMES = 1 / FPS;
							
							cPreloaderTimeout=setTimeout('continueAnimation()', SECONDS_BETWEEN_FRAMES/1000);
							
						}
						
						function continueAnimation(){
							
							cXpos += cFrameWidth;
							//increase the index so we know which frame of our animation we are currently on
							cIndex += 1;
							 
							//if our cIndex is higher than our total number of frames, we're at the end and should restart
							if (cIndex >= cTotalFrames) {
								cXpos =0;
								cIndex=0;
							}
							
							if(document.getElementById('loaderImage'))
								document.getElementById('loaderImage').style.backgroundPosition=(-cXpos)+'px 0';
							
							cPreloaderTimeout=setTimeout('continueAnimation()', SECONDS_BETWEEN_FRAMES*1000);
						}
						
						function stopAnimation(){//stops animation
							clearTimeout(cPreloaderTimeout);
							cPreloaderTimeout=false;
						}
						
						function imageLoader(s, fun)//Pre-loads the sprites image
						{
							clearTimeout(cImageTimeout);
							cImageTimeout=0;
							genImage = new Image();
							genImage.onload=function (){cImageTimeout=setTimeout(fun, 0)};
							genImage.onerror=new Function('alert(\'Could not load the image\')');
							genImage.src=s;
						}
						
						//The following code starts the animation
						new imageLoader(cImageSrc, 'startAnimation()');
					</script>				
				<div id="login-form" <?php if (is_user_logged_in()) { echo 'style="display: none;"'; } ?>>
					<form action="#" method="post">
						<input type="text" placeholder="usuario" id="user_name">
						<input type="password" placeholder="password" id="passwd">
						<input type="submit" value="ACCEDER" id="submitLogin">						
					</form>
				</div>
				<?php
				global $usuariologueado;

				  if ( !is_user_logged_in() ) {
				    $usuariologueado = false;  
				    $vds = 1;
				    $lg = 0;
				    $suscrito = false;      
				  } else {
				    $usuariologueado = true;
				    $lg = 1;
				    global $current_user;
					get_currentuserinfo();
					$user_id = $current_user->ID;
					$fechasuscripcion = get_user_meta( $user_id, 'fechasuscripcion', true );
					$diassuscripcion = get_user_meta( $user_id, 'diassuscripcion', true );
					$suscrito = false;
					$ahora = time();
					$nivel = get_user_meta( $user_id, 'nivel', true );
					if (($fechasuscripcion <= $ahora) && ($ahora <= $fechasuscripcion + ($diassuscripcion * 86400))) {
						$suscrito = true;
						$vds = getOption("eto_valordescuentosuscritos", 1);
						$vds = 1 - ($vds / 100);
					} else {
						$vds = 1;
					}
				  } 
				if ($usuariologueado) {

      				if ($suscrito) {
    					$iconMessages = "icon-messages.png";
    					$iconEdit = "icon-edit.png";
    					$nsClass = "";
    				} else {
    					$iconMessages = "icon-messages-ns.png";
    					$iconEdit = "icon-edit-ns.png";
    					$nsClass = " ns";
    				}

      				$new_tickets = getNewTickets($current_user->ID);    				
    				if ($new_tickets == 0) {
    					$supraTickets = '';
    				} else
    				if ($new_tickets < 10) {
    					$supraTickets = '<span class="supra1' . $nsClass . '">' . $new_tickets . '</span>';
    				} else {
    					$supraTickets = '<span class="supra2' . $nsClass . '">' . $new_tickets . '</span>';
    				}

    				$new_solutions = getNewSolutions($current_user->ID);    				
    				if ($new_solutions == 0) {
    					$supraSolutions = '';
    				} else
    				if ($new_solutions < 10) {
    					$supraSolutions = '<span class="supra1' . $nsClass . '">' . $new_solutions . '</span>';
    				} else {
    					$supraSolutions = '<span class="supra2' . $nsClass . '">' . $new_solutions . '</span>';
    				}    				
				}
				?>
				<?php 
				global $post;
    			$postSlug = $post->post_name;
    			if (($postSlug == "perfil")||($postSlug == "pedidos")||($postSlug == "suscripcion")||($postSlug == "recargar")||($postSlug == "historial")||($postSlug == "tickets")||($postSlug == "ticket")) {
    				$redirect = 1;
    			} else {
    				$redirect = 0;    				
    			}
				?>				
				<div id="user-data" <?php if (!is_user_logged_in()) { echo 'style="display: none;"'; } ?>>
					<ul>
						<li><a href="<?php bloginfo( 'url' ); ?>/perfil" title="Perfil"><span>Bienvenido <?php echo $current_user->user_login; ?></span></a></li>
						<li><a href="<?php bloginfo( 'url' ); ?>/pedidos" title="Realizar Pedidos"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-home.png" alt="Realizar Pedidos"></a></li>
						<li><a href="<?php bloginfo( 'url' ); ?>/tickets" title="Mensajes"><?php echo $supraTickets; ?><img src="<?php echo get_template_directory_uri(); ?>/images/<?php echo $iconMessages; ?>" alt="Mensajes"></a></li>
						<li><a href="<?php bloginfo( 'url' ); ?>/historial" title="Historial de Pedidos"><?php echo $supraSolutions; ?><img src="<?php echo get_template_directory_uri(); ?>/images/<?php echo $iconEdit; ?>" alt="Historial de Pedidos"></a></li>
						<li><a id="logout" href="#" data-redirect="<?php echo $redirect; ?>" title="Salir"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-off.png" alt="Salir"></a></li>						
					</ul>
					<div class="clear"></div>
				</div>
				<nav id="main-menu">
					<?php
					$defaults = array(
						'theme_location'  => 'mainmenu',
						'menu'            => '',
						'container'       => '',
						'container_class' => '',
						'container_id'    => '',
						'menu_class'      => 'menu',
						'menu_id'         => '',
						'echo'            => true,
						'fallback_cb'     => 'wp_page_menu',
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						'depth'           => 0,
						'walker'          => ''
					);
					wp_nav_menu( $defaults );
					?>					
				</nav>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>			
		</header>
	</div>
	<nav id="mobile-menu">
		<select name="mobile-menu" id="select-mobile-menu">
			<option value="0">- Menu -</option>			
		</select>
	</nav>