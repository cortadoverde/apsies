<?php 

/*
	Adding Pedido content type
*/
add_action('init', 'Pedido_content_type', 0);
function Pedido_content_type() {
	$args = array(
			'description' => 'Pedidos realizados',
			'show_ui' => true,
			'menu_position' => 4,
			'labels' => array(
				'name'=> 'Pedidos',
				'singular_name' => 'Pedidos',
				'add_new' => 'Agregar nuevo Pedido',
				'add_new_item' => 'Agregar nuevo Pedido',
				'edit' => 'Editar Pedido',
				'edit_item' => 'Editar Pedido',
				'new-item' => 'Nuevo Pedido',
				'view' => 'Ver Pedidos',
				'view_item' => 'Ver Pedido',
				'search_items' => 'Buscar Pedidos',
				'not_found' => 'No se han encontrado Pedidos',
				'not_found_in_trash' => 'No se han encontrado Pedidos en la papelera',
				'parent' => 'Pedido padre'
			),
			'public' => true,
			'taxonomies' => array(),
			'capability_type' => 'post',
			'hierarchical' => false,
			'rewrite' => array( 'slug' => 'pedido', 'with_front' => true ),
			'supports' => array('title', 'editor')
	);
	register_post_type( 'pedido' , $args );
}

/*
	Adding Ticket content type
*/
add_action('init', 'Ticket_content_type', 0);
function Ticket_content_type() {
	$args = array(
			'description' => 'Tickets realizados',
			'show_ui' => true,
			'menu_position' => 4,
			'labels' => array(
				'name'=> 'Tickets',
				'singular_name' => 'Tickets',
				'add_new' => 'Agregar nuevo Ticket',
				'add_new_item' => 'Agregar nuevo Ticket',
				'edit' => 'Editar Ticket',
				'edit_item' => 'Editar Ticket',
				'new-item' => 'Nuevo Ticket',
				'view' => 'Ver Tickets',
				'view_item' => 'Ver Ticket',
				'search_items' => 'Buscar Tickets',
				'not_found' => 'No se han encontrado Tickets',
				'not_found_in_trash' => 'No se han encontrado Tickets en la papelera',
				'parent' => 'Ticket padre'
			),
			'public' => true,
			'taxonomies' => array(),
			'capability_type' => 'post',
			'hierarchical' => false,
			'rewrite' => array( 'slug' => 'ticket', 'with_front' => true ),
			'supports' => array('title', 'editor', 'comments')
	);
	register_post_type( 'ticket' , $args );
}

