	<div id="wrapper-footer">
		<footer id="footer">
			<div id="footer-left">
				<!--<a href="#" title="T&eacute;minos y condiciones">T&eacute;minos y condiciones</a>-->
				<?php
					$defaults = array(
						'theme_location'  => 'footermenu',
						'menu'            => '',
						'container'       => '',
						'container_class' => '',
						'container_id'    => '',
						'menu_class'      => 'menu',
						'menu_id'         => '',
						'echo'            => true,
						'fallback_cb'     => 'wp_page_menu',
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						'depth'           => 0,
						'walker'          => ''
					);
					wp_nav_menu( $defaults );
				?>		
			</div>
			<div id="footer-right">				
				<?php socialsArea(); ?>
			</div>
			<div class="clear"></div>
		</footer>
	</div>

	<div id="notification" style="position: fixed; top: -50px; left: 50%; opacity: 0; font-family: OSRegular;">		
		<div class="inner" style="position: relative; left: -50%; color: white; padding: 20px;">			
		</div>		
	</div>
	<div id="notificationAjax" style="position: fixed; top: -50px; left: 50%; opacity: 0; font-family: OSRegular;">		
		<div class="inner" style="position: relative; left: -50%; color: white; padding: 20px; background: #E6BD45;">
			
					
		</div>		
	</div>

	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.easing.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/functions.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/spinner/smartspinner.js"></script>

	<style type="text/css">
    	html { 
    		margin-top: 0px !important; 
    	}
    	
    	#wpadminbar {
      		display: none;
    	}
  	</style>
	<?php wp_footer(); ?>
</body>
</html>