<?php
/*
If you would like to edit this file, copy it to your current theme's directory and edit it there.
Theme My Login will always look in your theme's directory first, before using this default template.
*/
?>
<div class="login profile" id="theme-my-login<?php $template->the_instance(); ?>">
	<?php $template->the_action_template_message( 'profile' ); ?>
	<?php $template->the_errors(); ?>
	<form id="your-profile" action="<?php $template->the_action_url( 'profile' ); ?>" method="post">
		<?php wp_nonce_field( 'update-user_' . $current_user->ID ); ?>
		<p>
			<input type="hidden" name="from" value="profile" />
			<input type="hidden" name="checkuser_id" value="<?php echo $current_user->ID; ?>" />
		</p>

		<?php if ( has_action( 'personal_options' ) ) : ?>

		<h3><?php _e( 'Personal Options' ); ?></h3>

		<table class="form-table">
		<?php do_action( 'personal_options', $profileuser ); ?>
		</table>

		<?php endif; ?>

		<?php do_action( 'profile_personal_options', $profileuser ); ?>

		<h3><?php _e( 'Tu cuenta' ); ?></h3>

		<table class="form-table">
		<tr>
			<th><label for="user_login"><?php _e( 'Username' ); ?></label></th>
			<td><input type="text" name="user_login" id="user_login" value="<?php echo esc_attr( $profileuser->user_login ); ?>" disabled="disabled" class="regular-text" size="35"/> </td>
		</tr>

		<tr>
			<th><label for="email"><?php _e( 'E-mail' ); ?> <span class="description"><?php _e( '(required)' ); ?></span></label></th>
			<td><input type="text" name="email" id="email" value="<?php echo esc_attr( $profileuser->user_email ); ?>" class="regular-text"  size="35"/><br/>
				<span class="description">Será el email donde recibas las notificaciones. Puedes cambiarlo si quieres.</span></td>
		</tr>

		<tr>
			<th><label for="nivel"><?php _e( 'Nivel' ); ?></label></th>
			<td>
                <input type="radio" value="0" name="nivel" id="nivelColegio" <?php if ($profileuser->nivel == 0) { echo "checked" ;} ?>><label style="display: inline-block; margin-left: 5px;" for="nivelColegio"> ESO / Bachiller</label><br/>
                <input type="radio" value="1" name="nivel" id="nivelUniversidad" <?php if ($profileuser->nivel == 1) { echo "checked" ;} ?>><label style="display: inline-block; margin-left: 5px;" for="nivelUniversidad"> Universidad</label>
            </td>
		</tr>

		<?php
		$show_password_fields = apply_filters( 'show_password_fields', true, $profileuser );
		if ( $show_password_fields ) :
		?>
		<tr id="password">
			<th><label for="pass1"><?php _e( 'New Password' ); ?></label></th>
			<td><input type="password" name="pass1" id="pass1" value="" autocomplete="off"  size="35"/> <br/><span class="description"><?php _e( 'If you would like to change the password type a new one. Otherwise leave this blank.' ); ?></span><br />
				<input type="password" name="pass2" id="pass2" value="" autocomplete="off"  size="35"/> <br/><span class="description"><?php _e( 'Type your new password again.' ); ?></span><br />
				<div id="pass-strength-result"><?php _e( 'Indicador de fortaleza', 'theme-my-login' ); ?></div><br/><br/>
				<p class="description indicator-hint"><?php _e( 'La contraseña debe tener al menos 7 caracteres. Para hacerla fuerte, usa letras minúsculas y mayúsculas, números y símbolos como ! " ? $ % ^ y ).' ); ?></p>
			</td>
		</tr>
		<?php endif; ?>

		</table>

		<p class="submit">
			<input type="hidden" name="action" value="profile" />
			<input type="hidden" name="instance" value="<?php $template->the_instance(); ?>" />
			<input type="hidden" name="user_id" id="user_id" value="<?php echo esc_attr( $current_user->ID ); ?>" />
			<input type="submit" class="button-primary" value="<?php esc_attr_e( 'Guardar' ); ?>" name="submit" />
		</p>

		<h3><?php _e( 'Datos Personales' ); ?></h3>

		<table class="form-table">
		
		<tr>
			<th><label for="first_name"><?php _e( 'First Name' ); ?></label></th>
			<td><input type="text" name="first_name" id="first_name" value="<?php echo esc_attr( $profileuser->first_name ); ?>" class="regular-text"  size="35"/></td>
		</tr>

		<tr>
			<th><label for="last_name"><?php _e( 'Last Name' ); ?></label></th>
			<td><input type="text" name="last_name" id="last_name" value="<?php echo esc_attr( $profileuser->last_name ); ?>" class="regular-text"  size="35"/></td>
		</tr>
		<script type="text/javascript">
			jQuery(document).ready(function () {
				jQuery('.spinnerEdad').spinit({  height: 36, min: 0, initValue: <?php echo esc_attr( $profileuser->edad ); ?>, max: 5000,  stepInc: 1 });
			});
		</script>
		<tr>
			<th><label for="edad"><?php _e( 'Edad' ); ?></label></th>
			<td><input type="text" name="edad" id="edad" value="<?php echo esc_attr( $profileuser->edad ); ?>" class="regular-text code spinnerEdad"  size="35"/><br/>
			<span class="description"><?php _e( 'En años.' ); ?></span></td>
		</tr>
		<tr class="tr-colegio">
			<th><label for="colegio"><?php _e( 'Colegio' ); ?></label></th>
			<td><input type="text" name="colegio" id="colegio" value="<?php echo esc_attr( $profileuser->colegio ); ?>" class="regular-text code"  size="35"/><br/>
			<span class="description"><?php _e( 'Colegio donde estudias.' ); ?></span></td>
		</tr>	
		<tr class="tr-universidad">
			<th><label for="universidad"><?php _e( 'Universidad' ); ?></label></th>
			<td><input type="text" name="universidad" id="universidad" value="<?php echo esc_attr( $profileuser->universidad ); ?>" class="regular-text code"  size="35"/><br/>
			<span class="description"><?php _e( 'Universidad donde estudias.' ); ?></span></td>
		</tr>
		<tr>
			<th><label for="ciudad"><?php _e( 'Ciudad' ); ?></label></th>
			<td><input type="text" name="ciudad" id="ciudad" value="<?php echo esc_attr( $profileuser->ciudad ); ?>" class="regular-text code"  size="35"/><br/>
			<span class="description"><?php _e( 'Ciudad donde vives.' ); ?></span></td>
		</tr>

		<!--<tr>
			<th><label for="nickname"><?php _e( 'Nickname' ); ?> <span class="description"><?php _e( '(required)' ); ?></span></label></th>
			<td><input type="text" name="nickname" id="nickname" value="<?php echo esc_attr( $profileuser->nickname ); ?>" class="regular-text" /></td>
		</tr>-->

		<!--<tr>
			<th><label for="display_name"><?php _e( 'Display name publicly as' ); ?></label></th>
			<td>
				<select name="display_name" id="display_name">
				<?php
					$public_display = array();
					$public_display['display_nickname']  = $profileuser->nickname;
					$public_display['display_username']  = $profileuser->user_login;

					if ( ! empty( $profileuser->first_name ) )
						$public_display['display_firstname'] = $profileuser->first_name;

					if ( ! empty( $profileuser->last_name ) )
						$public_display['display_lastname'] = $profileuser->last_name;

					if ( ! empty( $profileuser->first_name ) && ! empty( $profileuser->last_name ) ) {
						$public_display['display_firstlast'] = $profileuser->first_name . ' ' . $profileuser->last_name;
						$public_display['display_lastfirst'] = $profileuser->last_name . ' ' . $profileuser->first_name;
					}

					if ( ! in_array( $profileuser->display_name, $public_display ) )// Only add this if it isn't duplicated elsewhere
						$public_display = array( 'display_displayname' => $profileuser->display_name ) + $public_display;

					$public_display = array_map( 'trim', $public_display );
					$public_display = array_unique( $public_display );

					foreach ( $public_display as $id => $item ) {
				?>
					<option <?php selected( $profileuser->display_name, $item ); ?>><?php echo $item; ?></option>
				<?php
					}
				?>
				</select>
			</td>
		</tr>-->
		<!--<tr>
			<th><label for="url"><?php _e( 'Website' ); ?></label></th>
			<td><input type="text" name="url" id="url" value="<?php echo esc_attr( $profileuser->user_url ); ?>" class="regular-text code" /></td>
		</tr>-->

		<?php
			foreach ( _wp_get_user_contactmethods() as $name => $desc ) {
		?>
		<tr>
			<th><label for="<?php echo $name; ?>"><?php echo apply_filters( 'user_'.$name.'_label', $desc ); ?></label></th>
			<td><input type="text" name="<?php echo $name; ?>" id="<?php echo $name; ?>" value="<?php echo esc_attr( $profileuser->$name ); ?>" class="regular-text" /></td>
		</tr>
		<?php
			}
		?>
		</table>

		<!--<h3><?php _e( 'About Yourself' ); ?></h3>

		<table class="form-table">		
		

		<tr>
			<th><label for="description"><?php _e( 'Biographical Info' ); ?></label></th>
			<td><textarea name="description" id="description" rows="5" cols="30"><?php echo esc_html( $profileuser->description ); ?></textarea><br />
			<span class="description"><?php _e( 'Share a little biographical information to fill out your profile. This may be shown publicly.' ); ?></span></td>
		</tr>
		

		
		</table>-->

		<?php do_action( 'show_user_profile', $profileuser ); ?>

		<p class="submit">
			<input type="hidden" name="action" value="profile" />
			<input type="hidden" name="instance" value="<?php $template->the_instance(); ?>" />
			<input type="hidden" name="user_id" id="user_id" value="<?php echo esc_attr( $current_user->ID ); ?>" />
			<input type="submit" class="button-primary" value="<?php esc_attr_e( 'Guardar' ); ?>" name="submit" />
		</p>
	</form>
</div>
