<?php 

  global $usuariologueado;

  if ( !is_user_logged_in() ) {
    $usuariologueado = false;  
    $vds = 1;
    $lg = 0;
    $suscrito = false;      
  } else {
    $usuariologueado = true;
    $lg = 1;
    global $current_user;
	get_currentuserinfo();
	$user_id = $current_user->ID;
	$fechasuscripcion = get_user_meta( $user_id, 'fechasuscripcion', true );
	$diassuscripcion = get_user_meta( $user_id, 'diassuscripcion', true );
	$suscrito = false;
	$ahora = time();
	$nivel = get_user_meta( $user_id, 'nivel', true );
	if (($fechasuscripcion <= $ahora) && ($ahora <= $fechasuscripcion + ($diassuscripcion * 86400))) {
		$suscrito = true;
		$vds = getOption("eto_valordescuentosuscritos", 1);
		$vds = 1 - ($vds / 100);
	} else {
		$vds = 1;
	}
  }

  if ($usuariologueado) {
  	get_header(); 
 ?>
 <script type="text/javascript">
 	var vds = <?php echo $vds; ?>;
 	var lg = <?php echo $lg; ?>;
 </script>

	<div id="wrapper-content">
		<?php get_sidebar('left'); ?>	

		<div id="content">
			<div class="titling">
				<?php 
				if ($suscrito) {
				?>
				<h2 class="content-title"><?php echo getOption("eto_pedidostitular", "Titular para rellenar") ?></h2>
				<?php
				} else {
				?>
				<h2 class="content-title-ns"><?php echo getOption("eto_pedidostitular", "Titular para rellenar") ?></h2>
				<?php
				}
				?>				
				<p><?php echo getOption("eto_pedidosdesc", "Mandanos las especificaciones para que podamos realizar tu trabajo") ?></p>
			</div>
			<p class="notification">
				<?php 
				if (($nivel == 0) && ($suscrito)) {
					$cont = 0;
					$cant = getOption("eto_valorpedidosgratiscolegiosus", 3);
					$args = array( 'numberposts' => -1, 'post_type'=> 'pedido' );
					$query = new WP_Query( $args );	
					if ( $query->have_posts() )	{			
						while ( $query->have_posts() ) {
							$query->the_post();
							// Getting project thumbnail
							$post_id = get_the_ID();										    
							if (get_post_meta($post_id, "pedido_suscrito", true) == "1") {
								if (get_post_meta($post_id, 'usuario', true) == $user_id) {
									$post_date = get_the_date('Y/m/d');
									$today = date('Y/m/d');
									if ($post_date == $today) {
										$cont++;
									}
								}
							}
						}
					}
					wp_reset_postdata();
				?>			
				Tienes <span><?php echo getOption("eto_valorpedidosgratiscolegiosus", 3) - $cont; ?></span> pedidos aun disponibles
				<?php 
				}
				?>
			</p>
			<div id="form">
				<?php 
				if (($nivel == 0) && (canRequestForFree($user_id)) && ($suscrito)) {
				?>
				<div class="colegio-pedido-wrapper">
					<input type="radio" id="radio-pedido-instantaneo" name="radio-pedido" checked="true"><label for="radio-pedido-instantaneo">Pedido convencional</label>
					<input type="radio" id="radio-pedido-suscrito" name="radio-pedido"><label for="radio-pedido-suscrito">Pedido gratis por estar suscrito</label>
				</div>
				<?php 
				}
				?>	
				<?php 
				if ($nivel == 0) {
				?>
				<div class="colegio-pedido-instantaneo form-pedido" style="">				
					<!-- Nivel 0 -->
					<select data-nivel="0" class="form-element action" style="margin-bottom: 8px;">
						<option value="0">-- Tipo de Pedido --</option>
						<option value="1">Resoluci&oacute;n de ejercicios</option>
						<option value="2">Redacci&oacute;n sobre un tema</option>
						<option value="3">Comentario de texto</option>
						<option value="4">Comentario art&iacute;stico</option>
						<option value="5">Cr&iacute;tica literaria</option>
						<option value="6">AUTOCAD</option>
						<option value="7">Dudas/Investigacion sobre un tema</option>
						<option value="8">Trabajos</option>
					</select>
					<div class="clear"></div>
					<!-- End Nivel 0 -->
					<!-- Nivel 1 -->
					<select data-nivel="1" data-valor="1" class="form-element action asignatura" style="margin-right: 18px; display: none;">
						<option value="0">-- Asignatura --</option>
						<option value="1">Matem&aacute;ticas</option>
						<option value="2">Lengua Castellana y Literatura</option>
						<option value="3">F&iacute;sica</option>
						<option value="4">Qu&iacute;mica</option>
						<option value="5">Ingl&eacute;s</option>
						<option value="6">Franc&eacute;s</option>
						<option value="7">Dibujo T&eacute;cnico</option>
						<option value="8">Geograf&iacute;a</option>
						<option value="9">Arte</option>
						<option value="10">Inform&aacute;tica</option>
						<option value="11">Econom&iacute;a</option>
						<option value="12">Alem&aacute;n</option>
						<option value="13">Geograf&iacute;a</option>
						<option value="14">Filosof&iacute;a</option>
						<option value="15">Lat&iacute;n/Griego</option>
					</select>
					<select data-nivel="1" data-valor="2" class="form-element action asignatura" style="margin-right: 18px; display: none;">
						<option value="0">-- Asignatura --</option>
						<option value="1">Lengua Castellana y Literatura</option>
						<option value="2">Ciencias Sociales</option>
						<option value="3">Ingl&eacute;s</option>
						<option value="4">Franc&eacute;s</option>
						<option value="5">Geograf&iacute;a</option>	
						<option value="6">Historia</option>
						<option value="7">Alem&aacute;n</option>					
					</select>
					<select data-nivel="1" data-valor="3" class="form-element action asignatura" style="margin-right: 18px; display: none;">
						<option value="0">-- Asignatura --</option>
						<option value="1">Lengua Castellana y Literatura</option>
						<option value="2">Ciencias Sociales</option>
						<option value="3">Ingl&eacute;s</option>
						<option value="4">Franc&eacute;s</option>
						<option value="5">Geograf&iacute;a</option>	
						<option value="6">Historia</option>
						<option value="7">Econom&iacute;a</option>	
						<option value="8">Filosof&iacute;a</option>					
					</select>					
					<select data-nivel="1" data-valor="4" class="form-element action asignatura" style="margin-right: 18px; display: none;">
						<option value="0">-- Asignatura --</option>
						<option value="1">Arte</option>
					</select>
					<!-- Valor 5 -->
					<input data-nivel="1" data-valor="5" class="form-element titulo" type="text" placeholder="T&iacute;tulo de la obra" style="display: none; margin-right: 18px;margin-bottom: 12px;">
					<div class="span-element"><span style="display: none;" data-nivel="1" data-valor="5" class="form-element base4">Nº total de p&aacute;ginas </span></div>										
					<input data-nivel="1" data-valor="5" type="text" class="spinner1 form-element palabrascritica" style="margin-right: 20px; margin-left: 3px; display: none; margin-bottom: 12px;">
					<div data-nivel="1" data-valor="5" class="form-element partesincluir" style="display: none; width: 224px; float: left;">
						Partes a incluir: <br/>
						<input type="checkbox" class="partesincluir" data-valor="Argumento"> Argumento <br/>
						<input type="checkbox" class="partesincluir" data-valor="Personajes principales"> Personajes principales <br/>
						<input type="checkbox" class="partesincluir" data-valor="Personajes secundarios"> Personajes secundarios <br/>
						<input type="checkbox" class="partesincluir" data-valor="Estilo"> Estilo <br/>
						<input type="checkbox" class="partesincluir" data-valor="Estructura"> Estructura <br/>
						<input type="checkbox" class="partesincluir" data-valor="Conclusion"> Conclusi&oacute;n <br/>
					</div>					
					<select data-nivel="1" data-valor="5" class="form-element tiempocritica" style="margin-right: 0px; display: none; margin-bottom: 23px;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="2">2 horas</option>
						<option value="5">5 horas</option>
						<option value="8">8 horas</option>
						<option value="24">1 dia</option>
						<option value="48">2 dias</option>
						<option value="72">3 dias</option>
						<option value="96">4 dias</option>
						<option value="120">5 dias</option>
						<option value="144">6 dias</option>
						<option value="150">6+ dias</option>
					</select>
					<textarea data-nivel="1" data-valor="5" class="form-element observacionescritica" style="margin-right: 18px; display: none;" placeholder="Observaciones"></textarea>
					<!-- End Valor 5 -->
					<select data-nivel="1" data-valor="6" class="form-element action autocadtipo" style="margin-right: 18px; display: none;">
						<option value="0">-- Tipo --</option>
						<option value="1">Ejercicios</option>
						<option value="2">Piezas</option>
					</select>					
					<select data-nivel="1" data-valor="7" class="form-element action asignatura" style="margin-right: 18px; display: none;">
						<option value="0">-- Asignatura --</option>
						<option value="1">Lengua Castellana y Literatura</option>
						<option value="2">Ciencias Sociales</option>
						<option value="3">Geograf&iacute;a</option>
						<option value="4">Historia</option>
						<option value="5">Arte</option>
						<option value="6">Inform&aacute;tica</option>
						<option value="7">Econom&iacute;a</option>	
						<option value="8">Biolog&iacute;a</option>
						<option value="9">Filosof&iacute;a</option>	
						<option value="10">Latin / Griego</option>				
					</select>					
					<!-- Valor 8 -->
					<input data-nivel="1" data-valor="8" class="form-element titulo" type="text" placeholder="T&iacute;tulo" style="display: none; margin-right: 18px;">
					<select data-nivel="1" data-valor="8" class="form-element action tema" style="margin-right: 80px; display: none; margin-bottom: 8px;">
						<option value="0">-- Tema --</option>
						<option value="1">Matem&aacute;ticas</option>
						<option value="2">Lengua Castellana y Literatura</option>
						<option value="3">F&iacute;sica</option>
						<option value="4">Qu&iacute;mica</option>
						<option value="5">Ciencias Sociales</option>
						<option value="6">Ingl&eacute;s</option>
						<option value="7">Franc&eacute;s</option>	
						<option value="8">Geograf&iacute;a</option>
						<option value="9">Dibujo T&eacute;cnico</option>	
						<option value="10">Alem&aacute;n</option>
						<option value="11">Arte</option>
						<option value="12">Inform&aacute;tica</option>
						<option value="13">Econom&iacute;a</option>
						<option value="14">Biolog&iacute;a</option>
						<option value="15">Filos&iacute;a</option>	
						<option value="16">Lat&iacute;n / Griego</option>
						<option value="17">Historia</option>			
					</select>
					<div class="span-element"><span style="display: none;" data-nivel="1" data-valor="8" class="form-element">Nº de p&aacute;ginas </span></div>										
					<input data-nivel="1" data-valor="8" type="text" class="spinner1 form-element paginastrabajo" style="margin-right: 73px; margin-left: 3px; display: none;">
					<select data-nivel="1" data-valor="8" class="form-element tiempotrabajo" style="margin-right: 0px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="2">2 horas</option>
						<option value="5">5 horas</option>
						<option value="8">8 horas</option>
						<option value="24">1 dia</option>
						<option value="48">2 dias</option>
						<option value="72">3 dias</option>
						<option value="96">4 dias</option>
						<option value="120">5 dias</option>
						<option value="144">6 dias</option>
						<option value="150">6+ dias</option>
					</select>
					<select data-nivel="1" data-valor="8" class="form-element bibliografiatrabajo" style="margin-right: 18px; display: none;">
						<option value="0">-- Bibliograf&iacute;a --</option>
						<option value="1">Si</option>
						<option value="2">No</option>						
					</select>
					<textarea data-nivel="1" data-valor="8" class="form-element observaciones" style="margin-right: 0px; display: none;" placeholder="Observaciones"></textarea>
					<!-- End Valor 8 -->
					<!-- End Nivel 1 -->
					<!-- Base 1 -->
					<div class="span-element"><span style="display: none;" data-nivel="2" class="form-element base1">Nº de ejercicios </span></div>
					<input data-nivel="2" type="text" class="base1 spinner5 form-element ejercicios" style="margin-right: 43px; margin-left: 3px; display: none; margin-bottom: 12px;">
					<input data-nivel="2" type="text" class="base1 spinner3 form-element ejercicios" style="margin-right: 43px; margin-left: 3px; display: none; margin-bottom: 12px;">
					<select data-nivel="2" class="base1 form-element tiempo" style="margin-right: 0px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="2">2 horas</option>
						<option value="5">5 horas</option>
						<option value="8">8 horas</option>
						<option value="24">1 dia</option>
						<option value="48">2 dias</option>
						<option value="72">3 dias</option>
						<option value="96">4 dias</option>
						<option value="120">5 dias</option>
						<option value="144">6 dias</option>
						<option value="150">6+ dias</option>
					</select>
					<!-- End Base 1 -->
					<!-- Base 2 -->
					<input data-nivel="2" type="text" class="base2 form-element tema" placeholder="Tema" style="display: none; margin-right: 80px; margin-bottom: 12px;">
					<div class="span-element"><span style="display: none;" data-nivel="2" class="form-element base2">Nº de palabras </span></div>
					<input data-nivel="2" type="text" class="base2 spinner1 form-element palabras" style="margin-right: 69px; margin-left: 3px; display: none;">
					<select data-nivel="2" class="base2 form-element tiempo" style="margin-right: 0px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="2">2 horas</option>
						<option value="5">5 horas</option>
						<option value="8">8 horas</option>
						<option value="24">1 dia</option>
						<option value="48">2 dias</option>
						<option value="72">3 dias</option>
						<option value="96">4 dias</option>
						<option value="120">5 dias</option>
						<option value="144">6 dias</option>
						<option value="150">6+ dias</option>
					</select>
					<textarea data-nivel="2" class="base2 form-element observaciones" style="margin-right: 0px; display: none;" placeholder="Observaciones"></textarea>
					<!-- End Base 2 -->
					<!-- Base 3 -->
					<div class="span-element"><span style="display: none;" data-nivel="2" class="form-element base3">Nº de palabras </span></div>										
					<input data-nivel="2" type="text" class="base3 spinner1 form-element palabras" style="margin-right: 50px; margin-left: 3px; display: none; margin-bottom: 12px;">
					<select data-nivel="2" class="base3 form-element tiempo" style="margin-right: 18px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="2">2 horas</option>
						<option value="5">5 horas</option>
						<option value="8">8 horas</option>
						<option value="24">1 dia</option>
						<option value="48">2 dias</option>
						<option value="72">3 dias</option>
						<option value="96">4 dias</option>
						<option value="120">5 dias</option>
						<option value="144">6 dias</option>
						<option value="150">6+ dias</option>
					</select>
					<textarea data-nivel="2" class="base3 form-element observaciones" style="margin-right: 0px; display: none;" placeholder="Observaciones"></textarea>
					<textarea data-nivel="2" class="base3 form-element textocomentar" style="margin-right: 18px; display: none;" placeholder="Texto a comentar"></textarea>
					<!-- End Base 3 -->
					<!-- Base 4 -->
					<div class="span-element"><span style="display: none;" data-nivel="2" class="form-element base4">Nº de palabras </span></div>										
					<input data-nivel="2" type="text" class="base4 spinner1 form-element palabras" style="margin-right: 50px; margin-left: 3px; display: none; margin-bottom: 12px;">
					<select data-nivel="2" class="base4 form-element tiempo" style="margin-right: 18px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="2">2 horas</option>
						<option value="5">5 horas</option>
						<option value="8">8 horas</option>
						<option value="24">1 dia</option>
						<option value="48">2 dias</option>
						<option value="72">3 dias</option>
						<option value="96">4 dias</option>
						<option value="120">5 dias</option>
						<option value="144">6 dias</option>
						<option value="150">6+ dias</option>
					</select>
					<textarea data-nivel="2" class="base4 form-element observaciones" style="margin-right: 0px; display: none;" placeholder="Observaciones"></textarea>
					<textarea data-nivel="2" class="base4 form-element obracomentar" style="margin-right: 18px; display: none;" placeholder="Obra de arte a comentar"></textarea>
					<!-- End Base 4 -->
					<!-- Base 6 -->
					<div class="span-element"><span style="display: none;" data-nivel="2" data-valor="1" class="form-element base6">Nº de ejercicios </span></div>										
					<input data-nivel="2" data-valor="1" type="text" class="base6 spinner1 form-element ejercicios" style="margin-right: 50px; margin-left: 3px; display: none; margin-bottom: 12px;">
					<select data-nivel="2" data-valor="1" class="base6 form-element tiempoejercicios" style="margin-right: 18px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="2">2 horas</option>
						<option value="5">5 horas</option>
						<option value="8">8 horas</option>
						<option value="24">1 dia</option>
						<option value="48">2 dias</option>
						<option value="72">3 dias</option>
						<option value="96">4 dias</option>
						<option value="120">5 dias</option>
						<option value="144">6 dias</option>
						<option value="150">6+ dias</option>
					</select>
					<div class="span-element"><span style="display: none;" data-nivel="2" data-valor="2" class="form-element base6">Nº de piezas a dibujar </span></div>										
					<input data-nivel="2" data-valor="2" type="text" class="base6 spinner1 form-element piezas" style="margin-right: 10px; margin-left: 3px; display: none; margin-bottom: 12px;">
					<select data-nivel="2" data-valor="2" class="base6 form-element vistaspiezas" style="margin-right: 18px; display: none;">
						<option value="0">-- Vistas --</option>
						<option value="1">Si</option>
						<option value="2">No</option>						
					</select>
					<select data-nivel="2" data-valor="2" class="base6 form-element acotacionpiezas" style="margin-right: 0px; display: none;">
						<option value="0">-- Acotaci&oacute;n --</option>
						<option value="1">Si</option>
						<option value="2">No</option>						
					</select>
					<select data-nivel="2" data-valor="2" class="base6 form-element tiempopiezas" style="margin-right: 18px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="2">2 horas</option>
						<option value="5">5 horas</option>
						<option value="8">8 horas</option>
						<option value="24">1 dia</option>
						<option value="48">2 dias</option>
						<option value="72">3 dias</option>
						<option value="96">4 dias</option>
						<option value="120">5 dias</option>
						<option value="144">6 dias</option>
						<option value="150">6+ dias</option>
					</select>
					<textarea data-nivel="1" data-valor="6" class="form-element observaciones" style="margin-right: 0px; display: none;" placeholder="Observaciones"></textarea>
					<!-- End Base 6 -->
					<!-- Base 7 -->
					<input data-nivel="2" type="text" class="base7 form-element tema" placeholder="Tema" style="display: none; margin-right: 80px; margin-bottom: 12px;">
					<div class="span-element"><span style="display: none;" data-nivel="2" class="form-element base7">Nº de palabras </span></div>
					<input data-nivel="2" type="text" class="base7 spinner1 form-element palabras" style="margin-right: 68px; margin-left: 3px; display: none;">
					<select data-nivel="2" class="base7 form-element tiempo" style="margin-right: 0px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="2">2 horas</option>
						<option value="5">5 horas</option>
						<option value="8">8 horas</option>
						<option value="24">1 dia</option>
						<option value="48">2 dias</option>
						<option value="72">3 dias</option>
						<option value="96">4 dias</option>
						<option value="120">5 dias</option>
						<option value="144">6 dias</option>
						<option value="150">6+ dias</option>
					</select>
					<textarea data-nivel="2" class="base7 form-element observaciones" style="margin-right: 0px; display: none;" placeholder="Observaciones"></textarea>
					<!-- End Base 7 -->					
					<div class="clear"></div>
					<!--<img src="<?php echo get_template_directory_uri(); ?>/images/bg-drop.png" style="cursor: pointer; margin: 10px 20px 0px 0px; float: left;" alt="">-->
					<div class="wrapper-dnd-area">
						<div id="colegio-dnd-area" class="dnd-area">
							<form id="colegio-form" name="colegio-form" action="<?php bloginfo("url") ?>/wp-admin/admin-ajax.php" method="post" enctype="multipart/form-data">
								<input type="hidden" name="action" value="MyAjaxFunctions">
								<input type="hidden" name="toaction" value="uploadArchive">
								<input id="colegio-form-post-id" type="hidden" name="post_id" value="">
								<input id="colegio-file" type="file" style="display:none;" name="fileselect">
								<?php wp_nonce_field( 'fileselect', 'fileselect_nonce' ); ?>												
							</form>
						</div>
						<div id="colegio-details"></div>
					</div>
					<div class="points" style="float: left; width: 190px; text-align: center; margin-top: 10px; color: #838383;">
						<span class="price">Precio por este pedido<br/> <strong style="font-family: OSBold; font-size: 16px; color: black;">0 puntos</strong></span>					
						<a class="btn" href="#" title="Pru&eacute;balo!">Pru&eacute;balo!</a>
					</div>
				</div>
				<?php 
				}
				?>
				<?php 
				if ($nivel == 0) {
				?>
				<div class="colegio-suscritos form-pedido" style="display: none;">
					<!-- Nivel 0 -->
					<select data-nivel="0" class="form-element action">
						<option value="0">-- Asignatura --</option>
						<option value="1">Matem&aacute;ticas</option>
						<option value="2">Lengua Castellana</option>
						<option value="3">F&iacute;sica</option>
						<option value="4">Qu&iacute;mica</option>
						<option value="5">Ciencias Sociales</option>
						<option value="6">Ingl&eacute;s</option>
						<option value="7">Franc&eacute;s</option>
						<option value="8">Geograf&iacute;a</option>
						<option value="9">Dibujo T&eacute;cnico</option>
						<option value="10">Historia</option>
						<option value="11">Arte</option>
						<option value="12">Inform&aacute;tica</option>
						<option value="13">Econom&iacute;a</option>
						<option value="14">Biolog&iacute;a</option>
						<option value="15">Filosof&iacute;a</option>
						<option value="16">Alem&aacute;n</option>
						<option value="17">Lat&iacute;n / Griego</option>
					</select>
					<div class="clear"></div>
					<!-- End Nivel 0 -->
					<!-- Nivel 1 -->
					<select data-nivel="1" data-valor="1" class="form-element action" style="margin-right: 18px; display: none;">
						<option value="0">-- Tipo --</option>
						<option value="1">Resoluci&oacute;n de ejercicios</option>
						<option value="2">Demostraci&oacute;n</option>						
					</select>
					<select data-nivel="1" data-valor="2" class="form-element action" style="margin-right: 18px; display: none;">
						<option value="0">-- Tipo --</option>
						<option value="1">Resoluci&oacute;n de ejercicios</option>
						<option value="2">Redaci&oacute;n sobre un tema</option>	
						<option value="3">Comentario de texto</option>	
						<option value="4">Cr&iacute;tica obra literaria</option>	
						<option value="5">Dudas / investigaci&oacute;n sobre un tema</option>			
					</select>
					<select data-nivel="1" data-valor="3" class="form-element action" style="margin-right: 18px; display: none;">
						<option value="0">-- Tipo --</option>
						<option value="1">Resoluci&oacute;n de ejercicios</option>
						<option value="2">Demostraci&oacute;n</option>						
					</select>
					<select data-nivel="1" data-valor="4" class="form-element action" style="margin-right: 18px; display: none;">
						<option value="0">-- Tipo --</option>
						<option value="1">Resoluci&oacute;n de ejercicios</option>
						<option value="2">Demostraci&oacute;n</option>						
					</select>
					<select data-nivel="1" data-valor="5" class="form-element action" style="margin-right: 18px; display: none;">
						<option value="0">-- Tipo --</option>
						<option value="1">Redacci&oacute;n sobre un tema</option>
						<option value="2">Comentario de texto</option>	
						<option value="3">Dudas / investigaci&oacute;n sobre un tema</option>					
					</select>
					<select data-nivel="1" data-valor="6" class="form-element action" style="margin-right: 18px; display: none;">
						<option value="0">-- Tipo --</option>
						<option value="1">Resoluci&oacute;n de ejercicios</option>
						<option value="2">Demostraci&oacute;n</option>						
					</select>
					<select data-nivel="1" data-valor="7" class="form-element action" style="margin-right: 18px; display: none;">
						<option value="0">-- Tipo --</option>
						<option value="1">Resoluci&oacute;n de ejercicios</option>
						<option value="2">Demostraci&oacute;n</option>						
					</select>
					<select data-nivel="1" data-valor="8" class="form-element action" style="margin-right: 18px; display: none;">
						<option value="0">-- Tipo --</option>
						<option value="1">Resoluci&oacute;n de ejercicios</option>
						<option value="2">Redacci&oacute;n sobre un tema</option>						
						<option value="3">Comentario de texto</option>
						<option value="4">Dudas / investigaci&oacute;n sobre un tema</option>
					</select>
					<select data-nivel="1" data-valor="9" class="form-element action" style="margin-right: 18px; display: none;">
						<option value="0">-- Tipo --</option>
						<option value="1">Resoluci&oacute;n de ejercicios</option>
						<option value="2">Demostraci&oacute;n</option>						
						<option value="3">AutoCAD</option>						
					</select>
					<select data-nivel="1" data-valor="10" class="form-element action" style="margin-right: 18px; display: none;">
						<option value="0">-- Tipo --</option>
						<option value="1">Redacci&oacute;n sobre un tema</option>
						<option value="2">Comentario de texto</option>						
						<option value="3">Dudas / investigaci&oacute;n sobre un tema</option>						
					</select>
					<select data-nivel="1" data-valor="11" class="form-element action" style="margin-right: 18px; display: none;">
						<option value="0">-- Tipo --</option>
						<option value="1">Resoluci&oacute;n de ejercicios</option>
						<option value="2">Comentario art&iacute;stico</option>						
						<option value="3">Dudas / investigaci&oacute;n sobre un tema</option>						
					</select>
					<select data-nivel="1" data-valor="12" class="form-element action" style="margin-right: 18px; display: none;">
						<option value="0">-- Tipo --</option>
						<option value="1">Resoluci&oacute;n de ejercicios</option>						
						<option value="2">Dudas / investigaci&oacute;n sobre un tema</option>						
					</select>
					<select data-nivel="1" data-valor="13" class="form-element action" style="margin-right: 18px; display: none;">
						<option value="0">-- Tipo --</option>
						<option value="1">Resoluci&oacute;n de ejercicios</option>
						<option value="2">Comentario de texto</option>						
						<option value="3">Dudas / investigaci&oacute;n sobre un tema</option>						
					</select>
					<select data-nivel="1" data-valor="14" class="form-element action" style="margin-right: 18px; display: none;">
						<option value="0">-- Tipo --</option>
						<option value="1">Resoluci&oacute;n de ejercicios</option>
						<option value="2">Demostraci&oacute;n</option>						
						<option value="3">Dudas / investigaci&oacute;n sobre un tema</option>						
					</select>
					<select data-nivel="1" data-valor="15" class="form-element action" style="margin-right: 18px; display: none;">
						<option value="0">-- Tipo --</option>
						<option value="1">Resoluci&oacute;n de ejercicios</option>
						<option value="2">Comentario de texto</option>						
						<option value="3">Dudas / investigaci&oacute;n sobre un tema</option>						
					</select>
					<select data-nivel="1" data-valor="16" class="form-element action" style="margin-right: 18px; display: none;">
						<option value="0">-- Tipo --</option>
						<option value="1">Resoluci&oacute;n de ejercicios</option>
						<option value="2">Redacci&oacute;n sobre un tema</option>												
					</select>
					<select data-nivel="1" data-valor="17" class="form-element action" style="margin-right: 18px; display: none;">
						<option value="0">-- Tipo --</option>
						<option value="1">Resoluci&oacute;n de ejercicios</option>						
						<option value="2">Dudas / investigaci&oacute;n sobre un tema</option>						
					</select>
					<div class="clear"></div>
					<!-- End Nivel 1 -->
					<!-- Base 1 -->
					<p data-nivel="2" class="base1 form-element" data-valor="1" style="display: none;"></p>
					<p data-nivel="2" class="base1 form-element" data-valor="2" style="display: none;">
						Recuerda que la demostraci&oacute;n contendr&aacute; hasta 150 palabras m&aacute;ximo. Seguro que es suficiente!
					</p>
					<!-- End Base 1 -->
					<!-- Base 2 -->
					<p data-nivel="2" class="base2 form-element" data-valor="1" style="display: none;"></p>
					<p data-nivel="2" class="base2 form-element" data-valor="3" style="display: none;">
						Recuerda que los comentarios de texto/art&iacute;sticos contendr&aacute;n hasta 150 palabras como m&aacute;ximo.
					</p>
					<div data-nivel="2" data-valor="4" class="base2 form-element" style="display: none; width: 208px; margin-bottom: 10px;">
						Partes a incluir: <br/>
						<input type="checkbox" class="partesincluir" data-valor="Argumento"> Argumento <br/>
						<input type="checkbox" class="partesincluir" data-valor="Personajes principales"> Personajes principales <br/>
						<input type="checkbox" class="partesincluir" data-valor="Personajes secundarios"> Personajes secundarios <br/>
						<input type="checkbox" class="partesincluir" data-valor="Estilo"> Estilo <br/>
						<input type="checkbox" class="partesincluir" data-valor="Estructura"> Estructura <br/>
						<input type="checkbox" class="partesincluir" data-valor="Conclusion"> Conclusi&oacute;n <br/>
					</div>
					<p data-nivel="2" class="base2 form-element" data-valor="5" style="display: none;">
						Te resolveremos cualquier duda o pregunta que tengas o te hayan mandado acerca de cualquier tema. Recuerda tambi&eacute;n, que no ocupar&aacute; m&aacute;s de 150 palabras!
					</p>
					<!-- End Base 2 -->
					<!-- Base 3 -->
					<p data-nivel="2" class="base3 form-element" data-valor="1" style="display: none;"></p>
					<p data-nivel="2" class="base3 form-element" data-valor="2" style="display: none;">
						Recuerda que la demostraci&oacute;n contendr&aacute; hasta 150 palabras m&aacute;ximo. Seguro que es suficiente!
					</p>
					<!-- End Base 3 -->
					<!-- Base 4 -->
					<p data-nivel="2" class="base4 form-element" data-valor="1" style="display: none;"></p>
					<p data-nivel="2" class="base4 form-element" data-valor="2" style="display: none;">
						Recuerda que la demostraci&oacute;n contendr&aacute; hasta 150 palabras m&aacute;ximo. Seguro que es suficiente!
					</p>
					<!-- End Base 4 -->
					<!-- Base 5 -->
					<p data-nivel="2" class="base5 form-element" data-valor="2" style="display: none;">
						Recuerda que los comentarios de texto/art&iacute;sticos contendr&aacute;n hasta 150 palabras como m&aacute;ximo.
					</p>
					<p data-nivel="2" class="base5 form-element" data-valor="3" style="display: none;">
						Te resolveremos cualquier duda o pregunta que tengas o te hayan mandado acerca de cualquier tema. Recuerda tambi&eacute;n, que no ocupar&aacute; m&aacute;s de 150 palabras!
					</p>
					<!-- End Base 5 -->
					<!-- Base 6 -->
					<p data-nivel="2" class="base6 form-element" data-valor="1" style="display: none;"></p>
					<p data-nivel="2" class="base6 form-element" data-valor="2" style="display: none;">
						Recuerda que la demostraci&oacute;n contendr&aacute; hasta 150 palabras m&aacute;ximo. Seguro que es suficiente!
					</p>
					<!-- End Base 6 -->
					<!-- Base 7 -->
					<p data-nivel="2" class="base7 form-element" data-valor="1" style="display: none;"></p>
					<p data-nivel="2" class="base7 form-element" data-valor="2" style="display: none;">
						Recuerda que la demostraci&oacute;n contendr&aacute; hasta 150 palabras m&aacute;ximo. Seguro que es suficiente!
					</p>
					<!-- End Base 7 -->
					<!-- Base 8 -->
					<p data-nivel="2" class="base8 form-element" data-valor="1" style="display: none;"></p>
					<p data-nivel="2" class="base8 form-element" data-valor="3" style="display: none;">
						Recuerda que los comentarios de texto/art&iacute;sticos contendr&aacute;n hasta 150 palabras como m&aacute;ximo.
					</p>
					<p data-nivel="2" class="base8 form-element" data-valor="4" style="display: none;">
						Te resolveremos cualquier duda o pregunta que tengas o te hayan mandado acerca de cualquier tema. Recuerda tambi&eacute;n, que no ocupar&aacute; m&aacute;s de 150 palabras!
					</p>
					<!-- End Base 8 -->
					<!-- Base 9 -->
					<p data-nivel="2" class="base9 form-element" data-valor="1" style="display: none;"></p>
					<p data-nivel="2" class="base9 form-element" data-valor="2" style="display: none;">
						Recuerda que la demostraci&oacute;n contendr&aacute; hasta 150 palabras m&aacute;ximo. Seguro que es suficiente!
					</p>
					<select data-nivel="2" data-valor="3" class="base9 form-element" style="margin-right: 18px; display: none;">
						<option value="0">-- Tipo --</option>
						<option value="1">3D a partir de vistas</option>						
						<option value="2">vistas a partir de 3D</option>
						<option value="3">Ejercicios</option>						
					</select>
					<!-- End Base 9 -->
					<!-- Base 10 -->					
					<p data-nivel="2" class="base10 form-element" data-valor="2" style="display: none;">
						Recuerda que los comentarios de texto/art&iacute;sticos contendr&aacute;n hasta 150 palabras como m&aacute;ximo.
					</p>
					<p data-nivel="2" class="base10 form-element" data-valor="3" style="display: none;">
						Te resolveremos cualquier duda o pregunta que tengas o te hayan mandado acerca de cualquier tema. Recuerda tambi&eacute;n, que no ocupar&aacute; m&aacute;s de 150 palabras!
					</p>
					<!-- End Base 10 -->
					<!-- Base 11 -->
					<p data-nivel="2" class="base11 form-element" data-valor="1" style="display: none;"></p>					
					<p data-nivel="2" class="base11 form-element" data-valor="2" style="display: none;">
						Recuerda que los comentarios de texto/art&iacute;sticos contendr&aacute;n hasta 150 palabras como m&aacute;ximo.
					</p>
					<p data-nivel="2" class="base11 form-element" data-valor="3" style="display: none;">
						Te resolveremos cualquier duda o pregunta que tengas o te hayan mandado acerca de cualquier tema. Recuerda tambi&eacute;n, que no ocupar&aacute; m&aacute;s de 150 palabras!
					</p>
					<!-- End Base 11 -->
					<!-- Base 12 -->
					<p data-nivel="2" class="base12 form-element" data-valor="1" style="display: none;"></p>										
					<p data-nivel="2" class="base12 form-element" data-valor="2" style="display: none;">
						Te resolveremos cualquier duda o pregunta que tengas o te hayan mandado acerca de cualquier tema. Recuerda tambi&eacute;n, que no ocupar&aacute; m&aacute;s de 150 palabras!
					</p>
					<!-- End Base 12 -->
					<!-- Base 13 -->
					<p data-nivel="2" class="base13 form-element" data-valor="1" style="display: none;"></p>					
					<p data-nivel="2" class="base13 form-element" data-valor="2" style="display: none;">
						Recuerda que los comentarios de texto/art&iacute;sticos contendr&aacute;n hasta 150 palabras como m&aacute;ximo.
					</p>
					<p data-nivel="2" class="base13 form-element" data-valor="3" style="display: none;">
						Te resolveremos cualquier duda o pregunta que tengas o te hayan mandado acerca de cualquier tema. Recuerda tambi&eacute;n, que no ocupar&aacute; m&aacute;s de 150 palabras!
					</p>
					<!-- End Base 13 -->
					<!-- Base 14 -->
					<p data-nivel="2" class="base14 form-element" data-valor="1" style="display: none;"></p>					
					<p data-nivel="2" class="base14 form-element" data-valor="2" style="display: none;">
						Recuerda que la demostraci&oacute;n contendr&aacute; hasta 150 palabras m&aacute;ximo. Seguro que es suficiente!
					</p>
					<p data-nivel="2" class="base14 form-element" data-valor="3" style="display: none;">
						Te resolveremos cualquier duda o pregunta que tengas o te hayan mandado acerca de cualquier tema. Recuerda tambi&eacute;n, que no ocupar&aacute; m&aacute;s de 150 palabras!
					</p>
					<!-- End Base 14 -->
					<!-- Base 15 -->
					<p data-nivel="2" class="base15 form-element" data-valor="1" style="display: none;"></p>					
					<p data-nivel="2" class="base15 form-element" data-valor="2" style="display: none;">
						Recuerda que los comentarios de texto/art&iacute;sticos contendr&aacute;n hasta 150 palabras como m&aacute;ximo.
					</p>
					<p data-nivel="2" class="base15 form-element" data-valor="3" style="display: none;">
						Te resolveremos cualquier duda o pregunta que tengas o te hayan mandado acerca de cualquier tema. Recuerda tambi&eacute;n, que no ocupar&aacute; m&aacute;s de 150 palabras!
					</p>
					<!-- End Base 15 -->
					<!-- Base 16 -->
					<p data-nivel="2" class="base16 form-element" data-valor="1" style="display: none;"></p>									
					<!-- End Base 16 -->
					<!-- Base 17 -->
					<p data-nivel="2" class="base17 form-element" data-valor="1" style="display: none;"></p>
					<p data-nivel="2" class="base17 form-element" data-valor="2" style="display: none;">
						Te resolveremos cualquier duda o pregunta que tengas o te hayan mandado acerca de cualquier tema. Recuerda tambi&eacute;n, que no ocupar&aacute; m&aacute;s de 150 palabras!
					</p>
					<!-- End Base 17 -->
					<div class="clear"></div>
					<div class="wrapper-dnd-area">
						<div id="colegiosuscrito-dnd-area" class="dnd-area">
							<form id="colegiosuscrito-form" name="colegio-form" action="<?php bloginfo("url") ?>/wp-admin/admin-ajax.php" method="post" enctype="multipart/form-data">
								<input type="hidden" name="action" value="MyAjaxFunctions">
								<input type="hidden" name="toaction" value="uploadArchive">
								<input id="colegiosuscrito-form-post-id" type="hidden" name="post_id" value="">
								<input id="colegiosuscrito-file" type="file" style="display:none;" name="fileselect">
								<?php wp_nonce_field( 'fileselect', 'fileselect_nonce' ); ?>												
							</form>
						</div>
						<div id="colegiosuscrito-details"></div>
					</div>
					<div class="points" style="float: left; width: 190px; text-align: center; margin-top: 10px; color: #838383;">										
						<a class="btn btn-colegiosuscrito" href="#" title="Pru&eacute;balo!">Pru&eacute;balo!</a>
					</div>
				</div>
				<?php 
				}
				?>
				<?php 
				if ($nivel == 1) { 
				?>
				<div class="universidad-pedido form-pedido" style="">				
					<!-- Nivel 0 -->
					<select data-nivel="0" class="form-element action">
						<option value="0">-- Tipo de Pedido --</option>
						<option value="1">Trabajos</option>
						<option value="2">AutoCAD</option>
						<option value="3">Apuntes</option>
						<option value="4">PowerPoint</option>
						<option value="5">Exposiciones</option>						
					</select>
					<div class="clear"></div>
					<!-- End Nivel 0 -->
					<!-- Nivel 1 -->
					<!-- Valor 1 -->
					<input data-nivel="1" data-valor="1" class="form-element titulo" type="text" placeholder="T&iacute;tulo del trabajo" style="display: none; margin-right: 18px;margin-bottom: 12px;">
					<select data-nivel="1" data-valor="1" class="form-element action tema" style="margin-right: 80px; margin-bottom: 8px; display: none; margin-bottom: 12px;">
						<option value="0">-- Tema --</option>
						<option value="1">Aeron&aacute;utica</option>
						<option value="2">Agronom&iacute;a</option>
						<option value="3">Antropolog&iacute;a</option>
						<option value="4">Arqueolog&iacute;a</option>
						<option value="5">Arquitectura</option>
						<option value="6">Arte</option>
						<option value="7">Astrolog&iacute;a</option>
						<option value="8">Astronom&iacute;a</option>
						<option value="9">Automoci&oacute;n</option>
						<option value="10">Avi&oacute;nica</option>
						<option value="11">Biblioteconom&iacute;a</option>
						<option value="12">Biograf&iacute;a</option>
						<option value="13">Biolog&iacute;a</option>
						<option value="14">Bioqu&iacute;mica</option>
						<option value="15">Bot&aacute;nica</option>
						<option value="16">Contabilidad</option>
						<option value="17">Deporte y Ciencias de la Actividad F&iacute;sica</option>
						<option value="18">Derecho</option>
						<option value="19">Documentaci&oacute;n</option>
						<option value="20">Ecolog&iacute;a y Medio Ambiente</option>
						<option value="21">Econom&iacute;a y Empresa</option>
						<option value="22">Educaci&oacute;n F&iacute;sica</option>
						<option value="23">Educaci&oacute;n y Pedagog&iacute;a</option>
						<option value="24">Electr&oacute;nica y Electricidad</option>
						<option value="25">Enfermer&iacute;a</option>
						<option value="26">Estad&iacute;stica</option>
						<option value="27">&Eacute;tica y Moral</option>
						<option value="28">Farmacia</option>
						<option value="29">Filolog&iacute;a</option>
						<option value="30">Filosof&iacute;a</option>
						<option value="31">F&iacute;sica</option>
						<option value="32">Gastronom&iacute;a y Restauraci&oacute;n</option>
						<option value="33">Gen&eacute;tica</option>
						<option value="34">Geograf&iacute;a</option>
						<option value="35">Geolog&iacute;a</option>
						<option value="36">Historia</option>
						<option value="37">Hosteler&iacute;a</option>
						<option value="38">Imagen y Audiovisuales</option>
						<option value="39">Industria</option>
						<option value="40">Inform&aacute;tica</option>
						<option value="41">Ingenier&iacute;a</option>
						<option value="42">Juegos y Animaci&oacute;n</option>
						<option value="43">Lenguaje y Gram&aacute;tica</option>
						<option value="44">Literatura</option>
						<option value="45">Matem&aacute;ticas</option>
						<option value="46">Materiales</option>
						<option value="47">Mitolog&iacute;a</option>
						<option value="48">M&uacute;sica</option>
						<option value="49">Na&uacute;tica/Naval</option>
						<option value="50">Nutrici&oacute;n y Diet&eacute;tica</option>
						<option value="51">Obras y Construcci&oacute;n</option>
						<option value="52">&oacute;ptica y Optometr&iacute;a</option>
						<option value="53">Organizaci&oacute;n de Empresas</option>
						<option value="54">Periodismo</option>
						<option value="55">Pol&iacute;tica y Administraci&oacute;n P&uacute;blica</option>
						<option value="56">Psicolog&iacute;a</option>
						<option value="57">Publicidad y Marketing</option>
						<option value="58">Qu&iacute;mica</option>
						<option value="59">Qu&iacute;mica</option>
						<option value="60">Recursos Forestales</option>
						<option value="61">Recursos Humanos</option>
						<option value="62">Relaciones Laborales</option>
						<option value="63">Relaciones P&uacute;blicas</option>
						<option value="64">Religi&oacute;n y Creencias</option>
						<option value="65">Salud</option>
						<option value="66">Sociolog&iacute;a y Trabajo Social</option>
						<option value="67">Sonido</option>
						<option value="68">Tecnolog&iacute;a</option>
						<option value="69">Telecomunicaciones</option>
						<option value="70">Termodin&aacute;mica</option>
						<option value="71">Topograf&iacute;a</option>
						<option value="72">Traducci&oacute;n e Interpretaci&oacute;n</option>
						<option value="73">Turismo</option>
						<option value="74">Zoolog&iacute;a</option>
					</select>
					<div class="span-element"><span style="display: none;" data-nivel="1" data-valor="1" class="form-element">Nº de p&aacute;ginas </span></div>
					<input data-nivel="1" data-valor="1" type="text" class="spinner1 form-element paginas" style="margin-right: 73px; margin-left: 3px; display: none;">
					<select data-nivel="1" data-valor="1" class="form-element tiempo" style="margin-right: 0px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="24">24 horas</option>
						<option value="48">2 dias</option>
						<option value="72">3 dias</option>
						<option value="96">4 dias</option>
						<option value="120">5 dias</option>
						<option value="144">6 dias</option>
						<option value="168">7 dias</option>
						<option value="192">8 dias</option>
						<option value="216">9 dias</option>
						<option value="240">10 dias</option>
						<option value="264">11 dias</option>
						<option value="288">12 dias</option>
						<option value="312">13 dias</option>
						<option value="336">14 dias</option>
						<option value="360">15 dias</option>
						<option value="384">16 dias</option>
						<option value="408">17 dias</option>
						<option value="432">18 dias</option>
						<option value="456">19 dias</option>
						<option value="480">20+ dias</option>
					</select>
					<select data-nivel="1" data-valor="1" class="form-element bibliografia" style="margin-right: 18px; display: none;">
						<option value="0">-- Bibliograf&iacute;a --</option>
						<option value="1">Si</option>
						<option value="2">No</option>						
					</select>
					<textarea data-nivel="1" data-valor="1" class="form-element observaciones" style="margin-right: 0px; display: none;" placeholder="Observaciones"></textarea>
					<!-- End Valor 1 -->
					<!-- Valor 2 -->
					<div class="span-element"><span style="display: none;" data-nivel="1" data-valor="2" class="form-element">Nº de piezas a dibujar </span></div>										
					<input data-nivel="1" data-valor="2" type="text" class="spinner1 form-element piezas" style="margin-right: 28px; margin-left: 3px; display: none; margin-bottom: 10px;">
					<select data-nivel="1" data-valor="2" class="form-element vistas" style="margin-right: 0px; display: none;">
						<option value="0">-- Vistas --</option>
						<option value="1.1">Si</option>
						<option value="1">No</option>						
					</select>
					<select data-nivel="1" data-valor="2" class="form-element acotacion" style="margin-right: 18px; display: none;">
						<option value="0">-- Acotaci&oacute;n --</option>
						<option value="1.5">Si</option>
						<option value="1">No</option>						
					</select>
					<select data-nivel="1" data-valor="2" class="form-element tiempo" style="margin-right: 0px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="24">24 horas</option>
						<option value="48">2 dias</option>
						<option value="72">3 dias</option>
						<option value="96">4 dias</option>
						<option value="120">5 dias</option>
						<option value="144">6 dias</option>
						<option value="168">7 dias</option>
						<option value="192">8 dias</option>
						<option value="216">9 dias</option>
						<option value="240">10 dias</option>
						<option value="264">11 dias</option>
						<option value="288">12 dias</option>
						<option value="312">13 dias</option>
						<option value="336">14 dias</option>
						<option value="360">15 dias</option>
						<option value="384">16 dias</option>
						<option value="408">17 dias</option>
						<option value="432">18 dias</option>
						<option value="456">19 dias</option>
						<option value="480">20+ dias</option>
					</select>
					<textarea data-nivel="1" data-valor="2" class="form-element observaciones" style="margin-right: 0px; display: none;" placeholder="Observaciones"></textarea>
					<!-- End Valor 2 -->
					<!-- Valor 3 -->
					<select data-nivel="1" data-valor="3" class="form-element action subiropaquete" style="margin-right: 18px; display: none;">
						<option value="0">-- Tipo --</option>
						<option value="1">Subir a la Web</option>
						<option value="2">Paqueter&iacute;a</option>						
					</select>
					<!-- End Valor 3 -->
					<!-- Valor 4 -->
					<select data-nivel="1" data-valor="4" class="form-element action diapositivaotexto" style="margin-right: 18px; display: none;">
						<option value="0">-- Tipo --</option>
						<option value="1">Diapositivas a Texto</option>
						<option value="2">Texto a Diapositivas</option>						
					</select>
					<!-- End Valor 4 -->
					<!-- Valor 5 -->
					<select data-nivel="1" data-valor="5" class="form-element action partiendo" style="margin-right: 18px; display: none;">
						<option value="0">-- Tipo --</option>
						<option value="1">A partir de un trabajo escrito</option>
						<option value="2">Partiendo de cero</option>						
					</select>
					<!-- End Valor 5 -->
					<!-- End Nivel 1 -->
					<!-- Base 3 -->
					<div class="span-element"><span style="display: none;" data-nivel="2" data-valor="1" class="base3 form-element">Nº P&aacute;ginas a sucio </span></div>										
					<input data-nivel="2" data-valor="1" type="text" class="base3 spinner1 form-element paginassubir" style="margin-right: 20px; margin-left: 3px; display: none; margin-bottom: 12px;">
					<select data-nivel="2" data-valor="1" class="base3 form-element tiemposubir" style="margin-right: 0px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="120">5 dias</option>
						<option value="240">10 dias</option>
						<option value="360">15 dias</option>
						<option value="480">20 dias</option>
						<option value="500">20+ dias</option>
					</select>		
					<div class="span-element"><span style="display: none;" data-nivel="2" data-valor="2" class="base3 form-element">Nº P&aacute;ginas a sucio </span></div>										
					<input data-nivel="2" data-valor="2" type="text" class="base3 spinner1 form-element paginaspaqueteria" style="margin-right: 20px; margin-left: 3px; display: none; margin-bottom: 12px;">
					<select data-nivel="2" data-valor="2" class="base3 form-element tiempopaqueteria" style="margin-right: 0px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="120">5 dias</option>
						<option value="240">10 dias</option>
						<option value="360">15 dias</option>
						<option value="480">20 dias</option>
						<option value="500">20+ dias</option>
					</select>				
					<!-- End Base 3 -->
					<!-- Base 4 -->
					<div class="span-element"><span style="display: none;" data-nivel="2" data-valor="1" class="base4 form-element">Nº Diapositivas </span></div>										
					<input data-nivel="2" data-valor="1" type="text" class="base4 spinner1 form-element diapositivascantidad" style="margin-right: 40px; margin-left: 3px; display: none; margin-bottom: 12px;">
					<select data-nivel="2" data-valor="1" class="base4 form-element tiempodiapositivas" style="margin-right: 0px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="120">5 dias</option>
						<option value="240">10 dias</option>
						<option value="360">15 dias</option>
						<option value="480">20 dias</option>
						<option value="500">20+ dias</option>
					</select>		
					<div class="span-element"><span style="display: none;" data-nivel="2" data-valor="2" class="base4 form-element">Nº P&aacute;ginas </span></div>										
					<input data-nivel="2" data-valor="2" type="text" class="base4 spinner1 form-element paginascantidad" style="margin-right: 40px; margin-left: 3px; display: none; margin-bottom: 12px;">
					<select data-nivel="2" data-valor="2" class="base4 form-element tiempopaginas" style="margin-right: 0px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="120">5 dias</option>
						<option value="240">10 dias</option>
						<option value="360">15 dias</option>
						<option value="480">20 dias</option>
						<option value="500">20+ dias</option>
					</select>				
					<!-- End Base 4 -->
					<!-- Base 5 -->
					<div class="span-element"><span style="display: none;" data-nivel="2" data-valor="1" class="base5 form-element">Nº Diapositivas </span></div>										
					<input data-nivel="2" data-valor="1" type="text" class="base5 spinner1 form-element diapositivaspartiendoescrito" style="margin-right: 46px; margin-left: 3px; display: none; margin-bottom: 12px;">
					<select data-nivel="2" data-valor="1" class="base5 form-element action partehabladapartiendoescrito" style="margin-right: 18px; display: none;">
						<option value="0">-- Incluir parte hablada --</option>
						<option value="1">Si</option>
						<option value="2">No</option>						
					</select>
					<select data-nivel="2" data-valor="1" class="base5 form-element tiempopartiendoescrito" style="margin-right: 0px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="120">5 dias</option>
						<option value="240">10 dias</option>
						<option value="360">15 dias</option>
						<option value="480">20 dias</option>
						<option value="500">20+ dias</option>
					</select>
					<div data-nivel="2" data-valor="1" class="base5 form-element barratextoimagenes" style="float: left; position: relative; width: 430px; height: 80px; margin-right: 0px; display: none; margin-bottom: 15px;">				
						<span class="bartexttitle" style="position: absolute; top: 6px; left: 10px; font-size: 12px; color: black; line-height: 12px;">Indica que quieres que predomine</span>
						<span class="bartexttexto" style="position: absolute; top: 36px; left: 10px; font-size: 9px;">Texto</span>
						<span class="bartextimagenes" style="position: absolute; top: 36px; right: 4px; font-size: 9px;">Im&aacute;genes</span>
						<span class="bartextbalanceado" style="position: absolute; top: 36px; left: 191px; font-size: 9px;">Balanceado</span>
						<span class="barratextoimagenes-left" style="position: absolute; top: 55px; left: 12px; font-size: 9px; width: 20px; height: 20px; cursor: pointer;">&nbsp;</span>
						<span class="barratextoimagenes-right" style="position: absolute; top: 55px; right: 12px; font-size: 9px; width: 20px; height: 20px; cursor: pointer;">&nbsp;</span>
						<span class="barratextoimagenes-center" style="position: absolute; top: 55px; left: 206px; font-size: 9px; width: 20px; height: 20px; cursor: pointer;">&nbsp;</span>
						<span class="barratextoimagenes-handle" data-info="Balanceado" style="position: absolute; top: 55px; left: 206px; font-size: 9px;"><img style="cursor: pointer" src="<?php echo get_template_directory_uri(); ?>/images/bar-component-handle.png" alt=""></span>
					</div>		
					<div class="span-element"><span style="display: none;" data-nivel="2" data-valor="2" class="base5 form-element">Nº Diapositivas </span></div>										
					<input data-nivel="2" data-valor="2" type="text" class="base5 spinner1 form-element diapositivaspartiendocero" style="margin-right: 46px; margin-left: 3px; display: none; margin-bottom: 12px;">
					<select data-nivel="2" data-valor="2" class="base5 form-element action partehabladapartiendocero" style="margin-right: 18px; display: none;">
						<option value="0">-- Incluir parte hablada --</option>
						<option value="1">Si</option>
						<option value="2">No</option>						
					</select>
					<select data-nivel="2" data-valor="2" class="base5 form-element tiempopartiendocero" style="margin-right: 0px; display: none;">
						<option value="0">-- Tiempo de entrega --</option>
						<option value="120">5 dias</option>
						<option value="240">10 dias</option>
						<option value="360">15 dias</option>
						<option value="480">20 dias</option>
						<option value="500">20+ dias</option>
					</select>	
					<div data-nivel="2" data-valor="2" class="base5 form-element barratextoimagenes" style="float: left; position: relative; width: 430px; height: 80px; margin-right: 0px; display: none; margin-bottom: 15px;">				
						<span class="bartexttitle" style="position: absolute; top: 6px; left: 10px; font-size: 12px; color: black; line-height: 12px;">Indica que quieres que predomine</span>
						<span class="bartexttexto" style="position: absolute; top: 36px; left: 10px; font-size: 9px;">Texto</span>
						<span class="bartextimagenes" style="position: absolute; top: 36px; right: 4px; font-size: 9px;">Im&aacute;genes</span>
						<span class="bartextbalanceado" style="position: absolute; top: 36px; left: 191px; font-size: 9px;">Balanceado</span>
						<span class="barratextoimagenes-left" style="position: absolute; top: 55px; left: 12px; font-size: 9px; width: 20px; height: 20px; cursor: pointer;">&nbsp;</span>
						<span class="barratextoimagenes-right" style="position: absolute; top: 55px; right: 12px; font-size: 9px; width: 20px; height: 20px; cursor: pointer;">&nbsp;</span>
						<span class="barratextoimagenes-center" style="position: absolute; top: 55px; left: 206px; font-size: 9px; width: 20px; height: 20px; cursor: pointer;">&nbsp;</span>
						<span class="barratextoimagenes-handle" data-info="Balanceado" style="position: absolute; top: 55px; left: 206px; font-size: 9px;"><img style="cursor: pointer" src="<?php echo get_template_directory_uri(); ?>/images/bar-component-handle.png" alt=""></span>
					</div>			
					<!-- End Base 5 -->
					<div class="clear"></div>
					<!--<img src="<?php echo get_template_directory_uri(); ?>/images/bg-drop.png" style="cursor: pointer; margin: 10px 20px 0px 0px; float: left;" alt="">-->
					<div class="wrapper-dnd-area">
						<div id="universidad-dnd-area" class="dnd-area">
							<form id="universidad-form" name="universidad-form" action="<?php bloginfo("url") ?>/wp-admin/admin-ajax.php" method="post" enctype="multipart/form-data">
								<input type="hidden" name="action" value="MyAjaxFunctions">
								<input type="hidden" name="toaction" value="uploadArchive">
								<input id="universidad-form-post-id" type="hidden" name="post_id" value="">
								<input id="universidad-file" type="file" style="display:none;" name="fileselect">
								<?php wp_nonce_field( 'fileselect', 'fileselect_nonce' ); ?>												
							</form>
						</div>
						<div id="universidad-details"></div>
					</div>
					<div class="points" style="float: left; width: 190px; text-align: center; margin-top: 10px; color: #838383;">
						<span class="price">Precio por este pedido<br/> <strong style="font-family: OSBold; font-size: 16px; color: black;">0 puntos</strong></span>					
						<a class="btn" href="#" title="Pru&eacute;balo!">Pru&eacute;balo!</a>
					</div>
				</div>
				<?php 
				}
				?>
				<div class="clear"></div>
			</div>
		</div>
		<div class="clear"></div>
	</div>

	<?php get_footer(); 

	} else {
  	?>
  	<html>
  		<head>
  			<script type="text/javascript">      
            	window.location = "<?php echo wp_login_url(); ?>";        
          	</script>
  		</head>
  		<body></body>
  	</html>
  	<?
  }
	?>