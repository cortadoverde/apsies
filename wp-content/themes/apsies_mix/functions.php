<?php 

global $theme_base_dir;
$theme_base_dir = dirname(__FILE__);
include($theme_base_dir . '/inc/content-functions.php');
include($theme_base_dir . '/inc/content-types.php');
include($theme_base_dir . '/inc/ajax-requests.php');

/*******************************************************************
	Site Title
*******************************************************************/
function allthemes_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Página %s', 'apsies' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'allthemes_wp_title', 10, 2 );
/*******************************************************************
	End Title
*******************************************************************/

/*
	Adding menu support
*/
add_theme_support( 'menus' );
register_nav_menu('mainmenu', 'Menu Principal');
register_nav_menu('footermenu', 'Menu Footer');

function tml_edit_user_profile( $profileuser ) { ?>
<p>
<label for="colegio">Colegio</label>
<input id="colegio" type="text" name="colegio" value="<?php echo $profileuser->colegio; ?>" />
</p>
<p>
<label for="universidad">Universidad</label>
<input id="universidad" type="text" name="universidad" value="<?php echo $profileuser->universidad; ?>" />
</p>
<p>
<label for="ciudad">Ciudad</label>
<input id="ciudad" type="text" name="ciudad" value="<?php echo $profileuser->ciudad; ?>" />
</p>
<p>
<label for="edad">Edad</label>
<input id="edad" type="text" name="edad" value="<?php echo $profileuser->edad; ?>" />
</p>
<?php
}
add_action( 'edit_user_profile', 'tml_edit_user_profile' );

function tml_user_register( $user_id ) {
	//if ( !empty( $_POST['colegio'] ) )
		update_user_meta( $user_id, 'colegio', $_POST['colegio'] );
	//if ( !empty( $_POST['universidad'] ) )
		update_user_meta( $user_id, 'universidad', $_POST['universidad'] );
	//if ( !empty( $_POST['ciudad'] ) )
		update_user_meta( $user_id, 'ciudad', $_POST['ciudad'] );
	//if ( !empty( $_POST['edad'] ) )
		update_user_meta( $user_id, 'edad', $_POST['edad'] );
	//if ( !empty( $_POST['nivel'] ) )
		update_user_meta( $user_id, 'nivel', $_POST['nivel'] );

		update_user_meta( $user_id, 'fechasuscripcion', 0 );
		update_user_meta( $user_id, 'diassuscripcion', 0 );
		update_user_meta( $user_id, 'puntosusuario', 0 );
}
add_action( 'user_register', 'tml_user_register' );

function tml_user_save( $user_id ) {
	//if ( !empty( $_POST['colegio'] ) )
		update_user_meta( $user_id, 'colegio', $_POST['colegio'] );
	//if ( !empty( $_POST['universidad'] ) )
		update_user_meta( $user_id, 'universidad', $_POST['universidad'] );
	//if ( !empty( $_POST['ciudad'] ) )
		update_user_meta( $user_id, 'ciudad', $_POST['ciudad'] );
	//if ( !empty( $_POST['edad'] ) )
		update_user_meta( $user_id, 'edad', $_POST['edad'] );
	//if ( !empty( $_POST['nivel'] ) )
		update_user_meta( $user_id, 'nivel', $_POST['nivel'] );
}
add_action( 'personal_options_update', 'tml_user_save' );
add_action( 'edit_user_profile_update', 'tml_user_save' );

// sending email when new ticket is created
function my_custom_publish_ticket( $post_id ) {
	if ( "ticket" == $_POST['post_type'] ) {
		// Getting relative pedido
		$pedido_id = get_post_meta($post_id, "pedidoticket", true);
		// Getting relative user
		$user_id = get_post_meta($pedido_id, "usuario", true);
		// Getting user data
		$user_info = get_userdata($user_id);
		$nickname = $user_info->user_login;
		$email = $user_info->user_email;
		// Correo al usuario //////////////////////////////////////
		// Varios destinatarios
		$para = $email;
		// subject
		$titulo = 'Nuevo ticket enviado desde Apsies';
		// message
		$mensaje = 
"Se le ha enviado un nuevo ticket desde Apsies. Visite el siguiente enlace para poder ver el ticket: \n
" . get_permalink($post_id) . " \n
\n
Apsies
";
		// Para enviar un correo HTML mail, la cabecera Content-type debe fijarse
		$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
		// Cabeceras adicionales
		$cabeceras .= 'To: '. $nickname . ' <' . $email . '>' . "\r\n";
		$cabeceras .= 'From: Apsies <admin@apsies.com>' . "\r\n";
		// Mail it
		mail($para, $titulo, $mensaje, $cabeceras);
		// End Correo al usuario //////////////////////////////////////
		
	}
}
add_action( 'save_post', 'my_custom_publish_ticket' );

// sending email when a solutions has been uploaded to a pedido
function my_custom_publish_pedido( $post_id ) {
	if ( "pedido" == $_POST['post_type'] ) {
		// Getting solution
		$archivourl = get_field("solucion");
		if ($archivourl != "") {
			// Getting user data
			$user_info = get_userdata($user_id);
			$nickname = $user_info->user_login;
			$email = $user_info->user_email;
			// Correo al usuario //////////////////////////////////////
			// Varios destinatarios
			$para = $email;
			// subject
			$titulo = 'Solucion recibida desde Apsies';
			// message
			$mensaje = 
"Se le ha enviado una solucion de un pedido realizado por Ud. desde Apsies. Visite el siguiente enlace para acceder a la solucion a traves del historial de pedidos: \n
" . get_permalink($post_id) . " \n
\n
Apsies
";
			// Para enviar un correo HTML mail, la cabecera Content-type debe fijarse
			$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
			// Cabeceras adicionales
			$cabeceras .= 'To: '. $nickname . ' <' . $email . '>' . "\r\n";
			$cabeceras .= 'From: Apsies <admin@apsies.com>' . "\r\n";
			// Mail it
			mail($para, $titulo, $mensaje, $cabeceras);
			// End Correo al usuario //////////////////////////////////////
			update_post_meta($post_id, "solucionnueva", "yes");
		} else {
			update_post_meta($post_id, "solucionnueva", "no");
		}
	}
}
add_action( 'save_post', 'my_custom_publish_pedido' );

// Adding columns to pedido admin page
add_filter( 'manage_edit-pedido_columns', 'my_edit_pedido_columns' ) ;
function my_edit_pedido_columns( $columns ) {

	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Título' ),
		'estado' => __( 'Estado' ),
		'date' => __( 'Fecha' )
	);

	return $columns;
}

add_action( 'manage_pedido_posts_custom_column', 'my_manage_pedido_columns', 10, 2 );
function my_manage_pedido_columns( $column, $post_id ) {
	global $post;

	switch( $column ) {

		/* If displaying the 'estado' column. */
		case 'estado' :

			/* Get the post meta. */
			$estado = get_post_meta( $post_id, 'estado', true );

			echo $estado;

			break;

		/* Just break out of the switch statement for everything else. */
		default :
			break;
	}
}

add_filter( 'manage_edit-pedido_sortable_columns', 'my_pedido_sortable_columns' );
function my_pedido_sortable_columns( $columns ) {

	$columns['estado'] = 'estado';

	return $columns;
}

/* Only run our customization on the 'edit.php' page in the admin. */
add_action( 'load-edit.php', 'my_edit_pedido_load' );
function my_edit_pedido_load() {
	add_filter( 'request', 'my_sort_pedido' );
}

/* Sorts the movies. */
function my_sort_pedido( $vars ) {

	/* Check if we're viewing the 'pedido' post type. */
	if ( isset( $vars['post_type'] ) && 'pedido' == $vars['post_type'] ) {

		/* Check if 'orderby' is set to 'estado'. */
		if ( isset( $vars['orderby'] ) && 'estado' == $vars['orderby'] ) {

			/* Merge the query vars with our custom variables. */
			$vars = array_merge(
				$vars,
				array(
					'meta_key' => 'estado',
					'orderby' => 'meta_value'
				)
			);
		}
	}

	return $vars;
}

/*
	Get new tickets
*/
function getNewTickets($user_id) {
	$args = array( 'posts_per_page' => -1, 'post_type'=> 'ticket', user );
	$query = new WP_Query( $args );	
	$count = 0;
	if ( $query->have_posts() )	{			
		while ( $query->have_posts() ) {
			$query->the_post();
			// Getting relative pedido
			$pedido_id = get_post_meta(get_the_ID(), "pedidoticket", true);
			// Getting relative user
			$pedido_user_id = get_post_meta($pedido_id, "usuario", true);
			if ($user_id == $pedido_user_id) {
				$ticket_visto = get_post_meta(get_the_ID(), "ticketvisto", true);
				if ($ticket_visto != "yes") {
					$count++;
				}				
			}
		}							
	}
	wp_reset_postdata();
	return $count;
}

/*
	Get solutions arrived
*/
function getNewSolutions($user_id) {
	$args = array( 'posts_per_page' => -1, 'post_type'=> 'pedido', user );
	$query = new WP_Query( $args );	
	$count = 0;
	if ( $query->have_posts() )	{			
		while ( $query->have_posts() ) {
			$query->the_post();
			// Getting relative pedido
			$pedido_id = get_the_ID();
			// Getting relative user
			$pedido_user_id = get_post_meta($pedido_id, "usuario", true);
			if ($user_id == $pedido_user_id) {
				$solucion_nueva = get_post_meta(get_the_ID(), "solucionnueva", true);
				if ($solucion_nueva == "yes") {
					$count++;
				}				
			}
		}							
	}
	wp_reset_postdata();
	return $count;
}

// 
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');

add_action('comment_post', 'newcomment', 20, 2);
function newcomment($comment_id, $approved) {
    $user_id = get_current_user_id();

    if ($user_id == 1) {
    	// Getting comment data
    	$commentObj = get_comment( $comment_id );
    	$post_id = $commentObj->comment_post_ID;
    	// Getting relative pedido
		$pedido_id = get_post_meta($post_id, "pedidoticket", true);
		// Getting relative user
		$ruser_id = get_post_meta($pedido_id, "usuario", true);
    	// Correo al usuario //////////////////////////////////////
    	// Getting user data
		$user_info = get_userdata($ruser_id);
		$nickname = $user_info->user_login;
		$email = $user_info->user_email;
		// Varios destinatarios
		$para = $email;
		// subject
		$titulo = 'Nuevo comentario recibido desde Apsies';
		// message
		$mensaje = 
"Ha recibido un nuevo comentario del Administrador desde Apsies relacionado a un Ticket, visite el siguiente enlace para verlo: \n
" . get_permalink($post_id) . " \n
\n
Apsies
";
		// Para enviar un correo HTML mail, la cabecera Content-type debe fijarse
		$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
		// Cabeceras adicionales
		$cabeceras .= 'To: '. $nickname . ' <' . $email . '>' . "\r\n";
		$cabeceras .= 'From: Apsies <admin@apsies.com>' . "\r\n";
		// Mail it
		mail($para, $titulo, $mensaje, $cabeceras);
		// End Correo al usuario //////////////////////////////////////
    }    

}

function freeLeft($user_id) {
	$cont = 0;
	$cant = getOption("eto_valorpedidosgratiscolegiosus", 3);
	$args = array( 'numberposts' => -1, 'post_type'=> 'pedido' );
	$query = new WP_Query( $args );	
	if ( $query->have_posts() )	{			
		while ( $query->have_posts() ) {
			$query->the_post();
			// Getting project thumbnail
			$post_id = get_the_ID();
						    
			if (get_post_meta($post_id, "pedido_suscrito", true) == "1") {
				if (get_post_meta($post_id, 'usuario', true) == $user_id) {
					$post_date = get_the_date('Y/m/d');
					$today = date('Y/m/d');
					if ($post_date == $today) {
						$cont++;
					}
				}
			}
		}
	}
	wp_reset_postdata();
	return $post_date;
}

// // check compatibility
// if (version_compare(PHP_VERSION, '5.3', '>=')) {

//     // bootstrap warp
//     $warp = require(__DIR__.'/warp.php');

//     $warp['apsies']->init();
// }
