<?php

/* ----------------------------------------
* To retrieve a value use: $eto_options[$prefix.'var']
----------------------------------------- */

$prefix = 'eto_';

/* ----------------------------------------
* Create the TABS
----------------------------------------- */

$eto_custom_tabs = array(
		array(
			'label'=> __('Página Principal', 'eto'),
			'id'	=> $prefix.'home'
		),
		array(
			'label'=> __('Footer', 'eto'),
			'id'	=> $prefix.'footer'
		),
		array(
			'label'=> __('Barra lateral', 'eto'),
			'id'	=> $prefix.'sidebar'
		),
		array(
			'label'=> __('Página Pedidos', 'eto'),
			'id'	=> $prefix.'pedidos'
		),
		array(
			'label'=> __('Valores', 'eto'),
			'id'	=> $prefix.'valores'
		)

	);

/* ----------------------------------------
* Options Field Array
----------------------------------------- */

$eto_custom_meta_fields = array(

	/* -- TAB 1 -- */
	array(
		'id'	=> $prefix.'home', // Use data in $eto_custom_tabs
		'type'	=> 'tab_start'
	),
	
	array(
		'label'=> '"BANNER"',
		'id'	=> $prefix.'home4modules',
		'type'	=> 'title'
	),
	array(
		'label'=> 'Imagen',
		'id'	=> $prefix.'bannerImg',
		'type'	=> 'image',
		'desc' => 'Imagen del Banner.'
	),
	array(
		'label'=> 'Fondo',
		'id'	=> $prefix.'bannerBackground',
		'type'	=> 'image',
		'desc' => 'Fondo del Banner.'
	),


	array(
		'label'=> '"AREA DE 4 MODULOS"',
		'id'	=> $prefix.'home4modules',
		'type'	=> 'title'
	),
	array(
		'label'=> 'Icono 1er Módulo',
		'id'	=> $prefix.'home4modulesI1M',
		'type'	=> 'image',
		'desc' => 'Icono del 1er Módulo.'
	),
	array(
		'label'=> 'URL 1er Módulo',
		'id'	=> $prefix.'home4modulesU1M',
		'type'	=> 'text',
		'desc' => 'URL a enlazar en el 1er Módulo.'
	),
	array(
		'label'=> 'Título 1er Módulo',
		'id'	=> $prefix.'home4modulesT1M',
		'type'	=> 'text',
		'desc' => 'Título del 1er Módulo.'
	),
	array(
		'label'=> 'Descripción 1er Módulo',
		'id'	=> $prefix.'home4modulesD1M',
		'type'	=> 'textarea',
		'desc' => 'Descripción del 1er Módulo.'
	),		
	array(
		'label'=> 'Icono 2do Módulo',
		'id'	=> $prefix.'home4modulesI2M',
		'type'	=> 'image',
		'desc' => 'Icono del 2do Módulo.'
	),
	array(
		'label'=> 'URL 2do Módulo',
		'id'	=> $prefix.'home4modulesU2M',
		'type'	=> 'text',
		'desc' => 'URL a enlazar en el 2do Módulo.'
	),
	array(
		'label'=> 'Título 2do Módulo',
		'id'	=> $prefix.'home4modulesT2M',
		'type'	=> 'text',
		'desc' => 'Título del 2do Módulo.'
	),
	array(
		'label'=> 'Descripción 2do Módulo',
		'id'	=> $prefix.'home4modulesD2M',
		'type'	=> 'textarea',
		'desc' => 'Descripción del 2do Módulo.'
	),		
	array(
		'label'=> 'Icono 3er Módulo',
		'id'	=> $prefix.'home4modulesI3M',
		'type'	=> 'image',
		'desc' => 'Icono del 3er Módulo.'
	),
	array(
		'label'=> 'URL 3er Módulo',
		'id'	=> $prefix.'home4modulesU3M',
		'type'	=> 'text',
		'desc' => 'URL a enlazar en el 3er Módulo.'
	),
	array(
		'label'=> 'Título 3er Módulo',
		'id'	=> $prefix.'home4modulesT3M',
		'type'	=> 'text',
		'desc' => 'Título del 3er Módulo.'
	),
	array(
		'label'=> 'Descripción 3er Módulo',
		'id'	=> $prefix.'home4modulesD3M',
		'type'	=> 'textarea',
		'desc' => 'Descripción del 3er Módulo.'
	),		
	array(
		'label'=> 'Icono 4to Módulo',
		'id'	=> $prefix.'home4modulesI4M',
		'type'	=> 'image',
		'desc' => 'Icono del 4to Módulo.'
	),
	array(
		'label'=> 'URL 4to Módulo',
		'id'	=> $prefix.'home4modulesU4M',
		'type'	=> 'text',
		'desc' => 'URL a enlazar en el 4to Módulo.'
	),
	array(
		'label'=> 'Título 4to Módulo',
		'id'	=> $prefix.'home4modulesT4M',
		'type'	=> 'text',
		'desc' => 'Título del 4to Módulo.'
	),
	array(
		'label'=> 'Descripción 4to Módulo',
		'id'	=> $prefix.'home4modulesD4M',
		'type'	=> 'textarea',
		'desc' => 'Descripción del 4to Módulo.'
	),

	array(
		'label'=> '"AREA DE SUSCRIPCIÓN"',
		'id'	=> $prefix.'homesuscripcion',
		'type'	=> 'title'
	),	
	array(
		'label'=> 'Título',
		'id'	=> $prefix.'homesuscripcionT',
		'type'	=> 'text',
		'desc' => 'Título del área de Suscripción.'
	),
	array(
		'label'=> 'Descripción',
		'id'	=> $prefix.'homesuscripcionD',
		'type'	=> 'textarea',
		'desc' => 'Descripción del área de Suscripción.'
	),
	array(
		'label'=> 'Texto Botón',
		'id'	=> $prefix.'homesuscripcionB',
		'type'	=> 'text',
		'desc' => 'Texto del botón de Suscripción.'
	),
	array(
		'label'=> 'URL Botón',
		'id'	=> $prefix.'homesuscripcionU',
		'type'	=> 'text',
		'desc' => 'URL a enlazar el botón de Suscripción.'
	),	

	array(
		'label'=> '"AREA DE ARTÍCULOS"',
		'id'	=> $prefix.'homeposts',
		'type'	=> 'title'
	),
	array(
		'label'=> 'Título Artículo 1',
		'id'	=> $prefix.'homepostsA1T',
		'type'	=> 'text',
		'desc' => 'Título del Artículo 1.'
	),
	array(
		'label'=> 'Descripción Artículo 1',
		'id'	=> $prefix.'homepostsA1D',
		'type'	=> 'textarea',
		'desc' => 'Descripción del Artículo 1.'
	),
	array(
		'label'=> 'Imagen Artículo 1',
		'id'	=> $prefix.'homepostsA1I',
		'type'	=> 'image',
		'desc' => 'Imagen del Artículo 1.'
	),
	array(
		'label'=> 'Texto Botón Artículo 1',
		'id'	=> $prefix.'homepostsA1BT',
		'type'	=> 'text',
		'desc' => 'Texto del Botón del Artículo 1.'
	),
	array(
		'label'=> 'URL Botón Artículo 1',
		'id'	=> $prefix.'homepostsA1BU',
		'type'	=> 'text',
		'desc' => 'URL a enlazar desde el Botón del Artículo 1.'
	),
	array(
		'label'=> 'Título Artículo 2',
		'id'	=> $prefix.'homepostsA2T',
		'type'	=> 'text',
		'desc' => 'Título del Artículo 2.'
	),
	array(
		'label'=> 'Descripción Artículo 2',
		'id'	=> $prefix.'homepostsA2D',
		'type'	=> 'textarea',
		'desc' => 'Descripción del Artículo 2.'
	),
	array(
		'label'=> 'Imagen Artículo 2',
		'id'	=> $prefix.'homepostsA2I',
		'type'	=> 'image',
		'desc' => 'Imagen del Artículo 2.'
	),
	array(
		'label'=> 'Texto Botón Artículo 2',
		'id'	=> $prefix.'homepostsA2BT',
		'type'	=> 'text',
		'desc' => 'Texto del Botón del Artículo 2.'
	),
	array(
		'label'=> 'URL Botón Artículo 2',
		'id'	=> $prefix.'homepostsA2BU',
		'type'	=> 'text',
		'desc' => 'URL a enlazar desde el Botón del Artículo 2.'
	),
	array(
		'label'=> 'Título Artículo 3',
		'id'	=> $prefix.'homepostsA3T',
		'type'	=> 'text',
		'desc' => 'Título del Artículo 3.'
	),
	array(
		'label'=> 'Descripción Artículo 3',
		'id'	=> $prefix.'homepostsA3D',
		'type'	=> 'textarea',
		'desc' => 'Descripción del Artículo 3.'
	),
	array(
		'label'=> 'Imagen Artículo 3',
		'id'	=> $prefix.'homepostsA3I',
		'type'	=> 'image',
		'desc' => 'Imagen del Artículo 3.'
	),
	array(
		'label'=> 'Texto Botón Artículo 3',
		'id'	=> $prefix.'homepostsA3BT',
		'type'	=> 'text',
		'desc' => 'Texto del Botón del Artículo 3.'
	),
	array(
		'label'=> 'URL Botón Artículo 3',
		'id'	=> $prefix.'homepostsA3BU',
		'type'	=> 'text',
		'desc' => 'URL a enlazar desde el Botón del Artículo 3.'
	),
	
	array(
		'type'	=> 'tab_end'
	),
	/* -- /TAB 1 -- */

	/* -- TAB 2 -- */
	array(
		'id'	=> $prefix.'footer', // Use data in $eto_custom_tabs
		'type'	=> 'tab_start'
	),
		
	array(
		'label'=> '"FOOTER"',
		'id'	=> $prefix.'homefooter',
		'type'	=> 'title'
	),
	array(
		'label'=> 'Icono Facebook',
		'id'	=> $prefix.'homefooterFBI',
		'type'	=> 'image',
		'desc' => 'Icono de Facebook.'
	),
	array(
		'label'=> 'URL de Facebook',
		'id'	=> $prefix.'homefooterFBU',
		'type'	=> 'text',
		'desc' => 'URL de Facebook a enlazar.'
	),	
	array(
		'label'=> 'Icono Google+',
		'id'	=> $prefix.'homefooterGPI',
		'type'	=> 'image',
		'desc' => 'Icono de Google+.'
	),
	array(
		'label'=> 'URL de Google+',
		'id'	=> $prefix.'homefooterGPU',
		'type'	=> 'text',
		'desc' => 'URL de Google+ a enlazar.'
	),
	array(
		'label'=> 'Icono Twitter',
		'id'	=> $prefix.'homefooterTTI',
		'type'	=> 'image',
		'desc' => 'Icono de Twitter.'
	),
	array(
		'label'=> 'URL de Twitter',
		'id'	=> $prefix.'homefooterTTU',
		'type'	=> 'text',
		'desc' => 'URL de Twitter a enlazar.'
	),
	array(
		'label'=> 'Icono Tuenti',
		'id'	=> $prefix.'homefooterTUI',
		'type'	=> 'image',
		'desc' => 'Icono de Tuenti.'
	),
	array(
		'label'=> 'URL de Tuenti',
		'id'	=> $prefix.'homefooterTUU',
		'type'	=> 'text',
		'desc' => 'URL de Tuenti a enlazar.'
	),
	
	array(
		'type'	=> 'tab_end'
	),
	/* -- /TAB 2 -- */

	/* -- TAB 3 -- */
	array(
		'id'	=> $prefix.'sidebar', // Use data in $eto_custom_tabs
		'type'	=> 'tab_start'
	),
		
	array(
		'label'=> 'Texto Suscripción',
		'id'	=> $prefix.'sidebarsp',
		'type'	=> 'textarea',
		'desc' => 'Texto del area de Suscripción'
	),
	
	array(
		'type'	=> 'tab_end'
	),
	/* -- /TAB 3 -- */

	/* -- TAB 4 -- */
	array(
		'id'	=> $prefix.'pedidos', // Use data in $eto_custom_tabs
		'type'	=> 'tab_start'
	),
		
	array(
		'label'=> 'Texto Titular',
		'id'	=> $prefix.'pedidostitular',
		'type'	=> 'text',
		'desc' => 'Texto a mostrar en el titular de la página de pedidos'
	),
	array(
		'label'=> 'Texto Descripción',
		'id'	=> $prefix.'pedidosdesc',
		'type'	=> 'textarea',
		'desc' => 'Texto a mostrar debajo del titular de la página de pedidos'
	),
	
	array(
		'type'	=> 'tab_end'
	),
	/* -- /TAB 4 -- */
	
	/* -- TAB 5 -- */
	array(
		'id'	=> $prefix.'valores', // Use data in $eto_custom_tabs
		'type'	=> 'tab_start'
	),
		
	array(
		'label'=> 'Costo 30 dias Suscripción',
		'id'	=> $prefix.'valorsuscripcion',
		'type'	=> 'text',
		'desc' => 'Costo por 30 dias de Suscripción (10 Euros por defecto, si se deja en blanco)'
	),

	array(
		'label'=> 'Porciento de descuento para Suscritos',
		'id'	=> $prefix.'valordescuentosuscritos',
		'type'	=> 'text',
		'desc' => 'Porciento a descontar en los pedidos de usuarios suscritos (Sin descuento por defecto, si se deja en blanco)'
	),

	array(
		'label'=> 'Cantidad pedidos gratis para suscritos de Colegios',
		'id'	=> $prefix.'valorpedidosgratiscolegiosus',
		'type'	=> 'text',
		'desc' => 'Cantidad de pedidos gratis para los usuarios suscritos de Colegios (3 por defecto, si se deja en blanco)'
	),
	
	array(
		'type'	=> 'tab_end'
	),
	/* -- /TAB 5 -- */

);

?>