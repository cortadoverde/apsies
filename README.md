# Introducción #

El objetivo de este documento es hacer un pequeño resumen del puntos analizados para el proyecto pruebaapsiesnetwork.es

## División de Etapas ##

* Etapa 1 : Generales
* Etapa 2 : Cambio de estilos
* Etapa 3 : Redirección
* Etapa 4: Incluir pantallas intermedias
* Etapa 5: Funcionalidad
* Etapa 6: Migración

## Accesos rapidos ##
* [Código fuente](src)
* [Wiki](wiki)
* [Crear Ticket interno](issues/new)