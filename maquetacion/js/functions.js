jQuery(document).ready(function () {

	var mleftComponent;

	jQuery(window).load(function () {
		//alert(jQuery(window).width() + " - " + jQuery(window).height());

		if (jQuery(window).width() == 320) {
			mleftComponent = 60;
		} else {
			mleftComponent = 112;
		}

		prepareInformation();
	});

	jQuery(window).resize(function () {

		prepareInformation();
	});

	jQuery("#level-bachiller a").click(function (evt) {
		evt.preventDefault();
		selectBachiller();
	});
	jQuery("#level-universidad a").click(function (evt) {
		evt.preventDefault();
		selectUniversidad();
	});
	jQuery("#component-selector").click(function (evt) {
		evt.preventDefault();
		if (jQuery("#component-selector").attr("data-selection") == "1") {
			selectUniversidad();
		} else {
			selectBachiller();
		}
	});

	function selectBachiller() {
		jQuery("#component-selector #component-handle").animate({'margin-left' : 4}, 400, "easeInOutSine");
		jQuery("#component-selector").attr("data-selection", "1");
	}
	function selectUniversidad() {
		jQuery("#component-selector #component-handle").animate({'margin-left' : mleftComponent}, 400, "easeInOutSine");
		jQuery("#component-selector").attr("data-selection", "2");
	}

	function prepareInformation() {
		if (jQuery(window).width() <= 480) {
			jQuery(".information-module-right img").each(function () {
				var img = jQuery(this);
				var clone = img.clone().addClass("moved");
				var parent = img.parent(".information-module-right");
				var siblingParent = parent.siblings(".information-module-left");
				var elemToAppend = siblingParent.children(".info-module-title");
				elemToAppend.after(clone);
				img.remove();
			});
			jQuery(".information-module-left img").each(function () {
				if (jQuery(this).hasClass("moved") == false) {
					var img = jQuery(this);
					var clone = img.clone().addClass("moved");
					var parent = img.parent(".information-module-left");
					var siblingParent = parent.siblings(".information-module-right");
					var elemToAppend = siblingParent.children(".info-module-title");
					elemToAppend.after(clone);
					img.remove();
				}	
			});
		} else {
			jQuery(".information-module-right img.moved").each(function () {
				var img = jQuery(this);
				var clone = img.clone().removeClass("moved");
				var parent = img.parent(".information-module-right");
				var siblingParent = parent.siblings(".information-module-left");				
				siblingParent.append(clone);
				img.remove();
			});
			jQuery(".information-module-left img.moved").each(function () {				
				var img = jQuery(this);
				var clone = img.clone().removeClass("moved");
				var parent = img.parent(".information-module-left");
				var siblingParent = parent.siblings(".information-module-right");
				siblingParent.append(clone);
				img.remove();
			});
		}
	}
});